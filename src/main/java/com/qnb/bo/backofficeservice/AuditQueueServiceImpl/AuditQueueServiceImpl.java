package com.qnb.bo.backofficeservice.AuditQueueServiceImpl;



import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.auditQueueService.AuditQueueService;
import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.QueueDto;
import com.qnb.bo.backofficeservice.entity.CategoryMaster;
import com.qnb.bo.backofficeservice.entity.QueueEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.CategoryCodeMasterRepository;
import com.qnb.bo.backofficeservice.repository.QueueEntityRepository;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class AuditQueueServiceImpl implements AuditQueueService{
	ResultUtilVO resultVo = new ResultUtilVO();
	
	@Autowired
	QueueEntityRepository queueEntityRepository;
	

	@Autowired
	CategoryCodeMasterRepository categoryCodeMasterRepo;
	
	public GenericResponse<QueueEntity> saveAuditQueue(QueueDto request) {
	    var response = new GenericResponse<QueueEntity>();
	    try {
	        resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
	        
	        if (request.getCategoryCode() != null) {
	            CategoryMaster categoryCode = categoryCodeMasterRepo.findByCategoryCode(request.getCategoryCode());
	            
	            if (categoryCode != null) {
	                QueueEntity queueEntity = new QueueEntity();
	                queueEntity.setMessageId(request.getMessageId());
	                queueEntity.setMessageContent("{\"dateTime\":\"2023-12-25 09:39:40.858\",\"branchCode\":\"0011\",\"Encrypted\":\"TRUE\",\"guid\":\"MBJ001387644\",\"unitId\":\"PRD\",\"channelId\":\"MB\",\"Content-Type\":\"application/json\"}||{\"Country\":\"TURKEY\"}||{\"result\":{\"code\":\"000000\",\"description\":\"Success\"},\"data\":[{\"purposeCode\":\"OTHR\",\"purposeDesc\":\"Clearing And Handling Services\",\"b2bPurposeCode\":null},...]}||{\"unit\":\"PRD\",\"channel\":\"MB\",\"mobileNumber\":null,\"emailId\":null,\"lang\":\"en\",\"serviceId\":\"TRPOPRPL\",\"startTime\":1703486380863,\"endTime\":1703486381155,\"gid\":null}||{\"gId\":\"100000000198758\",\"endDate\":\"25-Dec-2023 09:39:41\",\"hostIP\":\"10.20.96.139\",\"userNo\":\"3501\",\"browser\":\"NA\",\"ipaddr\":\"10.20.96.139\",\"startDate\":\"25-Dec-2023 09:39:41\"}||{\"clientSessionID\":\"P2yfszaHkU-iMMwKofpKqvaMjNei4VccFYV2Wpkoo1dfvOgTbo3e!1952717704!1703486371463\",\"geo_location\":\"25.284431714109505|51.53462483380989\",\"app_version\":\"4.0.5\",\"device_model\":\"iPhone 13 Pro Max\",\"os_version\":\"17.1.1\",\"imei\":\"9B3228AF-7C74-4499-9AD5-A36D5AA5EC79\",\"serverSessionID\":\"P2yfszaHkU-iMMwKofpKqvaMjNei4VccFYV2Wpkoo1dfvOgTbo3e!1952717704!1703486371463\""
	                		+ ",\"device_type\":\"ios\",\"ipaddr\":\"192.168.152.254\"}");
	                queueEntity.setInsertDate(new Timestamp(System.currentTimeMillis()));
	                queueEntity.setTrackDetails(request.getTrackId());
	                queueEntity.setCategoryCode(request.getCategoryCode());
//	                queueEntity.setSessionId(request.getSessionId());
	                //hardcoded for testing
	                queueEntity.setSessionId("77e4becf-c0cb-4420-b427-5dd816182f7c");
	                queueEntity.setProcessStatus(request.getProcessStatus() != null ? request.getProcessStatus() : "ERROR");
	                
	                QueueEntity savedQueue = queueEntityRepository.save(queueEntity);
	                
	                response.setData(savedQueue);
	                resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
	            } else {
	                resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "Invalid Category Code");
	            }
	        } 
	        
	    } catch (Exception e) {
	        // Handle exception appropriately
	        log.error("Exception in saving audit queue: {}", e);
	        resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
	    }
	    
	    response.setStatus(resultVo);
	    return response;
	}


}
