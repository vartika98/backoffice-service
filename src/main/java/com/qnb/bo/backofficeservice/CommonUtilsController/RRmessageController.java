package com.qnb.bo.backofficeservice.CommonUtilsController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.CommonUtilsDto.AuditReq;
import com.qnb.bo.backofficeservice.CommonUtilsService.RRmessageAuditingService;
import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.mbAudit.ReqDto;
import com.qnb.bo.backofficeservice.entity.RRmessage;
import com.qnb.bo.backofficeservice.model.AppExceptionHandlerUtilDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.RRmessageRepository;
@RestController
@RequestMapping("/rraudit")
public class RRmessageController {
	
	@Autowired
	private RRmessageAuditingService rrMessageAuditingService;
	@Autowired
	 private RRmessageRepository rrMessageRepo;
	
	@PostMapping(value = "/auditSummary")
	public GenericResponse<List<AuditReq>> getSummary(@RequestBody ReqDto req) {
		return rrMessageAuditingService.getSummary(req);
	}
	
	@PostMapping("/ocsRRmessage")
	public GenericResponse<RRmessage> addRRAudit(@RequestHeader(required=true,name="unit") String unit,
	        @RequestHeader(required=true,name="channel") String channel,
	        @RequestHeader(required=true,name="serviceId") String serviceId,
	        @RequestHeader(required=true,name="guid") String guid) {
//	    HttpServletRequest request = new CustomHttpServletRequest();

	    AppExceptionHandlerUtilDto dto = new AppExceptionHandlerUtilDto("PRD", "IB", "VRCD","100000000128964", "client info");
	    String reqBody2 = "{\"payloadRequest\":\"{}\",\"deviceInfo\":\"{\\\"devModel\\\":\\\"CPH2113\\\",\\\"osVersion\\\":\\\"11\\\",\\\"appVer\\\":\\\"1.0.5\\\",\\\"formName\\\":\\\"frmWithinAcc\\\",\\\"imei\\\":\\\"176cf4fce3699c25\\\",\\\"userAgent\\\":\\\"CPH2113\\\",\\\"ipaddr\\\":\\\"192.168.152.254\\\",\\\"platform\\\":\\\"android\\\"}\"}";
	    String responseBody = "This is a dummy response body";
	    Map<String, String> reqBody = new HashMap<>();
	    reqBody.put("guid", "123446uid");
	    ResponseEntity<String> responseEntity = ResponseEntity.status(HttpStatus.OK).body(responseBody);
	    ResultUtilVO result = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
	    
	    String headerString = "{unit=PRD, channel=MB, Accept-Language=en, serviceId=GETALLUNITS, guid=123446}";
	    Map<String, String> header = new HashMap<>();
	    header.put("unit", unit);
	    header.put("channel", channel);
	    header.put("serviceId", serviceId);
	    header.put("guid", guid);

	    return  rrMessageAuditingService.updateRRMessage(dto, rrMessageRepo, reqBody2,responseBody, header, responseEntity, result,headerString);
		
	    
	   
	}


}
