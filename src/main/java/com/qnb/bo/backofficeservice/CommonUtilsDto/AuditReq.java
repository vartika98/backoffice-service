package com.qnb.bo.backofficeservice.CommonUtilsDto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonRawValue;

import lombok.Data;
import lombok.EqualsAndHashCode;
@Data 
@EqualsAndHashCode(callSuper = false)
public class AuditReq implements Serializable {
	
	private static final long serialVersionUID = -1906556783611212439L;
	private String unitId;
	private String channelId;
	private String globalId;
	private String userNo;
	private String uuid;
	private String categoryCode;
	private String categoryType;
	private String clientInfo;
	@JsonRawValue
	private String header;
	private String method;
	@JsonRawValue
	private String request;
	private Date requestDate;
	@JsonRawValue
	private String response;
	private String responseCode;
	private Date responseDate;
	private Long traceKey;
	private String url;
	@JsonRawValue
	private String rrMessage;
	private Date rrDate;
	private Long txnCategoryId;
	private String userNo2;

	
	
}
