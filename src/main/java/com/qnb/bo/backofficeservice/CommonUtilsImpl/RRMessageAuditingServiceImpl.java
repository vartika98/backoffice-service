package com.qnb.bo.backofficeservice.CommonUtilsImpl;
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
 
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qnb.bo.backofficeservice.CommonUtilsDto.AuditReq;
import com.qnb.bo.backofficeservice.CommonUtilsService.RRmessageAuditingService;
import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.mbAudit.ReqDto;
import com.qnb.bo.backofficeservice.entity.CategoryMaster;
import com.qnb.bo.backofficeservice.entity.RRmessage;
import com.qnb.bo.backofficeservice.model.AppExceptionHandlerUtilDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.CategoryCodeMasterRepository;
import com.qnb.bo.backofficeservice.repository.RRmessageRepository;
 
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotNull;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RRMessageAuditingServiceImpl implements RRmessageAuditingService, AppConstant {

	@Autowired
	private RRmessageRepository rrMessageRepo;

	@Autowired
	private CategoryCodeMasterRepository categoryMasterRepo;

	ResultUtilVO resultVo = new ResultUtilVO();
 
	@Override
	public @NotNull GenericResponse<List<AuditReq>> getSummary(@NotNull ReqDto req) {
		var response = new GenericResponse<List<AuditReq>>();
		var rrMessages = new ArrayList<RRmessage>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			CategoryMaster categoryCode = categoryMasterRepo.findByCategoryCode(req.getCategoryCode());
			log.info("categoryCode list:{}" + categoryCode);
			if (categoryCode != null && "Y".equals(categoryCode.getFoAuditFlag())
					&& "Y".equals(categoryCode.getRrAuditFlag())) {
				if(!req.getGuid().isEmpty()) {
					rrMessages = (ArrayList<RRmessage>)rrMessageRepo
							.findByChannelIdAndCategoryCodeAndRequestDateBetweenAndUuidOrderByDateCreatedDesc(req.getChannelId(),
									req.getCategoryCode(), req.getStartDate(), req.getEndDate(),req.getGuid());
				}else if(!req.getGId().isEmpty()) {
					rrMessages = (ArrayList<RRmessage>)rrMessageRepo
							.findByChannelIdAndCategoryCodeAndRequestDateBetweenAndGlobalIdOrderByDateCreatedDesc(req.getChannelId(),
									req.getCategoryCode(), req.getStartDate(), req.getEndDate(),req.getGId());
				}else {
					rrMessages = (ArrayList<RRmessage>)rrMessageRepo
							.findByChannelIdAndCategoryCodeAndRequestDateBetweenOrderByDateCreatedDesc(req.getChannelId(),
									req.getCategoryCode(), req.getStartDate(), req.getEndDate());
				}
				var auditReqs = new ArrayList<AuditReq>();
				for (RRmessage rrMessage : rrMessages) {
					AuditReq auditReq = mapRRMessageToAuditReq(rrMessage);
					auditReqs.add(auditReq);
				}
				response.setData(auditReqs);
			}
		} catch (Exception e) {
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
 
	private @NonNull AuditReq mapRRMessageToAuditReq(@Nullable RRmessage rrMessage) {
		AuditReq auditReq = new AuditReq();
		if (rrMessage != null) {
			auditReq.setUnitId(rrMessage.getUnitId());
			auditReq.setChannelId(rrMessage.getChannelId());
			auditReq.setGlobalId(rrMessage.getGlobalId());
			auditReq.setUserNo(rrMessage.getUserNo());
			auditReq.setUuid(rrMessage.getUuid());
			auditReq.setCategoryCode(rrMessage.getCategoryCode());
			auditReq.setCategoryType(rrMessage.getCategoryType());
			auditReq.setClientInfo(rrMessage.getClientInfo());
			auditReq.setHeader(rrMessage.getHeader());
			auditReq.setMethod(rrMessage.getMethod());
			auditReq.setRequest(rrMessage.getRequest());
			auditReq.setRequestDate(rrMessage.getRequestDate());
			auditReq.setResponse(rrMessage.getResponse());
			auditReq.setResponseCode(rrMessage.getResponseCode());
			auditReq.setResponseDate(rrMessage.getResponseDate());
			auditReq.setTraceKey(rrMessage.getTraceKey());
			auditReq.setUrl(rrMessage.getUrl());
			auditReq.setRrMessage(rrMessage.getRrMessage());
			auditReq.setRrDate(rrMessage.getRrDate());
			auditReq.setTxnCategoryId(rrMessage.getTxnCategoryId());
			auditReq.setUserNo2(rrMessage.getUserNo2());
		}
		return Objects.requireNonNullElseGet(auditReq, AuditReq::new); // Return a new instance if rrMessage is null
	}
 
	@Override
	public GenericResponse<RRmessage> updateRRMessage(AppExceptionHandlerUtilDto dto, RRmessageRepository rrMessageRepo,
			String reqBody2, String responseBody, Map<String, String> header, ResponseEntity<String> responseEntity,
			ResultUtilVO result, String headerString) {
		GenericResponse<RRmessage> response = new GenericResponse<>();
		Date date = new Date(System.currentTimeMillis());
		try {
			log.info("Inside updateRRMessage");
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			String gId = header.getOrDefault(GID, dto.getGId() != null ? dto.getGId() : NA);
			String userNo = header.getOrDefault(USERNO,
					Objects.nonNull(header.get(USERNO)) && !header.get(USERNO).isEmpty() ? header.get(USERNO)
							: (Objects.nonNull(header.get(USERNO)) && !header.get(USERNO).toString().isEmpty())
									? header.get(USERNO)
									: null);
			String userName = header.getOrDefault(USERNAME,
					Objects.nonNull(header.get(USERNAME)) && !header.get(USERNAME).isEmpty() ? header.get(USERNAME)
							: (Objects.nonNull(header.get(AppConstant.USERNAME))
									&& !header.get(AppConstant.USERNAME).toString().isEmpty())
											? header.get(AppConstant.USERNAME)
											: NA);

			RRmessage rrMessage = new RRmessage();
			rrMessage.setUnitId(header.get("unit"));// wrong value mapp
			rrMessage.setChannelId(header.get("channel"));
			rrMessage.setCategoryCode(header.get("serviceId"));
			rrMessage.setUserNo("1041"); // hardcodes as of now
			rrMessage.setUserNo2("1041");
			rrMessage.setGlobalId(header.get("guid"));
			rrMessage.setCategoryType("REQ");
			rrMessage.setCreatedBy(userName);
			rrMessage.setDateCreated(date);
			rrMessage.setTxnCategoryId(dto.getTxnCategory());
			rrMessage.setClientInfo(dto.getClientInfo() != null ? dto.getClientInfo() : NA);
			rrMessage.setRrDate(date);
			rrMessage.setGlobalId(header.get("guid"));
			rrMessage.setRrMessage("message comments");
			rrMessage.setResponse(responseBody);
			rrMessage.setHeader(headerString);
			rrMessage.setUrl("http://10.46.20.33:7000/auth/user/login");// hardcoded
			rrMessage.setTraceKey(1234567894L);
			;
//		        rrMessage.setHeader(header);
			if (reqBody2 != null) {
				rrMessage.setRequest(reqBody2);
				if (reqBody2.contains("[")) {
					reqBody2 = reqBody2.replace("[", "{");
					reqBody2 = reqBody2.replace("]", "}");
				}
				try {
					ObjectMapper objectMapper = new ObjectMapper();
					JsonNode headerJSON = objectMapper.readTree(reqBody2);
					rrMessage.setUuid(headerJSON.has("guid") ? headerJSON.get("guid").asText() : NA);
					rrMessage.setMethod("POST"); // Assuming it's always POST
					rrMessage.setRequestDate(dto.getStartTime() != null ? dto.getStartTime() : date);

				} catch (Exception e) {
					log.error("JSONException occurred while parsing reqBody2: {}", e.getMessage());
					rrMessage.setUuid(NA);

				}
			}

			// Remaining code for response, etc.

			// Save RRMessage if required
			String flag = rrMessageRepo.getAuditRequired("VCRD");
			log.info("flag value is :{}" + flag);
			if (Objects.nonNull(flag) && "Y".equalsIgnoreCase(flag)) {
				// Save RRMessage if required
				rrMessageRepo.save(rrMessage);
				log.info("Data updated successfully in RR Table");
			} else {
				log.info("Data is not auditable as flag is N");
			}
		} catch (Exception e) {
			log.error("Exception occurred in updateRRMessage: {}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}