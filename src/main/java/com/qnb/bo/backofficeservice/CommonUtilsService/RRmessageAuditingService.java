package com.qnb.bo.backofficeservice.CommonUtilsService;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.qnb.bo.backofficeservice.CommonUtilsDto.AuditReq;
import com.qnb.bo.backofficeservice.dto.mbAudit.ReqDto;
import com.qnb.bo.backofficeservice.entity.RRmessage;
import com.qnb.bo.backofficeservice.model.AppExceptionHandlerUtilDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.RRmessageRepository;

public interface RRmessageAuditingService {

	GenericResponse<List<AuditReq>> getSummary(ReqDto req);

	GenericResponse<RRmessage> updateRRMessage(AppExceptionHandlerUtilDto dto, RRmessageRepository rrMessageRepo,
			String reqBody2, String responseBody, Map<String, String> header, ResponseEntity<String> responseEntity, ResultUtilVO result, String headerString);

}
