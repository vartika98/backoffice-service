package com.qnb.bo.backofficeservice.auditQueueController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.auditQueueService.AuditQueueService;
import com.qnb.bo.backofficeservice.dto.QueueDto;
import com.qnb.bo.backofficeservice.entity.QueueEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;

@RestController
@RequestMapping("/auditQueue")
public class AuditQueueController {
	
	@Autowired
	private AuditQueueService auditQueueService;
	
	@PostMapping("/auditingQueue")
	public GenericResponse<QueueEntity>saveAuditMessage(@RequestBody QueueDto request ){
		return auditQueueService.saveAuditQueue(request);
		
	}

}
