package com.qnb.bo.backofficeservice.auditQueueService;

import com.qnb.bo.backofficeservice.dto.QueueDto;
import com.qnb.bo.backofficeservice.entity.QueueEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface AuditQueueService {

	GenericResponse<QueueEntity> saveAuditQueue(QueueDto request);

}
