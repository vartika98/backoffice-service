package com.qnb.bo.backofficeservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.BaseLdapPathContextSource;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.config.ldap.LdapBindAuthenticationManagerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.qnb.bo.backofficeservice.jwtfilters.AuthEntryPointJwt;
import com.qnb.bo.backofficeservice.jwtfilters.AuthTokenFilter;
import com.qnb.bo.backofficeservice.jwtproviders.LdapAuthenticationProvider;

@Configuration
@EnableWebSecurity
@EnableGlobalAuthentication

public class OCSSecurityConfiguration {

	@Autowired
	LdapAuthenticationProvider ldapAuthenticationProvider;
	@Autowired
	private AuthEntryPointJwt entryPointJwt;

	// @Autowired

	public OCSSecurityConfiguration() {
	}

	@Bean
	AuthenticationManager authenticationManager(final BaseLdapPathContextSource contextSource) {
		final var factory = new LdapBindAuthenticationManagerFactory(contextSource);
		factory.setUserSearchFilter("(uid={0})");
		factory.setUserSearchBase("ou=people");
		return factory.createAuthenticationManager();
	}

	@Bean
	public LdapContextSource contextSource() {
		final var ldapContextSource = new LdapContextSource();
		ldapContextSource.setUrl("ldap://localhost:10389");
		ldapContextSource.setBase("dc=planetexpress,dc=com");
		ldapContextSource.setUserDn("cn=admin,dc=planetexpress,dc=com");
		ldapContextSource.setPassword("GoodNewsEveryone");
		return ldapContextSource;
	}

//	@Bean
//	public DaoAuthenticationProvider authenticationProvider() {
//		final var authProvider = new DaoAuthenticationProvider();
//		authProvider.setUserDetailsService(userDetailsService);
//		authProvider.setPasswordEncoder(passwordEncoder());
//
//		return authProvider;
//	}

	@Bean
	public LdapTemplate ldapTemplate() {
		return new LdapTemplate(contextSource());
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public AuthTokenFilter provider() {
		return new AuthTokenFilter();
	}

	@Bean
	DefaultSecurityFilterChain web(final HttpSecurity http) {

		try {
			http.cors(cors -> {
				cors.disable();
			}).exceptionHandling(exception -> exception.authenticationEntryPoint(entryPointJwt)).csrf(csrf -> {
				csrf.disable();
			}).sessionManagement(sessionMgmt -> {
				sessionMgmt.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
			}).headers(headers -> {
				headers.frameOptions(frameOption -> {
					frameOption.disable();
				});
			}).authorizeHttpRequests(request -> {
				request.requestMatchers("/auth-server/**").permitAll()
				.requestMatchers("/txn/labels/**").permitAll()
//						.anyRequest().authenticated();
				.anyRequest().permitAll() ;
			});
			http.addFilterBefore(provider(), UsernamePasswordAuthenticationFilter.class);
			return http.build();
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
