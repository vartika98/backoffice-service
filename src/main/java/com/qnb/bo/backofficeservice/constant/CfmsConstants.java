package com.qnb.bo.backofficeservice.constant;

public class CfmsConstants {
	
	public static final String APPPROD = "APPPROD";
	public static final String PROD = "PROD";
	public static final String SUBPROD = "SUBPROD";
	public static final String PRI_CALL_ISS = "PRI_CALL_ISS";
	public static final String CONTBANK = "CONTBANK";
	public static final String PRI_CALL= "PRI_CALL";
 
	public static final String RECOMDFR ="RECOMDFR";
 
	public static final String ISSTYPE = "ISSTYPE";
	public static final String PRODSERV = "PRODSERV";
	public static final String CUSTSERV = "CUSTSERV";
	public static final String HOWCANWEHELP ="HOWCANWEHELP";
	public static final String REQ_PROD="REQ_PROD";
	public static final String CUST_SERVPROD = "CUST_SERVPROD";
	public static final String PRODLIST ="PRODLIST";

}
