package com.qnb.bo.backofficeservice.constant;

public class UtilConstants {

	private UtilConstants() {

		throw new IllegalStateException("UtilConstants class");

	}

	public static final String DEFAULT_PAGE_NUMBER = "0";

	public static final String DEFAULT_PAGE_SIZE = "10";

	public static final String DEFAULT_STATUS = "ACT";

	public static final String DEFAULT_STATUS_ACTIVE = "Active";

	public static final String TRANS_GROUP_UNIT = "UNIT";

	public static final String TRANS_GROUP_MENU = "MENU";

	public static final String TRANS_PAGE_UNIT = "UNIT";

	public static final String TRANS_GROUP_PRODUCT = "PRODUCT";

	public static final String TRANS_GROUP_SUB_PRODUCT = "SUB_PRODUCT";

	public static final String PUBLIC_URL = "/public";

	public static final String ENTITLEMENT_URL = "/ent";

	public static final String DEFAULT = "Default";

}
