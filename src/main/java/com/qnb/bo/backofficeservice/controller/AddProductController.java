package com.qnb.bo.backofficeservice.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.ProductDTO;
import com.qnb.bo.backofficeservice.entity.Product;
import com.qnb.bo.backofficeservice.entity.SubProduct;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.AddProductService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/add-product")
public class AddProductController {
	
	@Autowired
	private AddProductService addProductService;
	
	@PostMapping("/save")
	public GenericResponse<Product> addProduct(
			@RequestBody List<ProductDTO> addproductRequest) {
		return addProductService.addProduct(addproductRequest);
	}
	
	@PostMapping("/search")
	public GenericResponse<List<Product>> searchProduct(
			@RequestBody List<ProductDTO> addproductRequest) {
		return addProductService.searchProduct(addproductRequest);
	}
	
	@PostMapping("prod/search")
	public GenericResponse<List<Map<String,String>>> searchProdProduct() {
		return addProductService.searchProdProduct();
	}
	
}
