package com.qnb.bo.backofficeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.ProductDTO;
import com.qnb.bo.backofficeservice.entity.Product;
import com.qnb.bo.backofficeservice.entity.SubProduct;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.AddSubProductService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/add-sub-product")
public class AddSubProductController {
	
	@Autowired
	private AddSubProductService addSubProductService;
	
	@PostMapping("/save")
	public GenericResponse<SubProduct> addProduct(
			@RequestBody List<ProductDTO> addproductRequest) {
		return addSubProductService.addSubProduct(addproductRequest);
	}
	
	@PostMapping("/search")
	public GenericResponse<List<SubProduct>> searchProduct(
			@RequestBody List<ProductDTO> addproductRequest) {
		return addSubProductService.searchSubProduct(addproductRequest);
	}
	
	

}
