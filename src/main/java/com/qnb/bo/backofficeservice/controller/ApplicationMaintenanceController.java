package com.qnb.bo.backofficeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.ApplicationMaintenance.ApplicationDetailRequest;
import com.qnb.bo.backofficeservice.dto.ApplicationMaintenance.ApplicationResponse;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.ApplicationMaintenanceService;

@RestController
@RequestMapping("/app-maintenance")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class ApplicationMaintenanceController {
	
	@Autowired
	private ApplicationMaintenanceService applicationMaintenanceService;

	@PostMapping("/applications/list")
	public GenericResponse<List<ApplicationResponse>> getAllApplications(@RequestBody ApplicationDetailRequest request) {
		return applicationMaintenanceService.getApplicationList(request.getUnits(), request.getStatus());
	}
}
