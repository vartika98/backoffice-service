package com.qnb.bo.backofficeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.constant.CfmsConstants;
import com.qnb.bo.backofficeservice.dto.EmpIdAndCustSegDTO;
import com.qnb.bo.backofficeservice.entity.CfmsParamEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.ApplyForProductService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/apply-for-product")
public class ApplyForProductController {

	@Autowired
	private ApplyForProductService applyForProductService;

	@PostMapping("/save")
	public GenericResponse<EmpIdAndCustSegDTO> createApplyForProduct(
			@RequestBody List<EmpIdAndCustSegDTO> empAndCustSegRequest) {
		return applyForProductService.createApplyForProduct(empAndCustSegRequest, CfmsConstants.REQ_PROD,
				CfmsConstants.CUST_SERVPROD);
	}

	@DeleteMapping("/delete/{id}")
	public GenericResponse<EmpIdAndCustSegDTO> deleteApplyForProduct(@PathVariable long id) {
		return applyForProductService.deleteApplyForProduct(id);
	}

	@PostMapping("/getAll")
	public GenericResponse<List<CfmsParamEntity>> getApplyForProduct() {
		return applyForProductService.searchApplyForProduct(CfmsConstants.REQ_PROD, CfmsConstants.CUST_SERVPROD);
	}

	@PostMapping("/get/{id}")
	public GenericResponse<EmpIdAndCustSegDTO> get(@PathVariable long id) {
		return applyForProductService.getApplyForProduct(id);
	}

	@PutMapping("/update")
	public GenericResponse<EmpIdAndCustSegDTO> updateApplyForProduct(
			@RequestBody EmpIdAndCustSegDTO empAndCustSegRequest) {
		return applyForProductService.updateApplyForProduct(empAndCustSegRequest);
	}

}