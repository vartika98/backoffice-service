package com.qnb.bo.backofficeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.constant.CfmsConstants;
import com.qnb.bo.backofficeservice.dto.EmpIdAndCustSegDTO;
import com.qnb.bo.backofficeservice.dto.SearchRequest;
import com.qnb.bo.backofficeservice.entity.CfmsParamEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.ApplyForProductService;
import com.qnb.bo.backofficeservice.service.ApplyForSubProductService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/apply-for-subproduct")
public class ApplyForSubProductController {
	
	@Autowired
	private ApplyForSubProductService applyForSubProductService;
	
	
		@PostMapping("/save")
		public GenericResponse<EmpIdAndCustSegDTO> createApplyForSubProduct(@RequestBody List<EmpIdAndCustSegDTO> empAndCustSegRequest)
		{
			return applyForSubProductService.createApplyForSubProduct(empAndCustSegRequest,CfmsConstants.APPPROD,CfmsConstants.SUBPROD);
		}

		@PostMapping("/getAll/{typeOfproduct}")
		public GenericResponse<List<CfmsParamEntity>> getApplyForSubProduct(@PathVariable String typeOfproduct)
		{
			return applyForSubProductService.searchApplyForSubProduct(typeOfproduct,CfmsConstants.SUBPROD,CfmsConstants.APPPROD);
		}

		@PutMapping("/update")
		public GenericResponse<EmpIdAndCustSegDTO> updateApplyForSubProduct(@RequestBody EmpIdAndCustSegDTO empAndCustSegRequest)
		{
			return applyForSubProductService.updateApplyForSubProduct(empAndCustSegRequest);
		}

		@PostMapping("/search")
		public GenericResponse<List<EmpIdAndCustSegDTO>> searchApplyForSubProduct(@RequestBody SearchRequest unitId)
		{
			return applyForSubProductService.searchByUnitApplyForSubProduct(unitId,CfmsConstants.CUST_SERVPROD,CfmsConstants.REQ_PROD);
		} 

}
