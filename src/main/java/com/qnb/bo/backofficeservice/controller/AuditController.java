package com.qnb.bo.backofficeservice.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.audit.AuditListRequest;
import com.qnb.bo.backofficeservice.dto.audit.AuditListResponse;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.AuditService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/audit")
@Slf4j
public class AuditController {
	
	@Autowired
	AuditService boAuditService;
	
	@PostMapping("/list")
	public GenericResponse<List<AuditListResponse>> getAuditList(@RequestBody AuditListRequest auditRequest){
		return boAuditService.getAuditList(auditRequest);
	}
	
	@PostMapping("/details")
	public GenericResponse<List<Map<String,String>>> getAuditDetailsList(@RequestBody Map<String,String> request){
		return boAuditService.getAuditDetailsList(request);
	}
	
	@PostMapping("/excel-report")
	public GenericResponse<Map<String,String>> getExcelAuditBoReport(@RequestBody AuditListRequest auditRequest) throws IOException{
		return boAuditService.getExcelBoAuditReport(auditRequest);
	}

}
