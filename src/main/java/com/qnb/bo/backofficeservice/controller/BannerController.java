package com.qnb.bo.backofficeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.banner.BannerReqDto;
import com.qnb.bo.backofficeservice.dto.banner.BannerResponse;
import com.qnb.bo.backofficeservice.dto.banner.BannerSummaryReqDto;
import com.qnb.bo.backofficeservice.dto.banner.ScreenDropDownReq;
import com.qnb.bo.backofficeservice.dto.banner.ScreenIdDropdownDto;
import com.qnb.bo.backofficeservice.entity.BannerDetailsEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.BannerService;

@RestController
@RequestMapping("/commonservice")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class BannerController {
	
	@Autowired
	public BannerService bannerService;

	@PostMapping("/public/select-banner")
	public GenericResponse<List<BannerResponse>> select(@RequestBody BannerSummaryReqDto req) {
		return bannerService.selectBanner(req.getUnit(), req.getChannel(), req.getAcceptlanguage(), req.getScreenId(), req.getImgResol(), req.getDisPriority());
	}
	
	@PostMapping("/public/screenId-dropdown")
	public GenericResponse<List<ScreenIdDropdownDto>> screenIdDropdown(@RequestBody ScreenDropDownReq req) {
		return bannerService.selectScreenId(req.getUnit(), req.getChannel());
	}
	
	@PostMapping("/manage-banner")
	public GenericResponse<List<BannerDetailsEntity>> manageBanner(@RequestBody List<BannerReqDto> reqList) {
		return bannerService.manageBanner(reqList);
	}
	
}
