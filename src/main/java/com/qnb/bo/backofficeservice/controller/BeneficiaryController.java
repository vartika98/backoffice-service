package com.qnb.bo.backofficeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.BeneficiaryDetailRequest;
import com.qnb.bo.backofficeservice.entity.BeneficiaryEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.BeneficiaryService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/beneficiary")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class BeneficiaryController {
	
	@Autowired
	private BeneficiaryService benService;
	
	@PostMapping("/approvalList")
	public GenericResponse<List<BeneficiaryEntity>> getBeneficiariesForApporval(@RequestBody BeneficiaryDetailRequest request){
		log.debug("benef list request {}",request);
		return benService.getBeneficiaryListForApproval(request);
	}
	
	@PostMapping("/getBeneficiaryData")
	public GenericResponse<BeneficiaryEntity> getBeneficiaryData(@RequestBody BeneficiaryDetailRequest request){	
			log.debug(" getBeneficiaryData request {}",request);
			return benService.getBeneficiaryData(request);
	}
	
	@PostMapping("/approve-reject")
	public GenericResponse<BeneficiaryEntity> getBeneficiaryApproveReject(@RequestBody BeneficiaryDetailRequest request){	
			log.debug(" getBeneficiaryApproveReject request {}",request);
			return benService.getBeneficiaryApproveReject(request);
	}
	
	
	

}
