package com.qnb.bo.backofficeservice.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.billPay.DenominationDto;
import com.qnb.bo.backofficeservice.dto.billPay.ManagePayeeDto;
import com.qnb.bo.backofficeservice.dto.billPay.ManageUtilityCodesDto;
import com.qnb.bo.backofficeservice.dto.billPay.ManageUtilityTypesDto;
import com.qnb.bo.backofficeservice.dto.billPay.PayeeDto;
import com.qnb.bo.backofficeservice.dto.billPay.UtilityCodesDto;
import com.qnb.bo.backofficeservice.dto.billPay.UtilityDto;
import com.qnb.bo.backofficeservice.dto.billPay.UtilityTypesDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.BillPayService;

@RestController
@RequestMapping("/biller")
public class BillPayController {

	@Autowired
	private BillPayService billPayService;

	@GetMapping("/get-payees")
	public GenericResponse<List<PayeeDto>> getPayees() {
		return billPayService.getPayees();
	}

	@PostMapping("/manage-payees")
	public GenericResponse<List<PayeeDto>> managePayees(@RequestBody List<ManagePayeeDto> dtolist) {
		return billPayService.managePayees(dtolist);
	}

	@GetMapping("/get-utilitycodes")
	public GenericResponse<List<UtilityDto>> getUtilities(@RequestBody Map<String, String> req) {
		return billPayService.getUtilities(req.get("payeeId"));
	}

	@PostMapping("/manage-utilitycodes")
	public GenericResponse<List<UtilityDto>> manageUtilityCodes(@RequestBody List<ManageUtilityCodesDto> dtolist) {
		return billPayService.manageUtilityCodes(dtolist);
	}

	@GetMapping("/get-utilityTypes")
	public GenericResponse<List<UtilityTypesDto>> getUtilityTypes() {
		return billPayService.getUtilityTypes();
	}

	@PostMapping("/manage-utilityTypes")
	public GenericResponse<List<UtilityTypesDto>> manageUtilityTypes(@RequestBody List<ManageUtilityTypesDto> dtolist) {
		return billPayService.manageUtilityTypes(dtolist);
	}

	@PostMapping("/get-denominations")
	public GenericResponse<List<DenominationDto>> getDenominations(@RequestBody Map<String, String> req) {
		return billPayService.getDenominations(req.get("utilityCode"));
	}
	
	@PostMapping("/get-utilitycodesByUtilityTypes")
	public GenericResponse<List<UtilityCodesDto>> getUtilitiesByUtilityTypes(@RequestBody Map<String, String> req) {
		return billPayService.getUtilitiesByTypes(req.get("utilityType"));
	}

	@PostMapping("/manage-denominations")
	public GenericResponse<List<DenominationDto>> manageDenomination(@RequestBody List<DenominationDto> dtolist) {
		return billPayService.manageDenominations(dtolist);
	}
}
