package com.qnb.bo.backofficeservice.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.boMenu.AccessibleMenus;
import com.qnb.bo.backofficeservice.dto.boMenu.BoMenuDto;
import com.qnb.bo.backofficeservice.dto.boMenu.BoMenuEntitlementRes;
import com.qnb.bo.backofficeservice.dto.boMenu.ProductResDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.BoMenuEntitlementService;

@RestController()
@RequestMapping("/bo-menu")
public class BoMenuEntitlementController {

	@Autowired
	private BoMenuEntitlementService boMenuService;
	
	@GetMapping("/get-boMenu")
	public GenericResponse<List<BoMenuDto>> getBoMenuByUser(@RequestBody Map<String, String> req) {
		return boMenuService.getMenuByUser(req.get("userNo"));
	}
	
	@GetMapping("/menuEntitlement")
	public GenericResponse<List<BoMenuEntitlementRes>> getMenuEntitlement() {
		return boMenuService.getMenuEntitlement();
	}
	
	@GetMapping("/get-products-subproducts")
	public GenericResponse<List<ProductResDto>> getProductSubProduct() {
		return boMenuService.getProductSubProduct();
	}
	
	@GetMapping("/product")
	public GenericResponse<List<Map<String,String>>> productList() {
		return boMenuService.productList();
	}
	
	@PostMapping("/sub-product")
	public GenericResponse<List<Map<String,String>>> subProdList(@RequestBody Map<String, String> req) {
		return boMenuService.subProdList(req);
	}
	
	@PostMapping("/function")
	public GenericResponse<List<Map<String,String>>> functionList(@RequestBody Map<String, String> req) {
		return boMenuService.functionList(req);
	}
	
	@PostMapping("/access")
	public GenericResponse<List<AccessibleMenus>> boMenuAccess(@RequestBody Map<String, String> req) {
		return boMenuService.getAccessibleMenus(req);
	}
 	
}
