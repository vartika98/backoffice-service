package com.qnb.bo.backofficeservice.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.branch.BranchListDTO;
import com.qnb.bo.backofficeservice.dto.branch.BranchListRequest;
import com.qnb.bo.backofficeservice.dto.branch.BranchParameterRequest;
import com.qnb.bo.backofficeservice.dto.txn.ManageLabelReqDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.BranchListService;

@RestController
@RequestMapping("/branch")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class BranchListController {
	
	@Autowired
	private BranchListService branchServices;
	
	@PostMapping("/list")
	public GenericResponse<List<BranchListDTO>>  getBranchList(@RequestBody BranchListRequest branchReq){
		return branchServices.getBranchList(branchReq);
	}
	
	@PostMapping("/manage/branches")
	public GenericResponse<Map<String, String>> manageBranches(@RequestBody BranchListRequest reqBody) {
		return branchServices.manageBranches(reqBody);
	}


}
