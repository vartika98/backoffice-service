package com.qnb.bo.backofficeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.CategoryMasterDto;
import com.qnb.bo.backofficeservice.dto.ErrorConfigDto;
import com.qnb.bo.backofficeservice.dto.ErrorConfigResponse;
import com.qnb.bo.backofficeservice.dto.categoryCode.CategoryCodeDto;
import com.qnb.bo.backofficeservice.dto.errorConfig.ErrorConfigRequest;
import com.qnb.bo.backofficeservice.entity.CategoryMaster;
import com.qnb.bo.backofficeservice.entity.ErrorConfig;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.CategoryCodeMasterService;

@RestController
@RequestMapping("/categoryCodeMaster")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class CategoryCodeMasterController {

	@Autowired
	CategoryCodeMasterService categoryCodeMasterService;

	@PostMapping("/summary")
	public GenericResponse<List<CategoryMaster>> getCategoryCodeMasterList() {
		return categoryCodeMasterService.getCategoryCodeMasterList();
	}

	@PostMapping("/manage")
	public GenericResponse<List<CategoryMaster>> update(@RequestBody List<CategoryMasterDto> categoryMasterDto) {
		return categoryCodeMasterService.updateCategoryMaster(categoryMasterDto);
	}
	
	@GetMapping("/list")
	public GenericResponse<List<CategoryCodeDto>> getCategoryCodes() {
		return categoryCodeMasterService.getCategoryCodes();
	}

}
