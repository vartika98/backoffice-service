package com.qnb.bo.backofficeservice.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.CategoryDetailsDto;
import com.qnb.bo.backofficeservice.dto.CategoryMasterDto;
import com.qnb.bo.backofficeservice.entity.CategoryDetails;
import com.qnb.bo.backofficeservice.entity.CategoryMaster;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.CategoryCodeDetailsService;

@RestController
@RequestMapping("/categoryCodeDetails")
public class CategoryDetailsController {
	
	@Autowired
	CategoryCodeDetailsService  categoryCodeDetailsService;
	
	
	
	@PostMapping("/manage")
	public  GenericResponse<List<CategoryDetails>>update(@RequestBody  List<CategoryDetailsDto> categoryDetailsDto){
	    return categoryCodeDetailsService.updateCategoryDetails(categoryDetailsDto);
	
	}
	@GetMapping("/categoryCodeList")
	public  GenericResponse<List<Map<String,String>>>categoryCodeList(){
		 
	    return categoryCodeDetailsService.categoryCodeList();
	
	}
	@PostMapping("/Summary")
	public  GenericResponse<List<CategoryDetails>>summaryList(@RequestBody Map<String, String>request){
	    return categoryCodeDetailsService.summaryList(request);
	
	}
	
	
}
