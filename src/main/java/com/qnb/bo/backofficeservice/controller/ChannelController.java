package com.qnb.bo.backofficeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.constant.UtilConstants;
import com.qnb.bo.backofficeservice.dto.channel.ChannelListRes;
import com.qnb.bo.backofficeservice.dto.channel.ManageChannelReqDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.ChannelService;

@RestController
@RequestMapping("/channel")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class ChannelController {

	@Autowired
	private ChannelService channelServices;

	@GetMapping("/list")
	public GenericResponse<ChannelListRes> getAllChannels() {
		return channelServices.getAllChannels(UtilConstants.DEFAULT_STATUS);
	}
	
	@PostMapping("/manage-channel")
	public GenericResponse<String> manageChannels(@RequestBody List<ManageChannelReqDto> manageChannelReqDtoList) {
		return channelServices.manageChannels(manageChannelReqDtoList);
	}

	@GetMapping("/summary")
	public GenericResponse<ChannelListRes> getAllChannelsSummary() {
		return channelServices.getAllChannelsSummary();
	} 
}
