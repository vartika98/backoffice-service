package com.qnb.bo.backofficeservice.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.UserCountRequestlist;
import com.qnb.bo.backofficeservice.enums.WorkflowStatus;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.CountService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/pending")
@Slf4j
public class CountController {

	@Autowired
	CountService countService;

	@PostMapping("all/count")
	public GenericResponse<Map<String, Map<String, Integer>>> getUserCountRequest(
			@RequestBody UserCountRequestlist request) {
		List<String> userGroup = new ArrayList<>();
		List<WorkflowStatus> status = request.getStatus();
		return countService.getUserCountRequest(userGroup, status);
	}

	@PostMapping("usermonth/{months}")
	public GenericResponse<Map<String, Map<String, Integer>>> getUserCountRequest(@PathVariable Integer months,
			@RequestBody UserCountRequestlist request) {
		List<String> userGroup = new ArrayList<>();
		List<WorkflowStatus> status = request.getStatus();
		log.info("months :" + months);
		log.info("Status" + status);
		return countService.getCountBYMonthRequest(userGroup, status, months);
	}

	@PostMapping("countByMonth/{months}")
	public GenericResponse<Map<String, Map<String, Integer>>> getAllCountRequest(@PathVariable Integer months,
			@RequestBody UserCountRequestlist request) {
		List<String> userGroup = new ArrayList<>();
		List<WorkflowStatus> status = request.getStatus();
		log.info("months :" + months);
		log.info("Status" + status);
		return countService.getAllCountRequestByMonth(userGroup, status, months);

	}

}
