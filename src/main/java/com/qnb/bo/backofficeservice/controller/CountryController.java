package com.qnb.bo.backofficeservice.controller;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.qnb.bo.backofficeservice.dto.country.CountryDetails;
import com.qnb.bo.backofficeservice.dto.country.ManagenCntryReqDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.CountryService;

@RestController
@RequestMapping("/country")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class CountryController {
	
	@Autowired
	private CountryService countryService;
	
	@PostMapping("/savecountry")
	public GenericResponse<Map<String, String>> saveCountry(@RequestBody ManagenCntryReqDto requestBody){
		return countryService.saveCountry(requestBody);
	}
	
	
	@PostMapping("/list")
	public GenericResponse<List<CountryDetails>> cntryList(){
		return countryService.cntryList();
	}
	
	

}
