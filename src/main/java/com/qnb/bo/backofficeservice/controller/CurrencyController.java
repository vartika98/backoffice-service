package com.qnb.bo.backofficeservice.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.currency.CurrencyDetails;
import com.qnb.bo.backofficeservice.dto.currency.ManageCurrReqDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.CurrencyService;

@RestController
@RequestMapping("/currency")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class CurrencyController {

	@Autowired
	CurrencyService currencyService;
	
	@PostMapping("/list")
	public GenericResponse<List<CurrencyDetails>> currencyLst(@RequestBody Map<String, String> reqBody) {
		return currencyService.currencyLst(reqBody);
	}
	
	@PostMapping("/manage")
	public GenericResponse<Map<String,String>> manageCurrency(@RequestBody List<ManageCurrReqDto> reqBody) {
		return currencyService.manageCurrency(reqBody);
	}
	
}
