package com.qnb.bo.backofficeservice.controller;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.ErrorConfigDto;
import com.qnb.bo.backofficeservice.dto.ErrorConfigResponse;
import com.qnb.bo.backofficeservice.dto.ErrorConfigServiceType;
import com.qnb.bo.backofficeservice.dto.UpdateError;
import com.qnb.bo.backofficeservice.dto.errorConfig.ErrorConfigRequest;
import com.qnb.bo.backofficeservice.dto.errorConfig.ErrorConfigServiceTypeResponse;
import com.qnb.bo.backofficeservice.entity.ErrorConfig;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.ErrorConfigService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("error-config")
@Slf4j
public class ErrorConfigController {

	@Autowired
	ErrorConfigService errorConfigService;

	
	
	@PostMapping("/summary")
	public GenericResponse<List<ErrorConfigResponse>>  getErrorConfigList(@RequestBody ErrorConfigRequest errReq){
		return errorConfigService.getErrorConfigList(errReq);
	}
 
	@PostMapping("/manage")
	public  GenericResponse<List<ErrorConfig>>update(@RequestBody  List<ErrorConfigDto> errorConfigDto){
	    return errorConfigService.updateErrorConfigs(errorConfigDto);
	}
	
	@PostMapping("/service-type")
	public GenericResponse<ErrorConfigServiceTypeResponse> getLanguage(@RequestBody ErrorConfigRequest errReq){
		return errorConfigService.getServiceTypes(errReq);
	}
	
	
}
		
	

