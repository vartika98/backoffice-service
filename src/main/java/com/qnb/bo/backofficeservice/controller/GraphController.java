package com.qnb.bo.backofficeservice.controller;

import com.qnb.bo.backofficeservice.dto.ConfigBasedCategoryRequest;
import com.qnb.bo.backofficeservice.dto.GraphCategoryCodeCountDto;
import com.qnb.bo.backofficeservice.dto.GraphDateCountDto;
import com.qnb.bo.backofficeservice.dto.GraphDataDto;
import com.qnb.bo.backofficeservice.dto.audit.AuditListRequest;
import com.qnb.bo.backofficeservice.dto.audit.SummaryRequest;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.GraphService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/graph")
public class GraphController {

    @Autowired
    private GraphService graphService;

    @PostMapping("/entity-summary-by-date-range/{entityName}")
        public GenericResponse<GraphDataDto> getDataByDateRange(@PathVariable String entityName, @RequestBody SummaryRequest request) {
        return graphService.getAuditDataByDateRange(entityName, request);
    }

    @PostMapping("/entity-count-by-date-range/{graphName}")
    public GenericResponse<GraphDateCountDto> getCountByDateRange(@PathVariable String graphName, @RequestBody AuditListRequest auditRequest) {
        return graphService.getEntityCountByDateRange(graphName, auditRequest);
    }

    @PostMapping("/entity-count-by-config/{graphName}")
    public GenericResponse<GraphCategoryCodeCountDto> getCountByConfig(@PathVariable String graphName, @RequestBody ConfigBasedCategoryRequest configBasedCategoryRequest) {
        return graphService.getConfigBasedTransferCount(graphName, configBasedCategoryRequest);
    }

}
