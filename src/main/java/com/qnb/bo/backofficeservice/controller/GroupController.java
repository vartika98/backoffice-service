package com.qnb.bo.backofficeservice.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.userGroup.CreateGroupReq;
import com.qnb.bo.backofficeservice.dto.userGroup.GroupSummary;
import com.qnb.bo.backofficeservice.entity.BOGroupEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.GenericResult;
import com.qnb.bo.backofficeservice.service.GroupService;

@RestController
@RequestMapping("/group")
public class GroupController {

	@Autowired
	GroupService groupService;
	
	@PostMapping("/list")
	public GenericResponse<List<BOGroupEntity>> groupList(){
		return groupService.groupList();
	}
	
	@PostMapping("/manage")
	public GenericResult createGroup(@RequestBody List<CreateGroupReq> reqBody){
		return groupService.createGroup(reqBody);
	}
	
	@PostMapping("/summary")
	public GenericResponse<List<GroupSummary>> userGroupSummary(@RequestBody Map<String,String> reqBody){
		return groupService.userGroupSummary(reqBody);
	}
}
