package com.qnb.bo.backofficeservice.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.qnb.bo.backofficeservice.dto.unit.LanguageDto;
import com.qnb.bo.backofficeservice.entity.Language;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.LanguageService;

@RestController
@RequestMapping("/language")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class LanguageController {

	@Autowired
	private LanguageService languageServices;

	@PostMapping("/all")
	public GenericResponse<List<LanguageDto>> languageList(@RequestBody Map<String, String> reqBody) {
		return languageServices.languageList(reqBody);
	}
	@PostMapping("/manage")
	public GenericResponse<List<Language>>update(@RequestBody List<LanguageDto> requests){
		return languageServices.manageLang(requests);
	}
	@GetMapping("/summary")
	public GenericResponse<List<LanguageDto>> languageSummary() {
		return languageServices.languageSumarry();
	}
}