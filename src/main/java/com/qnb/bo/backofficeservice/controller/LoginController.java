package com.qnb.bo.backofficeservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.qnb.bo.backofficeservice.dto.login.LoginDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.LoginService;
import reactor.core.publisher.Flux;

import java.time.Duration;

@RestController
@RequestMapping("/login")
public class LoginController {

	@Autowired
	private LoginService loginService;

	@PostMapping("/user")
	public GenericResponse<String> userLogin(@RequestBody LoginDto loginDto) {
		return loginService.userAuthentication(loginDto.getUserId(), loginDto.getPassword());
	}

	@GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public Flux<Long> streamActiveUserCount() {
		return Flux.just(loginService.getActiveUserCount())
				.concatWith(Flux.interval(Duration.ofSeconds(10)))
				.map(i -> loginService.getActiveUserCount());
	}

}
