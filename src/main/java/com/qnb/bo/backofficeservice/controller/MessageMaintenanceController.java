package com.qnb.bo.backofficeservice.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.MessageMaintenance.MaintenanceListResponse;
import com.qnb.bo.backofficeservice.dto.MessageMaintenance.MaintenanceMessage;
import com.qnb.bo.backofficeservice.dto.MessageMaintenance.PlannedUnplannedRequest;
import com.qnb.bo.backofficeservice.entity.MaintenanceMessageEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.MessageMaintenanceService;

@RestController
@RequestMapping("/msg-maintenance")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class MessageMaintenanceController {
	
	@Autowired
	private MessageMaintenanceService messageMaintenanceService;

	@GetMapping("/list/{msgType}")
	public GenericResponse<MaintenanceListResponse> getAppMaintenanceList(@PathVariable(name = "msgType") String msgType) {
		return messageMaintenanceService.getMaintenanceList(msgType);
	}
	
	@PostMapping("/manage-maintenance")
	public GenericResponse<List<MaintenanceMessageEntity>> manageMaintenance(@RequestBody List<PlannedUnplannedRequest> req) {
		List<MaintenanceMessage> maintenanceMsg = new ArrayList<>(); 
		req.stream().forEach(reqDto -> {
			maintenanceMsg.add(PlannedUnplannedRequest.normalise(reqDto));
		});
		return messageMaintenanceService.manageMaintenance(maintenanceMsg);
	}
}
