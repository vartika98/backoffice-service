package com.qnb.bo.backofficeservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.notification.NotificationDto;
import com.qnb.bo.backofficeservice.enums.NotificationStatus;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.NotificationService;

@RestController
@RequestMapping("/notification")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class NotificationController {

	@Autowired
	private NotificationService notificationService;

	@PostMapping("/send")
	public GenericResponse<NotificationStatus> sendNotification(@RequestBody NotificationDto message) {
		return notificationService.sendNotification(message);
	}
}
