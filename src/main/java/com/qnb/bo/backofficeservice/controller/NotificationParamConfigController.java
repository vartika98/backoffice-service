package com.qnb.bo.backofficeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.NotificationParamConfigDTO;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.NotificationParamConfigService;

@RestController
@RequestMapping("/notification-param")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class NotificationParamConfigController {
	
	@Autowired
	private NotificationParamConfigService notificationParamConfigService;

	@PostMapping("/getSummary")
	public GenericResponse<List<NotificationParamConfigDTO>> getSummary(){
		return notificationParamConfigService.getSummary();
	}
	
	@PostMapping("/manage")
	public GenericResponse<List<NotificationParamConfigDTO>> manageNotifications(@RequestBody List<NotificationParamConfigDTO>request ){
		return notificationParamConfigService.manageNotifications(request);
	}

}
