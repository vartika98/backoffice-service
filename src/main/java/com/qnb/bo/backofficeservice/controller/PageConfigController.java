package com.qnb.bo.backofficeservice.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.PageConfigDto;
import com.qnb.bo.backofficeservice.dto.UpdateError;
import com.qnb.bo.backofficeservice.dto.UpdatePage;
import com.qnb.bo.backofficeservice.entity.ErrorConfig;
import com.qnb.bo.backofficeservice.entity.PageConfig;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.PageConfigService;

import lombok.extern.slf4j.Slf4j;

@RequestMapping("/screen")
@RestController
@Slf4j
public class PageConfigController {
	
	@Autowired
	private PageConfigService pageConfigService;
	
	@PostMapping("/summary")
	public GenericResponse<List<PageConfigDto>> getList(@RequestBody Map<String,String>request){
		return pageConfigService.getList(request);
	}
	
	@PostMapping("/manage-Screen")
	public  GenericResponse<List<PageConfig>>update(@RequestBody List<PageConfigDto> pageConfigDto){
		    return pageConfigService.updatePageConfigs(pageConfigDto);
	
	}
	@PostMapping("/screenId")
	public GenericResponse<List<Map<String,String>>>getScreenId(@RequestBody Map<String,String>request){
		return pageConfigService.getScreenId(request);
	}

}
