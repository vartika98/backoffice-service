package com.qnb.bo.backofficeservice.controller;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.categoryCode.CategoryCodeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.qnb.bo.backofficeservice.dto.parameter.ManageParamReqDto;
import com.qnb.bo.backofficeservice.dto.parameter.ParameterRes;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.GenericResult;
import com.qnb.bo.backofficeservice.service.ParameterMaintenanceService;

@RestController
@RequestMapping("/parameter")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class ParameterMaintenanceController {

	@Autowired
	ParameterMaintenanceService parameterMaintenanceService;
	
	@PostMapping("/list")
	public GenericResponse<List<ParameterRes>> parameterList(@RequestBody Map<String,String> reqBody) {
		return parameterMaintenanceService.parameterList(reqBody);
	}
	
	@PostMapping("/manage-parameter")
	public GenericResult manageParameter(@RequestBody List<ManageParamReqDto> reqBody) {
		return parameterMaintenanceService.manageParameter(reqBody);
	}

	@GetMapping("/config-values/{configKey}")
	public GenericResponse<List<CategoryCodeDto>> getDistinctParamConfig(@PathVariable String configKey){
		return parameterMaintenanceService.getDistinctParamConfig(configKey);
	}
}
