package com.qnb.bo.backofficeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.pendinRequest.PendingReq;
import com.qnb.bo.backofficeservice.dto.pendinRequest.PendingReqResponse;
import com.qnb.bo.backofficeservice.dto.pendinRequest.WorkFlowRequest;
import com.qnb.bo.backofficeservice.dto.role.RoleDetailsRes;
import com.qnb.bo.backofficeservice.dto.user.UserDetailsRes;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.GenericResult;
import com.qnb.bo.backofficeservice.service.PendingRequestService;

@RestController
@RequestMapping("/pending-request")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class PendingRequestController {
	
	@Autowired
	PendingRequestService pendingRequestService;
	
	@PostMapping("/user-list")
	public GenericResponse<List<PendingReqResponse<UserDetailsRes>>> userList(@RequestBody PendingReq reqBody) {
		return pendingRequestService.userList(reqBody);
	}
	
	@PostMapping("/role-list")
	public GenericResponse<List<PendingReqResponse<RoleDetailsRes>>> roleList(@RequestBody PendingReq reqBody) {
		return pendingRequestService.roleList(reqBody);
	}
	
	@PostMapping("/pending-req")
	public GenericResponse<GenericResult> pendingRequest(@RequestBody WorkFlowRequest workFlowRequest) {
		return pendingRequestService.pendingRequest(workFlowRequest);
	}
	
}
