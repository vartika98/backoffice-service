package com.qnb.bo.backofficeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.mbAudit.ReqDto;
import com.qnb.bo.backofficeservice.dto.mbAudit.ResponseDto;
import com.qnb.bo.backofficeservice.dto.mbAudit.ResponseDtoMB;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.RRAuditService;

@RestController
@RequestMapping("/rr")
public class RRMessageController {

	@Autowired
	private RRAuditService mbAuditService;

	@PostMapping("/mb-summary")
	public GenericResponse<List<ResponseDtoMB>> getSummary(@RequestBody ReqDto req) {
		return mbAuditService.getSummary(req);
	}

	@PostMapping("/summary")
	public GenericResponse<List<ResponseDto>> getSummaryByChannel(@RequestBody ReqDto req) {
		return mbAuditService.getSummaryByChannel(req);
	}
}
