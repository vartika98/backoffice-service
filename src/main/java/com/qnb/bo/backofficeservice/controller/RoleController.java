package com.qnb.bo.backofficeservice.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.role.RoleHierarchiRequest;
import com.qnb.bo.backofficeservice.dto.role.UserRoleDto;
import com.qnb.bo.backofficeservice.entity.UserRole;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.RoleService;

@RestController
@RequestMapping("/roleManagement")
public class RoleController {
	
	@Autowired RoleService roleService;
	
	@PostMapping("/create")
	public GenericResponse<List<UserRole>> addRole(@RequestBody RoleHierarchiRequest request){
		return roleService.crateRole(request);
	}
	
	@PostMapping("/roleList")
	public  GenericResponse<List<UserRoleDto>> roleList(@RequestBody Map<String,String>request){
	    return roleService.roleList(request);
	}

}
