package com.qnb.bo.backofficeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.rules.RulesDto;
import com.qnb.bo.backofficeservice.dto.rules.RulesSummaryResDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.RulesService;

@RestController
@RequestMapping("/rule")
public class RulesController {

	@Autowired
	private RulesService rulesService;

	@PostMapping("/manage")
	public GenericResponse<String> manageRule(@RequestBody List<RulesDto> dto) {
		return rulesService.manageRule(dto);
	}
	
	@GetMapping("/summary")
	public GenericResponse<List<RulesSummaryResDto>> getSummary() {
		return rulesService.getSummary();
	}

}
