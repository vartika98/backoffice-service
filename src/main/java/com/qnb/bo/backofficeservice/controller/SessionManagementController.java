package com.qnb.bo.backofficeservice.controller;

import java.util.List;import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.sessionManagement.ActiveUsersResponse;
import com.qnb.bo.backofficeservice.dto.sessionManagement.SessionReqDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.SessionManagementService;

@RestController()
@RequestMapping("/session-management")
public class SessionManagementController {
	
	@Autowired
	private SessionManagementService sessionManagementService;

	@PostMapping("/getActiveUsers")
	public GenericResponse<List<ActiveUsersResponse>> getActiveUsers() {
		return sessionManagementService.getActiveSessions();
	}
	
	@PostMapping("/getLoginCount")
	public GenericResponse<Map<String, Integer>> getLoginCount(@RequestBody SessionReqDto req) {
		return sessionManagementService.getLoginCount(req);
	}
}
