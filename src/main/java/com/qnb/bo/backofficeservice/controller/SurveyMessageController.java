package com.qnb.bo.backofficeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.SurveyRequestDTO;
import com.qnb.bo.backofficeservice.dto.SurveySearchRequest;
import com.qnb.bo.backofficeservice.entity.ParamConfig;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.SurveyService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/surveymessage")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
@Slf4j
public class SurveyMessageController {
	
	@Autowired
	private SurveyService surveyService;

	@PostMapping("save")
	public GenericResponse<SurveyRequestDTO> createSurveyMessage(@RequestBody SurveyRequestDTO surveyRequest) {
		log.info("The Suryey Request is {} ", surveyRequest);
		return surveyService.createSurveyMessage(surveyRequest);
	}
	
	@PostMapping("delete/{id}")
	public GenericResponse<SurveyRequestDTO> deleteSurveyMessage(@RequestBody SurveyRequestDTO id) {
		return surveyService.deleteSurveyMessage(id);
	}
	
	@PostMapping("search")
	public GenericResponse<List<SurveyRequestDTO>> getSurveyMessage(@RequestBody SurveySearchRequest surveySearchRequest)
	{
		return surveyService.searchSurveyMessage(surveySearchRequest);
	}
	
	@PostMapping("get")
	public GenericResponse<SurveyRequestDTO> get(@RequestBody SurveyRequestDTO id)
	{
		return surveyService.SurveyMessageByid(id);
	}
	
	@PostMapping("get-segment-key")
	List<ParamConfig> getSegmentKey(@RequestBody SurveyRequestDTO unitId) {
		return surveyService.getSegmentKeysByUnit(unitId);
	}
	
	@PutMapping("update")
	public GenericResponse<SurveyRequestDTO> updateSurveyMessage(@RequestBody SurveyRequestDTO surveyRequest)
	{
		log.info("The Suryey Request is {} ", surveyRequest);
		return surveyService.updateSurveyMessage(surveyRequest);		
	}
}
