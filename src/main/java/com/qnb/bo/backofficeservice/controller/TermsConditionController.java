package com.qnb.bo.backofficeservice.controller;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.qnb.bo.backofficeservice.dto.TermsCondition.TermsAndConditionDto;
import com.qnb.bo.backofficeservice.dto.TermsCondition.TermsAndConditionRequest;
import com.qnb.bo.backofficeservice.dto.TermsCondition.TermsAndConditionResponse;
import com.qnb.bo.backofficeservice.dto.TermsCondition.TncResponse;
import com.qnb.bo.backofficeservice.entity.TermsAndCondition;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.TermsConditionService;
@RestController
@RequestMapping("/tnc")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class TermsConditionController {
	
	@Autowired 
	TermsConditionService termsConditionService;
	
	
	@PostMapping("/summary")
	public GenericResponse<List<TncResponse>>getSummary(@RequestBody TermsAndConditionRequest tncReq){
		return termsConditionService.getSummary(tncReq) ;
	}
	
	@PostMapping("/manage")
	public GenericResponse<List<TermsAndCondition>>update(@RequestBody List<TermsAndConditionDto> tncDto){
		return termsConditionService.manageTermsAndCondition(tncDto);
		
	}
	@PostMapping("/moduleList")
	public GenericResponse<Map<String, List<Map<String, String>>>> getModuleId(@RequestBody Map<String, String> requestMap) {
	    String unitId = requestMap.get("unitId");
	    String channelId = requestMap.get("channelId");
	    return termsConditionService.getModuleIdList(unitId, channelId);
	}

	@PostMapping("/subModuleList")
	public GenericResponse<Map<String, List<Map<String, String>>>>getSubModuleId(@RequestBody Map<String, String> requestMap){
		String unitId = requestMap.get("unitId");
	    String channelId = requestMap.get("channelId");
	    String moduleId = requestMap.get("moduleId");
		return termsConditionService.getSubModuleIdList(unitId,channelId,moduleId);
		
	}
}
