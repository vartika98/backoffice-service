package com.qnb.bo.backofficeservice.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.txn.LabelListRes;
import com.qnb.bo.backofficeservice.dto.txn.ManageLabelReqDto;
import com.qnb.bo.backofficeservice.dto.txn.ManageMenuReqDto;
import com.qnb.bo.backofficeservice.dto.txn.MenuI18ReqDto;
import com.qnb.bo.backofficeservice.dto.txn.MenuListRes;
import com.qnb.bo.backofficeservice.dto.txn.MenuListResponse;
import com.qnb.bo.backofficeservice.dto.txn.MenuReqDto;
import com.qnb.bo.backofficeservice.dto.txn.MenuResponseDto;
import com.qnb.bo.backofficeservice.dto.txn.TransactionEntitlementResponse;
import com.qnb.bo.backofficeservice.entity.ScreenEntitlement;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.I18Service;
import com.qnb.bo.backofficeservice.service.MenuService;
import com.qnb.bo.backofficeservice.service.ScreenService;

import org.springframework.core.io.Resource;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/txn")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class TranslationController {

	@Autowired
	MenuService menuService;

	@Autowired
	ScreenService screenService;

	@Autowired
	I18Service i18Service;

	@PostMapping("/menus/list")
	public GenericResponse<MenuListRes> menuList(@RequestBody Map<String, String> reqBody) {
		return menuService.moduleList(reqBody);
	}

	@PostMapping("/menus/sub-menu")
	public GenericResponse<MenuListRes> screenList(@RequestBody Map<String, String> reqBody) {
		return screenService.screenList(reqBody);
	}

	@PostMapping("/labels")
	public GenericResponse<LabelListRes> labelList(@RequestBody Map<String, String> reqBody) {
		return i18Service.labelList(reqBody);
	}
	
	@PostMapping("/i18ToExcel")
	public GenericResponse<byte[]> i18ToExcel(@RequestBody Map<String, String> reqBody) {
		return i18Service.i18ToExcel(reqBody);
	}

	@PostMapping("/manage-labels")
	public GenericResponse<Map<String, String>> manageLabels(@RequestBody List<ManageLabelReqDto> reqBody) {
		return i18Service.manageLabels(reqBody);
	}

	@PostMapping("/manage-menus")
	public GenericResponse<Map<String, String>> manageMenus(@RequestBody ManageMenuReqDto reqBody) {
		return menuService.manageMenus(reqBody);
	}

	@PostMapping("/menus/entitlement")
	public GenericResponse<List<TransactionEntitlementResponse>> getTxnEntitlementList(@RequestBody Map<String, String> reqBody) {
		return menuService.getTxnEntitlementList(reqBody);
	}
	
	@PostMapping("/labels/list")
	public GenericResponse<Map<String, String>> getLabelByLang(
			@RequestHeader(name="unit") String unit,
			@RequestHeader(name="channel") String channel,
			@RequestHeader(name="Accept-Language") String lang,
			@RequestHeader(name="screenId") String screenId) {
		return i18Service.getLabelByLang(unit,channel,lang,screenId);
	}
	
	@PostMapping("/menu/all")
	public GenericResponse<List<Map<String, String>>> menusWithTranslation(@RequestBody Map<String, String> reqBody) {
		return menuService.menusWithTranslation(reqBody);
	}
	
	@PostMapping("/menu/manage")
	public GenericResponse<Map<String, String>> manageMenuTranslation(@RequestBody List<MenuI18ReqDto> reqBody) {
		return menuService.manageMenuTranslation(reqBody);
	}
	
	@PostMapping("/menu/submenu")
	public GenericResponse<List<Map<String, String>>> subMenusWithTranslation(@RequestBody Map<String, String> reqBody) {
		return menuService.subMenusWithTranslation(reqBody);
	}
	
	@PostMapping("/menu/subMenu/manage")
	public GenericResponse<Map<String, String>> manageSubMenuTranslation(@RequestBody List<MenuI18ReqDto> reqBody) {
		return menuService.manageSubMenuTranslation(reqBody);
	}
	
	@PostMapping("/menu/submenu/submenu2")
	public GenericResponse<List<Map<String, String>>> subMenus2WithTranslation(@RequestBody Map<String, String> reqBody) {
		return menuService.subMenus2WithTranslation(reqBody);
	}
	
	@PostMapping("/menu/submenu/submenu2-manage")
	public GenericResponse<Map<String, String>> manageSubMenu2Translation(@RequestBody List<MenuI18ReqDto> reqBody) {
		return menuService.manageSubMenu2Translation(reqBody);
	}
	
	//MB-MENU_ENTITLEMENT
	
	@PostMapping("/menu/manage-entitlement")
	public GenericResponse<Map<String, String>> manageMenuTranslationMb(@RequestHeader String unitId, @RequestHeader String channelId,
			@RequestBody List<MenuReqDto> menuReqDto) {
		List<MenuI18ReqDto> menuReq = new ArrayList<>();
		MenuI18ReqDto reqBody = new MenuI18ReqDto();
		reqBody.setUnitId(unitId);
		reqBody.setChannelId(channelId);
		reqBody.setMenuDetails(menuReqDto);
		menuReq.add(reqBody);
		return menuService.manageMenuTranslation(menuReq);
	}
	
	@PostMapping("/menus/entitlement-list")
	public GenericResponse<Map<String, List<Map<String, String>>>> getScreenEntitlementListMb(@RequestHeader String unitId, @RequestHeader String channelId, @RequestHeader String device) {
		Map<String, String> reqBody = new HashMap<>();
		reqBody.put("unitId", unitId);
		reqBody.put("channelId", channelId);
		reqBody.put("device", device);
		return menuService.getScreenEntitlementListMb(reqBody);
	}
	
	@PostMapping("/react/entitlement-list")
	public Flux<GenericResponse<Map<String, List<Map<String, String>>>>> getScreenEntitlementListReact(@RequestHeader String unitId, @RequestHeader String channelId, @RequestHeader String device) {
		Map<String, String> reqBody = new HashMap<>();
		reqBody.put("unitId", unitId);
		reqBody.put("channelId", channelId);
		reqBody.put("device", device);
		return menuService.getScreenEntitlementListReact(reqBody);
	}
	
	//IB MENU LIST CALL
	
	@PostMapping("/menus-list")
	GenericResponse<List<MenuListResponse>> getAllMenuList(@RequestHeader String unitId, @RequestHeader String channelId, @RequestHeader String custSegetgment, @RequestHeader String lang){
		Map<String, String> reqBody = new HashMap<>();
		reqBody.put(AppConstant.UNIT_ID, unitId);
		reqBody.put(AppConstant.CHANNEL_ID, channelId);
		reqBody.put(AppConstant.CUSTOMER_SEGMENT, custSegetgment);
		reqBody.put(AppConstant.REQUEST_ACCEPT_LANG, lang);
		return menuService.getAllMenuList(reqBody);
	}
	
	@PostMapping("/priority")
	GenericResponse<List<String>> priorityList(@RequestBody Map<String, String> reqBody){
		return menuService.priorityList(reqBody);
	}
	
	
}
