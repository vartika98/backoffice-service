package com.qnb.bo.backofficeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.constant.UtilConstants;
import com.qnb.bo.backofficeservice.dto.unit.LanguageDto;
import com.qnb.bo.backofficeservice.dto.unit.UnitDto;
import com.qnb.bo.backofficeservice.dto.unit.UnitListRes;
import com.qnb.bo.backofficeservice.dto.unit.UnitResponse;
import com.qnb.bo.backofficeservice.entity.Unit;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.UnitService;


@RestController
@RequestMapping("/units")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class UnitController {


	@Autowired
	private UnitService unitServices;

	@GetMapping("/all")
	public GenericResponse<UnitListRes> unitListToDto() {
		return unitServices.getAllUnits(UtilConstants.DEFAULT_STATUS);
	}
	@PostMapping("/manage")
	public GenericResponse<List<Unit>> update(@RequestBody List<UnitDto> unitRequest){
		return unitServices.manageUnit(unitRequest);
		
	}
	@PostMapping("/summary")
	public GenericResponse<List<UnitDto>> unitSummary() {
		return unitServices.unitSummary();
	}
}
