package com.qnb.bo.backofficeservice.controller;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.qnb.bo.backofficeservice.dto.unit.UnitLanguageDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.UnitLanguageService;

@RestController
@RequestMapping("/units/lang")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class UnitLanguageController {
	
	@Autowired
	UnitLanguageService unitLanguageService;
	
	@PostMapping("/manage-lang")
	public GenericResponse<Map<String, String>> saveUnitLanguage(@RequestBody List<UnitLanguageDto> requestBody){
		return unitLanguageService.saveUnitLanguage(requestBody);
	}
	
	@PostMapping("/list")
	public GenericResponse<List<UnitLanguageDto>> getSummaryUnitLanguage(){
		return unitLanguageService.getSummaryUnitLanguage();
	}
	
}
