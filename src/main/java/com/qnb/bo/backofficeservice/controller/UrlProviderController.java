package com.qnb.bo.backofficeservice.controller;

import com.qnb.bo.backofficeservice.dto.ApiRequest;
import com.qnb.bo.backofficeservice.dto.ApiResponse;
import com.qnb.bo.backofficeservice.dto.audit.AuditListRequest;
import com.qnb.bo.backofficeservice.entity.UrlProvider;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.UrlProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UrlProviderController {

    @Autowired
    private UrlProviderService urlProviderService;

    @GetMapping("/getAllUrl")
    public GenericResponse<List<UrlProvider>> getAllUrl() {
        return urlProviderService.getAllUrl();
    }

    @PostMapping("/execute")
    public GenericResponse<ApiResponse> postApi(@RequestBody ApiRequest request){
        return urlProviderService.executeApi(request);
    }

}
