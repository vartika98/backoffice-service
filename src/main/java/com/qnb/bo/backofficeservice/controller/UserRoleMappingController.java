package com.qnb.bo.backofficeservice.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.dto.GroupRoleDto;
import com.qnb.bo.backofficeservice.dto.userRole.UserRoleMappingDto;
import com.qnb.bo.backofficeservice.entity.UserEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.service.UserRoleMappingService;

@RestController
@RequestMapping("/userRole")
public class UserRoleMappingController {
	
	@Autowired
	UserRoleMappingService userRoleMappingService;
	
	@PostMapping("/userRoleManagement")
	public GenericResponse<List<UserEntity>>update(@RequestBody List<UserRoleMappingDto> userRequest){
		return userRoleMappingService.manageUser(userRequest);
	}
	
	@PostMapping("/summary")
	public GenericResponse<List<UserRoleMappingDto>>summary(){
		return userRoleMappingService.userList();
		
	}
	@PostMapping("/roleGroup")
	public GenericResponse<List<GroupRoleDto>>groupList(@RequestBody Map<String,String>groupRequest){
		return userRoleMappingService.groupList(groupRequest);
		
	}
	
	

}
