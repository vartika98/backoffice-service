package com.qnb.bo.backofficeservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiRequest {

    private int urlId;
    private String httpMethod;
    private String header;
    private String parameters;
    private String requestBody;

}
