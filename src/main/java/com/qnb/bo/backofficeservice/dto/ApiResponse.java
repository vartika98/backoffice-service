package com.qnb.bo.backofficeservice.dto;

import lombok.Data;
import org.springframework.http.HttpStatusCode;

import java.util.Map;

@Data
public class ApiResponse {

    //private HttpStatusCode statusCode;
    private Map<String, Object> response;

}
