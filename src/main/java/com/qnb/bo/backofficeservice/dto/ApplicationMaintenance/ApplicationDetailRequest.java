package com.qnb.bo.backofficeservice.dto.ApplicationMaintenance;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationDetailRequest {
 
	private List<String> units;
 
	private String status;
 
}