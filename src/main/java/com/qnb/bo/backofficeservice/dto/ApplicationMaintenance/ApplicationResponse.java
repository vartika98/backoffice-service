package com.qnb.bo.backofficeservice.dto.ApplicationMaintenance;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.bo.backofficeservice.enums.TxnStatus;
import com.qnb.bo.backofficeservice.views.ApplicationView;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApplicationResponse {
	private Long txnId;
	private String appName;
	private String appType;
	private String status;
	private Set<String> units;
	private Set<String> channels;
 
	public ApplicationResponse() {
		units = new HashSet<>();
		channels = new HashSet<>();
	}
 
	public static List<ApplicationResponse> toResponse(List<ApplicationView> views) {
 
		Map<Long, ApplicationResponse> response = new HashMap<>();
 
		if (views != null) {
			for (var view : views) {
				ApplicationResponse resp = response.get(view.getTxnId());
				if (resp == null) {
					resp = new ApplicationResponse();
					resp.setAppName(view.getAppName());
					resp.setTxnId(view.getTxnId());
					resp.setAppType(view.getAppType());
					resp.setStatus(view.getStatus().name());
					response.put(view.getTxnId(), resp);
				}
				resp.getUnits().add(view.getUnitId());
				if(view.getChannelStatus()==TxnStatus.ACT) resp.getChannels().add(view.getChannelId());
			}
		}
		var appList = response.values().stream().collect(Collectors.toList());
		if (!appList.isEmpty() && appList.size() > 1) {
			Collections.sort(appList, (o1, o2) -> o1.getAppName().compareToIgnoreCase(o2.getAppName()));
		}
		return appList;
	}
 
}
