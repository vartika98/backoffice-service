package com.qnb.bo.backofficeservice.dto;

import lombok.Data;

@Data
public class Beneficiary {

	private static final String SALT = "beneficiary";
	private Integer refNo;
	private String requestDate;
	private String requestStatus;
	private String transactionName;
	private String unit;
	private String channel;
	private String countryName;
	private String userNo;
	private String bankName;
	private String branch;
	private String city;
	private String beneficiaryFirstName;
	private String beneficiaryMidName;
	private String beneficiaryLastName;
	private String benAccountNumber;
	private String ibanCode;
	private String currency;
	private String intermediatorySwiftCode;
	private String swiftCode;
	private String routing;
	private String sortOrChip;
	private String customerName;
	private String customerNationalId;
	private String remark;
	private String makerComments;
	private String gId;
	private String mobileNo;
	private String lastModifiedBy;
    private String lastModifiedTime;
    private String txnNameFlag;
 
}
