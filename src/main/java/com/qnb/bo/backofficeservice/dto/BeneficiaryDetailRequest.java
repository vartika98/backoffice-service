package com.qnb.bo.backofficeservice.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BeneficiaryDetailRequest {
	private List<String> units;
	private String fromDate;
	private String toDate;
	private String withinQNB;
	private String modifyFlag; 
	private String status;
	private Integer refNo;
}
