package com.qnb.bo.backofficeservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class BeneficiaryResponse {
	private Integer refNo;
	private String requestDate;
	private String requestStatus;
	private String transactionName;
	private String unit;
	private String channel;
	private String countryName;
	private String userNo;
	private String bankName;
	private String branch;
	private String city;
	private String beneficiaryFirstName;
	private String beneficiaryMidName;
	private String beneficiaryLastName;
	private String benAccountNumber;
	private String ibanCode;
	private String currency;
	private String intermediatorySwiftCode;
	private String swiftCode;
	private String routing;
	private String sortOrChip;
	private String customerName;
	private String customerNationalId;	
	private String gId;
	private String mobileNo;
	private String lastModifiedBy;
    private String lastModifiedTime;
	private String txnNameFlag;
 
	private String lastViewedBy;
    private String lastViewedTime;
 
	public static BeneficiaryResponse toBeneficiaryResponse(Beneficiary beneficiaries) {
		return BeneficiaryResponse.builder()
				.refNo(beneficiaries.getRefNo())
				.requestDate(beneficiaries.getRequestDate())
				.requestStatus(beneficiaries.getRequestStatus())
				.transactionName(beneficiaries.getTransactionName())
				.unit(beneficiaries.getUnit())
				.channel(beneficiaries.getChannel())
				.countryName(beneficiaries.getCountryName())
//				.userNo(beneficiaries.getUserNo())
				.bankName(beneficiaries.getBankName())
				.branch(beneficiaries.getBranch())
				.city(beneficiaries.getCity())
				.beneficiaryFirstName(beneficiaries.getBeneficiaryFirstName())
				.beneficiaryLastName(beneficiaries.getBeneficiaryLastName())
				.beneficiaryMidName(beneficiaries.getBeneficiaryMidName())
				.benAccountNumber(beneficiaries.getBenAccountNumber())
				.ibanCode(beneficiaries.getIbanCode())
				.currency(beneficiaries.getCurrency())
				.intermediatorySwiftCode(beneficiaries.getIntermediatorySwiftCode())
				.swiftCode(beneficiaries.getSwiftCode())
				.routing(beneficiaries.getRouting())
				.sortOrChip(beneficiaries.getSortOrChip())
				.customerName(beneficiaries.getCustomerName())
				.customerNationalId	(beneficiaries.getCustomerNationalId())
				.gId(beneficiaries.getGId())
				.mobileNo(beneficiaries.getMobileNo())
				.lastModifiedBy(beneficiaries.getLastModifiedBy())
                .lastModifiedTime(beneficiaries.getLastModifiedTime())
                //.lastViewedBy(beneficiaries.getLastViewedBy())
                .lastViewedTime(beneficiaries.getLastModifiedTime())
				.txnNameFlag(beneficiaries.getTxnNameFlag())
				.build();
	}
}