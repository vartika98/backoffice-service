package com.qnb.bo.backofficeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDetailsDto {
	
	private String categoryCode;
	private String fieldName;
	private String description;
	private String action;
	private String status;

}
