package com.qnb.bo.backofficeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class CategoryMasterDto {

 
	private String categoryCode;
	private String boAuditFlag;
	private String foAuditFlag;
	private String rrAuditFlag;
	private String description;
	private String action;
	private String status;

}
