package com.qnb.bo.backofficeservice.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
 
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CfmsParamEntityIdDTO implements Serializable {
 
	private static final long serialVersionUID = 1L;
	public String serviceCode;
	public String custOption;
	public long transKey;
	public String unitId;
}