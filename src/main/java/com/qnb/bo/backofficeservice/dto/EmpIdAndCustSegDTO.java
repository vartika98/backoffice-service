package com.qnb.bo.backofficeservice.dto;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;
 

 
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
 
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmpIdAndCustSegDTO implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	public CfmsParamEntityIdDTO id;
	public String transValue;
	public String cfmsValue;
	public String typeOfProd;
	//public String unitId;
	public String employeeId;
	public LocalDateTime txnDate;
	public String valueArb;
	public long transId;
	public String action;
	public String status;
}
