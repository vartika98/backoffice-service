package com.qnb.bo.backofficeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorConfigDto {
	
	private String action;
	private String mWErrorCode;
	private String unitId;
	private String channelId;
	private String lang;
	private String status;
	private String ocsErrorCode;
	private String enErrorDesc;
	private String arErrorDesc;
	private String frErrorDesc;
	private String remarks;
	private String serviceType;
	

}
