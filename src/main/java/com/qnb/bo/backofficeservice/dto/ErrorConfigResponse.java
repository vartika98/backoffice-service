package com.qnb.bo.backofficeservice.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.bo.backofficeservice.enums.TxnStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorConfigResponse {
	
	private String unitId;
	
	private String channelId;
	
	private String serviceType;
	
	private String mwErrorCode;
	
	private String ocsErrorCode;
		
	private TxnStatus status;
	
	private String errDescEn;
	
	private String errDescArb;
	
	private String errDescFr;
	
	private String remarks;
	
	private String txnId;
	private Date dateCreated;
}
