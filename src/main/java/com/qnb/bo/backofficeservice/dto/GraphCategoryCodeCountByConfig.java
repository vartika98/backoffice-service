package com.qnb.bo.backofficeservice.dto;

import lombok.Data;

@Data
public class GraphCategoryCodeCountByConfig {

    private String categoryCode;
    private GraphDateCountDto categoryCodeData;
}
