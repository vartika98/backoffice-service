package com.qnb.bo.backofficeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GraphCategoryCodeCountDto {

    private List<GraphCategoryCodeCountByConfig> categoryCodes;
}
