package com.qnb.bo.backofficeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class GraphCountDto {

    private Map<String, Map<String, Long>> dataCount;
}
