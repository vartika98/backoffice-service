package com.qnb.bo.backofficeservice.dto;

import com.qnb.bo.backofficeservice.entity.audit.AuditMaster;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class GraphDataDto<T> {

    private long totalSuccessCount;
    private long totalFailureCount;
    private List<T> entities;
}
