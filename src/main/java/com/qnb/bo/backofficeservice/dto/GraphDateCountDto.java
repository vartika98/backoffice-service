package com.qnb.bo.backofficeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class GraphDateCountDto {

    private long totalSuccessCount;
    private long totalFailureCount;
    private List<GraphDayCountDto> dates;
}
