package com.qnb.bo.backofficeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GraphDayCountDto {

    private String date;
    private long successCount;
    private long failureCount;
}
