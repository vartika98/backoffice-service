package com.qnb.bo.backofficeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GroupRoleDto {

	private String txnId;
	private String groupName;
	private String groupDescription;
	private String groupId;
	private String status;
}
