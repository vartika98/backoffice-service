package com.qnb.bo.backofficeservice.dto.MessageMaintenance;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.http.HttpStatus;
import com.qnb.bo.backofficeservice.enums.ApplicationType;
import com.qnb.bo.backofficeservice.enums.RequestActionType;
import com.qnb.bo.backofficeservice.enums.TxnStatus;
import com.qnb.bo.backofficeservice.exceptions.ValidationException;
import com.qnb.bo.backofficeservice.service.Workflowable;
import com.qnb.bo.backofficeservice.enums.ExceptionMessages;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Application implements Workflowable{
	private static final String SALT = "application:";
	private final Long txnId;
	private final String appName;
	private final ApplicationType appType;
	private final TxnStatus status;
	private Set<ApplicationDetails> applicationDetails;
	private final RequestActionType action;
	private final String makerComments;
	private List<String> units;

	public static Application fromRequest(Long txnId, String appName, String appType, List<String> units,
			List<String> channels, String status, RequestActionType action, String makerComments) {

		Set<String> unitSet = Set.copyOf(units);
		Set<String> channelSet = Set.copyOf(channels);

		if (units != null && unitSet.size() < units.size()) {
			throw new ValidationException(ExceptionMessages.APPLICATION_REQUEST_DUPLICATE_UNITS.getErrorCode(),
					ExceptionMessages.APPLICATION_REQUEST_DUPLICATE_UNITS.getErrorDescription(), HttpStatus.BAD_REQUEST);
		}

		if (channels != null && channelSet.size() < channels.size()) {
			throw new ValidationException(ExceptionMessages.APPLICATION_REQUEST_DUPLICATE_UNITS.getErrorCode(),
					ExceptionMessages.APPLICATION_REQUEST_DUPLICATE_UNITS.getErrorDescription(), HttpStatus.BAD_REQUEST);
		}

		return Application.builder().txnId(txnId).appName(appName).appType(ApplicationType.getApplicationType(appType))
				.applicationDetails(getAppDetails(unitSet, channelSet)).status(TxnStatus.parseTxnStatus(status))
				.action(action).makerComments(makerComments).units(units).build();
	}

	public static Application fromRequest(Long txnId, RequestActionType action, String makerComments) {
		return Application.builder()
				.txnId(txnId)
				.action(action)
				.makerComments(makerComments)
				.build();
	}

	private static Set<ApplicationDetails> getAppDetails(Set<String> units, Set<String> channels) {
		Set<ApplicationDetails> appDetails = new HashSet<>();
		if (units != null && channels != null) {
			for (var unit : units) {
				for (var channel : channels) {
					appDetails.add(ApplicationDetails.fromRequest(unit, channel));
				}
			}
		}
		return appDetails;
	}
	public Application createNewApplicationDomainByUpdating(RequestActionType actionType, String makerComments) {
		return Application.builder()
				.txnId(txnId)
				.appName(appName)
				.appType(appType)
				.applicationDetails(applicationDetails)
				.status(status)
				.action(actionType)
				.makerComments(makerComments)
				.build();
	}

	@Override
	public List<String> getIdempotentRequestIdentifier() {
		return List.of( SALT + appName);
	}

	@Override
	public String getIdempotentRequestDetails() {
		return "Request : Application Management, Application Name : "+appName ;
	}
}

