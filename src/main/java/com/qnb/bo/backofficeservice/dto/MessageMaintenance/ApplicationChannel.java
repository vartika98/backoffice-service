package com.qnb.bo.backofficeservice.dto.MessageMaintenance;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@Builder
public class ApplicationChannel {

	private String channelId;
	private String channelDesc;
	
	public static ApplicationChannel toApplicationChannel(String channelId, String channelDesc) {
		return ApplicationChannel.builder()
				.channelId(channelId)
				.channelDesc(channelDesc)
				.build();
	}
}
