package com.qnb.bo.backofficeservice.dto.MessageMaintenance;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@Builder
public class ApplicationDetails {

	private final long txnId;
	private final long appId;
	private final String unit;
	private final ApplicationChannel channel;
	
	public static ApplicationDetails fromRequest(String unit, String channelId) {
		return ApplicationDetails.builder()
				.unit(unit)
				.channel(ApplicationChannel.toApplicationChannel(channelId, null))
				.build();
	}
	
	public static ApplicationDetails fromRequest(String unit, ApplicationChannel channel) {
		return ApplicationDetails.builder()
				.unit(unit)
				.channel(channel)
				.build();
	}
}
