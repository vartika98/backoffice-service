package com.qnb.bo.backofficeservice.dto.MessageMaintenance;

import java.util.Date;

import com.qnb.bo.backofficeservice.enums.TxnStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MaintenanceAppDetail {
	
	private Long refNo;
	
	private Long appId;
	
	private String application;
	
	private String unitId;
	
	private String channelIds;
		
	private Date outageStartDateTime;
	
	private Date outageEndDateTime;
	
	private TxnStatus status;
	
	private String action;
 
}
