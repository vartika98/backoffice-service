package com.qnb.bo.backofficeservice.dto.MessageMaintenance;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.bo.backofficeservice.enums.TxnStatus;
import com.qnb.bo.backofficeservice.utils.DateUtil;

import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MaintenanceDetails {
	private Long refNo;
 
	@Size(max = 3, message = "{unitId.exceedLimit}")
	private String unitId;
	
	private Long appId;
	
	@Size(max = 50, message = "{application.exceedLimit}")
	private String application;
	
	@Size(max = 3, message = "{channelId.exceedLimit}")
	private String channelIds;
		
	private String outageStartDateTime;
	
	private String outageEndDateTime;
	
	private TxnStatus status;
	
	
	public static MaintenanceAppDetail toMaintenanceDetail(MaintenanceDetails details) {
		
		return MaintenanceAppDetail.builder()
				.application(details.getApplication())
				.channelIds(details.getChannelIds())
				.outageStartDateTime(details.getOutageStartDateTime() != null ? DateUtil.getDateTime(details.getOutageStartDateTime()) : null)
				.outageEndDateTime(details.getOutageEndDateTime() != null ? DateUtil.getDateTime(details.getOutageEndDateTime()) : null)
				.status(details.getStatus())
				.refNo(details.getRefNo())
				.unitId(details.getUnitId())
				.appId(details.getAppId())
				.build();
	}
 
	public static MaintenanceDetails toResponse(MaintenanceAppDetail details) {
		
		return MaintenanceDetails.builder()
				.application(details.getApplication())
				.channelIds(details.getChannelIds())
				.outageStartDateTime(details.getOutageStartDateTime() != null
						? DateUtil.dateTimeInStringFormat(details.getOutageStartDateTime())
						: null)
				.outageEndDateTime(details.getOutageEndDateTime() != null ? DateUtil.dateTimeInStringFormat(details.getOutageEndDateTime())
						: null)
				.status(details.getStatus())
				.refNo(details.getRefNo())
				.unitId(details.getUnitId())
				.appId(details.getAppId())
				.build();
	}
}
