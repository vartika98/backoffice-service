package com.qnb.bo.backofficeservice.dto.MessageMaintenance;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.bo.backofficeservice.enums.TxnStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MaintenanceDto {

	private long refNo;
	private String purposeOfMsg;
	private String outageStartDate;
	private String outageEndDate;
	private TxnStatus status;
	private Set<String> impactedChannels;
}
