package com.qnb.bo.backofficeservice.dto.MessageMaintenance;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class MaintenanceListResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	private List<MaintenanceDto> maintenanceMessages;

}
