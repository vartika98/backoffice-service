package com.qnb.bo.backofficeservice.dto.MessageMaintenance;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessageDetail {
 
	private String lang;
	
	private String reason;
	
	public static MessageDetail toMessageDetail(MessageDetails details) {
		return MessageDetail.builder()
				.lang(details.getLang())
				.reason(details.getReason())
				.build();
	}
	
	public static MessageDetail toMessageDetail(String lang, String reason) {
		return MessageDetail.builder()
				.lang(lang)
				.reason(reason)
				.build();
	}
}
