package com.qnb.bo.backofficeservice.dto.MessageMaintenance;

import lombok.Data;

@Data
public class MessageMaintenanceReq {

	String action;
}
