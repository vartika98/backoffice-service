package com.qnb.bo.backofficeservice.dto.MessageMaintenance;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.qnb.bo.backofficeservice.enums.TxnStatus;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlannedUnplannedRequest {
 
	private List<MaintenanceDetails> applicationDetails;
	
	private List<MessageDetails> messageDetails;
 
	@Size(max = 100, message = "{makerComments.exceedLimit}")
	private String makerComments;
	
	@Size(max = 200, message = "{purposeOfMessage.exceedLimit}")
	private String purposeOfMsg;
	
	@Size(max = 1, message = "{messageType.exceedLimit}")
	private String messageType;
	
	@Pattern(regexp = "ACT|IAC", message = "{status.format}")
	private String status;
	
	private Long refNo;
	
	private String action;
	
	
	public static MaintenanceMessage normalise(PlannedUnplannedRequest req) {
		List<MaintenanceAppDetail> maintenanceDetailList = req.getApplicationDetails() != null ? req.getApplicationDetails().stream()
				.map(MaintenanceDetails::toMaintenanceDetail).collect(Collectors.toList()): new ArrayList<>();
		List<MaintenanceLangDetails> msgDetails =  req.messageDetails != null ? req.messageDetails.stream().map(MessageDetails::toMessageDetail)
				.collect(Collectors.toList()) : new ArrayList<>();
		
		if(req.getAction().equals("DELETE")) {
		return 	MaintenanceMessage.builder()
				.refNo(req.getRefNo())
				.action(req.getAction())
				.build();
		}
		
		return MaintenanceMessage.builder().makerComments(req.getMakerComments())
				.applicationDetails(maintenanceDetailList).messageDetails(msgDetails)
				.messageType(req.messageType)
				.purposeOfMsg(req.getPurposeOfMsg())
				.status(TxnStatus.valueOf(req.getStatus()))
				.refNo(req.getRefNo())
				.action(req.getAction())
				.build();
	}
 
}
