package com.qnb.bo.backofficeservice.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotificationParamConfigDTO {

	private Long txnId;
	private String notificationId;
	private String notificationDesc;
	private String action;
	private String status;
	private String createdBy;
	private LocalDateTime createdTime;
	private String modifiedBy;
	private LocalDateTime modifiedTime;

}
