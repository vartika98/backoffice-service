package com.qnb.bo.backofficeservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class PageConfigDto {
	private String action;
	private String unitId;
	private String channelId;
	private String screenId;
	private String configKey;
	private String status;
	private String comments ;
	private String configValue;

}
