package com.qnb.bo.backofficeservice.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class ProductDTO implements Serializable {

	private static final long serialVersionUID = 4982661739454658687L;
	private String id;
	private String unitId;
	private String prodCode;
	private String subprod;
	private String langCode;
	private String prodDesc;
	private String status;
	private String desc;
	private String createdBy;
	private String dateCreated;
	private String modifiedBy;
	private String dateModified;
	private String action;

}
