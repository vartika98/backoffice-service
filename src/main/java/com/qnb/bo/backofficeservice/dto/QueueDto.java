package com.qnb.bo.backofficeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueueDto {
	private String messageContent;
	private String messageId;
	private String trackId;
	private String categoryCode;
	private String sessionId;
	private String  processStatus;

}
