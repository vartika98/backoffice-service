package com.qnb.bo.backofficeservice.dto;

import java.io.Serializable;
import java.sql.Date;
 
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
 
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SurveyRequestDTO implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
 
	private long surveyId;
	private String surveyCaptionEng;
	private String surveyCaptionArb;
	private String surveyQuestionEng;
	private String surveyQuestionArb;
	private String surveyAnswerOneCaptionEng;
	private String surveyAnswerTwoCaptionEng;
	private String surveyAnswerThreeCaptionEng;
	private String surveyAnswerFourCaptionEng;
	private String surveyAnswerOneCaptionArb;
	private String surveyAnswerTwoCaptionArb;
	private String surveyAnswerThreeCaptionArb;
	private String surveyAnswerFourCaptionArb;
	private Date creationDate;
	private String unitId;
	private String enableFlag;
	private String skipCount;
	private Date startDate;
	private Date endDate;
	private String createdBy;
	private String modifiedBy;
	private Date modifiedDate;
	private String status;
	private String segmentType;
 
}