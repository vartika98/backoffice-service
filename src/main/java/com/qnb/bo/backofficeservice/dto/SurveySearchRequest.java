package com.qnb.bo.backofficeservice.dto;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;
 
@Data
@NoArgsConstructor
public class SurveySearchRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 981067035025983174L;
	String unitId;
	String segmentType;
}
