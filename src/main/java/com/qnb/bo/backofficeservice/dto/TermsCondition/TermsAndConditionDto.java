package com.qnb.bo.backofficeservice.dto.TermsCondition;

import lombok.Data;

@Data
public class TermsAndConditionDto {
	private String unitId;
	private String channelId;
	private String moduleId;
	private String subModuleId;
	private String screenId;
	private String tcUrlId;
	private String tcUrlEn;
	private String tcUrlAr;
	private String tcUrlFr;
	private String remarks;
	private String action;
	private String status;
	

}
