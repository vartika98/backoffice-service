package com.qnb.bo.backofficeservice.dto.TermsCondition;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TermsAndConditionRequest {
	
	private String unitId;
	private String channelId;
	private String moduleId;
	private String subModuleId;
	private String screenId;
	

}
