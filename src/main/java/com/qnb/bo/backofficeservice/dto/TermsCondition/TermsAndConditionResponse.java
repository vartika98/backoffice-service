package com.qnb.bo.backofficeservice.dto.TermsCondition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TermsAndConditionResponse {
	private String unitId;
	private String channelId;
	private String moduleId;
	private String subModuleId;
	private String screenId;
	private String tcUrlId;
	private String tcUrl;
	private String lang;
	private String remarks;
	
}
