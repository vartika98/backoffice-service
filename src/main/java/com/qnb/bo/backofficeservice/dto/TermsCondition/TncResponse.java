package com.qnb.bo.backofficeservice.dto.TermsCondition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TncResponse {
	private String unitId;
	private String channelId;
	private String moduleId;
	private String subModuleId;
	private String screenId;
	private String tcUrlId;
	private String tcUrlEn;
	private String tcUrlAr;
	private String tcUrlFr;
	private String remarks;
	private String status;
	
}
