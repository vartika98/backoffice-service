package com.qnb.bo.backofficeservice.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateError {
	
	   private List<ErrorConfigDto> errorConfigDto;
	    private String action;

}
