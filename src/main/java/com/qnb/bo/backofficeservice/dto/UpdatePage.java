package com.qnb.bo.backofficeservice.dto;

import java.util.List;

import lombok.Data;
@Data
public class UpdatePage {
	
	   private List<PageConfigDto> pageConfigDto;
	    private String action;

}
