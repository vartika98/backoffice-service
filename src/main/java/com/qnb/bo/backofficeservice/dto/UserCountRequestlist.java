package com.qnb.bo.backofficeservice.dto;

import java.util.List;

import com.qnb.bo.backofficeservice.enums.ProcessFlow;
import com.qnb.bo.backofficeservice.enums.WorkflowStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCountRequestlist {
	
	private List<ProcessFlow> processIds;
    private List<WorkflowStatus> status;

}
