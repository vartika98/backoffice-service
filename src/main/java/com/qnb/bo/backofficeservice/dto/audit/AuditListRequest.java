package com.qnb.bo.backofficeservice.dto.audit;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuditListRequest implements Serializable {
	private static final long serialVersionUID = 5982878654994798867L;
	private String channelId;
	private String categoryCode;
	private String fromDate;
	private String toDate;
	private String userId;
	private String guid;
	private String gid;
	private String userNo;
}
