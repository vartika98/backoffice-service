package com.qnb.bo.backofficeservice.dto.audit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.qnb.bo.backofficeservice.dto.currency.CurrencyDetails;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonInclude(Include.NON_NULL)
public class AuditListResponse implements Serializable{
	
	private static final long serialVersionUID = -242525594899632456L;
	private String auditRefno;
	private String auditDate;
	private String unitId;
	private String channelId;
	private String guid;
	private String mw_errCode;
	private String mw_errDesc;
	private String ocs_errCode;
	private String ocs_errDesc;
	private String traceId;
	private String categoryCode;
	private String status;
	private String ipAddress;
	private String imei;
	
	

}
