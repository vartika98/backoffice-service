package com.qnb.bo.backofficeservice.dto.audit;

import lombok.Data;

@Data
public class SummaryRequest extends AuditListRequest{

    private String globalId;
    private String guid;
    private String txnRefId;
}
