package com.qnb.bo.backofficeservice.dto.banner;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BannerReqDto implements Serializable{

	private static final long serialVersionUID = 7722312544958164232L;
	
	Integer txnId;
	String action;
	String unitId;
	String channelId;
	String segmentType;
	String screenId;
	String langCode;
	String bannerId;
	String bannerUrl;
	String redirectionUrl;
	String linkType;
	Integer dispPriority;
	String imageResolution;
	String status;
	String description;

}
