package com.qnb.bo.backofficeservice.dto.banner;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BannerResDto implements Serializable{

	private static final long serialVersionUID = 3292809764948152555L;
	
	private String segmentType;
	private String bannerId;
	private String bannerUrl;
	private String redirectionUrl;
	private String linkType;
	private int dispPriority;
	private String imageResolution;
	private String status;
	private String description;
	private String unitId;
	private String channelId;
}
