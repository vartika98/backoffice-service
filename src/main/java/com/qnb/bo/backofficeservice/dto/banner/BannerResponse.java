package com.qnb.bo.backofficeservice.dto.banner;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BannerResponse implements Serializable{

	private static final long serialVersionUID = -4344089601331411564L;
	
	private String segmentType;
	private String bannerId;
	private String bannerUrl;
	private String redirectionUrl;
	private String linkType;
	private int dispPriority;
	private String imageResolution;
	private String status;
	private String description;
	private String langCode;

}
