package com.qnb.bo.backofficeservice.dto.banner;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BannerSummaryReqDto implements Serializable{

	private static final long serialVersionUID = 8720110629009678552L;
	
	String unit;
	String channel;
	String acceptlanguage;
	String screenId;
	String imgResol;
	String disPriority;
}
