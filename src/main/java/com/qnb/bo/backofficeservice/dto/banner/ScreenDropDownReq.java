package com.qnb.bo.backofficeservice.dto.banner;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ScreenDropDownReq implements Serializable{

	private static final long serialVersionUID = -6114180322000696512L;
	
	String unit;
	String channel;
}
