package com.qnb.bo.backofficeservice.dto.billPay;

import lombok.Data;

@Data
public class Denomination {

	Integer denomination;
	String status;
}
