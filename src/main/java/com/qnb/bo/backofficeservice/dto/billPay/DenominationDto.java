package com.qnb.bo.backofficeservice.dto.billPay;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DenominationDto {

	private String utilityCode;
	private Integer denomination;
	private String status;
	private String utilityDesc;
	private Long txnId;
	private String action;

	public DenominationDto(String utilityCode, Integer denomination, String status, String utilityDesc, Long txnId) {
		super();
		this.utilityCode = utilityCode;
		this.denomination = denomination;
		this.status = status;
		this.utilityDesc = utilityDesc;
		this.txnId = txnId;
	}
}
