package com.qnb.bo.backofficeservice.dto.billPay;

import java.util.List;

import lombok.Data;

@Data
public class ManageDenominationDto {

	String utilityCode;
	List<Denomination> denominationList;
	String action;
}
