package com.qnb.bo.backofficeservice.dto.billPay;

import lombok.Data;

@Data
public class ManagePayeeDto {

	private String payeeId;
	private String payeeDesc;
	private String payeeLogo;
	private String unitCode;
	private Integer priority;
	private String status;
	private String action;
}
