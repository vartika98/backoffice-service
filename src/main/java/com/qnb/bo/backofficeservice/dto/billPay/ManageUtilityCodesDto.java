package com.qnb.bo.backofficeservice.dto.billPay;

import java.util.List;

import lombok.Data;

@Data
public class ManageUtilityCodesDto {

	private String utilityCode;
	private String utilityType;
	private String utilityDesc;
	private String payeeId;
	private String status;
	private String utilitySuspAcc;
	private String unitCode;
	private String partialPayment;
	private String addPayeeCheck;
	private String utilityCmsnFee;
	private List<Denomination> denominationList;
	private String action;
}
