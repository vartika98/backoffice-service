package com.qnb.bo.backofficeservice.dto.billPay;

import lombok.Data;

@Data
public class ManageUtilityTypesDto {

	private String utilityType;
	private String utilityDesc;
	private String status;
	private String action;
}
