package com.qnb.bo.backofficeservice.dto.billPay;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PayeeDto {

	private String payeeId;
	private String payeeDesc;
	private String status;
	private String payeeLogo;
	private String unitCode;
	private String unitCodeDesc;
	private Integer priority;
}
