package com.qnb.bo.backofficeservice.dto.billPay;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UtilityCodesDto {
	private String utilityCode;
    private String utilityCodeDesc;
    private String payeeId;
    private String payeeDesc;
    private String utilityType;
    private String utilityTypeDesc;
    private String unitCode;
    private String unitCodeDesc;
    private String commissionFee;
    private String status;
}
