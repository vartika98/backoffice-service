package com.qnb.bo.backofficeservice.dto.billPay;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UtilityDto {

	private String utilityCode;
	private String utilityType;
	private String utilityDesc;
	private String utilityTypeDesc;
	private String partialPayment;
	private String addPayeeCheck;
	private List<Integer> denominations;
}
