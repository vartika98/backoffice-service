package com.qnb.bo.backofficeservice.dto.billPay;

import lombok.Data;

@Data
public class UtilityTypesDto {

	private String utilityType;
	private String utilityDesc;
	private String status;
}
