package com.qnb.bo.backofficeservice.dto.boMenu;

import lombok.Data;

@Data
public class BoMenuDto {
	private int txnId;
	private String groupName;
	private String userNo;
	private String unitId;
	private String channelId;
	private String menuId;
	private String productCode;
	private String subProductCode;
	private String functionCode;
	private String status;
	private Integer dispPriority;
}
