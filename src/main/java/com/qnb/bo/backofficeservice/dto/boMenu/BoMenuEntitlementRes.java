package com.qnb.bo.backofficeservice.dto.boMenu;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class BoMenuEntitlementRes implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String productCode;
	private String productDesc;
	private List<BoSubMenuRes> subProduct;

}
