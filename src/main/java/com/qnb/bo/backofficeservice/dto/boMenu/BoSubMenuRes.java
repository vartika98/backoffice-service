package com.qnb.bo.backofficeservice.dto.boMenu;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class BoSubMenuRes implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String subProductcode;
	private String subProductDesc;
	private List<Map<String,String>> funtion;

}
