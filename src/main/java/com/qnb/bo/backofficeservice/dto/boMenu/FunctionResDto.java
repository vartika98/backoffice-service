package com.qnb.bo.backofficeservice.dto.boMenu;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FunctionResDto {

	private String functionCode;
	private String functionDesc;
}
