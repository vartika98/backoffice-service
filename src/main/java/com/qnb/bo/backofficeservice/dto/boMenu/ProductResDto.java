package com.qnb.bo.backofficeservice.dto.boMenu;

import java.util.List;

import lombok.Data;

@Data
public class ProductResDto {

	private String productCode;
	private String productDesc;
	private List<SubproductResDto> subProduct;
	
}
