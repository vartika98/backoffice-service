package com.qnb.bo.backofficeservice.dto.boMenu;

import java.util.List;

import lombok.Data;

@Data
public class SubProductWithFunctions {
    private String subProductCode;
    private String subProductDesc;
    private String subProductUrl;
    private List<FunctionResDto> functions;
}
