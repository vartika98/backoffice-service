package com.qnb.bo.backofficeservice.dto.boMenu;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubproductResDto {

	private String subProductCode;
	private String subProductDesc;
}
