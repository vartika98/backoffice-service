package com.qnb.bo.backofficeservice.dto.branch;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class BranchListRequest {

	private String unitId;
	private BranchParameterRequest paramDetails;
}

