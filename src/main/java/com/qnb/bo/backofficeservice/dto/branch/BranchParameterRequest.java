package com.qnb.bo.backofficeservice.dto.branch;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class BranchParameterRequest {

	private String unitId;
	private int txnId;
	private String action;
	private String countryCode;
	

	private String branchCode;
	private String branchDesc;
	private String branchDescAr;
	private String branchDescFr;
	private String city;
	private String address;
	private String collectionBranchFlag;
	private String status;
	private String createdBy;

	private Date dateCreated;
	private String modifiedBy;
	private Date dateModified;
	private int branchId;
}
