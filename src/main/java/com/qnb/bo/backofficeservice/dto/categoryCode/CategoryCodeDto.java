package com.qnb.bo.backofficeservice.dto.categoryCode;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class CategoryCodeDto implements Serializable{
	
	private static final long serialVersionUID = 7043299538945614684L;
	String categoryCode;
	String description;
}
