package com.qnb.bo.backofficeservice.dto.channel;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
public class ManageChannelReqDto {

	private String action;
	private String status;
	private String channelId;
	private String channelDesc;
	private String description;
}
