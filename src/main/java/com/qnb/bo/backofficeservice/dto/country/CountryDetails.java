package com.qnb.bo.backofficeservice.dto.country;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(Include.NON_NULL)
public class CountryDetails {
	
	private String action;
	
	private String nationality;
	
	private String countryCode;
	
	private String mobNoPrefix;
	
	private String remarks;
	
	private String eurozoneFlag;
	
	private String baseCurrency;
	
	private String countryCodeIso3a;
	
	private String qcbCountryCode;
	
	private String isoCountryCode;
	
	private String countryDesc;
	
	private String unitDesc;
	
	private String langCode;
	
	private String status;
	
	private String color;
	
	private String descEn;
	
	private String descAr;
	
	private String descFr;
	

}
