package com.qnb.bo.backofficeservice.dto.country;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonInclude(Include.NON_NULL)
@Builder
public class ManagenCntryReqDto implements Serializable {
	

	private static final long serialVersionUID = 7163855861527802499L;
	
	private List<CountryDetails> countryDetails;

}
