package com.qnb.bo.backofficeservice.dto.currency;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(Include.NON_NULL)
public class CurrencyDetails {

	private String action;

	private String currencyCode;
	
	private String currencyIsoCode;
	
	private String qcb;
	
	private String spotRateReciprocal;
	
	private String noOfDecimal;

	private String status;

	private String desc_en;
	
	private String desc_ar;
	
	private String desc_fr;
	
	private boolean isEditable;
}
