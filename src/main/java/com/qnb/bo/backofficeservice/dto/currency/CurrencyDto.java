package com.qnb.bo.backofficeservice.dto.currency;

import java.io.Serializable;

import lombok.Data;

@Data
public class CurrencyDto implements Serializable{

	private static final long serialVersionUID = 1L;

	private String unitId;
	
	private String currencyCode;
	
	private String currIsoCode;
 
	private String qcb;
 
	private String spotRateReciprocal;
	
	private String noOfDecimal;
	
	private String langCode;
	
	private String langDesc;
}
