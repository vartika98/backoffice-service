package com.qnb.bo.backofficeservice.dto.currency;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.qnb.bo.backofficeservice.dto.txn.LabelDetails;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(Include.NON_NULL)
@Builder
public class ManageCurrReqDto implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private String unitId;

	private List<CurrencyDetails> currencyDetails;

}
