package com.qnb.bo.backofficeservice.dto.errorConfig;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorConfigRequest {
	
	private int txnId;	
	private String unitId;
	private String channelId;
	private String createdBy;
	private Date dateCreated;
	private String modifiedBy;
	private Date dateModified;
	private String makerComments;
	private int erConfigId;
	private String serviceType;
	private List<ErrorParameterRequest> errDetails;
 
//	public  ErrorConfigMaster normalize(ErrorConfigRequest errReq) {
//		return ErrorConfigMaster.builder()
//				.unitId(errReq.getUnitId())
//				.channelId(errReq.getChannelId())
//				.createdBy(errReq.getCreatedBy())
//				.dateCreated(errReq.getDateCreated())
//				.modifiedBy(errReq.getModifiedBy())
//				.dateModified(errReq.getDateModified())
//				.erConfigId(errReq.getErConfigId())
//				.serviceType(errReq.getServiceType())
//				.paramDetails(errReq.getErrDetails())
//				.makerComments(errReq.getMakerComments())
//				.build();
//	}
 
}
