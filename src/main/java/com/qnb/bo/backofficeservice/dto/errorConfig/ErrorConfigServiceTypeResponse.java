package com.qnb.bo.backofficeservice.dto.errorConfig;

import java.io.Serializable;
import java.util.List;

import com.qnb.bo.backofficeservice.dto.ErrorConfigServiceType;

import lombok.Data;
@Data
public class ErrorConfigServiceTypeResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8468597017122679327L;
	
	private List<ErrorConfigServiceType>serviceTypes;
	

}
