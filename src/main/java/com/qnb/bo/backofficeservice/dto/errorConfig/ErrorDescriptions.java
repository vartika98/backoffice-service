package com.qnb.bo.backofficeservice.dto.errorConfig;

import com.qnb.bo.backofficeservice.entity.ErrorConfig;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorDescriptions {
	private String langCode;
	private String langDesc;
	private String status;
	private String remarks;
	private String ocsErrorCode;
	
	public static ErrorDescriptions toErrorDescriptions(ErrorConfig err) {
		return ErrorDescriptions.builder().langCode(err.getLang()).langDesc(err.getErrDesc()).status(err.getStatus()).remarks(err.getRemarks()).
				ocsErrorCode(err.getOcsErrCode()).build();
	}
}
