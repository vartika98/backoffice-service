package com.qnb.bo.backofficeservice.dto.errorConfig;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.bo.backofficeservice.enums.TxnStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
//@JsonInclude(Include.NON_NULL)
public class ErrorParameterRequest {
	
	private int txnId;
	
	private String action;
 
	private String lagCode;
	
	private String serviceType;
	
	private String mwErrCode;
	
	private String ocsErrCode;
		
	private String errDescEng;
	
	private String errDescArb;
	
	private String errDescFr;
	
	private TxnStatus status;
	
	private String remarks;
	
	private int erConfigId;
	
	/*
	 * public static Parameter normalize(ErrorParameterRequest req) { return
	 * Parameter.builder() .txnId(req.getTxnId()) //.confKey(req.getConfKey())
	 * //.confValue(req.getConfValue()) .status(req.getStatus())
	 * .remarks(req.getRemarks()) .build(); }
	 */
}
