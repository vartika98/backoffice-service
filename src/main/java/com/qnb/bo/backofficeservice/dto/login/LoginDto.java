package com.qnb.bo.backofficeservice.dto.login;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)

public class LoginDto implements Serializable {

	static final long serialVersionUID = 687796445933874601L;
	String userId;
	String password;
}
