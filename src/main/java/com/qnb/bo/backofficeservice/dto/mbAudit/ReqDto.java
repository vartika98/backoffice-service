
package com.qnb.bo.backofficeservice.dto.mbAudit;
 
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.EqualsAndHashCode;
 
@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReqDto implements Serializable {

	private static final long serialVersionUID = 6092605851461586228L;

	private String channelId;

	private String categoryCode;

	private String startDate;

	private String endDate;

	private String guid;

	private String gId;
 
}
