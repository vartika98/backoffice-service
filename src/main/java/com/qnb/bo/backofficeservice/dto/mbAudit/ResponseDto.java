package com.qnb.bo.backofficeservice.dto.mbAudit;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
public class ResponseDto {

	int txnId;
	String unitId;
	String globalId;
	String channelId;
	String userNo;
	String uuid;
	String categoryCode;
	String categoryType;
	String clientInfo;
	String method;
	String header;
	String request;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy HH:mm:ss")
	Date requestDate;
	String response;
	String responseCode;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy HH:mm:ss")
	Date responseDate;
	Long traceKey;
	String url;
	String rrMessage;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy HH:mm:ss")
	Date rrDate;
	Long txnCategoryId;
	String userNo2;
}
