package com.qnb.bo.backofficeservice.dto.mbAudit;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
public class ResponseDtoMB {

	int txnId;
	String unitId;
	String globalId;
	String userNo;
	String categoryCode;
	String serviceId;
	String header;
	String request;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy HH:mm:ss")
	Date requestDate;
	String response;
	String responseCode;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy HH:mm:ss")
	Date responseDate;
	String traceKey;
	String rrMessage;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy HH:mm:ss")
	Date rrDate;
}
