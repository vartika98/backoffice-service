package com.qnb.bo.backofficeservice.dto.notification;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class KeyConfigDTO implements Serializable {

	private static final long serialVersionUID = -7465263798779755204L;

	private long id;
	private String keyName;
	private String type;
	private String value0;
	private byte[] fileData;
	private String filePassword;
	private String status;
	private String value1;
	private String value2;
	private String value3;
	private String value4;
	private String value5;
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "Asia/Kolkata")
	private LocalDateTime createdDate;
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "Asia/Kolkata")	
	private LocalDateTime updatedDate;

}
