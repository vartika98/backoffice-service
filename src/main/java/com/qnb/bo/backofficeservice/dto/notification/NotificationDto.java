package com.qnb.bo.backofficeservice.dto.notification;

import java.io.Serializable;

import lombok.Data;

@Data
public class NotificationDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private String title;
	private String body;
	private String deviceId;

}
