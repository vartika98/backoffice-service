package com.qnb.bo.backofficeservice.dto.parameter;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(Include.NON_NULL)
public class ParamDetails implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private String action;

	private String key;

	private String value;
	
	private String remark;
	
	private String status;

}
