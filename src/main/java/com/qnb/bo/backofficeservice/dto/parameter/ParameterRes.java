package com.qnb.bo.backofficeservice.dto.parameter;

import java.io.Serializable;

import lombok.Data;

@Data
public class ParameterRes implements Serializable {
	private static final long serialVersionUID = 1L;
	private String key;
	private String value;
	private String status;
	private String description;

}
