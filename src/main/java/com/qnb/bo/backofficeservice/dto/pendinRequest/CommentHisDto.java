package com.qnb.bo.backofficeservice.dto.pendinRequest;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class CommentHisDto {

	private String makerComments;
	private String checkerComments;
	private String createdBy;
	private String createdTime;
	private String lastModifiedBy;
	private String lastModifiedTime;
}


