package com.qnb.bo.backofficeservice.dto.pendinRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.qnb.bo.backofficeservice.enums.WorkflowStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PendingReq {

	private List<String> workflowStatus;

	public List<WorkflowStatus> toWorkflowStatus() {
		if (workflowStatus != null) {
			return workflowStatus.stream().map(WorkflowStatus::valueOf).collect(Collectors.toList());
		}
		return new ArrayList<>();
	}
}
