package com.qnb.bo.backofficeservice.dto.pendinRequest;

import java.io.Serializable;
import java.time.Instant;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.bo.backofficeservice.enums.WorkflowStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PendingReqResponse<T> implements Serializable {

	private static final long serialVersionUID = -848439782168993599L;

	
	private String workflowId;
	private String requestId;
	private String processId;
	private String taskId;
	private String assignTo;
	private String initiatedGroup;
	private String workflowGroup;
	private Set<String> availableActions;
	private WorkflowStatus status;
	private String processStatus;
	private String historyCount;
	private String createdBy;
//	private Instant createdTime;
//	private String lastModifiedBy;
//	private Instant lastModifiedTime;
//	private String checkerComments;
//	private String makerComments;
	private CommentHisDto commentHistory;
	private T pendingDetails;
}

