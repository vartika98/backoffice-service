package com.qnb.bo.backofficeservice.dto.pendinRequest;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class WorkFlowRequest {
	
	private String refNo;
//	private String makerId;
//	private String ruleId;
	private String action;
	private String approverId;

}
