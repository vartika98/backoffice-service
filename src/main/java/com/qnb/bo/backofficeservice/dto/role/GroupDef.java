package com.qnb.bo.backofficeservice.dto.role;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class GroupDef {
	
	private final String groupId;
	private final String groupCode;
	private final String groupDescription;

}
