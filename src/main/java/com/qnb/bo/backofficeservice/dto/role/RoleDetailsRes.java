package com.qnb.bo.backofficeservice.dto.role;


import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.bo.backofficeservice.enums.ActionType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleDetailsRes implements Serializable {

	private static final long serialVersionUID = 1L;
	private String requestId;
	private List<String> roles;
	private String groupId;
	private String groupCode;
	private String groupDescription;
	private ActionType action;
	
}
