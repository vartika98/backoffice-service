package com.qnb.bo.backofficeservice.dto.role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleDto {
	private String action;
	private String roleLevel;
	private String description;
	private String status;
	

}
