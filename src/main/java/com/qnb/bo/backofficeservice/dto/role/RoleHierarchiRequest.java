package com.qnb.bo.backofficeservice.dto.role;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleHierarchiRequest {
	
	private String groupId;
	private String hierarchyLevel;
	private List<RoleDto> hierarchy;

}
