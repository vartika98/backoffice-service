package com.qnb.bo.backofficeservice.dto.role;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class UserRoleDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String groupName;
	private String groupIdDesc;
	private String description;
	private String userRoleHierichy;
	private String roleLevel;
	private String status;

}
