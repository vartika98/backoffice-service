package com.qnb.bo.backofficeservice.dto.rules;

import java.util.List;

import lombok.Data;

@Data
public class RulesDto {
	private long ruleId;
	private String groupId;
	private String corporateName;
	private String corporateId;
	private String ruleName;
	private String ruleDesc;
	private String crossChannelEnabled;
	private String product;
	private String subProduct;
	private String criteriaValue;
	private String selectedCriteria;
	private List<String> functionIdList;
	private String status;
	private String action;
}
