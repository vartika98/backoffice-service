package com.qnb.bo.backofficeservice.dto.rules;

import java.util.List;

import lombok.Data;

@Data
public class RulesSummaryResDto {
	private long ruleId;
	private String groupId;
	private String groupIdDesc;
	private String ruleName;
	private String ruleDesc;
	private String criteriaValue;
	private String product;
	private String subProduct;
	private List<String> functionIdList;
	private String boMakerId;
	private String boMakerName;
	private String boAuthId;
	private String boAuthName;
}
