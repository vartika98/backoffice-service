package com.qnb.bo.backofficeservice.dto.sessionManagement;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActiveUsersResponse implements Serializable {

	private static final long serialVersionUID = -3266241144068250408L;
	private String sessionId;
	private long creationTime;
	private long lastAccessTime;
	private int maxInactiveInterval;
	private long expiryTime;
	private String principalName;
	private List<SpringSessionAttributesDto> attributes;

	public String getexpiryTime() {
		LocalDateTime dateTime = LocalDateTime.ofEpochSecond(expiryTime / 1000, 0, ZoneOffset.UTC);
		return dateTime.format(DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss"));
	}

	public String getlastAccessTime() {
		LocalDateTime dateTime = LocalDateTime.ofEpochSecond(lastAccessTime / 1000, 0, ZoneOffset.UTC);
		return dateTime.format(DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss"));
	}

	public String getcreationTime() {
		LocalDateTime dateTime = LocalDateTime.ofEpochSecond(creationTime / 1000, 0, ZoneOffset.UTC);
		return dateTime.format(DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss"));
	}
}
