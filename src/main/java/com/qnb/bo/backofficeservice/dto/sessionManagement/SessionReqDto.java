package com.qnb.bo.backofficeservice.dto.sessionManagement;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class SessionReqDto implements Serializable {

	private static final long serialVersionUID = 6092605851461586228L;

	private String channelId;
	private String startDate;
	private String endDate;

}
