package com.qnb.bo.backofficeservice.dto.sessionManagement;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SpringSessionAttributesDto implements Serializable {
	private static final long serialVersionUID = 8869938090979260385L;

	private String sessionPrimaryId;
	private String attributeName;
	private String attributeBytes;
	
}
