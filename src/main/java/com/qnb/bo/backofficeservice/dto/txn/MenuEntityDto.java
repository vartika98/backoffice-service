package com.qnb.bo.backofficeservice.dto.txn;

import lombok.Data;

@Data
public class MenuEntityDto {

	private String menuId;
	private String description;
	private String parent;
	private String status;
}
