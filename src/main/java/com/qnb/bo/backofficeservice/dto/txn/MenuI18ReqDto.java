package com.qnb.bo.backofficeservice.dto.txn;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class MenuI18ReqDto {

	private String unitId;
	private String channelId;
	private String menuId;
	private String subMenuId;
	private List<MenuReqDto> menuDetails;
}
