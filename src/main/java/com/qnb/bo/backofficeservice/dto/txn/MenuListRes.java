package com.qnb.bo.backofficeservice.dto.txn;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class MenuListRes implements Serializable{
	private static final long serialVersionUID = 1L;
	private List<MenuResponseDto> menus;

}
