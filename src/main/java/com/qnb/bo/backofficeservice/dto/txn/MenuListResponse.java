package com.qnb.bo.backofficeservice.dto.txn;

import java.util.List;

import lombok.Data;

@Data
public class MenuListResponse {
	
	private String menuId;
    private String title;
    private String description;
    private int priority;
    private String menuType;
    private String pageId;    //customerSegment
    private String link;
    private List<MenuListResponse> subMenus;

}
