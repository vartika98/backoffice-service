package com.qnb.bo.backofficeservice.dto.txn;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class MenuMaster {
	private final Integer priority;
	private final String menuId;
	private final String description;
	private final String parent;
	private final String screenId;
	private List<TxnEntitlementDto> txnEntitlement;
	private List<MenuMaster> subMenus;
}
