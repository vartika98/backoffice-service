package com.qnb.bo.backofficeservice.dto.txn;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
 
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MenuMasterModel {
	  private String menuId;
      private String description;
      private String parent;
      private String screenId;
      private List<MenuMasterModel> subMenus;      
      private List<TxnEntitlementDto> txnEntitlement;

      public static MenuMasterModel menuMasterResponse(MenuMaster menuMaster) {
                      return MenuMasterModel.builder().menuId(menuMaster.getMenuId()).description(menuMaster.getDescription())
                                                      .parent(menuMaster.getParent()).screenId(menuMaster.getScreenId())
                                                      .txnEntitlement(menuMaster.getTxnEntitlement()).build();
      }

}
