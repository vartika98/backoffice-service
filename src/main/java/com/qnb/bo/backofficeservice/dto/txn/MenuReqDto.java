package com.qnb.bo.backofficeservice.dto.txn;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class MenuReqDto {

	private String menuId;
	private String subMenuId;
	private String subMenuId2;
	private String action;
	private String status;
	private String description;
	private String priority;
	private String value_en;
	private String value_fr;
	private String value_ar;
	private String screenId;
}
