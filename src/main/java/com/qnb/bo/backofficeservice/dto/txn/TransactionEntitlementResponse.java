package com.qnb.bo.backofficeservice.dto.txn;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionEntitlementResponse {
	private String menuId;

	private String description;

	private List<MenuMasterModel> subMenus;

	public static TransactionEntitlementResponse menuMasterResponse(MenuMaster menuMaster) {
		List<MenuMasterModel> menuMasterList = new ArrayList<>();
		return TransactionEntitlementResponse.builder().menuId(menuMaster.getMenuId())
				.description(menuMaster.getDescription())
				.subMenus(getMenuMasterResponse(menuMaster.getSubMenus(), menuMasterList)).build();
	}

	private static List<MenuMasterModel> getMenuMasterResponse(List<MenuMaster> menuMaster,
			List<MenuMasterModel> menuMasterList) {

		for (MenuMaster menu : menuMaster) {
			MenuMasterModel sub = MenuMasterModel.menuMasterResponse(menu);
			if (menu.getSubMenus() != null && !menu.getSubMenus().isEmpty()) {
				List<MenuMasterModel> subMenus = new ArrayList<>();
				sub.setSubMenus(getMenuMasterResponse(menu.getSubMenus(), subMenus));
			}
			menuMasterList.add(sub);
		}
		return menuMasterList;
	}

}
