package com.qnb.bo.backofficeservice.dto.txn;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.qnb.bo.backofficeservice.dto.channel.ChannelDto;
import com.qnb.bo.backofficeservice.dto.unit.LanguageDto;
import com.qnb.bo.backofficeservice.dto.unit.UnitDto;
import com.qnb.bo.backofficeservice.entity.Channel;
import com.qnb.bo.backofficeservice.entity.Unit;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class TranslationDto implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private Channel channel;

	private String language;

	private Unit unit;

	private String page;

	private String group;

	private String key;

	private String value;

	private String description;

	private String menuId;

	private String status;

	private String createdBy;

	private String langCode;

}
