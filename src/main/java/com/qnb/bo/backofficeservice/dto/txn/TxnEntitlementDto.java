package com.qnb.bo.backofficeservice.dto.txn;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TxnEntitlementDto {
	private final String parentId;
	private final String menuId;
	private final String unitId;
	private final String channelId;
	private final String status;
	private String externalId;

	public static TxnEntitlementDto toTxnEntitlementDto(String menuId, String unitId, String channelId,
			String parentId) {
		return TxnEntitlementDto.builder().menuId(menuId).unitId(unitId).channelId(channelId).parentId(parentId)
				.build();
	}

	public static TxnEntitlementDto toTxnEntitlementDtoStatus(String menuId, String unitId, String channelId,
			String status) {
		return TxnEntitlementDto.builder().menuId(menuId).unitId(unitId).channelId(channelId).status(status).build();
	}
	
}
