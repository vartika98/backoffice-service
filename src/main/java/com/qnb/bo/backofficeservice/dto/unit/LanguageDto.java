package com.qnb.bo.backofficeservice.dto.unit;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data

@EqualsAndHashCode(callSuper = false)
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LanguageDto implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private String langCode;

	private String langDesc;

	private String remarks;
	
	private String status;
	private String action;

}
