package com.qnb.bo.backofficeservice.dto.unit;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@JsonIgnoreProperties({ "status", "createdBy", "createdTime", "modifiedBy", "modifiedTime" })
public class SubProductDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	private String module;

	private String catagory;

	private String subProdKey;

	private String title;

}