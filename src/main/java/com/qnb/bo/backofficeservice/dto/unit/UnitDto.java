package com.qnb.bo.backofficeservice.dto.unit;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qnb.bo.backofficeservice.entity.Country;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties({ "createdBy", "createdTime", "modifiedBy", "modifiedTime" })
@Builder
public class UnitDto implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private String unitId;
	private String unitDesc;
	private String color;
	private String title;
	private String baseCur;
	private String cur2;
	private String cur3;
	private String country;
	private String timeZone;
	private String iban;
	private String branchCode;
	private String smsUnitId;
	private String action;
	private String status;
	private int orderBy;
	private String remarks;
//	private String langCode;

}
