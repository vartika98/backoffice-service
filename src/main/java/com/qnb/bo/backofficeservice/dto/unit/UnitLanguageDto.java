package com.qnb.bo.backofficeservice.dto.unit;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class UnitLanguageDto implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

//	private String id;
	private String unitId;
	
	private String action;

//	private LanguageDto language;
	
	private String isDefault;
	
	private List<String> langCode;
	
	private String status;
	
	private String remarks;
	
	private String channelId;
	
	
	private String langCodeEn;
	
	private String langCodeAr;
	
	private String langCodeFr;
	
	private String isDefaultEn;
	
	private String isDefaultAr;
	
	private String isDefaultFr;
	
	
	

}
