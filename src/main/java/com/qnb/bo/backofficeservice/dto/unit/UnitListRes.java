package com.qnb.bo.backofficeservice.dto.unit;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class UnitListRes implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<UnitMasterDto> units;
}
