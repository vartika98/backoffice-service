package com.qnb.bo.backofficeservice.dto.unit;

import java.io.Serializable;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UnitMasterDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String unitCode;
	private String unitDesc;
	private String countryCode;
	private String countryDesc;
	private String status;
	

}
