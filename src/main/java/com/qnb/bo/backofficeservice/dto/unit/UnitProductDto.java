package com.qnb.bo.backofficeservice.dto.unit;

import java.io.Serializable;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@JsonIgnoreProperties({ "status", "createdBy", "createdTime", "modifiedBy", "modifiedTime" })
public class UnitProductDto implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private String id;

	private String module;

	private String category;

	private String prodkey;

	private String title;

	private Set<SubProductDto> subProduct;

}
