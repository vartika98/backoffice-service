package com.qnb.bo.backofficeservice.dto.unit;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
public class UnitResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6059084755006362412L;
	private String unitId;
	private String unitDesc;
	private String status;
	private String remarks;

}
