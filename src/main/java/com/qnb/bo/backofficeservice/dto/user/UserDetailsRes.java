package com.qnb.bo.backofficeservice.dto.user;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.bo.backofficeservice.dto.role.GroupDef;
import com.qnb.bo.backofficeservice.enums.ActionType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDetailsRes implements Serializable {

	private static final long serialVersionUID = 1L;
	private String requestId;
	private List<String> unitId;
	private ActionType action;
	private String userId;
	private String firstName;
	private String expiryDate;
	private List<GroupDef> groups;
	
}
