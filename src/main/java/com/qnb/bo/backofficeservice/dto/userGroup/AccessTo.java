package com.qnb.bo.backofficeservice.dto.userGroup;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccessTo {

	private String productName;
	private String subProdName;
	private String functionName;
}
