package com.qnb.bo.backofficeservice.dto.userGroup;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class CreateGroupReq implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String groupId;
	private String groupName;
	private String groupDesc;
	private String unitId;
	private List<Map<String,Object>> accessTo;
	private String comment;
	private String action;
	private String status;
	

}
