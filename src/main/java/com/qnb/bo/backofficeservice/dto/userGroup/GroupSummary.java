package com.qnb.bo.backofficeservice.dto.userGroup;

import java.util.List;

import lombok.Data;

@Data
public class GroupSummary {

	private String groupId;
	private String groupIdDesc;
	private String groupName;
	private String groupDesc;
	private String unitId;
	private List<String> productName;
	private List<String> subProdName;
	private List<String> functionName;
	private String remarks;
	private String status;
	
}
