package com.qnb.bo.backofficeservice.dto.userGroup;

import java.io.Serializable;

import lombok.Data;

@Data
public class UserGroupSumDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String groupId;
	private String groupName;
	private String groupDesc;
	private String productName;
	private String subProdName;
	private String functionName;

}
