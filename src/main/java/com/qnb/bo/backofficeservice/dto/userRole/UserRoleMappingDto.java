package com.qnb.bo.backofficeservice.dto.userRole;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.bo.backofficeservice.entity.UserRole;
import com.qnb.bo.backofficeservice.entity.UserRoleMapping;
import com.qnb.bo.backofficeservice.repository.UserRoleMappingRepository;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserRoleMappingDto {
	
	private String userId;
	
	private String groupName;
	
	private String userRoleLevel;
	
	private String groupCode;
	
	private String role;
	
	private String name;
	
	private Date expiryDate;
	 	   
	private String userType;
		
	private String comments;
	private String action;
	private String status;

	private List<UserRoleMapping> userRoleMappings = new ArrayList<>();

    public void addUserRoleMapping(UserRoleMapping userRoleMapping) {
        this.userRoleMappings.add(userRoleMapping);
    }

	

}
