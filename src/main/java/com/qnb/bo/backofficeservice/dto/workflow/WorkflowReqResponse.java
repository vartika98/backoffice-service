package com.qnb.bo.backofficeservice.dto.workflow;

import java.io.Serializable;
import java.time.Instant;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.bo.backofficeservice.dto.pendinRequest.CommentHisDto;
import com.qnb.bo.backofficeservice.enums.WorkflowStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WorkflowReqResponse implements Serializable {

	private static final long serialVersionUID = -848439782168993599L;

	private String workflowId;
	private String requestId;
	private String processId;
	private String taskId;
	private String assignTo;
	private String initiatedGroup;
	private String workflowGroup;
	private Set<String> availableActions;
	private WorkflowStatus status;
	private String processStatus;
	private String historyCount;
//	private String createdBy;
//	private Instant createdTime;
//	private String lastModifiedBy;
//	private Instant lastModifiedTime;
//	private String checkerComments;
//	private String makerComments;
	private CommentHisDto commentHistory;
}
