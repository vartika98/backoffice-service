package com.qnb.bo.backofficeservice.entity;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public abstract class AbstractAuditEntity implements Serializable {
 
    @Column(name = "created_by", nullable = false)
    private String createdBy;
 
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_time", nullable = false)
    private Date createdTime;
 
    @Column(name = "last_modified_by", nullable = false)
    private String lastModifiedBy;
 
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_modified_time", nullable = false)
    private Date lastModifiedTime;
 
//    @PrePersist
//    protected void onCreate() {
//        lastModifiedTime = createdTime = new Date();
//        createdBy = AuthContextUtil.getUserName();
//        lastModifiedBy = AuthContextUtil.getUserName();
// 
//    }
// 
//    @PreUpdate
//    protected void onUpdate() {
//        lastModifiedTime = new Date();
//        lastModifiedBy = AuthContextUtil.getUserName();
// 
//    }
}
