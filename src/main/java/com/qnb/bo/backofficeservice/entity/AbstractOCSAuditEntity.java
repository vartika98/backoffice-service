package com.qnb.bo.backofficeservice.entity;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public class AbstractOCSAuditEntity implements Serializable{
 
		private static final long serialVersionUID = -8306360426593833398L;
 
		@Column(name = "created_by", nullable = false)
	    private String createdBy;
 
	    @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "date_created", nullable = false)
	    private Date dateCreated;
 
	    @Column(name = "modified_by", nullable = false)
	    private String modifiedBy;
 
	    @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "date_modified", nullable = false)
	    private Date dateModified;
 
	    @PrePersist
	    protected void onCreate() {
	    	dateModified = dateCreated = new Date();
	        createdBy = "BO";
	        modifiedBy = "BO";        	   	
	    }
 
	    @PreUpdate
	    protected void onUpdate() {
	    	dateModified = new Date();
	    	modifiedBy = "BO";
	    }
	    
//	    @PrePersist
//	    protected void onCreate() {
//	    	dateModified = dateCreated = new Date();
//	        createdBy = AuthContextUtil.getUserName();
//	        modifiedBy = AuthContextUtil.getUserName();	        	   	
//	    }
// 
//	    @PreUpdate
//	    protected void onUpdate() {
//	    	dateModified = new Date();
//	    	modifiedBy = AuthContextUtil.getUserName();
//	    }
 
}