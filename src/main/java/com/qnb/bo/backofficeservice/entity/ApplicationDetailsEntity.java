package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.dto.MessageMaintenance.ApplicationDetails;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "OCS_T_APPLICATION_DETAILS")
public class ApplicationDetailsEntity extends AbstractOCSAuditEntity {
	
	private static final long serialVersionUID = -7976857613566932667L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_APPS_DETAILS")
	@SequenceGenerator(sequenceName = "OCS_SEQ_APPS_DETAILS", allocationSize = 1, name = "SEQ_APPS_DETAILS")
	@Column(name = "TXN_ID")
	private Long txnId;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "APP_ID", referencedColumnName = "TXN_ID")
	private ApplicationEntity application;
	
	@Column(name = "UNIT_ID")
	private String unitId;
 
	@Column(name = "CHANNEL_ID")
	private String channelId;
 
 
	public static ApplicationDetailsEntity toEntity(ApplicationDetails appDetails, ApplicationEntity appEntity) {
		ApplicationDetailsEntity entity = new ApplicationDetailsEntity();
		entity.unitId = appDetails.getUnit();
		entity.channelId = appDetails.getChannel().getChannelId();
		entity.application = appEntity;
		return entity;
	}
}
