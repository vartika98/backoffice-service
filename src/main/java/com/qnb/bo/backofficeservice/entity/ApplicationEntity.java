package com.qnb.bo.backofficeservice.entity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.qnb.bo.backofficeservice.dto.MessageMaintenance.Application;
import com.qnb.bo.backofficeservice.dto.MessageMaintenance.ApplicationDetails;
import com.qnb.bo.backofficeservice.enums.ApplicationType;
import com.qnb.bo.backofficeservice.enums.TxnStatus;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "OCS_T_APPLICATIONS")
public class ApplicationEntity extends AbstractOCSAuditEntity {
 
	private static final long serialVersionUID = -7697967528619422224L;
 
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_APPS")
	@SequenceGenerator(sequenceName = "OCS_SEQ_APPS", allocationSize = 1, name = "SEQ_APPS")
	@Column(name = "TXN_ID")
	private Long txnId;
 
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private List<ApplicationDetailsEntity> applicationDetalils;
 
	@Column(name = "APPS_NAME")
	private String appName;
 
	@Column(name = "APP_TYPE")
	@Enumerated(EnumType.STRING)
	private ApplicationType appType;
 
	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private TxnStatus status;
 
	public static ApplicationEntity toEntity(Application app) {
		ApplicationEntity entity = new ApplicationEntity();
		entity.appName = app.getAppName();
		entity.appType = app.getAppType();
		entity.status = app.getStatus();
		if (app.getApplicationDetails() != null) {
			entity.applicationDetalils = app.getApplicationDetails().stream()
					.map(a -> ApplicationDetailsEntity.toEntity(a, entity)).collect(Collectors.toList());
		}
		return entity;
	}
 
	public static ApplicationEntity updateEntity(ApplicationEntity existingEntity, Application app) {
		existingEntity.appName = app.getAppName();
		existingEntity.appType = app.getAppType();
		existingEntity.status = app.getStatus();
 
		Set<ApplicationDetailsEntity> appDetailEntitiesToKeep = new HashSet<>();
		Set<ApplicationDetails> newAppDetails = new HashSet<>();
		for (var appDetail : app.getApplicationDetails()) {
			var isExist = false;
			for (var entityDetail : existingEntity.getApplicationDetalils()) {
				if (appDetail.getUnit().contentEquals(entityDetail.getUnitId())
						&& appDetail.getChannel().getChannelId().contentEquals(entityDetail.getChannelId())) {
					isExist = true;
					appDetailEntitiesToKeep.add(entityDetail);
					break;
				}
			}
			if (!isExist) {
				newAppDetails.add(appDetail);
			}
		}
 
		var appDetailsToRemove = new HashSet<ApplicationDetailsEntity>();
		for (var entityDetail : existingEntity.getApplicationDetalils()) {
			var isExist = false;
			for (var entityToKeep : appDetailEntitiesToKeep) {
				if (entityToKeep.getUnitId().contentEquals(entityDetail.getUnitId())
						&& entityToKeep.getChannelId().contentEquals(entityDetail.getChannelId())) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				appDetailsToRemove.add(entityDetail);
			}
		}
 
		existingEntity.getApplicationDetalils().removeAll(appDetailsToRemove);
 
		var appDetailsEntities = newAppDetails.stream().map(a -> ApplicationDetailsEntity.toEntity(a, existingEntity))
				.collect(Collectors.toSet());
		existingEntity.applicationDetalils.addAll(appDetailsEntities);
		return existingEntity;
	}
}
