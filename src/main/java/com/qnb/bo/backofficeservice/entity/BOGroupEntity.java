package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_BKO_OCS_GROUPS)
public class BOGroupEntity {

	@Id
	@Column(name = "GROUP_CODE")
	private String groupCode;
	
	@Column(name = "GROUP_NAME")
	private String groupName;
}
