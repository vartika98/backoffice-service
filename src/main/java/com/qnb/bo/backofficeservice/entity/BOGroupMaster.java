package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_BKO_OCS_BO_GROUP_MASTER)
public class BOGroupMaster extends BaseModel{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "TXN_ID")
	private String txnId;
	
	@Column(name = "GROUP_ID")
	private String groupId;
	
	@Column(name = "GROUP_NAME")
	private String groupName;
	
	@Column(name = "GROUP_DESCRIPTION")
	private String groupDesc;
	
	@Column(name = "UNIT")
	private String unitId;
	
	@Column(name = "REMARKS")
	private String remarks;
	
}
