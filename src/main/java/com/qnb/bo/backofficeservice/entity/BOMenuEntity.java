package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "BKO_T_MENU_MASTER")
public class BOMenuEntity extends BaseModel {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BKO_T_MENU_MASTER_SEQ")
	@Column(name = "TXN_ID")
	private int txnId;

	@Column(name = "MENU_ID")
	private String menuId;

	@Column(name = "MENU_DESCRIPTION")
	private String menuDescription;

	@Column(name = "GROUP_TYPE")
	private String groupType;

}
