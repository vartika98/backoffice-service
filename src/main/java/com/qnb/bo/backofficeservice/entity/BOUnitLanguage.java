package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;



@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_UNIT_LANGUAGE)
public class BOUnitLanguage extends BaseModel {
	

	private static final long serialVersionUID = -378614000896262887L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OCS_UNIT_LANGUAGE_SEQ")
	@SequenceGenerator(name = "OCS_UNIT_LANGUAGE_SEQ", sequenceName = "OCS_UNIT_LANGUAGE_SEQ", allocationSize = 1)
	@Column(name = "TXN_ID", nullable = false, length = 15)
//	@EqualsAndHashCode.Exclude
	private int id;

	@Column(name = "UNIT_ID")
	private String unitId;

	@Column(name = "CHANNEL_ID")
	private String channelId;

	@Column(name = "LANG_CODE")
	private  String langCode;

	@Column(name = "IS_DEFAULT")
	private String isDefault;
	
	@Column(name = "DESCRIPTION")
	private String remarks;

}
