package com.qnb.bo.backofficeservice.entity;

import java.util.Date;
import java.util.Objects;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.dto.banner.BannerReqDto;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_BANNER)
public class BannerDetailsEntity extends AbstractOCSAuditEntity {

	  private static final long serialVersionUID = -2650494736662741934L;
	  
	  @Column(name = "TXN_ID", insertable = false)
	  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BANNER_SEQ_NO")
      @SequenceGenerator(name = "BANNER_SEQ_NO", sequenceName = "OCS_BANNER_SEQUENCE",allocationSize = 1)
      private Long txnId;
	  
	  @EmbeddedId
	  private BannerEntityId id;

	  @Column(name = "BANNER_URL")
	  private String bannerUrl;

	  @Column(name = "REDIRECTION_URL")
	  private String redirectionUrl;

	  @Column(name = "START_DATE")
	  private Date startDate;

	  @Column(name = "END_DATE")
	  private Date endDate;

	  @Column(name = "DISP_PRIORITY")
	  private int disPriority;

	  @Column(name = "STATUS")
	  private String status;

	  @Column(name = "IMAGE_RESOLUTION")
	  private String imageResol;

	  @Column(name = "LINK_TYPE")
	  private String linkType;
	  
	  @Column(name = "DESCRIPTION")
	  private String description;
	  
	  public static BannerDetailsEntity toEntity(BannerReqDto dto) {
		  
		  BannerEntityId id = new BannerEntityId();
		  id.setUnitId(dto.getUnitId());
		  id.setBannerId(dto.getBannerId());
		  id.setChannelId(dto.getChannelId());
		  id.setScreenId(dto.getScreenId());
		  id.setSegmentType(dto.getSegmentType());
		  id.setLangCode(dto.getLangCode());
		  
		  return BannerDetailsEntity.builder()
				  .id(id)
				  .bannerUrl(dto.getBannerUrl())
				  .disPriority(dto.getDispPriority())
				  .endDate(new Date())
				  .imageResol(dto.getImageResolution())
				  .linkType(dto.getLinkType())
				  .redirectionUrl(dto.getRedirectionUrl())
				  .startDate(new Date())
				  .status(Objects.nonNull(dto.getStatus()) ? dto.getStatus() : AppConstant.DEFAULT_STATUS_ACT)
				  .description(dto.getDescription())
				  .build(); 
	  }
	  
	  public static BannerDetailsEntity toModify(BannerReqDto dto, BannerDetailsEntity entity) {
		   entity.setBannerUrl(dto.getBannerUrl() != null ? dto.getBannerUrl() : entity.getBannerUrl());
		   entity.setDisPriority(dto.getDispPriority() != null ? dto.getDispPriority() : entity.getDisPriority());
		   entity.setImageResol(dto.getImageResolution() != null ? dto.getImageResolution() : entity.getImageResol());
		   entity.setLinkType(dto.getLinkType() != null ? dto.getLinkType() : entity.getLinkType());
		   entity.setRedirectionUrl(dto.getRedirectionUrl() != null ? dto.getRedirectionUrl() : entity.getRedirectionUrl());
		   entity.setStatus(dto.getStatus() != null ? dto.getStatus() : entity.getStatus());
		   entity.setDescription(dto.getDescription() != null ? dto.getDescription() : entity.getDescription());
		   return entity;
	  }
}
