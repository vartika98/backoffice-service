package com.qnb.bo.backofficeservice.entity;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class BannerEntityId implements Serializable {

	private static final long serialVersionUID = 2718098742553619962L;

	@Column(name = "UNIT_ID")
	private String unitId;

	@Column(name = "CHANNEL_ID")
	private String channelId;

	@Column(name = "SEGMENT_TYPE")
	private String segmentType;

	@Column(name = "SCREEN_ID")
	private String screenId;

	@Column(name = "LANG_CODE")
	private String langCode;

	@Column(name = "BANNER_ID")
	private String bannerId;

}
