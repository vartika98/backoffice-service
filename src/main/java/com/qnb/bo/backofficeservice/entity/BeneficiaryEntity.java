package com.qnb.bo.backofficeservice.entity;

import java.util.Date;


 
import org.hibernate.annotations.Type;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
 
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "OCS_T_BENEFICIARIES")
public class BeneficiaryEntity extends AbstractOCSAuditEntity {
 
	private static final long serialVersionUID = 426337629057043856L;
 
	@Id
	@Column(name = "ref_no")
	private Integer refNo;
 
	@Column(name = "GLOBAL_ID")
	private String globalId;
 
	@Column(name = "FIRST_NAME")
	private String firstName;
 
	@Column(name = "MIDDLE_NAME")
	private String middleName;
 
	@Column(name = "LAST_NAME")
	private String lastName;
 
	@Column(name = "BANK_BIC")
	private String bankBic;
 
	@Column(name = "BRANCH_CODE")
	private String branchCode;
 
	@Column(name = "BANK_CITY ")
	private String bankCity;
 
	@Column(name = "BANK_COUNTRY")
	private String bankCountry;
 
	@Column(name = "RES_COUNTRY")
	private String resCountry;
 
	@Column(name = "STATE")
	private String state;
 
	@Column(name = "ACCOUNT_NO")
	private String accountNo;
 
	@Column(name = "IBAN")
	private String iban;
 
	@Column(name = "CURRENCY")
	private String currency;
 
	@Column(name = "CLEARING_CODE")
	private String clearingCode;
 
	@Column(name = "INTERMEDIARY_CODE")
	private String intermediaryCode;
 
	@Column(name = "ADDRESS_LINE1")
	private String addressLine1;
 
	@Column(name = "ADDRESS_LINE2")
	private String addressLine2;
 
	@Column(name = "ROUTING_CHANNEL")
	private String routingChannel;
 
	@Column(name = "STATUS")
	private String status;
 
	@Column(name = "SI_ENABLED")
	private String siEnabled;
 
	@Column(name = "SI_REF_NO")
	private String siRefNo;
 
	@Column(name = "CREATED_BY")
	private String createdBy;
 
	@Column(name = "CREATED_CHANNEL")
	private String createdChannel;
 
	@Column(name = "DATE_CREATED")
	private Date dateCreated;
 
	@Column(name = "modified_by")
	private String modifiedBy;
 
	@Column(name = "MODIFIED_CHANNEL")
	private String modifiedChannel;
 
	@Column(name = "date_modified")
	private Date dateModified;
 
	@Column(name = "USER_NO")
	private byte[] userNo;
 
	@Column(name = "BANK_NAME")
	private String bankName;
 
	@Column(name = "BO_APPROVAL")
	private String boApprval;
 
	@Column(name = "BENE_CITY")
	private String beneCity;
 
	@Column(name = "ZIP_CODE")
	private String zipCode;
 
	@Column(name = "BRANCH_NAME")
	private String branchName;
 
	@Column(name = "IS_IBAN")
	private String isBan;
 
	@Column(name = "CLEARING_TYPE")
	private String clearingType;
 
	@Column(name = "UNIT_CODE")
	private String unitcode;
 
	@Column(name = "TXN_NAME_FLAG")
	private String txnNameFlag;
 
	@Column(name = "REMARKS")
	private String remarks;
 
}

