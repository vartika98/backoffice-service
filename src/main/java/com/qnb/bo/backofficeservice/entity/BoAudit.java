package com.qnb.bo.backofficeservice.entity;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "OCS_T_BO_AUDIT_MASTER")
public class BoAudit {
 
	@Id
	@Column(name = "AUDIT_REFNO")
	private long auditRefno;
	
	@Column(name = "AUDIT_DATE")
	private Date auditDate;
	
	@Column(name = "UNIT_ID")
	private String unitId;
	
	@Column(name = "CHANNEL_ID")
	private String channelId;
	
	@Column(name = "HOST_IP")
	private String hostIp;
	
	@OneToOne
	@JoinColumn(name = "CATEGORY_CODE")
	private CategoryCode categoryCode;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "LOCALE")
	private String locale;
	
	@Column(name = "TXN_REFERENCE_NO")
	private String txnReferenceNo;
	
	@Column(name = "BO_USERNAME")
	private String boUsername;
}
