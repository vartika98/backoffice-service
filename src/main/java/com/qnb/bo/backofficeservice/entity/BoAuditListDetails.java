package com.qnb.bo.backofficeservice.entity;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "OCS_T_BO_AUDIT_DETAILS")
public class BoAuditListDetails {
 
	@Id
	@Column(name = "TXN_ID")
	private long txnId;
 
	@Column(name = "AUDIT_REFNO")
	private long auditRefNo;
 
	@Column(name = "AUDIT_SEQ")
	private long auditSeq;
 
	@Column(name = "AUDIT_DATE")
	private Date auditDate;
 
	@Column(name = "PARAM_NAME")
	private String paramName;
 
	@Lob
	@Column(name = "PARAM_VALUE")
	private String paramValue;
 
}
