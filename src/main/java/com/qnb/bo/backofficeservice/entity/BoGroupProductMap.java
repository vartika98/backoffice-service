package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_BKO_OCS_BO_GROUP_PRO_MAP)
public class BoGroupProductMap {

	@Id
	@Column(name = "TXN_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BKO_OCS_T_GROUP_PRODUCT_MAPPING_SEQ")
	@SequenceGenerator(name = "BKO_OCS_T_GROUP_PRODUCT_MAPPING_SEQ", sequenceName = "BKO_OCS_T_GROUP_PRODUCT_MAPPING_SEQ", allocationSize = 1)
	private int txnId;

	@Column(name = "GROUP_PRODUCT_MAPPING_ID")
	private String groupdProdMapId;

	@Column(name = "GROUP_ID")
	private String groupId;

	@Column(name = "GROUP_NAME")
	private String groupName;

	@Column(name = "GROUP_DESC")
	private String groupDesc;

	@Column(name = "PROD_CODE")
	private String prodCode;

	@Column(name = "SUB_PROD_CODE")
	private String subProdCode;

	@Column(name = "FUNCTION_CODE")
	private String functionCode;

}
