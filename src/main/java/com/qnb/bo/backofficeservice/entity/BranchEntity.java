package com.qnb.bo.backofficeservice.entity;

import java.util.Date;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_BRANCH_MASTER)
public class BranchEntity {

	@Id
	@Column(name = "TXN_ID")
	private int txnId;
	
	@Column(name = "UNIT_ID")
	private String unitId;
	
	@Column(name = "COUNTRY_CODE")
	private String countryCode;

	@Column(name = "BRANCH_CODE")
	private String branchCode;

	@Column(name = "BRANCH_NAME_EN")
	private String branchDesc;

	@Column(name = "BRANCH_NAME_AR")
	private String branchDescAr;

	@Column(name = "BRANCH_NAME_FR")
	private String branchDescFr;

	@Column(name = "CITY")
	private String city;

	@Column(name = "ADDRESS")
	private String address;

	@Column(name = "COLLECTION_BRANCH_FLAG")
	private String collectionBranchFlag;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "DATE_CREATED")
	private Date dateCreated;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@Column(name = "DATE_MODIFIED")
	private Date dateModified;

}
