package com.qnb.bo.backofficeservice.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "OCS_T_BO_CATEGORY_CODE")
public class CategoryCode {
	
	@Id
	@Column(name = "CATEGORY_CODE")
	private String categoryCode;
	
	@Column(name = "DESCRIPTION")
	private String description;
 
	@Column(name = "BO_AUDIT_FLAG")
	private String boAuditFlag;
 
}
