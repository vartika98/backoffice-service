package com.qnb.bo.backofficeservice.entity;

import java.time.LocalDateTime;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;
@Data
@Entity
@Table(name = "OCS_T_CATEGORY_DETAILS")
@SequenceGenerator(name = "OCS_CATEGORY_DETAILS_SEQ", sequenceName = "OCS_CATEGORY_DETAILS_SEQ", allocationSize = 1)

public class CategoryDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OCS_CATEGORY_DETAILS_SEQ")
	@Column(name = "TXN_ID")
	
	private int txnId;
	
	@Column(name = "CATEGORY_CODE")
	private String categoryCode;
	
	@Column(name = "FIELD_NAME")
	private String fieldName;
	
	@Column(name ="DESCRIPTION")
	private String description;
	
	@Column(name ="DATE_CREATED")
	private LocalDateTime createdAt;
	
	@Column(name ="CREATED_BY")
	private String createdBy;
	
	@Column(name ="STATUS")
	private String status;

}
