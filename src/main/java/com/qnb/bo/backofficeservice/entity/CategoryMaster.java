package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@Entity
@Table(name = "OCS_CATEGORY_MASTER")
@SequenceGenerator(name = "OCS_CATEGORY_MASTER_SEQ", sequenceName = "OCS_CATEGORY_MASTER_SEQ", allocationSize = 1)
public class CategoryMaster extends BaseModel{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OCS_CATEGORY_MASTER_SEQ")
	@Column(name = "TXN_ID")
	private int txnId;

	@Column(name = "CATEGORY_CODE")
	private String categoryCode;

	@Column(name = "BO_AUDIT_FLAG")
	private String boAuditFlag;

	@Column(name = "FO_AUDIT_FLAG")
	private String foAuditFlag;

	@Column(name = "RR_AUDIT_FLAG")
	private String rrAuditFlag;
	
	@Column(name = "DESCRIPTION")
	private String description;

}
