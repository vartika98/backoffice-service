package com.qnb.bo.backofficeservice.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EnableJpaAuditing
@JsonAutoDetect
@EntityListeners(AuditingEntityListener.class)
@Table(name = TableConstants.TABLE_OCS_T_CFMS_PARAM)
public class CfmsParamEntity implements Serializable {
	
	private static final long serialVersionUID = 5509969529823583303L;
	
	@EmbeddedId
	public CfmsParamEntityId id;
 
	@Column(name = "TRANS_VALUE")
	public String transValue;
 
	@Column(name = "CFMS_VALUE")
	public String cfmsValue;
 
	@Column(name = "TYPE_OF_PROD")
	public String typeOfProd;
 
	@Column(name = "EMPLOYEE_ID")
	public String employeeId;

	@Column(name = "TXN_DATE", nullable = false, updatable = false)
	@CreatedDate
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm")
	public LocalDateTime txnDate;
 
	@Column(name = "VALUE_ARB")
	public String valueArb;
	
	@Column(name = "STATUS")
	public String status;
	
	

}
