package com.qnb.bo.backofficeservice.entity;

import java.io.Serializable;
import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@jakarta.persistence.Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class CfmsParamEntityId implements Serializable {

	private static final long serialVersionUID = 5715923796319871549L;

	@Column(name = "SERVICE_CODE")
	public String serviceCode;

	@Column(name = "CUST_OPTION")
	public String custOption;

	@Column(name = "TRANS_KEY")
	public long transKey;

	@Column(name = "UNIT_ID")
	public String unitId;

}
