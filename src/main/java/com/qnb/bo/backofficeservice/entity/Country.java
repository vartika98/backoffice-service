package com.qnb.bo.backofficeservice.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_COUNTRY_MASTER)
public class Country extends BaseModel{
	private static final long serialVersionUID = 8615705817557328343L;
	 
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OCS_COUNTRY_SEQ")
	@SequenceGenerator(name = "OCS_COUNTRY_SEQ", sequenceName = "OCS_COUNTRY_SEQ", allocationSize = 1)
	@Column(name = "TXN_ID", length = 3)
	private int id;
 
	@Column(name = "COUNTRY_CODE",unique=true)
	private String countryCode;
	
	@Column(name = "COUNTRY_DESC")
	private String countryDesc;
	
	@Column(name = "ISO_COUNTRY_CODE")
	private String isoCountryCode;
	
	@Column(name = "QCB", nullable = false, length = 30)
	private String color;
 
	@Column(name = "UNIT_DESC", nullable = false, length = 30)
	private String unitDesc;
	
	@Column(name = "COUNTRY_CODE_ISO3A")
	private String countryCodeIso3a;
	
	@Column(name = "BASE_CURRENCY")
	private String baseCurrency;
	
	@Column(name = "EUROZONE_FLAG")
	private String eurozoneFlag;
	
	@Column(name = "REMARKS")
	private String remarks;
	
	@Column(name = "MOB_NO_PREFIX")
	private String mobNoPrefix;
	
	@Column(name = "NATIONALITY")
	private String nationality;
	
	@Column(name = "LANG_EN")
	private String langEn;
	
	@Column(name = "LANG_AR")
	private String langAr;
	
	@Column(name = "LANG_FR")
	private String langFr;
 
}
