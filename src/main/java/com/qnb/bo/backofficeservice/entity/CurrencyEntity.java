package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_CURRENCY_MASTER)
public class CurrencyEntity extends BaseModel{
	private static final long serialVersionUID = 8615705817557328343L;
	 
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_no")
	@SequenceGenerator(name = "seq_no", sequenceName = "OCS_CURRENCY_SEQ", allocationSize = 1)
	@Column(name = "TXN_ID", nullable = false, length = 15)
	@EqualsAndHashCode.Exclude
	private int id;
 
	@Column(name = "UNIT_ID",nullable = false)
	private String unitId;
	
	@Column(name = "CURRENCY_CODE")
	private String currencyCode;
	
	@Column(name = "CURRENCY_ISO_CODE")
	private String currIsoCode;
 
	@Column(name = "QCB_CURRENCY_CODE", length = 30,nullable = true)
	private Integer qcb;
 
	@Column(name = "SPOT_RATE_RECIPROCAL",  length = 30,nullable = true)
	private String spotRateReciprocal;
	
	@Column(name = "NO_OF_DECIMALS")
	private Integer noOfDecimal;
	
	@Column(name = "LANG_CODE")
	private String langCode;
	
	@Column(name = "LANG_DESC")
	private String langDesc;
 
}
