package com.qnb.bo.backofficeservice.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import com.qnb.bo.backofficeservice.constant.TableConstants;


@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_ERROR_CONF)
@SequenceGenerator(name = "ocs_t_error_config_SEQ", sequenceName = "OCS_T_ERROR_CONFIG_SEQ", allocationSize = 1)
//@DynamicInsert
//@EnableJpaAuditing
//@EntityListeners(AuditingEntityListener.class)
public class ErrorConfig implements Serializable {
 
	private static final long serialVersionUID = -8032944473314781424L;
 
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ocs_t_error_config_SEQ")
	@Column(name = "TXN_ID")
	private int txnId;
 
	@Column(name = "UNIT_ID")
	private String unitId;
 
	@Column(name = "CHANNEL_ID")
	private String channelId;
 
	@Column(name = "LANG_CODE")
	private String lang;
 
	@Column(name = "SERVICE_TYPE")
	private String serviceType;
 
	@Column(name = "MW_ERR_CODE")
	private String mwErrCode;
 
	@Column(name = "OCS_ERR_CODE")
	private String ocsErrCode;
 
	@Column(name = "OCS_ERR_DESC")
	private String errDesc;
 
	@Column(name = "STATUS")
	private String status;
 
	@Column(name = "REMARKS")
	private String remarks;
 
	@Column(name = "CREATED_BY")
	private String createdBy;
 
	@Column(name = "DATE_CREATED" , insertable = false, updatable = false)
	private Date dateCreated;
 
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;
 
//	@LastModifiedDate
	@Column(name = "DATE_MODIFIED")
	private Date dateModified;
 
}
