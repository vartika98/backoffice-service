package com.qnb.bo.backofficeservice.entity;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.qnb.bo.backofficeservice.dto.role.RoleDetailsRes;
import com.qnb.bo.backofficeservice.entity.workflow.WorkflowAuditEntity;
import com.qnb.bo.backofficeservice.enums.ActionType;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "BKO_T_GROUP_DEF_REQUEST_STAGING")
public class GroupRequestStagingEntity extends WorkflowAuditEntity {

	private static final long serialVersionUID = -5074380764871901887L;

	@Id
	@Column(name = "request_id")
	private String requestId;

	@Column(name = "request_comments")
	private String requestComments;

	@Column(name = "workflow_id")
	private String workflowId;

	@Column(name = "action")
	@Enumerated(EnumType.STRING)
	private ActionType action;

	@Column(name = "external_id")
	private String externalId;

	@Column(name = "group_code")
	private String groupCode;

	@Column(name = "group_description")
	private String groupDescription;

	@Column(name = "roles")
	private String roles;

	@Column(name = "unit")
	private String unit;

	public static RoleDetailsRes toRoleDetailsRes(GroupRequestStagingEntity groupEntity) {
		RoleDetailsRes roleDet = new RoleDetailsRes();
		roleDet.setGroupId(groupEntity.getExternalId());
		roleDet.setGroupCode(groupEntity.getGroupCode());
		roleDet.setGroupDescription(groupEntity.getGroupDescription());
		roleDet.setRoles(Stream.of(groupEntity.getRoles().split(",", -1)).collect(Collectors.toList()));
		roleDet.setAction(groupEntity.getAction());
		return roleDet;
	}

	private static String getGroupRoles(Set<String> roles) {
		if (roles != null && !roles.isEmpty()) {
			return String.join(",", roles);
		}
		return null;
	}
}
