package com.qnb.bo.backofficeservice.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import com.qnb.bo.backofficeservice.entity.workflow.WorkflowAuditEntity;

import lombok.Data;

@Data
@Entity
@Table(name = "BKO_T_GROUP_ROLES")
public class GroupRolesEntity extends WorkflowAuditEntity {

	private static final long serialVersionUID = -556090304935030373L;

	@Id
	@Column(name = "memb_id")
	private String membershipId;

	@Column(name = "role_id")
	private String roleId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "group_id")
	private GroupDefEntity groupDef;

	public static GroupRolesEntity toEntity(String membershipId, GroupDefEntity groupDefEntity, String roleId) {
		GroupRolesEntity entity = new GroupRolesEntity();
		entity.membershipId = membershipId;
		entity.groupDef = groupDefEntity;
		entity.roleId = roleId;
		return entity;
	}
}
