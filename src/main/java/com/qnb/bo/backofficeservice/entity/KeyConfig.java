package com.qnb.bo.backofficeservice.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = TableConstants.KEY_CONFIG)
@EntityListeners(AuditingEntityListener.class)
@EnableJpaAuditing
public class KeyConfig implements Serializable {

	private static final long serialVersionUID = -7465263398779755204L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "TXN_ID")
	private long txnId;
			
	@Column(name = "KEY_NAME",unique = true)
	private String keyName;
	
	@Column(name = "TYPE")
	private String type;
	
	@Column(name = "VALUE0")
	private String value0;
	
	@Column(name = "FILE_DATA")
	@Lob
	private byte[] fileData;
	
	@Column(name = "FILE_PASSWORD")
	private String filePassword;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "VALUE1")
	private String value1;
	
	@Column(name = "VALUE2")
	private String value2;
	
	@Column(name = "FILE_NAME")
	private String fileName;
	
	@Column(name = "DATE_CREATED",columnDefinition = "TIMESTAMP", nullable = false, updatable =false)
	@CreatedDate
	private LocalDateTime createdDate;

	@Column(name = "DATE_MODIFIED",columnDefinition = "TIMESTAMP", nullable = true)
	@LastModifiedDate
	private LocalDateTime updatedDate;

}
