package com.qnb.bo.backofficeservice.entity;

import java.util.Date;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_MB_RR_MESSAGE)
public class MBRRMessages extends AbstractOCSAuditEntity {

	private static final long serialVersionUID = -2754999353691146218L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TXN_ID")
	private int txnId;

	@Column(name = "UNIT_ID")
	private String unitId;

	@Column(name = "GLOBAL_ID")
	private String globalId;

	@Column(name = "USER_NO")
	private String userNo;

	@Column(name = "CATEGORY_CODE")
	private String categoryCode;

	@Column(name = "SERVICE_ID")
	private String serviceId;

	@Lob
	@Column(name = "HEADER")
	private String header;

	@Lob
	@Column(name = "REQUEST")
	private String request;

	@Column(name = "REQUEST_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date requestDate;

	@Lob
	@Column(name = "RESPONSE")
	private String response;

	@Column(name = "RESPONSE_CODE")
	private String responseCode;

	@Column(name = "RESPONSE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date responseDate;

	@Column(name = "TRACE_KEY")
	private String traceKey;

	@Lob
	@Column(name = "RR_MESSAGE")
	private String rrMessage;

	@Column(name = "RR_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date rrDate;

}
