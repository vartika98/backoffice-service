package com.qnb.bo.backofficeservice.entity;

import java.util.Date;

import com.qnb.bo.backofficeservice.dto.MessageMaintenance.MaintenanceAppDetail;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "OCS_T_MAINTENANCE_MSG_APP_DETAILS")
public class MaintenanceMessageAppDetailsEntity extends AbstractOCSAuditEntity {

	private static final long serialVersionUID = 7755674725685915392L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MAINT_MSG_APP")
	@SequenceGenerator(name = "SEQ_MAINT_MSG_APP", sequenceName = "OCS_SEQ_MAINTENANCE_MSG_APP_TXNID", allocationSize = 1)
	@Column(name = "TXN_ID")
	private Long txnId;
	
	@ManyToOne()
	@JoinColumn(name = "MSG_REF_NO", referencedColumnName = "TXN_ID")
	private MaintenanceMessageEntity refNo;

	@Column(name = "UNIT_ID")
	private String unitId;

	@Column(name = "APP_ID")
	private Long appId;

	@Column(name = "APP_NAME")
	private String application;

	@Column(name = "CHANNEL_ID")
	private String channelId;

	@Column(name = "OUTAGE_START_DATE_TIME")
	private Date outageStartDateTime;

	@Column(name = "OUTAGE_END_DATE_TIME")
	private Date outageEndDateTime;

	public static MaintenanceMessageAppDetailsEntity toEntity(MaintenanceMessageEntity maintenanceMsg,
			MaintenanceAppDetail appDetails) {

		return MaintenanceMessageAppDetailsEntity.builder()
				.appId(appDetails.getAppId())
				.refNo(maintenanceMsg)
				.unitId(appDetails.getUnitId())
				.application(appDetails.getApplication())
				.channelId(appDetails.getChannelIds())
				.outageStartDateTime(appDetails.getOutageStartDateTime())
				.outageEndDateTime(appDetails.getOutageEndDateTime())
				.build();

	}

}
