package com.qnb.bo.backofficeservice.entity;

import java.util.Set;
import java.util.stream.Collectors;

import com.qnb.bo.backofficeservice.dto.MessageMaintenance.MaintenanceMessage;
import com.qnb.bo.backofficeservice.enums.TxnStatus;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedAttributeNode;
import jakarta.persistence.NamedEntityGraph;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "OCS_T_MAINTENANCE_MSG")
@NamedEntityGraph(name = "MaintenanceMessageEntity.messageEntityGraph", attributeNodes = {@NamedAttributeNode("msgAppDetails"),@NamedAttributeNode("msgLangDetails")})
public class MaintenanceMessageEntity extends AbstractOCSAuditEntity{
 
	private static final long serialVersionUID = -6770501232834132428L;
 
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MAINT_MSG")
	@SequenceGenerator(name = "SEQ_MAINT_MSG",sequenceName = "OCS_SEQ_MAINTENANCE_MSG_TXNID", allocationSize = 1)
	@Column(name = "TXN_ID")
	private Long txnId;
	@Column(name = "MESSAGE_TYPE")
	private String messageType;
	@Column(name = "PURPOSE_OF_MSG")
	private String purposeOfMsg;
	@Column(name="STATUS")
	@Enumerated(EnumType.STRING)
	private TxnStatus status;
	@OrderBy("application asc, unitId asc, channelId asc")
	@OneToMany(mappedBy = "refNo", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<MaintenanceMessageAppDetailsEntity> msgAppDetails;
	@OneToMany(mappedBy = "refNo", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<MaintenanceMessageLangDetailsEntity> msgLangDetails;
	
	@Override
	public String toString() {
		return "MaintenanceMessageEntity [txnId=" + txnId + ", messageType=" + messageType + ", purposeOfMsg="
				+ purposeOfMsg + ", status=" + status + ", msgAppDetails=" + msgAppDetails + ", msgLangDetails="
				+ msgLangDetails + ", getTxnId()=" + getTxnId() + ", getMessageType()=" + getMessageType()
				+ ", getPurposeOfMsg()=" + getPurposeOfMsg() + ", getStatus()=" + getStatus() + ", getMsgAppDetails()="
				+ getMsgAppDetails() + ", getMsgLangDetails()=" + getMsgLangDetails() + ", getCreatedBy()="
				+ getCreatedBy() + ", getDateCreated()=" + getDateCreated() + ", getModifiedBy()=" + getModifiedBy()
				+ ", getDateModified()=" + getDateModified() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}
 
	public static MaintenanceMessageEntity toPlannedEntity(MaintenanceMessage maintenanceReq) {
		var mainMessage = MaintenanceMessageEntity.builder()
				.txnId(maintenanceReq.getRefNo())
				.messageType(maintenanceReq.getMessageType())
				.purposeOfMsg(maintenanceReq.getPurposeOfMsg())
				.status(maintenanceReq.getStatus() != null ? maintenanceReq.getStatus() : TxnStatus.ACT)
				.build();
		Set<MaintenanceMessageAppDetailsEntity> appDetails = maintenanceReq.getApplicationDetails().stream()
				.map(app -> MaintenanceMessageAppDetailsEntity.toEntity(mainMessage, app))
				.collect(Collectors.toSet());
		Set<MaintenanceMessageLangDetailsEntity> langDetails = maintenanceReq.getMessageDetails().stream()
				.map(lang -> MaintenanceMessageLangDetailsEntity.toEntity(mainMessage, lang))
				.collect(Collectors.toSet());
		mainMessage.setMsgAppDetails(appDetails);
		mainMessage.setMsgLangDetails(langDetails);
		return mainMessage;
	}
 
	public static MaintenanceMessageEntity toUnplannedEntity(MaintenanceMessage maintenanceReq) {
		var mainMessage = MaintenanceMessageEntity.builder()
				.messageType(maintenanceReq.getMessageType())
				.txnId(maintenanceReq.getRefNo())
				.purposeOfMsg(maintenanceReq.getPurposeOfMsg())
				.status(maintenanceReq.getStatus() != null ? maintenanceReq.getStatus() : TxnStatus.ACT)
				.build();
		Set<MaintenanceMessageLangDetailsEntity> langDetails = maintenanceReq.getMessageDetails().stream()
				.map(lang -> MaintenanceMessageLangDetailsEntity.toEntity(mainMessage, lang))
				.collect(Collectors.toSet());
		mainMessage.setMsgLangDetails(langDetails);
		return mainMessage;
	}
	
	
 
}
