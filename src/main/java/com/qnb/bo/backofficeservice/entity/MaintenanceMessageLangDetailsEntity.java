package com.qnb.bo.backofficeservice.entity;

import java.io.Serializable;

import com.qnb.bo.backofficeservice.dto.MessageMaintenance.MaintenanceLangDetails;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="OCS_T_MAINTENANCE_MSG_LANG_DETAILS")
public class MaintenanceMessageLangDetailsEntity implements Serializable{

	private static final long serialVersionUID = 262968249528509614L;
 
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MAINT_MSG_LANG_DETAILS")
	@SequenceGenerator(name = "SEQ_MAINT_MSG_LANG_DETAILS",sequenceName = "OCS_SEQ_MAINTENANCE_MSG_LANG_TXNID", allocationSize = 1)
	@Column(name = "TXN_ID")

	private Long txnId;

	@ManyToOne()
	@JoinColumn(name = "MSG_REF_NO", referencedColumnName = "TXN_ID")
	private MaintenanceMessageEntity refNo;

	@Column(name="LANG")
	private String lang;

	@Column(name="REASON")
	private String reason;

	public static MaintenanceMessageLangDetailsEntity toEntity(MaintenanceMessageEntity maintenanceMsg, MaintenanceLangDetails langDetails) {

		return MaintenanceMessageLangDetailsEntity.builder()
				.refNo(maintenanceMsg)
				.lang(langDetails.getLang())
				.reason(langDetails.getReason())
				.build();
	}
}
