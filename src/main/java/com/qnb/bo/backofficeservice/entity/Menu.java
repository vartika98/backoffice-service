package com.qnb.bo.backofficeservice.entity;

import java.util.LinkedHashSet;
import java.util.Set;

import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(of = "menuId", callSuper = false)
@Table(name = TableConstants.TABLE_MENU_MASTER)
public class Menu extends BaseModel {

	private static final long serialVersionUID = 8615705817557328343L;

	@Id
	@Column(name = "MENU_ID")
	private String menuId;

	@OrderBy
	private Integer priority;

	private String menuType;

	@Column(name = "PAGE_ID")
	private String pageId;

	
	@Column(name = "I18N_SCREEN_ID")
	private String screenId;

	private String link;

	private String description;
	
	@Column(name = "STATUS")
	private String status;

	@Transient
	private String title;

//	@ManyToOne(fetch = FetchType.LAZY)
	@Column(name = "PARENT_MENU_ID")
	private String parent;

	@OrderBy("priority")
	@OneToMany(mappedBy = "parent", fetch = FetchType.EAGER, orphanRemoval = true)
	private Set<Menu> subMenus = new LinkedHashSet<>();
	
//	@OneToMany(mappedBy = "menuId", fetch = FetchType.EAGER)
//	private List<TxnEntitlement> txnEntitlement;
}
