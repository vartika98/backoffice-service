package com.qnb.bo.backofficeservice.entity;

import java.sql.Timestamp;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "OCS_T_BO_MESSAGE_QUEUE")
public class MessageQueueEntity {

    @Id
    @Column(name = "MESSAGE_ID", length = 100, nullable = false)
    private String messageId;

    @Lob
    @Column(name = "MESSAGE_CONTENT")
    private String messageContent;

    @Column(name = "INSERT_DATE", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp insertDate;

    @Lob
    @Column(name = "TRACK_DETAILS")
    private String trackDetails;

    @Column(name = "CATEGORY_CODE", length = 100)
    private String categoryCode;

    @Column(name = "PROCESS_STATUS", length = 20, columnDefinition = "VARCHAR(20) DEFAULT 'PND'")
    private String processStatus;


}

