package com.qnb.bo.backofficeservice.entity;

import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "OCS_T_MESSAGE_QUEUE_HIS")
public class MessageQueueHistoryEntity {

    @Id
    @Column(name = "MESSAGE_ID", length = 100, nullable = false)
    private String messageId;

    @Column(name = "MESSAGE_CONTENT", columnDefinition = "CLOB NOT NULL")
    private String messageContent;

    @Column(name = "TXN_ID", nullable = false)
    private Long txnId;

    @Column(name = "INSERTED_DATE", columnDefinition = "DATE DEFAULT SYSDATE")
    private Date insertedDate;

}

