package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "OCS_T_NOTIFICATION_PARAMCONFIG", schema = "ADMIN")
public class NotificationParamConfigEntity extends BaseModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NOTIFI_DETAILS")
	@SequenceGenerator(sequenceName = "OCS_T_NOTIFICATION_PARAMCONFIG_SEQ", allocationSize = 1, name = "NOTIFI_DETAILS")
	@Column(name = "TXN_ID", nullable = false)
    private Long txnId;

    @Column(name = "NOTIFICATION_ID", length = 15)
    private String notificationId;

    @Column(name = "NOTIFICATION_DESC", length = 200)
    private String notificationDesc;


}
