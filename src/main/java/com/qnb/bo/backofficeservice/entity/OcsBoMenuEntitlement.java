package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_BKO_OCS_BO_MENU_ENTITLEMENT)
public class OcsBoMenuEntitlement {

	@Id
    @Column(name = "TXN_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BKO_OCS_BO_MENU_ENTITLEMENT_SEQ")
	@SequenceGenerator(name = "BKO_OCS_BO_MENU_ENTITLEMENT_SEQ", sequenceName = "BKO_OCS_BO_MENU_ENTITLEMENT_SEQ", allocationSize = 1)
    private int txnId;
    
    @Column(name = "GROUP_NAME", nullable = false)
    private String groupName;
    
    @Column(name = "USER_NO", nullable = false)
    private String userNo;
    
    @Column(name = "UNIT_ID", nullable = false)
    private String unitId;
    
    @Column(name = "CHANNEL_ID", nullable = false)
    private String channelId;
    
    @Column(name = "MENU_ID", nullable = false)
    private String menuId;
    
    @Column(name = "PRODUCT_CODE", nullable = false)
    private String productCode;
    
    @Column(name = "SUB_PRODUCT_CODE", nullable = false)
    private String subProductCode;
    
    @Column(name = "FUNCTION_CODE", nullable = false)
    private String functionCode;
    
    @Column(name = "STATUS", nullable = false)
    private String status;
    
    @Column(name = "DISP_PRIORITY", nullable = false)
    private Integer dispPriority;
}
