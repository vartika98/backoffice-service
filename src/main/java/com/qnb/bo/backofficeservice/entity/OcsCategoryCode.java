package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = TableConstants.TABLE_CATEGORY_CODE)
public class OcsCategoryCode {

	@Id
	@Column(name = "CATEGORY_CODE")
	private String categoryCode;
	
	@Column(name = "DESCRIPTION")
	private String description;
}
