package com.qnb.bo.backofficeservice.entity;

import java.util.Date;
import java.util.List;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_BKO_OCS_T_PRODUCT_MASTER)
public class OcsProductMaster {

	@Id
	@Column(name = "PRODUCT_CODE", nullable = false)
	private String productCode;

	@Column(name = "PRODUCT_DESC", nullable = false)
	private String productDescription;

	@Column(name = "BO_MAKER_ID", nullable = false)
	private String boMakerId;

	@Column(name = "BO_MAKER_DATE")
	private Date boMakerDate;

	@Column(name = "BO_MAKER_NAME", nullable = false)
	private String boMakerName;

	@Column(name = "BO_AUTH_ID", nullable = false)
	private String boAuthId;

	@Column(name = "BO_AUTH_DATE")
	private Date boAuthDate;

	@Column(name = "BO_AUTH_NAME", nullable = false)
	private String boAuthName;
	
	@Column(name = "PERIORITY")
	private String priority;

	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<OcsSubProductMaster> subProducts;

	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<OcsFunctionMaster> functionMaster;

	@Override
	public String toString() {
		return "OcsProductMaster [productCode=" + productCode + ", productDescription=" + productDescription
				+ ", boMakerId=" + boMakerId + ", boMakerDate=" + boMakerDate + ", boMakerName=" + boMakerName
				+ ", boAuthId=" + boAuthId + ", boAuthDate=" + boAuthDate + ", boAuthName=" + boAuthName
				+ ", subProducts=" + subProducts + ", functionMaster=" + functionMaster + "]";
	}

}
