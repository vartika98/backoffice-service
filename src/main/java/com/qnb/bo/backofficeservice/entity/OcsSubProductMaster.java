package com.qnb.bo.backofficeservice.entity;

import java.util.Date;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_BKO_OCS_SUB_PRODUCT_MASTER)
public class OcsSubProductMaster {

//	@Id
	@Column(name = "PRODUCT_CODE", nullable = false)
	private String productCode;

	@Id
	@Column(name = "SUB_PRODUCT_CODE", nullable = false)
	private String subProductCode;

	@Column(name = "SUB_PRODUCT_DESC", nullable = false)
	private String subProductDesc;

	@Column(name = "BO_MAKER_ID", nullable = false)
	private String boMakerId;

	@Column(name = "BO_MAKER_DATE")
	private Date boMakerDate;

	@Column(name = "BO_MAKER_NAME", nullable = false)
	private String boMakerName;

	@Column(name = "BO_AUTH_ID", nullable = false)
	private String boAuthId;

	@Column(name = "BO_AUTH_DATE")
	private Date boAuthDate;

	@Column(name = "BO_AUTH_NAME", nullable = false)
	private String boAuthName;
	
	@Column(name = "SUB_PRODUCT_URL")
	private String subProductUrl;
	
	@Column(name = "PERIORITY")
	private String priority;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCT_CODE", referencedColumnName = "PRODUCT_CODE", nullable = false, insertable = false, updatable = false)
	private OcsProductMaster product;

	@Override
	public String toString() {
		return "OcsSubProductMaster [productCode=" + productCode + ", subProductCode=" + subProductCode
				+ ", subProductDesc=" + subProductDesc + ", boMakerId=" + boMakerId + ", boMakerDate=" + boMakerDate
				+ ", boMakerName=" + boMakerName + ", boAuthId=" + boAuthId + ", boAuthDate=" + boAuthDate
				+ ", boAuthName=" + boAuthName + "]";
	}

}
