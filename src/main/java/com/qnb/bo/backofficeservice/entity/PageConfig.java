package com.qnb.bo.backofficeservice.entity;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.qnb.bo.backofficeservice.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import com.qnb.bo.backofficeservice.constant.TableConstants;
@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_PAGE_CONFIG)
@EnableJpaAuditing
@EntityListeners(AuditingEntityListener.class)
public class PageConfig extends BaseModel {
 
	private static final long serialVersionUID = 8615705817557328343L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "screen_seq_no")
	@SequenceGenerator(name = "screen_seq_no", sequenceName = "OCS_T_SCREEN_CONFIG_SEQ",allocationSize = 1)
	@Column(name = "TXN_ID", nullable = false)
	private Integer id;
 
	@ManyToOne
	@JoinColumn(name = "UNIT_ID", referencedColumnName = "UNIT_ID")
	private Unit unit;
 
	@ManyToOne
	@JoinColumn(name = "CHANNEL_ID", referencedColumnName = "CHANNEL_ID")
	private Channel channelId;
 
	@Column(name = "SCREEN_ID", nullable = false)
	private String page;
 
	@Column(name = "CONFIG_KEY", nullable = false)
	private String key;
 
	@Column(name = "CONFIG_VALUE", nullable = false)
	private String value;
 
	@Column(name = "DESCRIPTION", nullable = true)
	private String description;
 
}
