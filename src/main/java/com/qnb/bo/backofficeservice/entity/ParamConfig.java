package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import jakarta.persistence.SequenceGenerator;

import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "OCS_T_PARAM_CONFIG")
public class ParamConfig extends BaseModel {

	  private static final long serialVersionUID = 8615705817557328343L;

	  	@Id
		@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "param_seq_no")
		@SequenceGenerator(name = "param_seq_no", sequenceName = "OCS_PARAM_CONFIG_SEQ", allocationSize = 1)
		@Column(name = "TXN_ID", nullable = false, length = 15)
		private Integer id;
	 
		@ManyToOne
		@JoinColumn(name = "UNIT_ID", referencedColumnName = "UNIT_ID")
		private Unit unit;
	 
		@ManyToOne
		@JoinColumn(name = "CHANNEL_ID", referencedColumnName = "CHANNEL_ID", nullable = false)
		private Channel channel;
	 
		@Column(name = "CONF_KEY", nullable = false, length = 200)
		private String key;
	 
		@Column(name = "CONF_VALUE", nullable = false, length = 200)
		private String value;
	 
		@Column(name = "REMARKS", length = 1000)
		private String description;

}
