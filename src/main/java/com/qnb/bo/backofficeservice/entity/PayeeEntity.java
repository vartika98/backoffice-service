package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = TableConstants.TABLE_PAYEES)
public class PayeeEntity extends AbstractOCSAuditEntity {

	private static final long serialVersionUID = 6074717875351590697L;

	@Id
	@Column(name = "TXN_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PAYEE_ID_SEQ")
	@SequenceGenerator(name = "PAYEE_ID_SEQ", sequenceName = "PAYEE_ID_SEQ", allocationSize = 1)
	Long txnId;
	
	@Column(name = "PAYEE_ID", nullable = false)
	private String payeeId;

	@Column(name = "PAYEE_DESC", length = 100)
	private String payeeDesc;

	@Column(name = "STATUS", length = 3)
	private String status;

	@Lob
	@Column(name = "PAYEE_LOGO")
	private String payeeLogo;

	@Column(name = "UNIT_CODE", length = 3, columnDefinition = "VARCHAR2(3 BYTE) DEFAULT 'PRD'")
	private String unitCode;

	@Column(name = "PRIORITY")
	private Integer priority;

}
