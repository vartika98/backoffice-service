package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = TableConstants.TABLE_PREPAID_DENOMINATIONS)
public class PrePaidDenominations extends AbstractOCSAuditEntity {

	private static final long serialVersionUID = 8707119199385818588L;

	@Id
	@Column(name = "TXN_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DENOMINATION_SEQ")
    @SequenceGenerator(name = "DENOMINATION_SEQ", sequenceName = "OCS_DENOMINATION_SEQ",allocationSize = 1)
	private Long txnId;
	
	@Column(name = "UTILITY_CODE")
	private String utilityCode;
	
	@Column(name ="DENOMINATION")
	private Integer denomination;
	
	@Column(name = "STATUS")
	private String status;
}
