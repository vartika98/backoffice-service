package com.qnb.bo.backofficeservice.entity;

import java.io.Serializable;

import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_PRODUCT_MASTER)
public class Product extends BaseModel implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OCS_PROD_SEQ")
	@SequenceGenerator(name = "OCS_PROD_SEQ", sequenceName = "OCS_PROD_SEQ", allocationSize = 1)
	@Column(name = "TXN_ID", nullable = false)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "UNIT_ID",referencedColumnName = "UNIT_ID")
	private Unit unit;

	@Column(name = "PROD_CODE", nullable = false, length = 15)
	private String prodCode;

	@Column(name = "LANG_CODE", nullable = false, length = 2)
	private String langCode;

	@Column(name = "STATUS", nullable = true, length = 1)
	private String status;

	@Column(name = "PROD_DESC", nullable = false, length = 1000)
	private String prodDesc;

	@Column(name = "DESCRIPTION", length = 1000)
	private String desc;
	
}
