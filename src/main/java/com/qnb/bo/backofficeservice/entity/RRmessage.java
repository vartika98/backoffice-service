package com.qnb.bo.backofficeservice.entity;

import java.util.Date;

import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.entity.AbstractOCSAuditEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_RR_MESSAGE)
public class RRmessage extends AbstractOCSAuditEntity {

	private static final long serialVersionUID = -3112488739332575459L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int txnId;

    @Column(name = "UNIT_ID")
    private String unitId;

    @Column(name = "CHANNEL_ID")
    private String channelId;

    @Column(name = "GLOBAL_ID")
    private String globalId;

    @Column(name = "USER_NO")
    private String userNo;

    @Column(name = "UUID", nullable = false)
    private String uuid;

    @Column(name = "CATEGORY_CODE")
    private String categoryCode;

    @Column(name = "CATEGORY_TYPE")
    private String categoryType;

    @Column(name = "CLIENT_INFO")
    private String clientInfo;

    @Lob
    @Column(name = "HEADER")
    private String header;

    @Column(name = "METHOD")
    private String method;

    @Lob
    @Column(name = "REQUEST")
    private String request;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "REQUEST_DATE")
    private Date requestDate;

    @Lob
    @Column(name = "RESPONSE")
    private String response;

    @Column(name = "RESPONSE_CODE")
    private String responseCode;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RESPONSE_DATE")
    private Date responseDate;

    @Column(name = "TRACE_KEY")
    private Long traceKey;

    @Column(name = "URL")
    private String url;

    @Lob
    @Column(name = "RR_MESSAGE", nullable = false)
    private String rrMessage;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RR_DATE", nullable = false)
    private Date rrDate;

    @Column(name = "TXN_CATEGORY_ID")
    private Long txnCategoryId;

    @Column(name = "USER_NO_2")
    private String userNo2;

	
}
