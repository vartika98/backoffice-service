package com.qnb.bo.backofficeservice.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "BKO_T_ROLES_DEF")
public class RolesDefEntity extends AbstractAuditEntity {
 
	private static final long serialVersionUID = 8472672911138875776L;
 
	@Id
    @Column(name = "role_id")
    private String roleId;
 
    @Column(name = "role_description")
    private String roleDescription;
 
    @Column(name = "app_module")
    private String appModule;
    
    @Column(name = "role_group")
    private String roleGroup;
 
    public static RolesDefEntity toEntity(String roleId){
        RolesDefEntity entity = new RolesDefEntity();
        entity.roleId = roleId;
        return entity;
    }
}
