package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_BKO_RULES_ACC_MAP_MASTER)
public class RulesAccMapEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RULES_ACC_MAP_SEQ")
	@SequenceGenerator(sequenceName = "OCS_RULES_ACC_MAP_SEQ", allocationSize = 1, name = "RULES_ACC_MAP_SEQ")
	@Column(name = "TXN_ID")
	private Long txnId;
	
	@Column(name = "RULE_ID", nullable = false)
	private long ruleId;
	
	@Column(name = "FUNCTION_ID")
	private String functionId;
	
	@Column(name = "PRODUCT_CODE")
	private String productCode;
	
	@Column(name = "SUB_PRODUCT_CODE")
	private String subProductCode;
	
	@Column(name = "ACCOUNT_NO")
	private String accountNo;
	
	@Column(name = "UNIQUE_IDENTIFICAION")
	private String uniqueIdentification;
	
	@Column(name = "CRITERIA_TYPE")
	private String criteriaType;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RULE_ID", nullable = false, insertable = false, updatable = false)
	private RulesMaster rulesMaster;

	@Override
	public String toString() {
		return "RulesAccMapEntity [txnId=" + txnId + ", ruleId=" + ruleId + ", functionId=" + functionId
				+ ", productCode=" + productCode + ", subProductCode=" + subProductCode + ", accountNo=" + accountNo
				+ ", uniqueIdentification=" + uniqueIdentification + ", criteriaType=" + criteriaType + "]";
	}
}
