package com.qnb.bo.backofficeservice.entity;

import java.io.Serializable;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_BKO_OCS_RULES_DEFINITION)
public class RulesDefinition implements Serializable{

	private static final long serialVersionUID = 6783948372400023032L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RULES_DEFINITION_SEQ")
	@SequenceGenerator(sequenceName = "OCS_RULES_DEFINITION_SEQ", allocationSize = 1, name = "RULES_DEFINITION_SEQ")
	@Column(name = "RULE_PARSED_ID")
	private long ruleParsedId;
	
	@Column(name = "RULE_ID")
	private long ruleId;
	
	@Column(name = "MIN_AMOUNT")
	private String minAmount;
	
	@Column(name = "MAX_AMOUNT")
	private String maxAmount;
	
	@Column(name = "RULE_DEF")
	private String ruleDef;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RULE_ID", nullable = false, insertable = false, updatable = false)
	private RulesMaster rulesMaster;

	@Override
	public String toString() {
		return "RulesDefinition [ruleParsedId=" + ruleParsedId + ", ruleId=" + ruleId + ", minAmount=" + minAmount
				+ ", maxAmount=" + maxAmount + ", ruleDef=" + ruleDef + "]";
	}

}

