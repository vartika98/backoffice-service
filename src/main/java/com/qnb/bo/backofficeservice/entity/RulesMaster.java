package com.qnb.bo.backofficeservice.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_BKO_OCS_RULES_MASTER)
public class RulesMaster implements Serializable {

	private static final long serialVersionUID = 2556570103214054163L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RULES_MASTER_SEQ")
	@SequenceGenerator(sequenceName = "OCS_RULES_MASTER_SEQ", allocationSize = 1, name = "RULES_MASTER_SEQ")
	@Column(name = "RULE_ID", nullable = false)
	private long ruleId;

	@Column(name = "GROUP_ID", nullable = false)
	private String groupId;

	@Column(name = "RULE_NAME")
	private String ruleName;

	@Column(name = "RULE_DESCRIPTION")
	private String ruleDescription;

	@Column(name = "BO_MAKER_ID")
	private String boMakerId;

	@Column(name = "BO_MAKER_DATE")
	private Date boMakerDate;

	@Column(name = "BO_MAKER_NAME")
	private String boMakerName;

	@Column(name = "BO_AUTH_ID")
	private String boAuthId;

	@Column(name = "BO_AUTH_DATE")
	private Date boAuthDate;

	@Column(name = "BO_AUTH_NAME")
	private String boAuthName;
	
	@Column(name = "STATUS")
	private String status;
	
	@OneToMany(mappedBy = "rulesMaster", cascade = CascadeType.ALL)
	private List<RulesDefinition> rulesDefinitionList;
	
	@OneToMany(mappedBy = "rulesMaster", cascade = CascadeType.ALL)
	private List<RulesAccMapEntity> rulesAccMapMasterList;

	@Override
	public String toString() {
		return "RulesMaster [ruleId=" + ruleId + ", groupId=" + groupId + ", ruleName=" + ruleName
				+ ", ruleDescription=" + ruleDescription + ", boMakerId=" + boMakerId + ", boMakerDate=" + boMakerDate
				+ ", boMakerName=" + boMakerName + ", boAuthId=" + boAuthId + ", boAuthDate=" + boAuthDate
				+ ", boAuthName=" + boAuthName + ", rulesDefinitionList=" + rulesDefinitionList
				+ ", rulesAccMapMasterList=" + rulesAccMapMasterList + "]";
	}

}
