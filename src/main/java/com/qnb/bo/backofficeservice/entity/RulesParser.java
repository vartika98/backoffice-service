package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_BKO_OCS_RULES_PARSER)
public class RulesParser {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RULES_PARSER_SEQ")
	@SequenceGenerator(sequenceName = "OCS_RULES_PARSER_SEQ", allocationSize = 1, name = "RULES_PARSER_SEQ")
	@Column(name = "TXN_ID")
	private Long txnId;
	
	@Column(name = "RULE_PARSED_ID")
	private long ruleParsedId;
	
	@Column(name = "RULE_LEVEL")
	private String ruleLevel;
	
	@Column(name = "RULE_COUNT")
	private long ruleCount;
}
