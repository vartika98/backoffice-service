package com.qnb.bo.backofficeservice.entity;

import java.util.List;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PostLoad;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_SPRING_SESSION)
public class SpringSession {

	@Id
	@Column(name = "PRIMARY_ID", nullable = false)
	private String primaryId;

	@Column(name = "SESSION_ID", nullable = false)
	private String sessionId;

	@Column(name = "CREATION_TIME", nullable = false)
	private long creationTime;

	@Column(name = "LAST_ACCESS_TIME", nullable = false)
	private long lastAccessTime;

	@Column(name = "MAX_INACTIVE_INTERVAL", nullable = false)
	private int maxInactiveInterval;

	@Column(name = "EXPIRY_TIME", nullable = false)
	private long expiryTime;

	@Column(name = "PRINCIPAL_NAME")
	private String principalName;

	@OneToMany(mappedBy = "session", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<SpringSessionAttributes> attributes;
	
	@PostLoad
	private void trimPrimaryId() {
	    if (primaryId != null) {
	        primaryId = primaryId.trim();
	    }
	}

	public String getPrimaryId() {
	    return primaryId != null ? primaryId.trim() : null;
	}

	public void setPrimaryId(String primaryId) {
	    this.primaryId = primaryId != null ? primaryId.trim() : null;
	}

	@Override
	public String toString() {
		return "SpringSession{" + "primaryId='" + primaryId + '\'' + ", sessionId='" + sessionId + '\''
				+ ", creationTime=" + creationTime + ", lastAccessTime=" + lastAccessTime + ", maxInactiveInterval="
				+ maxInactiveInterval + ", expiryTime=" + expiryTime + ", principalName='" + principalName + '}';
	}
}
