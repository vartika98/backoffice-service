package com.qnb.bo.backofficeservice.entity;

import java.sql.Blob;

import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.model.SpringSessionAttributeId;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_SPRING_SESSION_ATTRIBUTES)
public class SpringSessionAttributes {

	@EmbeddedId
	private SpringSessionAttributeId id;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SESSION_PRIMARY_ID", referencedColumnName = "PRIMARY_ID", insertable = false, updatable = false)
    private SpringSession session;

	@Lob
	@Column(name = "ATTRIBUTE_BYTES", nullable = false)
	private Blob attributeBytes;

}
