package com.qnb.bo.backofficeservice.entity;



import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "OCS_T_SURVEY_MASTER")
@EnableJpaAuditing
@EntityListeners(AuditingEntityListener.class)
public class SurveyMessage {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SURVEY_ID")
	@SequenceGenerator(sequenceName = "BKO_SEQ_SURVEY_ID", allocationSize = 1, name = "SEQ_SURVEY_ID")
	@Column(name = "SURVEY_ID")
	private long surveyId;

	@Column(name = "SURVEYCAPTION_ENG")
	private String surveyCaptionEng;
	
	@Column(name = "SURVEYCAPTION_ARB")
	private String surveyCaptionArb;
	
	@Column(name = "SURVEYQUESTION_ENG")
	private String surveyQuestionEng;
	
	@Column(name = "SURVEYQUESTION_ARB")
	private String surveyQuestionArb;
	
	@Column(name = "SURVEYANSWER_1_CAPTION_ENG")
	private String surveyAnswerOneCaptionEng;
	
	@Column(name = "SURVEYANSWER_2_CAPTION_ENG")
	private String surveyAnswerTwoCaptionEng;
	
	@Column(name = "SURVEYANSWER_3_CAPTION_ENG")
	private String surveyAnswerThreeCaptionEng;
	
	@Column(name = "SURVEYANSWER_4_CAPTION_ENG")
	private String surveyAnswerFourCaptionEng;
	
	@Column(name = "SURVEYANSWER_1_CAPTION_ARB")
	private String surveyAnswerOneCaptionArb;
	
	@Column(name = "SURVEYANSWER_2_CAPTION_ARB")
	private String surveyAnswerTwoCaptionArb;
	
	@Column(name = "SURVEYANSWER_3_CAPTION_ARB")
	private String surveyAnswerThreeCaptionArb;
	
	@Column(name = "SURVEYANSWER_4_CAPTION_ARB")
	private String surveyAnswerFourCaptionArb;
	@CreatedDate
	@Column(name = "CREATION_DATE")
	private Date creationDate;
	
	@Column(name = "UNIT_ID")
	private String unitId;
	
	@Column(name = "ENABLEFLAG")
	private String enableFlag;
	
	@Column(name = "SKIP_COUNT")
	private String skipCount;
	
	@Column(name = "START_DATE")
	private Date startDate;
	
	@Column(name = "END_DATE")
	private Date endDate;
	
	@Column(name = "CREATED_BY")
	private String createdBy;
	
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;
	
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "SEGMENT_TYPE")
	private String segmentType;

}