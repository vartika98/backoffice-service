package com.qnb.bo.backofficeservice.entity;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.model.BaseModel;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@SequenceGenerator(name = "OCS_TERMS_COND_SEQ", sequenceName = "OCS_TERMS_COND_SEQ", allocationSize = 1)
@Table(name = "OCS_T_TERMS_CONDITIONS")
public class TermsAndCondition extends BaseModel {
	
	private static final long serialVersionUID = -8032944473314781424L;
	
 
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OCS_TERMS_COND_SEQ")
	@Column(name = "TXN_ID")	
	private int txnd;	
	
	@Column(name = "UNIT_ID")
	private String unitId;
 
	@Column(name = "CHANNEL_ID")
	private String channelId;
 
	@Column(name = "MODULE_ID")
	private String modId;
 
	@Column(name = "SUBMODULE_ID")
	private String subModId;
	
	@Column(name = "SCREEN_ID")
	private String screenId;
	
	@Column(name = "TC_URL_ID")
	private String tcUrlId;
	
	@Column(name = "TC_URL")
	private String tcUrl;
	
	@Column(name = "LANG_CODE")
	private String lang;
	
	@Column(name = "REMARKS")
	private String remarks;
}
