package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = TableConstants.TABLE_TRANSFERS)
public class TransferEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TRANSFER_ID", nullable = false)
    private Long transferId;

    @Column(name = "TRANSFER_TYPE", length = 15)
    private String transferType;

    @Column(name = "GLOBAL_ID", length = 15)
    private String globalId;

    @Column(name = "USER_NO")
    private byte[] userNo;

    @Column(name = "TRANSFER_INIT")
    private Date transferInit;

    @Column(name = "TRANSFER_END")
    private Date transferEnd;

    @Column(name = "VALUE_DATE")
    private Date valueDate;

    @Column(name = "CHANNEL_ID", length = 10)
    private String channelId;

    @Column(name = "UNIT_ID", length = 3)
    private String unitId;

    @Column(name = "DR_ACCOUNT", length = 15, nullable = false)
    private String drAccount;

    @Column(name = "CR_ACCOUNT", length = 50)
    private String crAccount;

    @Column(name = "WALLET_NO", length = 15)
    private String walletNo;

    @Column(name = "DR_CURRENCY", length = 3, nullable = false)
    private String drCurrency;

    @Column(name = "CR_CURRENCY", length = 3, nullable = false)
    private String crCurrency;

    @Column(name = "SELECTED_CURRENCY", length = 3)
    private String selectedCurrency;

    @Column(name = "BASE_CURRENCY", length = 3, nullable = false)
    private String baseCurrency;

    @Column(name = "DR_AMOUNT", precision = 20, scale = 3, nullable = false)
    private BigDecimal drAmount;

    @Column(name = "CR_AMOUNT", precision = 20, scale = 3, nullable = false)
    private BigDecimal crAmount;

    @Column(name = "BASE_AMOUNT", precision = 20, scale = 3, nullable = false)
    private BigDecimal baseAmount;

    @Column(name = "DR_UNIT", length = 3, nullable = false)
    private String drUnit;

    @Column(name = "CR_UNIT", length = 3)
    private String crUnit;

    @Column(name = "CHARGE_AMOUNT", precision = 18, scale = 3, nullable = false)
    private BigDecimal chargeAmount;

    @Column(name = "CHARGE_CODE", length = 25)
    private String chargeCode;

    @Column(name = "CHARGE_UID", length = 25)
    private String chargeUid;

    @Column(name = "VAT_AMOUNT", precision = 18, scale = 3)
    private BigDecimal vatAmount;

    @Column(name = "CUSTOMER_TYPE", length = 2)
    private String customerType;

    @Column(name = "SELL_RATE", precision = 20, scale = 10, nullable = false)
    private BigDecimal sellRate;

    @Column(name = "BUY_RATE", precision = 20, scale = 10)
    private BigDecimal buyRate;

    @Column(name = "TRANSFER_RATE", precision = 20, scale = 10, columnDefinition = "DEFAULT 0")
    private BigDecimal transferRate;

    @Column(name = "RECIPROCAL_FLAG", length = 1)
    private String reciprocalFlag;

    @Column(name = "MTCN_CODE", length = 15)
    private String mtcnCode;

    @Column(name = "CUSTOMER_SEGMENT", length = 1)
    private String customerSegment;

    @Column(name = "USER_COMMENT", length = 255)
    private String userComment;

    @Column(name = "STATUS", length = 10)
    private String status;

    @Column(name = "LAST_STATUS_TIME")
    private Date lastStatusTime;

    @Column(name = "TRACE_KEY", length = 100)
    private String traceKey;

    @Column(name = "GUID", length = 20)
    private String guid;

    @Column(name = "TXN_REFID", length = 20)
    private String txnRefid;

    @Column(name = "PAYMENT_REFID", length = 50)
    private String paymentRefid;

    @Column(name = "BEN_NAME", length = 256)
    private String benName;

    @Column(name = "DESTINATION", length = 100)
    private String destination;

    @Column(name = "ROUTING_CHANNEL", length = 10)
    private String routingChannel;

    @Column(name = "EXP_PAY_AMT", precision = 20, scale = 3, columnDefinition = "DEFAULT 0", nullable = false)
    private BigDecimal expPayAmt;

    @Column(name = "BANK_NAME", length = 256)
    private String bankName;

    @Column(name = "PURPOSE_OF_REMIT", length = 1000)
    private String purposeOfRemit;

    @Column(name = "IS_B2B", length = 1, columnDefinition = "DEFAULT 'N'")
    private char isB2B;

    @Column(name = "POP_CODE", length = 10)
    private String popCode;

    @Column(name = "RECEIVER_PHONE", length = 20)
    private String receiverPhone;

    @Column(name = "RECEIVER_EMAIL", length = 60)
    private String receiverEmail;

    @Column(name = "PAYMENT_STATUS", length = 1)
    private String paymentStatus;

    @Column(name = "PAYMENT_DATE")
    private Date paymentDate;

    @Column(name = "POSTING_STATUS", length = 1, columnDefinition = "DEFAULT 'N'")
    private String postingStatus;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "QCBR_FLAG", length = 2)
    private String qcbrFlag;

    @Column(name = "SECRET_QUESTION", length = 50)
    private String secretQuestion;

    @Column(name = "SECRET_ANSWER", length = 50)
    private String secretAnswer;

    @Column(name = "DESTINATION_STATE", length = 30)
    private String destinationState;

    @Column(name = "DESTINATION_CITY", length = 50)
    private String destinationCity;

}

