package com.qnb.bo.backofficeservice.entity;

import java.io.Serializable;
import java.sql.Date;

import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
 
import lombok.Data;
import lombok.NoArgsConstructor;
 
@Data
@NoArgsConstructor
@Entity
@Table(name = TableConstants.TABLE_OCS_T_TXN_CATEGORY)
public class TxnCategoryEntity extends BaseModel implements Serializable{
 
	private static final long serialVersionUID = -1542102012307070272L;
 
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OCS_TXN_CATEGORY_SEQ_AUD")
	@SequenceGenerator(name = "OCS_TXN_CATEGORY_SEQ_AUD", sequenceName = "OCS_TXN_CATEGORY_SEQ_AUD", allocationSize = 1)
	@Column(name="TXN_CATEGORY_ID")
	private int txnId;
 
	@Column(name="CATEGORY_TYPE")
	private String catType;
 
	@Column(name="category_code")
	private String categoryCode;
	
    @Column(name="DESCRIPTION")
    private String desc;
    
    @Column(name="ENCRYPTION_FLAG")
    private String encryFlag;
}