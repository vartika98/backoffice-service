package com.qnb.bo.backofficeservice.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.model.BaseModel;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = TableConstants.TABLE_TXN_ENTITLEMENT)
public class TxnEntitlement extends BaseModel {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "TXN_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "txn_generator")
	@SequenceGenerator(name = "txn_generator", sequenceName = "OCS_TXN_ENT_SEQ", allocationSize = 1)
	private Integer txnId;

	@Column(name = "UNIT_ID")
	private String unitId;

	@Column(name = "CHANNEL_ID")
	private String channelId;

	@Column(name = "MENU_ID")
	private String menuId;

	@Column(name = "CUSTOMER_SEGMENT")
	private String customerSegment;

	@Column(name = "DISP_PRIORITY")
	private int dispPriority;

}
