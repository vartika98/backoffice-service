package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.OCS_T_UNIT_DESC)
public class UnitDesc extends BaseModel {
	
	private static final long serialVersionUID = -9104049990076432883L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OCS_COUNTRY_SEQ")
	@SequenceGenerator(name = "OCS_COUNTRY_SEQ", sequenceName = "OCS_COUNTRY_SEQ", allocationSize = 1)
	@Column(name = "TXN_ID", length = 3)
	private int id;
	
	@Column(name = "UNIT_ID")
	private String unitId;
	
	@Column(name = "UNIT_DESC")
	private String unitDesc;
	
	@Column(name = "LANG_CODE")
	private String langcode;
	
	@Column(name = "REMARKS")
	private String remarks;
	
}
