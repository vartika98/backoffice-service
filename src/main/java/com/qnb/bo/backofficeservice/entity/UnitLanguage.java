package com.qnb.bo.backofficeservice.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.model.BaseModel;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_UNIT_LANGUAGE)
public class UnitLanguage extends BaseModel {

	private static final long serialVersionUID = 8615705817557328343L;

	@Id
	@Column(name = "TXN_ID", nullable = false, length = 15)
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNIT_ID", referencedColumnName = "UNIT_ID")
	private Unit unit;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHANNEL_ID", referencedColumnName = "CHANNEL_ID", insertable = false, updatable = false, nullable = false)
	private Channel channel;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "LANG_CODE")
	private Language language;

	@Column(name = "IS_DEFAULT")
	private String isDefault;
}

