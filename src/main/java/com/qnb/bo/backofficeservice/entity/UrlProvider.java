package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@Table(name = TableConstants.TABLE_URL_PROVIDER)
public class UrlProvider implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "TXN_ID")
    private long id;

    @Column(name = "MICRO_SERVICE_ID")
    private String micServId;

    @Column(name = "CATEGORY_CODE")
    private String catCode;

    @Column(name = "SERVICE_TYPE")
    private String serviceType;

    @Column(name = "MW_URL")
    private String mwUrl;

    @Column(name = "URL_VERSION")
    private String urlVersion;

    @Column(name = "URL_STAGE")
    private String urlStage;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "HTTP_METHOD")
    private String httpMethod;

    @Column(name = "HEADER")
    private String header;

    @Column(name = "PARAMETERS")
    private String parameters;

    @Column(name = "REQUEST_BODY")
    private String requestBody;

    @Column(name = "IS_AUDIT_REQ")
    private String isAuditReq;

    @Column(name = "DESCRIPTION")
    private String desc;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "date_created")
    private Date createdDate;

    @Column(name = "modified_by")
    private String modBy;

    @Column(name = "date_modified")
    private Date modDate;

    @Column(name = "IS_UI_ENC_FLAG")
    private String isUIEncFlag;

    @Column(name = "IS_MW_ENC_FLAG")
    private String isMWEncFlag;

}
