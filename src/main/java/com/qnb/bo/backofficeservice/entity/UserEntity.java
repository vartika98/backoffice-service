package com.qnb.bo.backofficeservice.entity;

import java.util.Date;
import java.util.Set;

import com.qnb.bo.backofficeservice.enums.UserStatus;
import com.qnb.bo.backofficeservice.enums.UserType;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "BKO_T_USER")
public class UserEntity extends AbstractAuditEntity {

	private static final long serialVersionUID = -5161840300649570621L;

	@Id
	@Column(name = "user_id")
	private String userId;

	@OneToOne(mappedBy = "userEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private UserAuthEntity userAuthEntity;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "expiry_date")
	@Temporal(TemporalType.DATE)
	private Date expiryDate;

	@Column(name = "date_of_birth")
	@Temporal(TemporalType.DATE)
	private Date dob;

	@Column(name = "mobile_no")
	private String mobileNumber;

	@Column(name = "email_id")
	private String emailId;

	@Column(name = "user_status")
	@Enumerated(EnumType.STRING)
	private UserStatus userStatus;

	@Column(name = "user_type")
	@Enumerated(EnumType.STRING)
	private UserType userType;

	@Column(name = "created_source_ip")
	private String createdSourceIp;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "USER_COMMENTS")
	private String comments;
	
	@Column(name = "STATUS")
	private String status;

}
