package com.qnb.bo.backofficeservice.entity;

import java.util.Date;
import java.util.Set;

import com.qnb.bo.backofficeservice.enums.UserStatus;
import com.qnb.bo.backofficeservice.enums.UserType;
import com.qnb.bo.backofficeservice.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@Entity
@SequenceGenerator(name = "OCS_ROLE_SEQ", sequenceName = "OCS_ROLE_SEQ", allocationSize = 1)
@Table(name = "BKO_T_ROLE_MASTER")
public class UserRole extends AbstractOCSAuditEntity {
	private static final long serialVersionUID = 3434802555579995199L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OCS_ROLE_SEQ")
	@Column(name = "TXN_ID")
	private int txnId;

	@Column(name = "GROUP_NAME")
	private String groupName;

	@Column(name = "USER_ROLE_LEVEL")
	private String roleLevel;

	@Column(name = "USER_ROLE_NAME")
	private String description;
	@Column(name = "BO_MAKER_ID")
	private String boMakerId;
	@Column(name = "BO_MAKER_DATE")
	private Date boMakerDate;
	@Column(name = "BO_MAKER_NAME")
	private String boMakerName;
	@Column(name = "BO_AUTH_ID")
	private String boAuthId;
	@Column(name = "BO_AUTH_DATE")
	private Date boAuthDate;
	@Column(name = "BO_AUTH_NAME")
	private String boAuthName;
	@Column(name = "USER_ROLE_HIERICHY")
	private String userRoleHierichy;
	@Column(name = "STATUS")
	private String status;

}
