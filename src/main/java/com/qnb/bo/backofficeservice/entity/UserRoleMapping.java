package com.qnb.bo.backofficeservice.entity;

import java.util.Date;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter 
@Entity  
@Table(name = "BKO_USER_ROLES_MAPPING")
@SequenceGenerator(name = "OCS_T_USER_ROLE_MAPPING_SEQ", sequenceName = "OCS_T_USER_ROLE_MAPPING_SEQ", allocationSize = 1)
public class UserRoleMapping {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OCS_T_USER_ROLE_MAPPING_SEQ")
	@Column(name="TXN_ID")
	private int txnId;
	
	@Column(name="USER_ID")
	private String userId;
	
	@Column(name="GROUP_NAME")
	private String groupName;
	
	@Column(name="USER_ROLE_LEVEL")
	private String userRoleLevel;
	
	@Column(name="USER_GROUP_CODE")
	private String groupCode;
	@Column(name="STATUS")
	private String status;

}
