package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = TableConstants.TABLE_UTILITY_CODES)
public class UtilityCodesEntity extends AbstractOCSAuditEntity {

	private static final long serialVersionUID = 7793428118174477294L;

	@Id
	@Column(name = "TXN_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UTILITY_CODES_SEQ")
	@SequenceGenerator(name = "UTILITY_CODES_SEQ", sequenceName = "UTILITY_CODES_SEQ", allocationSize = 1)
	Long txnId;
	
	@Column(name = "UTILITY_CODE")
	private String utilityCode;

	@Column(name = "UTILITY_DESC", length = 100)
	private String utilityDesc;

	@Column(name = "PAYEE_ID", length = 15)
	private String payeeId;

	@Column(name = "STATUS", length = 3)
	private String status;

	@Column(name = "UTILITY_TYPE", columnDefinition = "VARCHAR2(3) DEFAULT 'POP'", length = 3)
	private String utilityType;

	@Column(name = "UTILITY_SUSP_ACC", length = 20)
	private String utilitySuspAcc;

	@Column(name = "UNIT_CODE", columnDefinition = "VARCHAR2(3) DEFAULT 'PRD'", length = 3)
	private String unitCode;

	@Column(name = "PARTIAL_PAYMENT", columnDefinition = "VARCHAR2(1) DEFAULT 'N'", length = 1)
	private String partialPayment;

	@Column(name = "ADD_PAYEE_CHECK", columnDefinition = "VARCHAR2(1) DEFAULT 'Y'", length = 1)
	private String addPayeeCheck;

	@Column(name = "UTILITY_CMSN_FEE", length = 6)
	private String utilityCmsnFee;
}
