package com.qnb.bo.backofficeservice.entity;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = TableConstants.TABLE_UTILITY_TYPES)
public class UtilityTypesEntity extends AbstractOCSAuditEntity {
	
	private static final long serialVersionUID = -6415118419975400891L;

	@Id
	@Column(name = "TXN_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UTILITY_TYPE_SEQ")
	@SequenceGenerator(name = "UTILITY_TYPE_SEQ", sequenceName = "UTILITY_TYPE_SEQ", allocationSize = 1)
	Long txnId;
	
	@Column(name = "UTILITY_TYPE")
	String utilityType;
	
	@Column(name = "UTILITY_DESC")
	String utilityDesc;
	
	@Column(name = "STATUS")
	String status;

}
