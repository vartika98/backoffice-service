package com.qnb.bo.backofficeservice.entity;

import java.util.Date;

import com.qnb.bo.backofficeservice.constant.TableConstants;
import com.qnb.bo.backofficeservice.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_BKO_OCS_T_WORKFLOW_HISTORY)
public class WorkflowHistoryEntityOCS {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "WORKFLOW_SEQ_NO")
	@SequenceGenerator(name = "WORKFLOW_SEQ_NO", sequenceName = "WORKFLOW_SEQ_NO", allocationSize = 1)
	@Column(name = "REF_NO")
	private Integer refNo;

	@Column(name = "GROUP_NAME", nullable = false)
	private String grpName;

	@Column(name = "UNIT_ID")
	private String unitId;

	@Column(name = "CHANNEL_ID")
	private String channelId;

	@Column(name = "PRODUCT_CODE")
	private String prodCode;

	@Column(name = "SUBPROD_CODE")
	private String subProdCode;

	@Column(name = "FUNCTION_CODE")
	private String funCode;

	@Column(name = "MAKER_ID")
	private String makerId;

	@Column(name = "MAKER_DATE")
	private Date makerDate;

	@Column(name = "MAKER_COMMENTS")
	private String makerComments;

	@Column(name = "AUTH_ID")
	private String authId;

	@Column(name = "AUTH_DATE")
	private Date authDate;

	@Column(name = "AUTH_COMMENTS")
	private String authComments;

	@Column(name = "RULE_ID")
	private String ruleId;

	@Column(name = "HIERARCHIAL_RULE")
	private String hierarchialRule;

	@Column(name = "REASON")
	private String reason;

	@Column(name = "STATUS")
	private String status;

}
