package com.qnb.bo.backofficeservice.entity.audit;


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
 
import lombok.Data;
 
@Entity
@Table(name = TableConstants.TABLE_AUDIT_DETAILS)
@Data
public class AuditDetails {
 
	@Id	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auditdet_seqno")
	@SequenceGenerator(name = "auditdet_seqno", sequenceName = "OCS_AUDIT_DETAILS_SEQ",allocationSize = 1)
	@Column(name = "TXN_ID",updatable = false, nullable = false)
	private Integer id;
	
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name ="AUDIT_REFNO",referencedColumnName = "AUDIT_REFNO", nullable = false)
	private AuditMaster  auditRef;
	
	@Column(name="AUDIT_SEQ")
	private String  auditSeq;
	
	@Column(name="AUDIT_DATE")
	private Date auditDate;
	
	@Column(name="PARAM_NAME")
	private String  paramName;
 
	@Column(name="PARAM_VALUE")
	private String paramValue;
 
}
