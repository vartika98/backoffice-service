package com.qnb.bo.backofficeservice.entity.audit;


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.qnb.bo.backofficeservice.constant.TableConstants;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
 
import lombok.Data;
import lombok.NoArgsConstructor;
 
@Entity
@Data
@NoArgsConstructor
@Table(name = TableConstants.TABLE_AUDIT_MASTER)
public class AuditMaster {
 
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "audit_seq_no")
	@SequenceGenerator(name = "audit_seq_no", sequenceName = "OCS_AUDIT_SEQ_NO",allocationSize = 1)
	@Column(name = "AUDIT_REFNO")
	private Integer auditRefno;
	
	@Column(name="AUDIT_DATE", updatable=false)
	private  Date  auditDate; 
	
	@Column(name="UNIT_ID", updatable=false)
	private String unitId;  
 
    @Column(name="CHANNEL_ID", updatable=false)
    private String channel;
    
    @Column(name="GLOBAL_ID", updatable=false)
    private String gId;
    
    @Column(name="USER_NO", updatable=false)
    private String userNo;
    
    @Column(name="APP_VERSION", updatable=false)
    private String appVersion;
    
	@Column(name="DEVICE_TYPE", updatable=false)
	private String deviceType;
	
	@Column(name="HOST_IP", updatable=false)
	private String hostIp;
	
	@Column(name="SESSION_ID", updatable=false)
	private String sessionId;
 
    @Column(name="IMEI", updatable=false)
    private String imei;
    
	@Column(name="DEVICE_MODEL", updatable=false)
	private String deviceModel;
	
	@Column(name="OS_VERSION", updatable=false)
	private String osVersion;
	
	@Column(name="CATEGORY_CODE", updatable=false)
	private String catCode;
	
    @Column(name="STATUS")
    private String status;
    
	@Column(name="MWARE_ERR_CODE")
	private String mwErrCode;
	
	@Column(name="MWARE_ERR_DESC")
	private String mwErrDesc;
	
    @Column(name="LOCALE", updatable=false)
    private String lang;
    
	@Column(name="START_TIME", updatable=false)
	private Date startTime;
	
	@Column(name="END_TIME")
	private Date endTime;
 
	@Column(name="OCS_ERR_CODE")
	private String ocsErrCode;
	
	@Column(name="OCS_ERR_DESC")
	private String ocsErrDesc;
	
	@Column(name="CLIENT_TIME", updatable=false)
	private Date clientDate;
	
	@Column(name="USER_AGENT", updatable=false)
	private String userAent;
	
	@Column(name="GEO_LOCATION", updatable=false)
	private String geoLocation;
	
	@Column(name="AUDIT_TYPE", updatable=false)
	private String auditType;
	
	@Column(name="USER_ID")
	private String userId;
	
	@Column(name="TXN_REFERENCE_NO")
	private String txnRefNo;
}
