package com.qnb.bo.backofficeservice.entity.workflow;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

import lombok.Data;

@MappedSuperclass
@Data
public class WorkflowAuditEntity implements Serializable {

	private static final long serialVersionUID = 4385359473070143982L;

	@Column(name = "created_by", nullable = false)
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_time", nullable = false)
	private Date createdTime;

	@Column(name = "last_modified_by", nullable = false)
	private String lastModifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_modified_time", nullable = false)
	private Date lastModifiedTime;

}
