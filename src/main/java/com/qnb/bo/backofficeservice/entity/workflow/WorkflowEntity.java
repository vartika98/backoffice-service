package com.qnb.bo.backofficeservice.entity.workflow;

import java.time.Instant;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import com.qnb.bo.backofficeservice.enums.WorkflowStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "BKO_T_WORKFLOW")
public class WorkflowEntity extends WorkflowAuditEntity{
	
	private static final long serialVersionUID = -5244610030322809007L;
	 
    @Id
    @Column(name = "workflow_id")
    private String workflowId;

    @OneToMany(mappedBy = "workflow", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<WorkflowRequestIdentifierEntity> requestIdentifiers;

    @Column(name = "request_id")
    private String requestId;

    @Column(name = "process_id")
    private String processId;

    @Column(name = "task_id")
    private String taskId;

    @Column(name = "assigned_group")
    private String assignedGroup;

    @Column(name = "initiated_group")
    private String initiatedGroup;

    @Column(name = "workflow_group")
    private String workflowGroup;

    @Column(name = "available_actions")
    private String availableActions;

    @Column(name = "last_performed_action")
    private String lastPerformedAction;

    @Column(name = "workflow_status")
    @Enumerated(EnumType.STRING)
    private WorkflowStatus workflowStatus;

    @Column(name = "scheduled_time")
    private Instant scheduledTime;

    @Column(name = "history_count")
    private Integer historyCount;
  
    @Column(name = "created_by")
    private String createdBy;


}
