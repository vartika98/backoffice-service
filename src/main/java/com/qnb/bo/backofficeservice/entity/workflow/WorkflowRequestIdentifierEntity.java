package com.qnb.bo.backofficeservice.entity.workflow;

import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "BKO_T_WORKFLOW_IDENTIFIERS")
public class WorkflowRequestIdentifierEntity extends WorkflowAuditEntity{

	private static final long serialVersionUID = -5968236726066591679L;

	@Id
	@Column(name = "id")
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "workflow_id")
	private WorkflowEntity workflow;

	@Column(name = "request_id")
	private String requestId;

	@Column(name = "request_hash")
	private String requestHash;

	public static WorkflowRequestIdentifierEntity toEntity(WorkflowEntity workflow, String hashValue) {
		WorkflowRequestIdentifierEntity entity = new WorkflowRequestIdentifierEntity();
		entity.id = UUID.randomUUID().toString();
		entity.requestId = workflow.getRequestId();
		entity.workflow = workflow;
		entity.setCreatedBy(workflow.getCreatedBy());
		entity.setLastModifiedBy(workflow.getLastModifiedBy());
		entity.requestHash = hashValue;
		return entity;
	}
}
