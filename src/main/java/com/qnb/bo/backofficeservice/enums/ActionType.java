package com.qnb.bo.backofficeservice.enums;

public enum ActionType {

	ADD,MODIFY,DELETE,ENABLE,DISABLE,TERMINATE,NA, NO_ACTION, CXL_ADD
}
