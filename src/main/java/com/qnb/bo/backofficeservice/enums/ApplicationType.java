package com.qnb.bo.backofficeservice.enums;

public enum ApplicationType {
	A, 		//Type is Application
	M;		//Type is Module	
	
	public static ApplicationType getApplicationType(String type) {
		if ("A".equalsIgnoreCase(type)) {
			return A;
		} else if ("M".equalsIgnoreCase(type)) {
			return M;
		}
		return null;
	}
}
