package com.qnb.bo.backofficeservice.enums;

import lombok.Getter;

@Getter
public enum ExceptionMessages {

	WORKFLOW_EXCEPTION("1000", "Error in workflow execution"),
	WORKFLOW_REQUEST_NOT_FOUND_EXCEPTION("1001", "User Request not found"),

	RUNTIME_EXCEPTION("2001", "Unknown Exception occured. Please refer the log for more details."),
	DB_EXCEPTION("2002", "Data integrity violation exception."),
	PRIMARY_KEY_VIOLATION_EXCEPTION("2003", "Primary key violation exception."),
	FOREIGN_KEY_VIOLATION_EXCEPTION("2004", "Foreign key violation exception."),
	OTHER_VIOLATION_EXCEPTION("2005", "Data integrity violation exception."),
	UNIT_NOT_FOUND_EXCEPTION("2006", "Unit not found exception"),
	ROLE_NOT_FOUND_EXCEPTION("2007", "Role not found exception"),
	DATE_NOT_VALID_EXCEPTION("2008", "Date cannot be previous date"),

	USER_NOT_FOUND_EXCEPTION("3100", "User not found exception"),
	USER_ALREADY_EXISTS_EXCEPTION("3101", "User already exists exception"),
	USER_STATUS_EXCEPTION("3102", "User status should be any of : ENABLED/DISABLED/TERMINATED"),
	USER_ID_IS_BEEN_CHANGED_IN_RESUBMIT_REQUEST_EXCEPTION("3103", "User id has been changed in resubmit request"),

	GROUP_NOT_FOUND_EXCEPTION("3200", "Role not found exception"),
	GROUP_ALREADY_EXISTS_EXCEPTION("3201", "Role already exists exception"),
	GROUP_ID_DOES_NOT_EXISTS_TO_DELETE_EXCEPTION("3202", "Role id not provided for deletion"),
	GROUP_MEMBERS_EXIST_EXCEPTION("3203", "Users are present in the Role. Role deletion failed!"),
	GROUP_ID_IS_BEEN_CHANGED_IN_RESUBMIT_REQUEST_EXCEPTION("3204", "Role id has been changed in resubmit request"),
	GROUP_CANNOT_BE_MODIFIED_OR_DELETED_EXCEPTION("3205", "System generated roles cannot be modified or deleted"),
	WORKFLOW_USER_REQUEST_NOT_FOUND_EXCEPTION("1001", "User Request not found"),
	BENEFICIARY_INVALID("4100", "Invalid beneficiary"), INVALID_ACTION_REQUEST("4101", "Invalid Action Type"),

	CHANNEL_NOT_FOUND_EXCEPTION("4200", "Channel not found exception"),
	CHANNEL_ALREADY_EXISTS_EXCEPTION("4201", "Channel already exists exception"),

	APPLICATION_REQUEST_DUPLICATE_UNITS("4300", "Duplicate units in request"),
	APPLICATION_REQUEST_DUPLICATE_CHANNELS("4301", "Duplicate channels in request"),
	APPLICATION_ALREADY_EXISTS_EXCEPTION("4302", "Application already exists exception"),
	APPLICATION_NOT_FOUND_EXCEPTION("4303", "Application not found exception"),
	TXN_ID_HAS_BEEN_CHANGED_IN_RESUBMIT_REQUEST_EXCEPTION("4304", "txnId has been changed in resubmit request"),
	INVALID_STATUS("4305", "Status should be ACT or IAC"),

	PARAMETER_REQUEST_EXISTS_EXCEPTION("4400", "Parameter request already exists"),
	PARAMETER_NOT_FOUND_EXCEPTION("4401", "Parameter not found exception"),

	LABEL_ALREADY_EXISTS_EXCEPTION("4500", "Label already exists"),
	LANGUAGE_EXCEPTION("4501", "Lanuage description not provided for label"),

	TXN_ENTITLEMENT_REQUEST_DUPLICATE("4600", "Duplicate entitlements in request"),
	ENTITLEMENT_REQUEST_EXISTS_EXCEPTION("4601", "Duplicate request"),
	ENTITLEMENT_NOT_FOUND_EXCEPTION("4602", "Entitlement not found exception"),

	INVALID_MAINTENANCE("4700", "Invalid maintenance message type"),
	MAINTENANCE_NOT_FOUND_EXCEPTION("4701", "Maintenance not found exception"),
	REQUEST_ALREADY_EXIST("4702", "Request already exist");

	private final String errorCode;
	private final String errorDescription;

	private ExceptionMessages(String errorCode, String errorDescription) {
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
	}
}
