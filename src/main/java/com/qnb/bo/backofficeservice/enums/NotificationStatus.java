package com.qnb.bo.backofficeservice.enums;

public enum NotificationStatus {
	INITIATED, SUCCESS, FAILED, DELIVERED;
}
