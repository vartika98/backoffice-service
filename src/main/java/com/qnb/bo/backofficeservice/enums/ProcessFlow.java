package com.qnb.bo.backofficeservice.enums;

import java.util.ArrayList;
import java.util.List;

public enum ProcessFlow {

	ADD_PARAM_PROCESS_FLOW, DELETE_CHANNEL_PROCESS_FLOW, ADD_USER_PROCESS_FLOW, DELETE_GROUP_PROCESS_FLOW,
	CUSTOMER_REISSUE_PWD_PROCESS_FLOW, MODIFY_GROUP_PROCESS_FLOW, MODIFY_PLAN_PROCESS_FLOW, ADD_BENF_PROCESS_FLOW,
	DELETE_MAINTENANCE_PROCESS_FLOW, OOREDOO_PAYMENT_PROCESS_FLOW, ERR_CONF_PROCESS_FLOW, LH_PAYMENT_PROCESS_FLOW,
	B2B_PROCESS_FLOW, SMARTINSTALLMENT_PROCESS_FLOW, CC_PAYMENT_PROCESS_FLOW, ADD_APPLICATION_PROCESS_FLOW,
	ADD_MAINTENANCE_PROCESS_FLOW, WU_PROCESS_FLOW, DELETE_APPLICATION_PROCESS_FLOW, MODIFY_USER_STATUS_PROCESS_FLOW,
	HBTF_PROCESS_FLOW, DELETE_USER_PROCESS_FLOW, ADD_GROUP_PROCESS_FLOW, MODIFY_APPLICATION_PROCESS_FLOW,
	DELETE_PLAN_PROCESS_FLOW, ADD_PLAN_PROCESS_FLOW, RTP_PROCESS_FLOW, MODIFY_USER_PROCESS_FLOW, ADD_TXN_PROCESS_FLOW,
	MODIFY_MAINTENANCE_PROCESS_FLOW, ADD_LABEL_PROCESS_FLOW, ADD_CHANNEL_PROCESS_FLOW, BRANCH_LIST_PROCESS_FLOW,
	CUSTOMER_REG_PROCESS_FLOW, IPO_ADMIN_PROCESS_FLOW, MODIFY_CHANNEL_PROCESS_FLOW, USER_UPDATE_STATUS_PROCESS_FLOW,
	ROLE_MANAGEMENT_MODIFY_MAKER, LABEL_MANAGEMENT_CHECKER, USER_MANAGEMENT_DELETE_MAKER, PLAN_MANAGEMENT_MODIFY_MAKER,
	PARAM_MANAGEMENT_MODIFY_MAKER, BENF_MANAGEMENT_MODIFY_MAKER, USER_MANAGEMENT_ADD_CHECKER, ROLE_MANAGEMENT_CHECKER,
	DELETE_USERNAME_REQUEST_CHECKER, ROLE_MANAGEMENT_ADD_MAKER, CHANNEL_MANAGEMENT_ADD_MAKER,
	LABEL_MANAGEMENT_MODIFY_MAKER, TXN_MANAGEMENT_CHECKER, CHANNEL_MANAGEMENT_MODIFY_MAKER,
	CUST_MANAGEMENT_REISSUE_PWD_MAKER, TXN_MANAGEMENT_MODIFY_MAKER, ROLE_MANAGEMENT_DELETE_MAKER,
	USER_MANAGEMENT_CHECKER, PARAM_MANAGEMENT_DELETE_MAKER, PLAN_MANAGEMENT_ADD_MAKER, USER_MANAGEMENT_ADD_MAKER,
	TXN_MANAGEMENT_ADD_MAKER, PARAM_MANAGEMENT_CHECKER, BENF_MANAGEMENT_ADD_MAKER, APPLICATION_MANAGEMENT_DELETE_MAKER,
	TXN_MANAGEMENT_DELETE_MAKER, APPLICATION_MANAGEMENT_CHECKER, PARAM_MANAGEMENT_ADD_MAKER,
	PLAN_MANAGEMENT_DELETE_MAKER, APPLICATION_MANAGEMENT_ADD_MAKER, LABEL_MANAGEMENT_DELETE_MAKER,
	LABEL_MANAGEMENT_ADD_MAKER, PLAN_MANAGEMENT_CHECKER, CUST_MANAGEMENT_CHECKER, BENF_MANAGEMENT_CHECKER,
	CHANNEL_MANAGEMENT_DELETE_MAKER, USER_MANAGEMENT_MODIFY_MAKER, DELETE_USERNAME_REQUEST_MAKER,
	CHANNEL_MANAGEMENT_CHECKER, APPLICATION_MANAGEMENT_MODIFY_MAKER, CUST_MANAGEMENT_REG_MAKER,
	BENF_MANAGEMENT_DELETE_MAKER;

	public static List<String> getUserProcessId() {
		List<String> userProcess = new ArrayList<>();
		userProcess.add(ADD_USER_PROCESS_FLOW.toString());
		userProcess.add(MODIFY_USER_STATUS_PROCESS_FLOW.toString());
		userProcess.add(MODIFY_USER_PROCESS_FLOW.toString());
		userProcess.add(DELETE_USER_PROCESS_FLOW.toString());
		userProcess.add(USER_UPDATE_STATUS_PROCESS_FLOW.toString());
		return userProcess;
	}

	public static List<String> getRoleProcessId() {
		List<String> userProcess = new ArrayList<>();
		userProcess.add(DELETE_GROUP_PROCESS_FLOW.toString());
		userProcess.add(MODIFY_GROUP_PROCESS_FLOW.toString());
		userProcess.add(ADD_GROUP_PROCESS_FLOW.toString());
		return userProcess;
	}

	public static List<String> getB2BProcessId() {
		List<String> b2BProcess = new ArrayList<>();
		b2BProcess.add(B2B_PROCESS_FLOW.toString());
		return b2BProcess;

	}

	public static List<String> getDeleteProcessId() {
		List<String> deleteProcess = new ArrayList<>();
		deleteProcess.add(DELETE_USER_PROCESS_FLOW.toString());
		return deleteProcess;

	}

	public static List<String> getOodredooProcessId() {
		List<String> ooredooProcess = new ArrayList<>();
		ooredooProcess.add(OOREDOO_PAYMENT_PROCESS_FLOW.toString());
		return ooredooProcess;

	}

	public static List<String> getParamProcessId() {
		List<String> paramProcess = new ArrayList<>();
		paramProcess.add(ADD_PARAM_PROCESS_FLOW.toString());
		return paramProcess;

	}

	public static List<String> getCreditCardProcessId() {
		List<String> creditCrdProcess = new ArrayList<>();
		creditCrdProcess.add(CC_PAYMENT_PROCESS_FLOW.toString());
		return creditCrdProcess;

	}

	public static List<String> getPlannedProcessId() {
		List<String> plannedProcess = new ArrayList<>();
		plannedProcess.add(MODIFY_PLAN_PROCESS_FLOW.toString());
		return plannedProcess;

	}

	public static List<String> getHBTFProcessId() {
		List<String> HBTFProcess = new ArrayList<>();
		HBTFProcess.add(HBTF_PROCESS_FLOW.toString());
		return HBTFProcess;

	}

	public static List<String> getTxnProcessId() {
		List<String> txnProcess = new ArrayList<>();
		txnProcess.add(MODIFY_PLAN_PROCESS_FLOW.toString());
		return txnProcess;

	}

	public static List<String> getCustomerProcessId() {
		List<String> plannedProcess = new ArrayList<>();
		plannedProcess.add(CUSTOMER_REISSUE_PWD_PROCESS_FLOW.toString());
		return plannedProcess;

	}

	public static List<String> applicationProcessId() {
		List<String> applicationProcess = new ArrayList<>();
		applicationProcess.add(ADD_APPLICATION_PROCESS_FLOW.toString());
		return applicationProcess;

	}

	public static List<String> getChannelProcessId() {
		List<String> channelProcess = new ArrayList<>();
		channelProcess.add(DELETE_CHANNEL_PROCESS_FLOW.toString());
		channelProcess.add(ADD_CHANNEL_PROCESS_FLOW.toString());
		channelProcess.add(MODIFY_CHANNEL_PROCESS_FLOW.toString());
		return channelProcess;
	}

	public static List<String> getLabelProcessId() {
		List<String> lableProcess = new ArrayList<>();
		lableProcess.add(ADD_LABEL_PROCESS_FLOW.toString());
		return lableProcess;
	}

	public static List<String> getIPOProcessId() {
		List<String> ipoProcess = new ArrayList<>();
		ipoProcess.add(IPO_ADMIN_PROCESS_FLOW.toString());
		return ipoProcess;
	}

	public static List<String> getSmartInstProcessId() {
		List<String> smartInstProcess = new ArrayList<>();
		smartInstProcess.add(SMARTINSTALLMENT_PROCESS_FLOW.toString());
		return smartInstProcess;
	}

	public static List<String> getERRProcessId() {
		List<String> errorProcess = new ArrayList<>();
		errorProcess.add(ERR_CONF_PROCESS_FLOW.toString());
		return errorProcess;

	}

	public static List<String> getBranchProcessId() {
		List<String> branchProcess = new ArrayList<>();
		branchProcess.add(BRANCH_LIST_PROCESS_FLOW.toString());
		return branchProcess;
	}

	public static List<String> getLoyaltyHarrods() {
		List<String> loyaltyHarrods = new ArrayList<>();
		loyaltyHarrods.add(LH_PAYMENT_PROCESS_FLOW.toString());
		return loyaltyHarrods;

	}

	public static List<String> getRoleAssigedGroup() {
		List<String> roleAssigned = new ArrayList<>();
		roleAssigned.add(ROLE_MANAGEMENT_CHECKER.toString());
		roleAssigned.add(ROLE_MANAGEMENT_MODIFY_MAKER.toString());
		roleAssigned.add(ROLE_MANAGEMENT_ADD_MAKER.toString());
		roleAssigned.add(ROLE_MANAGEMENT_DELETE_MAKER.toString());
		return roleAssigned;

	}

	public static List<String> getUserAssigedGroup() {
		List<String> userAssiged = new ArrayList<>();
		userAssiged.add(USER_MANAGEMENT_ADD_CHECKER.toString());
		userAssiged.add(USER_MANAGEMENT_CHECKER.toString());
		return userAssiged;

	}

	public static List<String> getLableAssigedGroup() {
		List<String> labelAssigned = new ArrayList<>();
		labelAssigned.add(LABEL_MANAGEMENT_CHECKER.toString());
		labelAssigned.add(LABEL_MANAGEMENT_MODIFY_MAKER.toString());
		labelAssigned.add(LABEL_MANAGEMENT_DELETE_MAKER.toString());

		return labelAssigned;

	}

	public static List<String> getPlanAssigedGroup() {
		List<String> plannedAssiged = new ArrayList<>();
		plannedAssiged.add(PLAN_MANAGEMENT_MODIFY_MAKER.toString());
		plannedAssiged.add(PLAN_MANAGEMENT_ADD_MAKER.toString());
		plannedAssiged.add(PLAN_MANAGEMENT_DELETE_MAKER.toString());
		plannedAssiged.add(PLAN_MANAGEMENT_CHECKER.toString());
		return plannedAssiged;

	}

	public static List<String> getParamManagementAssigedGroup() {
		List<String> paramManagementAssiged = new ArrayList<>();
		paramManagementAssiged.add(PARAM_MANAGEMENT_MODIFY_MAKER.toString());
		paramManagementAssiged.add(PARAM_MANAGEMENT_DELETE_MAKER.toString());
		paramManagementAssiged.add(PARAM_MANAGEMENT_CHECKER.toString());
		paramManagementAssiged.add(PARAM_MANAGEMENT_ADD_MAKER.toString());
		return paramManagementAssiged;

	}

	public static List<String> getBenfiAssigedGroup() {
		List<String> beneficiaryAssiged = new ArrayList<>();
		beneficiaryAssiged.add(BENF_MANAGEMENT_ADD_MAKER.toString());
		beneficiaryAssiged.add(BENF_MANAGEMENT_CHECKER.toString());
		beneficiaryAssiged.add(BENF_MANAGEMENT_DELETE_MAKER.toString());
		beneficiaryAssiged.add(BENF_MANAGEMENT_MODIFY_MAKER.toString());
		return beneficiaryAssiged;

	}

	public static List<String> getDeleteAssigedGroup() {
		List<String> deleteAssiged = new ArrayList<>();
		deleteAssiged.add(DELETE_USERNAME_REQUEST_MAKER.toString());
		deleteAssiged.add(DELETE_USERNAME_REQUEST_CHECKER.toString());
		return deleteAssiged;

	}

	public static List<String> getTxnManagement() {
		List<String> txnManagementAssiged = new ArrayList<>();
		txnManagementAssiged.add(TXN_MANAGEMENT_CHECKER.toString());
		txnManagementAssiged.add(TXN_MANAGEMENT_MODIFY_MAKER.toString());
		txnManagementAssiged.add(TXN_MANAGEMENT_ADD_MAKER.toString());
		txnManagementAssiged.add(TXN_MANAGEMENT_DELETE_MAKER.toString());
		return txnManagementAssiged;

	}

	public static List<String> getBranchAssigedGroup() {
		return null;
	}

	public static List<String> getLoyaltyHarrodsAssigedGroup() {
		List<String> loyaltyHarrods = new ArrayList<>();
		loyaltyHarrods.add(LH_PAYMENT_PROCESS_FLOW.toString());
		return loyaltyHarrods;
	}

	public static List<String> getERRAssigedGroup() {
		return null;
	}

	public static List<String> getSmartInstAssigedGroup() {
		return null;
	}

	public static List<String> getIPOAssigedGroup() {
		return null;
	}

	public static List<String> getChannelAssigedGroup() {
		return null;
	}

}
