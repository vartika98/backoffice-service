package com.qnb.bo.backofficeservice.enums;

public enum RemarkType {
	SYSTEM, MAKER, CHECKER
}
