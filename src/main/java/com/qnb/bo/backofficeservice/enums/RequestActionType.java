package com.qnb.bo.backofficeservice.enums;

public enum RequestActionType {
	ADD,
    MODIFY,
    DELETE,
    ENABLE,
    DISABLE,
    TERMINATE,
    NA
}
