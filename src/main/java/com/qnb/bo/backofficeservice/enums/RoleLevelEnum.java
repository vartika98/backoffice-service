package com.qnb.bo.backofficeservice.enums;

public enum RoleLevelEnum {
    A(1),
    B(2),
    C(3),
    D(4),
    E(5);

    private final int roleLevel;

    RoleLevelEnum(int roleLevel) {
        this.roleLevel = roleLevel;
    }

    public int getRoleLevel() {
        return roleLevel;
    }

    public static RoleLevelEnum fromRoleLevel(int roleLevel) {
        for (RoleLevelEnum level : RoleLevelEnum.values()) {
            if (level.getRoleLevel() == roleLevel) {
                return level;
            }
        }
        throw new IllegalArgumentException("Invalid Role Level: " + roleLevel);
    }
}
