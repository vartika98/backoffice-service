package com.qnb.bo.backofficeservice.enums;

public enum TxnStatus {
	ACT,
	IAC,
	TER,
	ICT,
	DEL;
 
	public static TxnStatus parseTxnStatus(String status) {
		for(var stat : TxnStatus.values()) {
			if(stat.name().equalsIgnoreCase(status)) {
				return stat;
			}
		}
		return null;
	}
}
