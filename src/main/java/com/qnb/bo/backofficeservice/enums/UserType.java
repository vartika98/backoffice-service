package com.qnb.bo.backofficeservice.enums;

public enum UserType {
    ADMIN,
    SYSTEM,
    STATIC,
    FUNCTIONAL,
    LDAP
}
