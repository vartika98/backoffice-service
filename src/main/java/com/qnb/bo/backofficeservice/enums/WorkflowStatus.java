package com.qnb.bo.backofficeservice.enums;

public enum WorkflowStatus {
	STARTED,IN_PROGRESS,RETURNED,PENDING_FOR_APPROVAL

}
