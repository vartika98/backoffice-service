package com.qnb.bo.backofficeservice.exceptions;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public abstract class BaseRuntimeException extends RuntimeException {
	private static final long serialVersionUID = -5131703422025088561L;
	private final String errorCode;
	private final String errorMessage;
	private final HttpStatus status;

	public BaseRuntimeException(String errorCode, String errorMessage, HttpStatus status, Throwable cause) {
		super(errorMessage, cause);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.status = status;
	}

	public BaseRuntimeException(String errorCode, String errorMessage, HttpStatus status) {
		super(errorMessage);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.status = status;
	}

}
