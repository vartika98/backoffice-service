package com.qnb.bo.backofficeservice.exceptions;

import org.springframework.http.HttpStatus;

public class ResourceNotFoundException extends BaseRuntimeException {
	private static final long serialVersionUID = -8313291215652013647L;

	public ResourceNotFoundException(String errorCode, String errorMessage, HttpStatus status, Throwable cause) {
		super(errorCode, errorMessage, status, cause);
	}

	public ResourceNotFoundException(String errorCode, String errorMessage, HttpStatus status) {
		super(errorCode, errorMessage, status);
	}

	public ResourceNotFoundException(String errorCode, String errorMessage) {
		super(errorCode, errorMessage, HttpStatus.NOT_FOUND);
	}
}
