package com.qnb.bo.backofficeservice.jwtcontroller;

import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.bo.backofficeservice.jwtproviders.JWTAuthenticationTokenProvider;
import com.qnb.bo.backofficeservice.jwtproviders.LdapAuthenticationProvider;
import com.qnb.bo.backofficeservice.jwtresponse.AuthenticationResponse;

@RestController
@RequestMapping("/auth-server")
public class AuthenticationController {
	@Autowired
	private LdapAuthenticationProvider authenticationProvider;

	@Autowired
	private JWTAuthenticationTokenProvider jwtAuthTokenProvider;

	@PostMapping("/login")
	public AuthenticationResponse authenticateRequest(@RequestBody final Map<String, String> requestMap)
			throws Exception {

		final var authRequest = new UsernamePasswordAuthenticationToken(requestMap.get("un"), requestMap.get("ps"));

		// Authenticate the user
		final var authObj = authenticationProvider.authenticate(authRequest);
		final var securityContext = SecurityContextHolder.getContext();
		securityContext.setAuthentication(authObj);
		System.out.println(authObj);

		final var res = new AuthenticationResponse();
		if (Objects.nonNull(authObj)) {
			final var token = jwtAuthTokenProvider.generateToken(authObj.getName(), false);
			System.out.println(token);
			res.setToken(token);
			res.setAuthStatus("LOGIN SUCCESS");
		} else {
			res.setToken("");
			res.setAuthStatus("INCORRECT LOGIN");
		}
		return res;

	}

}