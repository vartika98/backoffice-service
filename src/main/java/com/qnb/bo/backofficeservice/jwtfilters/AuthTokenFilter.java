package com.qnb.bo.backofficeservice.jwtfilters;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.qnb.bo.backofficeservice.jwtimpl.UserDetailsServiceImpl;
import com.qnb.bo.backofficeservice.jwtproviders.JWTAuthenticationTokenProvider;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class AuthTokenFilter extends OncePerRequestFilter {
	private static final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);

	@Autowired
	private JWTAuthenticationTokenProvider jwtUtils;

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	public AuthTokenFilter() {
	}

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException {
		try {
			final var jwt = parseJwt(request);

			if (jwt != null && jwtUtils.validateToken(jwt)) {
				final var username = jwtUtils.getAllClaimsFromToken(jwt).getSubject();

				final var userDetails = userDetailsService.loadUserByUsername(username);
				System.out.println(userDetails);
				final var authentication = new UsernamePasswordAuthenticationToken(userDetails, null,
						userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		} catch (final Exception e) {
			AuthTokenFilter.logger.error("Cannot set user authentication: {}", e);
		}

		filterChain.doFilter(request, response);
	}

	private String parseJwt(final HttpServletRequest request) {
		final var headerAuth = request.getHeader("Authorization");

		if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
			return headerAuth.substring(7);
		}

		return null;
	}
}
