package com.qnb.bo.backofficeservice.jwtproviders;

import java.util.ArrayList;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.Filter;
import org.springframework.ldap.support.LdapUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.enums.UserStatus;
import com.qnb.bo.backofficeservice.enums.UserType;
import com.qnb.bo.backofficeservice.repository.UserRepository;

@Component
public class LdapAuthenticationProvider implements AuthenticationProvider {
	public static void main(final String ss[]) {

		final var rawPassword = "sysadmin123";
		final var passEncoder = new BCryptPasswordEncoder();
		final var hashPassword = passEncoder.encode(rawPassword);
		System.out.println("HashPassword :" + hashPassword);
		System.out.println("Matched :" + passEncoder.matches(rawPassword, hashPassword));
	}

	private LdapContextSource contextSource;

	private LdapTemplate ldapTemplate;

	@Autowired
	UserRepository repo;

	public LdapAuthenticationProvider(final Environment environment) {
	}

	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
		final var username = authentication.getName();
		if (Objects.nonNull(username)) {
			final var user = repo.findByUserIdAndStatus(username, AppConstant.ACT);

			System.out.println("User detail::: " + user);
			if (Objects.nonNull(user)) {
				final UserType userType = user.getUserType();

				if (userType.name().equals(UserType.SYSTEM.name()) || userType.name().equals(UserType.STATIC.name())) {
					final var passwordEncoder = new BCryptPasswordEncoder();
					final var password = authentication.getCredentials().toString();
					final var validPassword = passwordEncoder.matches(password, user.getPassword());
					System.out.println("validPassword>>" + validPassword);
					if (validPassword) {
						final UserDetails userDetails = new User(authentication.getName(),
								authentication.getCredentials().toString(), new ArrayList<>());
						System.out.println("userDetails>>" + userDetails);

						return new UsernamePasswordAuthenticationToken(userDetails,
								authentication.getCredentials().toString(), new ArrayList<>());
					}
				} else if (user != null && (userType.name().equals(UserType.FUNCTIONAL.name())
						|| (userType.name().equals(UserType.LDAP.name())))) {
					initContext();
					final Filter filter = new EqualsFilter("uid", authentication.getName());
					final Boolean authenticate = ldapTemplate.authenticate(LdapUtils.emptyLdapName(), filter.encode(),
							authentication.getCredentials().toString());
					System.out.println(authenticate);
					if (authenticate) {
						final UserDetails userDetails = new User(authentication.getName(),
								authentication.getCredentials().toString(), new ArrayList<>());
						return new UsernamePasswordAuthenticationToken(userDetails,
								authentication.getCredentials().toString(), new ArrayList<>());
					}
				}
			}
		}

		return null;
	}

	private void initContext() {
		contextSource = new LdapContextSource();
		contextSource.setUrl("ldap://localhost:10389");
		contextSource.setBase("dc=planetexpress,dc=com");
		contextSource.setUserDn("cn=admin, dc=planetexpress,dc=com");
		contextSource.setPassword("GoodNewsEveryone");

		// contextSource.setBase("ou=groups");
		contextSource.afterPropertiesSet();

		ldapTemplate = new LdapTemplate(contextSource);
	}

	@Override
	public boolean supports(final Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
