package com.qnb.bo.backofficeservice.jwtresponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationResponse {
	private String token;

	private String authStatus;

	public String getAuthStatus() {
		return authStatus;
	}

	public String getToken() {
		return token;
	}

	public void setAuthStatus(final String authStatus) {
		this.authStatus = authStatus;
	}

	public void setToken(final String token) {
		this.token = token;
	}

}
