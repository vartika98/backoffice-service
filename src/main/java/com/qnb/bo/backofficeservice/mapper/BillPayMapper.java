package com.qnb.bo.backofficeservice.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.bo.backofficeservice.dto.billPay.PayeeDto;
import com.qnb.bo.backofficeservice.dto.billPay.UtilityTypesDto;
import com.qnb.bo.backofficeservice.entity.PayeeEntity;
import com.qnb.bo.backofficeservice.entity.UtilityTypesEntity;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface BillPayMapper {

	List<PayeeDto> toDto(List<PayeeEntity> payeeList);

	List<UtilityTypesDto> toUtilityTypesDto(List<UtilityTypesEntity> utilityTypesList);

}
