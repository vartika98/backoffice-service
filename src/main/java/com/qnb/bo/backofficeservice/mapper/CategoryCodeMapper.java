package com.qnb.bo.backofficeservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.bo.backofficeservice.dto.categoryCode.CategoryCodeDto;
import com.qnb.bo.backofficeservice.entity.CategoryMaster;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface CategoryCodeMapper {

	CategoryCodeDto toDto(CategoryMaster entity);
}
