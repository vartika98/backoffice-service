package com.qnb.bo.backofficeservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.bo.backofficeservice.dto.channel.ChannelDto;
import com.qnb.bo.backofficeservice.entity.Channel;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ChannelMapper {

	ChannelDto channelModeltoDto(Channel channel);

	Channel channelDtotoModel(ChannelDto channelDto);
	

}
