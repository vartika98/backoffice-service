package com.qnb.bo.backofficeservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.bo.backofficeservice.dto.currency.CurrencyDto;
import com.qnb.bo.backofficeservice.entity.CurrencyEntity;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface CurrencyMapper {

	CurrencyDto currencyModeltoDto(CurrencyEntity channel);

	

}