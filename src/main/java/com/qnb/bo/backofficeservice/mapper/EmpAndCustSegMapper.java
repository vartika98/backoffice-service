package com.qnb.bo.backofficeservice.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

import com.qnb.bo.backofficeservice.dto.EmpIdAndCustSegDTO;
import com.qnb.bo.backofficeservice.entity.CfmsParamEntity;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface EmpAndCustSegMapper {
	
	EmpAndCustSegMapper INSTANCE = Mappers.getMapper(EmpAndCustSegMapper.class);

	CfmsParamEntity toEntity(EmpIdAndCustSegDTO empAndCustSegRequest);

	EmpIdAndCustSegDTO toDto(CfmsParamEntity cfmsParam);

	List<EmpIdAndCustSegDTO> toDto(List<CfmsParamEntity> findById_CustOptionAndId_ServiceCodeAndId_UnitIdIn);
	 

}
