package com.qnb.bo.backofficeservice.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.bo.backofficeservice.dto.boMenu.BoMenuDto;
import com.qnb.bo.backofficeservice.dto.txn.MenuListResponse;
import com.qnb.bo.backofficeservice.entity.Menu;
import com.qnb.bo.backofficeservice.entity.OcsBoMenuEntitlement;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface MenuMapper {

	MenuListResponse menuToDto(Menu menu);

	// bo menu entitlement mapper
	List<BoMenuDto> boMenutoDto(List<OcsBoMenuEntitlement> boMenuList);
}
