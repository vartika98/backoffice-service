package com.qnb.bo.backofficeservice.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.bo.backofficeservice.dto.NotificationParamConfigDTO;
import com.qnb.bo.backofficeservice.entity.NotificationParamConfigEntity;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface NotificationParamConfigMapper {

	NotificationParamConfigEntity toEntity(NotificationParamConfigDTO dto);
	
	List<NotificationParamConfigEntity> toEntity(List<NotificationParamConfigDTO> dto);

	NotificationParamConfigDTO toDto(NotificationParamConfigEntity entity);

	List<NotificationParamConfigDTO> toDto(List<NotificationParamConfigEntity> entity);
}
