package com.qnb.bo.backofficeservice.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.bo.backofficeservice.dto.parameter.ParameterRes;
import com.qnb.bo.backofficeservice.entity.ParamConfig;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ParamConfigMapper {

	List<ParameterRes> paramConfigToDto(List<ParamConfig> paramConfig);
}
