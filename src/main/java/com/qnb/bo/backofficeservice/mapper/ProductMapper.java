package com.qnb.bo.backofficeservice.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

import com.qnb.bo.backofficeservice.dto.ProductDTO;
import com.qnb.bo.backofficeservice.entity.Product;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface ProductMapper {
	
	ProductMapper Instance = Mappers.getMapper(ProductMapper.class);

	Product toEntity(ProductDTO addproductRequest);

	ProductDTO toDto(Product product);

	List<ProductDTO> toDto(List<Product> productList);

}
