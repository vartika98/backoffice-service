package com.qnb.bo.backofficeservice.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

import com.qnb.bo.backofficeservice.dto.SurveyRequestDTO;
import com.qnb.bo.backofficeservice.entity.SurveyMessage;
 
@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface SurveyMessageMapper {
	
	SurveyMessageMapper INSTANCE = Mappers.getMapper(SurveyMessageMapper.class);

	SurveyMessage toEntity(SurveyRequestDTO surveyRequest);

	SurveyRequestDTO toDto(SurveyMessage updatesurveyMessage);

	List<SurveyRequestDTO> toDto(List<SurveyMessage> findByUnitIdAndSegmentTypeAndStatus);


}
