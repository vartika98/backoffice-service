package com.qnb.bo.backofficeservice.model;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AppExceptionHandlerUtilDto implements Serializable {

	private static final long serialVersionUID = 8062153858017811179L;

	private String gId;

	private String unit;

	private String channel;

	private String mobileNumber;

	private String emailId;

	private String lang;

	private String serviceId;

	private Date startTime;

	private Date endTime;
	private String clientInfo;
	private long txnCategory;

	public AppExceptionHandlerUtilDto(String unit, String channel, String lang, String serviceId,String clientInfo) {

		super();

		this.unit = unit;

		this.channel = channel;

		this.lang = lang;

		this.serviceId = serviceId;
		this.clientInfo= clientInfo;

	}

	public AppExceptionHandlerUtilDto(String gId, String unit, String channel, String mobileNumber, String emailId,

			String lang, String serviceId) {

		super();

		this.gId = gId;

		this.unit = unit;

		this.channel = channel;

		this.mobileNumber = mobileNumber;

		this.emailId = emailId;

		this.lang = lang;

		this.serviceId = serviceId;

	}

}
