package com.qnb.bo.backofficeservice.model;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.PostLoad;
import lombok.Data;

@Embeddable
@Data
public class SpringSessionAttributeId implements Serializable {

	private static final long serialVersionUID = -2253763738748506192L;

	@Column(name = "SESSION_PRIMARY_ID", nullable = false)
	private String sessionPrimaryId;

	@Column(name = "ATTRIBUTE_NAME", nullable = false, length = 200)
	private String attributeName;

	@PostLoad
    private void trimSessionPrimaryId() {
        if (sessionPrimaryId != null) {
            sessionPrimaryId = sessionPrimaryId.trim();
        }
    }
	
	public String getSessionPrimaryId() {
        return sessionPrimaryId != null ? sessionPrimaryId.trim() : null;
    }

    public void setSessionPrimaryId(String sessionPrimaryId) {
        this.sessionPrimaryId = sessionPrimaryId != null ? sessionPrimaryId.trim() : null;
    }
	
}
