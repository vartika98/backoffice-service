package com.qnb.bo.backofficeservice.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.ApplicationEntity;
import com.qnb.bo.backofficeservice.enums.TxnStatus;
import com.qnb.bo.backofficeservice.views.ApplicationView;

@Repository
public interface ApplicationRepository extends JpaRepository<ApplicationEntity, Long>{
	
	@Query("SELECT app.txnId as txnId, app.appName as appName, app.appType as appType, app.status as status, "
			+ "appDetails.txnId as appsDetailsTxnId, appDetails.unitId as unitId, appDetails.channelId as channelId,"
			+ "channel.status as channelStatus"
			+ " FROM ApplicationEntity app "
			+ "   INNER JOIN ApplicationDetailsEntity appDetails ON appDetails.application.txnId = app.txnId "
			+ "  	INNER JOIN Channel channel "
			+ "			on appDetails.channelId=channel.channelId "
			+ " WHERE app.status = :status "
			+ "    AND appDetails.unitId IN :units")
	List<ApplicationView> getAllApplicationsListByUnitAndStatus(@Param("units") List<String> units, @Param("status") TxnStatus status);
	
	@Query("SELECT app.txnId as txnId, app.appName as appName, app.appType as appType, app.status as status, "
			+ "appDetails.txnId as appsDetailsTxnId, appDetails.unitId as unitId, appDetails.channelId as channelId,"
			+ "channel.status as channelStatus"
			+ " FROM ApplicationEntity app "
			+ "    INNER JOIN ApplicationDetailsEntity appDetails ON appDetails.application.txnId = app.txnId "
			+ "  	 INNER JOIN Channel channel "
			+ "			on appDetails.channelId=channel.channelId "
			+ " WHERE appDetails.unitId IN :units")
	List<ApplicationView> getAllApplicationsListByUnit(@Param("units") List<String> units);
	
	@Query("SELECT app.txnId as txnId, app.appName as appName, app.appType as appType, app.status as status, "
			+ "appDetails.txnId as appsDetailsTxnId, appDetails.unitId as unitId, appDetails.channelId as channelId,"
			+ "channel.status as channelStatus"
			+ " FROM ApplicationEntity app "
			+ "   INNER JOIN ApplicationDetailsEntity appDetails ON appDetails.application.txnId = app.txnId"
			+ "  	INNER JOIN Channel channel "
			+ "			on appDetails.channelId=channel.channelId ")
	List<ApplicationView> getAllApplications();
	
	@Query("SELECT app.txnId as txnId, app.appName as appName, app.appType as appType, app.status as status, "
			+ "appDetails.txnId as appsDetailsTxnId, appDetails.unitId as unitId, appDetails.channelId as channelId,"
			+ "channel.status as channelStatus"
			+ " FROM ApplicationEntity app "
			+ " INNER JOIN ApplicationDetailsEntity appDetails ON appDetails.application.txnId = app.txnId "
			+ "  	INNER JOIN Channel channel "
			+ "			on appDetails.channelId=channel.channelId "
			+ " WHERE app.status = :status")
	List<ApplicationView> getApplicationsListByStatus(@Param("status") TxnStatus status);
	
	@Query("SELECT app.txnId as txnId, app.appName as appName, app.appType as appType, app.status as status, "
			+ "appDetails.txnId as appsDetailsTxnId, appDetails.unitId as unitId, appDetails.channelId as channelId,"
			+ "channel.status as channelStatus"
			+ " FROM ApplicationEntity app "
			+ " INNER JOIN ApplicationDetailsEntity appDetails ON appDetails.application.txnId = app.txnId "
			+ "  	INNER JOIN Channel channel "
			+ "			on appDetails.channelId=channel.channelId "
			+ " WHERE app.status = :status "
			+ " AND app.txnId IN :appIds "
			+ " AND appDetails.unitId IN :units" )
	List<ApplicationView> getAllApplicationsListByUnitAppIdStatus(@Param("units") List<String> units, @Param("appIds") List<Long> appIds,@Param("status") TxnStatus status);
}
