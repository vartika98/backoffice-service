package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.audit.AuditDetails;

@Repository
public interface AuditDetailsRepository extends JpaRepository<AuditDetails, Integer>{

	List<AuditDetails> findByAuditRef_AuditRefno(Integer auditRefNo);

}
