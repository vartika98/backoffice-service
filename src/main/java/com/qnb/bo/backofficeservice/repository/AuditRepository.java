package com.qnb.bo.backofficeservice.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.audit.AuditMaster;

@Repository
public interface AuditRepository extends JpaRepository<AuditMaster, Integer> {

	@Query(value = "select CATEGORY_CODE,DESCRIPTION from OCS_T_BO_CATEGORY_CODE where UPPER(BO_AUDIT_FLAG)='Y'", nativeQuery = true)
	List<Object[]> getTransactionNameList();

	@Query(value = "SELECT a.*, a.category_code AS a_category_code, b.description FROM "
			+ "    ocs_t_audit_master  a, ocs_category_master  b WHERE " + "    a.category_code = b.category_code AND "
			+ "    a.category_code =:txnName AND a.USER_ID=:userId AND UPPER(b.BO_AUDIT_FLAG)='Y' AND UPPER(b.FO_AUDIT_FLAG)='Y' AND a.channel_id =:channel AND "
			+ "    trunc(a.audit_date) "
			+ "    BETWEEN TO_CHAR(TO_DATE(:fromDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:toDate, 'DD-MM-YY')+1,'DD-MON-YY')", nativeQuery = true)
	List<AuditMaster> getAuditSumByUserId(@Param("txnName") String txnName, @Param("fromDate") String fromDate,
			@Param("toDate") String toDate, @Param("channel") String channel, @Param("userId") String userId);
	
	@Query(value = "SELECT a.*, a.category_code AS a_category_code, b.description FROM "
			+ "    ocs_t_audit_master  a, ocs_category_master  b WHERE " + "    a.category_code = b.category_code AND "
			+ "    a.category_code =:txnName AND UPPER(b.BO_AUDIT_FLAG)='Y' AND UPPER(b.FO_AUDIT_FLAG)='Y' AND a.channel_id =:channel AND "
			+ "    trunc(a.audit_date) "
			+ "    BETWEEN TO_CHAR(TO_DATE(:fromDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:toDate, 'DD-MM-YY')+1,'DD-MON-YY')", nativeQuery = true)
	List<AuditMaster> getAuditSum(@Param("txnName") String txnName, @Param("fromDate") String fromDate,
			@Param("toDate") String toDate, @Param("channel") String channel);
	
	@Query(value = "SELECT a.*, a.category_code AS a_category_code, b.description FROM "
			+ "    ocs_t_audit_master  a, ocs_category_master  b WHERE " + "    a.category_code = b.category_code AND "
			+ "    a.category_code =:txnName AND a.USER_NO=:userNo AND UPPER(b.BO_AUDIT_FLAG)='Y' AND UPPER(b.FO_AUDIT_FLAG)='Y' AND a.channel_id =:channel AND "
			+ "    trunc(a.audit_date) "
			+ "    BETWEEN TO_CHAR(TO_DATE(:fromDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:toDate, 'DD-MM-YY')+1,'DD-MON-YY')", nativeQuery = true)
	List<AuditMaster> getAuditSumByUserNo(@Param("txnName") String txnName, @Param("fromDate") String fromDate,
			@Param("toDate") String toDate, @Param("channel") String channel, @Param("userNo") String userNo);

	@Query(value = "SELECT a.*, a.category_code AS a_category_code, b.description FROM "
			+ "    ocs_t_audit_master  a, ocs_category_master  b WHERE "
			+ "    a.category_code = b.category_code AND"
			+ "    UPPER(b.BO_AUDIT_FLAG)='Y' AND UPPER(b.FO_AUDIT_FLAG)='Y' AND a.channel_id =:channel AND "
			+ "    trunc(a.audit_date) "
			+ "    BETWEEN TO_CHAR(TO_DATE(:fromDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:toDate, 'DD-MM-YY')+1,'DD-MON-YY')", nativeQuery = true)
	List<AuditMaster> getAuditList(@Param("fromDate") String fromDate, @Param("toDate") String toDate,
			@Param("channel") String channel);
	
	@Query(value = "SELECT a.*, a.category_code AS a_category_code, b.description FROM "
			+ "    ocs_t_audit_master  a, ocs_category_master  b WHERE "
			+ "    a.category_code = b.category_code AND a.USER_NO=:userNo AND"
			+ "    UPPER(b.BO_AUDIT_FLAG)='Y' AND UPPER(b.FO_AUDIT_FLAG)='Y' AND a.channel_id =:channel AND "
			+ "    trunc(a.audit_date) "
			+ "    BETWEEN TO_CHAR(TO_DATE(:fromDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:toDate, 'DD-MM-YY')+1,'DD-MON-YY')", nativeQuery = true)
	List<AuditMaster> getAuditListByUserNo(@Param("fromDate") String fromDate, @Param("toDate") String toDate,
			@Param("channel") String channel, @Param("userNo") String userNo);

	@Query(value = "SELECT  a.*, a.category_code AS a_category_code,b.description FROM "
			+ "    ocs_t_audit_master  a,ocs_category_master b WHERE " + " a.category_code = b.category_code AND "
			+ "    UPPER(b.BO_AUDIT_FLAG)='Y' AND UPPER(b.FO_AUDIT_FLAG)='Y' AND a.channel_id =:channel AND"
			+ "   a.USER_ID =:userId AND trunc(a.audit_date) "
			+ "    BETWEEN TO_CHAR(TO_DATE(:fromDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:toDate, 'DD-MM-YY')+1,'DD-MON-YY')", nativeQuery = true)
	List<AuditMaster> getAuditLstByUserId(@Param("fromDate") String fromDate, @Param("toDate") String toDate,
			@Param("channel") String channel, @Param("userId") String userId);
	
	@Query(value = "SELECT * FROM ocs_t_audit_master WHERE channel_id = :channelId AND category_code = :categoryCode AND audit_date BETWEEN TO_DATE(:startDate, 'DD-MM-YY') AND TO_DATE(:endDate, 'DD-MM-YY') + INTERVAL '1' DAY ORDER BY audit_date DESC", nativeQuery = true)
	List<AuditMaster> findByChannelAndCategoryAndDate(@Param("channelId") String channelId,
			@Param("categoryCode") String categoryCode, @Param("startDate") String startDate,
			@Param("endDate") String endDate);

	@Query(value = "SELECT * FROM ocs_t_audit_master WHERE category_code = :categoryCode AND audit_date BETWEEN TO_DATE(:startDate, 'DD-MM-YY') AND TO_DATE(:endDate, 'DD-MM-YY') + INTERVAL '1' DAY ORDER BY audit_date DESC", nativeQuery = true)
	List<AuditMaster> findLoginCountByDateRange(@Param("categoryCode") String categoryCode,
			@Param("startDate") String startDate, @Param("endDate") String endDate);

	ArrayList<AuditMaster> findByTxnRefNo(String guid);
	
	@Query(value = "SELECT  a.*, a.category_code AS a_category_code,b.description FROM "
			+ "    ocs_t_audit_master  a,ocs_category_master b WHERE " + " a.category_code = b.category_code AND "
			+ "    UPPER(b.BO_AUDIT_FLAG)='Y' AND UPPER(b.FO_AUDIT_FLAG)='Y' AND a.channel_id =:channel AND"
			+ "   a.USER_ID =:userId AND a.GLOBAL_ID=:gId AND trunc(a.audit_date) "
			+ "    BETWEEN TO_CHAR(TO_DATE(:fromDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:toDate, 'DD-MM-YY')+1,'DD-MON-YY')", nativeQuery = true)
	List<AuditMaster> getAuditLstByUserIdAndGId(@Param("fromDate") String fromDate, @Param("toDate") String toDate,
			@Param("channel") String channel, @Param("userId") String userId,@Param("gId") String gId);
	
	@Query(value = "SELECT a.*, a.category_code AS a_category_code, b.description FROM "
			+ "    ocs_t_audit_master  a, ocs_category_master  b WHERE " + "    a.category_code = b.category_code AND "
			+ "    a.category_code =:txnName AND a.USER_ID=:userId AND a.GLOBAL_ID=:gId AND UPPER(b.BO_AUDIT_FLAG)='Y' AND UPPER(b.FO_AUDIT_FLAG)='Y' AND a.channel_id =:channel AND "
			+ "    trunc(a.audit_date) "
			+ "    BETWEEN TO_CHAR(TO_DATE(:fromDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:toDate, 'DD-MM-YY')+1,'DD-MON-YY')", nativeQuery = true)
	List<AuditMaster> getAuditSumByUserIdAndGId(@Param("txnName") String txnName, @Param("fromDate") String fromDate,
			@Param("toDate") String toDate, @Param("channel") String channel, @Param("userId") String userId,@Param("gId") String gId);
	

	@Query(value = "SELECT  a.*, a.category_code AS a_category_code,b.description FROM "
			+ "    ocs_t_audit_master  a,ocs_category_master b WHERE " + " a.category_code = b.category_code AND "
			+ "    UPPER(b.BO_AUDIT_FLAG)='Y' AND UPPER(b.FO_AUDIT_FLAG)='Y' AND a.channel_id =:channel AND"
			+ "    a.GLOBAL_ID=:gId AND trunc(a.audit_date) "
			+ "    BETWEEN TO_CHAR(TO_DATE(:fromDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:toDate, 'DD-MM-YY')+1,'DD-MON-YY')", nativeQuery = true)
	List<AuditMaster> getAuditLstByGId(@Param("fromDate") String fromDate, @Param("toDate") String toDate,
			@Param("channel") String channel,@Param("gId") String gId);

	@Query(value = "SELECT a.*, a.category_code AS a_category_code, b.description FROM "
			+ "    ocs_t_audit_master  a, ocs_category_master  b WHERE " + "    a.category_code = b.category_code AND "
			+ "    a.category_code =:txnName AND a.GLOBAL_ID=:gId AND UPPER(b.BO_AUDIT_FLAG)='Y' AND UPPER(b.FO_AUDIT_FLAG)='Y' AND a.channel_id =:channel AND "
			+ "    trunc(a.audit_date) "
			+ "    BETWEEN TO_CHAR(TO_DATE(:fromDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:toDate, 'DD-MM-YY')+1,'DD-MON-YY')", nativeQuery = true)
	List<AuditMaster> getAuditSumByGId(@Param("txnName") String txnName, @Param("fromDate") String fromDate,
			@Param("toDate") String toDate, @Param("channel") String channel, @Param("gId") String gId);
	


}
