package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.BOGroupMaster;
import com.qnb.bo.backofficeservice.views.GroupSummaryView;

@Repository
public interface BOGroupMasterRepository extends JpaRepository<BOGroupMaster, String> {

	BOGroupMaster findByGroupIdAndGroupName(String groupId, String groupName);

	@Query(value = "SELECT gm.GROUP_ID as groupId, gm.GROUP_NAME as groupName, gm.GROUP_DESCRIPTION as groupDescription, gm.UNIT as unit,gm.REMARKS as remarks, "
			+ "gm.STATUS as status,gpm.PROD_CODE as prodCode, gpm.SUB_PROD_CODE as subProdCode, gpm.FUNCTION_CODE as functionCode FROM BKO_OCS_T_GROUP_MASTER gm "
			+ "INNER JOIN BKO_OCS_T_GROUP_PRODUCT_MAPPING gpm"
			+ " ON gm.TXN_ID = gpm.GROUP_PRODUCT_MAPPING_ID WHERE gm.GROUP_ID = :groupId AND gm.status IN ('ACT','IAC')", nativeQuery = true)
	List<GroupSummaryView> getSummary(@Param("groupId") String groupId);

	List<BOGroupMaster> findByGroupId(String groupId);

	BOGroupMaster findByGroupIdAndGroupNameAndStatusNot(String groupId, String groupName, String status);

	List<BOGroupMaster> findAllByGroupId(String groupId);
	
	List<BOGroupMaster> findAllByGroupIdOrderByCreatedTimeDesc(String groupId);

	@Query(value = "SELECT (BKO_OCS_T_GROUP_MASTER.GROUP_NAME|| '_'|| OCS_T_UNIT_DESC.UNIT_ID) AS groupName "
			+ "FROM BKO_OCS_T_GROUP_MASTER , OCS_T_UNIT_DESC  "
			+ "GROUP BY BKO_OCS_T_GROUP_MASTER.GROUP_NAME, OCS_T_UNIT_DESC.UNIT_ID", nativeQuery = true)
	List<String> getConcatenatedGroupNames();
}