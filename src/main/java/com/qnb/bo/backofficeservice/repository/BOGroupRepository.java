package com.qnb.bo.backofficeservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.BOGroupEntity;

@Repository
public interface BOGroupRepository extends JpaRepository<BOGroupEntity,String>{

	BOGroupEntity findByGroupCode(String groupCode);

}
