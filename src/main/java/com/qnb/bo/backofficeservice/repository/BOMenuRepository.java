package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.BOMenuEntity;

@Repository
public interface BOMenuRepository extends JpaRepository<BOMenuEntity, Integer> {

	List<BOMenuEntity> findByGroupType(String string);

}
