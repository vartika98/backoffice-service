package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.BOUnitLanguage;


@Repository
public interface BOUnitLanguageRepository extends JpaRepository<BOUnitLanguage, Integer> {

	
	List<BOUnitLanguage> findByStatusIn(List<String> status);

	List<BOUnitLanguage> findByStatusInOrderByCreatedTimeDesc(List<String> status);

	@Query(value = "SELECT DISTINCT LANG_CODE FROM OCS_T_UNIT_LANGUAGE WHERE STATUS = 'ACT' ", nativeQuery = true)
	List<String> getAllDistinctLanguages();
}
