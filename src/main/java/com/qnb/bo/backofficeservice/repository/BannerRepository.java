package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.BannerDetailsEntity;
import com.qnb.bo.backofficeservice.entity.BannerEntityId;

@Repository 
public interface BannerRepository extends JpaRepository<BannerDetailsEntity, BannerEntityId>{

	List<BannerDetailsEntity> findById_ScreenIdAndId_UnitIdAndId_ChannelIdAndId_LangCodeAndImageResolAndDisPriority(String screenId, String unitId, 
			String channelId, String langCode, String imageResol, int disPriority, Sort sortBy);
	
	BannerDetailsEntity findByTxnId(int txnId);
	
	List<BannerDetailsEntity> findById_UnitIdAndId_ChannelId(String unitId, String channelId);
	
	BannerDetailsEntity findById_UnitIdAndId_ChannelIdAndId_SegmentTypeAndId_ScreenIdAndId_LangCodeAndId_BannerId(String unitId, String channelId, String segmentType, String screenId, String langCode, String bannerId);
	
	@Query(value="SELECT DISTINCT B.SCREEN_ID, B.DESCRIPTION FROM OCS_T_BANNER B WHERE B.UNIT_ID = :unitId AND B.CHANNEL_ID = :channelId AND STATUS IN ('ACT', 'IAC') ORDER BY DESCRIPTION ASC", nativeQuery=true)
	List<Object[]> findDistinctScreenIdDescriptionByUnitAndChannelAndStatus(String unitId, String channelId);
	
	List<BannerDetailsEntity> findById_ScreenIdAndId_UnitIdAndId_ChannelIdAndId_LangCodeInAndImageResolInAndDisPriorityInAndStatusInOrderByDateCreatedDesc
	(String screenId, String unitId, String channelId, List<String> langCodeList, List<String> imageResolList, List<Integer> disPriorityList, List<String> status);
	
	BannerDetailsEntity findById_BannerId(String bannerId);
	
	List<BannerDetailsEntity> findById_UnitIdAndId_ChannelIdAndId_ScreenIdAndId_LangCodeAndImageResolAndDisPriorityAndStatus(String unitId,String channelId,String screenId,String langCode,String imageResol, int disPriority, String status);
}
