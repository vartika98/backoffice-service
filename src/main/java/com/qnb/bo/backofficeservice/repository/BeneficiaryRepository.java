package com.qnb.bo.backofficeservice.repository;

import java.util.List;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.BeneficiaryEntity;

@Repository
public interface BeneficiaryRepository extends JpaRepository<BeneficiaryEntity, Integer> {

	@Query(value ="SELECT * from OCS_T_BENEFICIARIES  where BO_APPROVAL=:approval and UNIT_CODE IN :units and STATUS=:status and ROUTING_CHANNEL <> 'QNB' and TXN_NAME_FLAG IN :flag Order By DATE_CREATED desc", nativeQuery = true)
	List<BeneficiaryEntity> getBenficiaryForApprovalByUnitsAndTxnNameFlagOrderByCreatedDateDes(List<String> units,
			String approval, String status, List<String> flag);

	@Query(value ="SELECT * from OCS_T_BENEFICIARIES ben where BO_APPROVAL=:approval and UNIT_CODE IN :units and STATUS=:status and ROUTING_CHANNEL = 'QNB' and TXN_NAME_FLAG IN :flag Order By DATE_CREATED desc", nativeQuery = true)
	List<BeneficiaryEntity> getQNBBenficiaryForApprovalByUnitsAndTxnNameFlagOrderByCreatedDateDes(List<String> units,
			String approval, String status, List<String> flag);

	@Query(value ="SELECT * from OCS_T_BENEFICIARIES ben where BO_APPROVAL=:approval and STATUS=:status and ROUTING_CHANNEL <> 'QNB' and TXN_NAME_FLAG IN :txnNameFlag Order By DATE_CREATED desc", nativeQuery = true)
	List<BeneficiaryEntity> findByBoApprvalAndStatusAndTxnNameFlagOrderByDateCreatedDes(String approval, String status,
			List<String> txnNameFlag);

	@Query(value ="SELECT * from OCS_T_BENEFICIARIES ben where BO_APPROVAL=:approval and STATUS=:status and ROUTING_CHANNEL =:routingChannel and TXN_NAME_FLAG IN :txnNameFlag Order By DATE_CREATED desc", nativeQuery = true)
	List<BeneficiaryEntity> findByBoApprvalAndStatusAndRoutingChannelAndTxnNameFlagOrderByDateCreatedDes(
			String approval, String status, String routingChannel, List<String> txnNameFlag);

	BeneficiaryEntity findByRefNo(Integer refNo);

	boolean existsByRefNo(Integer refNo);
}
