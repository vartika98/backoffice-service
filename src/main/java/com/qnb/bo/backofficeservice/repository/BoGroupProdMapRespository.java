package com.qnb.bo.backofficeservice.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.BoGroupProductMap;

import jakarta.transaction.Transactional;

@Repository
public interface BoGroupProdMapRespository extends JpaRepository<BoGroupProductMap, Integer> {

	List<BoGroupProductMap> findByGroupdProdMapId(String groupId);

	List<BoGroupProductMap> findByGroupIdAndGroupName(String groupId, String groupName);

	@Query(value = "SELECT prodMap.txn_id as txnId from BKO_OCS_T_GROUP_PRODUCT_MAPPING prodMap where prodMap.group_id=:groupId AND "
			+ "	prodMap.group_name=:groupName", nativeQuery = true)
	List<String> getList(String groupId, String groupName);

	@Transactional
	@Modifying
	@Query(value = "DELETE FROM BKO_OCS_T_GROUP_PRODUCT_MAPPING d WHERE d.txn_id IN (:txnId)", nativeQuery = true)
	void deleteProductById(@Param("txnId") List<String> txnId);

	List<BoGroupProductMap> findByGroupId(String groupId);

	@Transactional
	@Modifying
	@Query(value = "DELETE FROM BKO_OCS_T_GROUP_PRODUCT_MAPPING d WHERE d.GROUP_PRODUCT_MAPPING_ID IN (:txnId)", nativeQuery = true)
	void deleteProductByProdId(@Param("txnId") List<String> txnId);

}
