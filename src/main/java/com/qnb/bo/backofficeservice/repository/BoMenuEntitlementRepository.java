package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.OcsBoMenuEntitlement;

@Repository
public interface BoMenuEntitlementRepository extends JpaRepository<OcsBoMenuEntitlement, Integer>{

	List<OcsBoMenuEntitlement> findByUserNoAndStatus(String userNo, String status);
}
