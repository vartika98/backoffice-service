package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.BranchEntity;

@Repository
public interface BranchesRepository extends JpaRepository<BranchEntity, Integer> {

	List<BranchEntity> findByUnitIdAndStatus(String unit, String status);

	/*@Query(value = "select max(TXN_ID) from OCS_T_BRANCH_MASTER", nativeQuery = true)
	public int getMaxNum();

	@Query(value = "select * from OCS_T_BRANCH_MASTER where TXN_ID=:id", nativeQuery = true)
	BranchEntity findById(@Param("id") int id);*/

	@Query(value = "select * from OCS_T_BRANCH_MASTER where UNIT_ID=:unit AND BRANCH_CODE=:branchCode", nativeQuery = true)
	List<BranchEntity> findByunitAndBranchCode(@Param("unit") String unit, @Param("branchCode") String branchCode);

}
