package com.qnb.bo.backofficeservice.repository;

import java.util.Collection;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.qnb.bo.backofficeservice.entity.CategoryDetails;

public interface CategoryCodeDetailsRepository extends JpaRepository<CategoryDetails, Integer> {
	@Query("SELECT DISTINCT cd.categoryCode  FROM CategoryDetails cd")
	List<String> findDistinctCategogyCode();

    List<CategoryDetails> findByCategoryCodeAndStatusInOrderByCreatedAtDesc(String categoryCode, List<String> statuses);

	Collection<CategoryDetails> findByCategoryCode(String categoryCode);

}
