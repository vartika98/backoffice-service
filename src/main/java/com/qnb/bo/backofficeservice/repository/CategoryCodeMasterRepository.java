package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.CategoryMaster;
@Repository
public interface CategoryCodeMasterRepository extends JpaRepository<CategoryMaster, Integer>{

	CategoryMaster findByCategoryCode(String categoryCode);

	List<CategoryMaster> findAllByStatusIn(List<String> statuses);

}
