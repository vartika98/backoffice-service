package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.CfmsParamEntity;
import com.qnb.bo.backofficeservice.entity.CfmsParamEntityId;

@Repository
public interface CfmsParamRepo extends JpaRepository<CfmsParamEntity, CfmsParamEntityId> {

	@Query(value = "select max(TRANS_KEY) from  OCS_T_CFMS_PARAM ", nativeQuery = true)
	long findMaxTransKey();

	boolean existsById_TransKey(long id);

	CfmsParamEntity findById_TransKey(long id);

	void deleteById_TransKey(long id);

	List<CfmsParamEntity> findById_CustOption(String string);

	List<CfmsParamEntity> findByTypeOfProd(String typeOfProd);

	List<CfmsParamEntity> findById_ServiceCode(String custOption);

	@Query(value = "Select * from OCS_T_CFMS_PARAM where CUST_OPTION =:custOption And SERVICE_CODE=:serviceCode", nativeQuery = true)

	List<CfmsParamEntity> findById_CustOptionAndId_ServiceCode(String custOption, String serviceCode);

	List<CfmsParamEntity> findByTransValueAndId_CustOptionAndId_ServiceCode(String typeOfProd, String custOption,
			String serviceCode);

	List<CfmsParamEntity> findById_UnitIdAndId_CustOptionAndId_ServiceCode(String unitId, String subprod,
			String appprod);

	List<CfmsParamEntity> findById_CustOptionAndId_ServiceCodeOrderById_UnitIdAsc(String custoption,

			String serviceCode);

	List<CfmsParamEntity> findById_UnitIdAndCfmsValueAndEmployeeId(String unitId, String cfmsValue, String employeeId);

	List<CfmsParamEntity> findById_CustOptionAndId_ServiceCodeAndId_UnitIdInAndStatusIn(String custOption, String serviceCode,

			List<String> accessedUnit, List<String> statusList);

	List<CfmsParamEntity> findById_CustOptionAndId_UnitIdIn(String string, List<String> accessedUnit);

	List<CfmsParamEntity> findByTypeOfProdAndId_CustOptionAndId_ServiceCode(String typeOfProd, String custOption,

			String serviceCode);

	List<CfmsParamEntity> findByTypeOfProdAndId_CustOptionAndId_ServiceCodeAndId_UnitIdIn(String typeOfProd,

			String custOption, String serviceCode, List<String> accessedUnit);

	List<CfmsParamEntity> findById_CustOptionAndId_ServiceCodeAndId_UnitIdInOrderById_UnitIdAsc(String custoption,

			String serviceCode, List<String> accessedUnit);

}
