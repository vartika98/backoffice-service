package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.Channel;


@Repository
public interface ChannelRespository extends JpaRepository<Channel, Integer> {

	List<Channel> findByStatus(String status);

	Channel findByChannelId(String channelId);
	
	List<Channel> findByChannelIdIn(List<String> channelIds);

	List<Channel> findByStatusInOrderByCreatedTimeDesc(List<String> statusList);
}
