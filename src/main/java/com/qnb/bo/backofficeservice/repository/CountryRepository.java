package com.qnb.bo.backofficeservice.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.qnb.bo.backofficeservice.entity.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {

	List<Country> findByCountryCode(String countryCode);

	List<Country> findAllByStatusIn(List<String> status);

	List<Country> findAllByStatusInOrderByModifiedTimeDesc(List<String> status);
	
}
