package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.qnb.bo.backofficeservice.entity.CurrencyEntity;

public interface CurrencyRepository extends JpaRepository<CurrencyEntity, Integer> {

	List<CurrencyEntity> findByUnitIdAndStatusInOrderByModifiedTimeDesc(String unitId, List<String> status);

	@Query(value = "select DISTINCT currency_code from ocs_t_currency where unit_id=:unitId AND status IN ('ACT','IAC')", nativeQuery = true)
	List<String> getCurrencyCodeList(@Param("unitId") String unitId);

	List<CurrencyEntity> findByUnitId(String unitId);
}
