package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.PrePaidDenominations;
import com.qnb.bo.backofficeservice.views.BillPayDenominationView;

import jakarta.transaction.Transactional;

@Repository
public interface DenominationRepository extends JpaRepository<PrePaidDenominations, Long>{

	@Query(value = "SELECT T1.UTILITY_CODE as utilityCode, T1.DENOMINATION as denomination, T1.STATUS as status, T2.UTILITY_DESC as utilityDesc, T1.TXN_ID as txnId FROM "
			+ "OCS_T_UTILITY_PREPAID_DENOMINATIONS T1 LEFT JOIN OCS_T_UTILITY_CODES T2 ON "
			+ "T1.UTILITY_CODE = T2.UTILITY_CODE WHERE T1.UTILITY_CODE=:utilityCode AND T1.STATUS IN ('ACT', 'IAC') ORDER BY T1.DATE_MODIFIED DESC", nativeQuery=true)
	List<BillPayDenominationView> findByUtilityCodeAndStatus(@Param("utilityCode") String utilityCode);
	
	@Query(value = "SELECT T1.UTILITY_CODE as utilityCode, T1.DENOMINATION as denomination, T1.STATUS as status, T2.UTILITY_DESC as utilityDesc, T1.TXN_ID as txnId FROM "
			+ "OCS_T_UTILITY_PREPAID_DENOMINATIONS T1 LEFT JOIN OCS_T_UTILITY_CODES T2 ON "
			+ "T1.UTILITY_CODE = T2.UTILITY_CODE WHERE T1.STATUS IN ('ACT', 'IAC') ORDER BY T1.DATE_MODIFIED DESC", nativeQuery=true)
	List<BillPayDenominationView> findByStatus();
	
	List<PrePaidDenominations> findByUtilityCodeAndDenomination(String utilityCode, Integer denomination);
	
	List<PrePaidDenominations> findByUtilityCode(String utilityCode);
	
	PrePaidDenominations findByTxnId(Long txnId);
	
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM OCS_T_UTILITY_PREPAID_DENOMINATIONS d WHERE d.UTILITY_CODE =:utilityCode AND d.DENOMINATION =:denomination", nativeQuery = true)
	void deleteByUtilityCodeAndDenomination(@Param("utilityCode") String utilityCode, @Param("denomination") Integer denomination);
}
