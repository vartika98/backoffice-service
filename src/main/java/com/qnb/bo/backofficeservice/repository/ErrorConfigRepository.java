package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.ErrorConfig;


@Repository
public interface ErrorConfigRepository extends JpaRepository<ErrorConfig,Integer>{
	
//	 List<ErrorConfig> findByUnitIdAndChannelIdAndServiceType(String unitId, String channelId, String serviceType);
	 
	 List<ErrorConfig> findByUnitIdAndChannelIdAndServiceTypeAndMwErrCodeAndOcsErrCode(
	            String unitId, String channelId, 
	            String serviceType, String mwErrCode, String ocsErrCode
	    );

	ErrorConfig findByUnitIdAndChannelIdAndServiceTypeAndMwErrCodeAndOcsErrCodeAndStatus(String unitId,
			String channelId, String serviceType, String mwErrorCode, String ocsErrorCode, String status);

	@Query("SELECT DISTINCT ec.serviceType ,ec.remarks FROM ErrorConfig ec WHERE ec.unitId = :unitId AND ec.channelId = :channelId")
    List<String> findDistinctServiceTypes(@Param("unitId") String unitId, @Param("channelId") String channelId);
	 
	@Query(value = "SELECT * FROM OCS_T_ERROR_CONFIG er " +
		       "WHERE er.status = 'ACT' AND er.unit_id = :unitId AND er.channel_id = :channelId AND er.service_type = :serviceType " +
		       "ORDER BY er.date_created desc" ,nativeQuery = true)
		public List<ErrorConfig> getErrorConfig(@Param("unitId")String unitId, @Param("channelId")String channelId,@Param("serviceType")String serviceType);
}
