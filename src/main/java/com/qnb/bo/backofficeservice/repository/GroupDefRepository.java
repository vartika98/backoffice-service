package com.qnb.bo.backofficeservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.GroupDefEntity;


@Repository
public interface GroupDefRepository extends JpaRepository<GroupDefEntity, String>{

    GroupDefEntity findByGroupId(@Param("groupId") String groupId);
 
    Boolean existsByGroupCode(String groupCode);

    Boolean existsByGroupId(String groupId);
 
}
