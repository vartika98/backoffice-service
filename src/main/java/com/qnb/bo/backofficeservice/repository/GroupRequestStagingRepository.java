package com.qnb.bo.backofficeservice.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.GroupRequestStagingEntity;


@Repository
public interface GroupRequestStagingRepository extends JpaRepository<GroupRequestStagingEntity, String>{

	Set<GroupRequestStagingEntity> findByRequestIdInOrderByCreatedTimeAsc(List<String> requestIdLst);
}
