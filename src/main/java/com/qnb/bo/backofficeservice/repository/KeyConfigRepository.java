package com.qnb.bo.backofficeservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.KeyConfig;

@Repository
public interface KeyConfigRepository extends JpaRepository<KeyConfig, Long>{

	KeyConfig findByKeyName(String fcmKey);

}
