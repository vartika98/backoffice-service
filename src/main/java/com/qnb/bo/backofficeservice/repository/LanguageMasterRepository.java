package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import javax.print.DocFlavor.STRING;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;

import com.qnb.bo.backofficeservice.entity.Language;

public interface LanguageMasterRepository extends JpaRepository<Language, String> {
	
	@Query(value = "select DISTINCT lang_code from ocs_t_unit_language",nativeQuery = true)
	List<String> getDistinctLangCode();

}
