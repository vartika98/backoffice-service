package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.Language;

@Repository
public interface LanguageRepository extends JpaRepository<Language, String>{

  @Query(value = "SELECT * FROM OCS_T_LANGUAGE_MASTER WHERE status<>'DEL'ORDER BY date_created desc", nativeQuery = true)
  List<Language> findByStatus();
  
  List<Language> findByStatusOrderByModifiedTimeDesc(String status);

}
