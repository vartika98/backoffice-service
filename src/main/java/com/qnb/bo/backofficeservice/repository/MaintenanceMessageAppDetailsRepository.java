package com.qnb.bo.backofficeservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.MaintenanceMessageAppDetailsEntity;
import jakarta.transaction.Transactional;

@Repository
public interface MaintenanceMessageAppDetailsRepository
		extends JpaRepository<MaintenanceMessageAppDetailsEntity, Long> {

	@Transactional
	@Modifying
	@Query("DELETE FROM MaintenanceMessageAppDetailsEntity msgApp WHERE msgApp.refNo.txnId = :refNo")
	void deleteByRefNo(@Param("refNo") Long refNo);
}
