package com.qnb.bo.backofficeservice.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.MBRRMessages;

@Repository
public interface MbRRrepository extends JpaRepository<MBRRMessages, Integer> {

	@Query(value = "SELECT * FROM OCS_T_MB_RR_MESSAGES WHERE CATEGORY_CODE = :categoryCode AND REQUEST_DATE BETWEEN TO_CHAR(TO_DATE(:startDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:endDate, 'DD-MM-YY')+1,'DD-MON-YY') ORDER BY DATE_CREATED DESC", nativeQuery = true)
	List<MBRRMessages> findByCategoryCodeAndRequestDateBetweenOrderByDateCreatedDesc(
			@Param("categoryCode") String categoryCode, @Param("startDate") String startDate,
			@Param("endDate") String endDate);

	@Query(value = "SELECT * FROM OCS_T_MB_RR_MESSAGES WHERE REQUEST_DATE BETWEEN TO_CHAR(TO_DATE(:startDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:endDate, 'DD-MM-YY')+1,'DD-MON-YY') ORDER BY DATE_CREATED DESC", nativeQuery = true)
	List<MBRRMessages> findByRequestDateBetweenOrderByDateCreatedDesc(@Param("startDate") String startDate,
			@Param("endDate") String endDate);

	@Query(value = "SELECT * FROM OCS_T_MB_RR_MESSAGES WHERE GLOBAL_ID =:gId AND REQUEST_DATE BETWEEN TO_CHAR(TO_DATE(:startDate, 'DD-MM-YY'),'DD-MON-YY')"
			+ " AND TO_CHAR(TO_DATE(:endDate, 'DD-MM-YY')+1,'DD-MON-YY') ORDER BY DATE_CREATED DESC", nativeQuery = true)
	List<MBRRMessages> findByRequestDateBetweenAndGlobalIdOrderByDateCreatedDesc(
			 @Param("startDate") String startDate,
			@Param("endDate") String endDate,@Param("gId") String gId);
	
	
	@Query(value = "SELECT * FROM OCS_T_MB_RR_MESSAGES WHERE CATEGORY_CODE = :categoryCode AND GLOBAL_ID =:gId AND REQUEST_DATE BETWEEN TO_CHAR(TO_DATE(:startDate, 'DD-MM-YY'),'DD-MON-YY')"
			+ " AND TO_CHAR(TO_DATE(:endDate, 'DD-MM-YY')+1,'DD-MON-YY') ORDER BY DATE_CREATED DESC", nativeQuery = true)
	List<MBRRMessages> findByRequestDateBetweenAndGlobalIdANdCategoryCodeOrderByDateCreatedDesc(
			 @Param("startDate") String startDate,
			@Param("endDate") String endDate,@Param("gId") String gId,@Param("categoryCode") String categoryCode);
}
