package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.Menu;
import com.qnb.bo.backofficeservice.views.MenuEntitlementView;
import com.qnb.bo.backofficeservice.views.MenuView;

@Repository
public interface MenuRepository extends JpaRepository<Menu, String> {

	@Query("SELECT m.menuId as menuId, t.status as menuStatus,t.dispPriority as priority,m.description as description,"
			+ "	m.screenId as screenId"
			+ " FROM Menu m  LEFT OUTER JOIN TxnEntitlement t ON m.menuId = t.menuId"
			+ "  WHERE t.unitId=:unitId AND t.channelId=:channelId AND m.parent=:menuId AND m.status IN ('ACT','IAC') "
			+ "AND m.screenId IS NOT NULL ORDER BY m.parent, t.dispPriority ASC")
	List<MenuView> getScreenList(@Param("unitId") String unitId, @Param("channelId") String channelId,
			@Param("menuId") String menuId);
	
	@Query("SELECT m.menuId as menuId, t.status as menuStatus,t.dispPriority as priority,m.description as description,"
			+ "	m.screenId as screenId"
			+ " FROM Menu m  LEFT OUTER JOIN TxnEntitlement t ON m.menuId = t.menuId"
			+ "  WHERE t.unitId=:unitId AND t.channelId=:channelId AND m.parent=:menuId AND m.status IN ('ACT','IAC') AND t.dispPriority=:priority "
			+ "AND m.screenId IS NOT NULL ORDER BY m.parent, t.dispPriority ASC")
	List<MenuView> getSubMenuByPriority(@Param("unitId") String unitId, @Param("channelId") String channelId,
			@Param("menuId") String menuId,@Param("priority") String priority);

	@Query(value = "(SELECT m.menu_id as menuId, m.parent_menu_id as parent, m.priority as priority, m.status as menuStatus, m.description as description, "
			+ "	  :unitId as unitId, :channelId as channelId, 'IAC' as txnEntStatus, '' as txnEntDispPriority "
			+ "  FROM OCS_T_MENU_MASTER m left join ocs_t_txn_entitlement e on m.menu_id=e.menu_id "
			+ "	  WHERE e.unit_id = :unitId AND e.channel_id=:channelId and m.menu_id not in ( SELECT m1.menu_id  AS menuid FROM ocs_t_menu_master m1 LEFT OUTER JOIN ocs_t_txn_entitlement  t1 ON m1.menu_id = t1.menu_id "
			+ " WHERE t1.unit_id = :unitId AND t1.channel_id = :channelId AND t1.status = 'ACT' AND m1.status = 'ACT' ) ) "
			+ "	 UNION "
			+ " (SELECT m.menu_id as menuId, m.parent_menu_id as parent, m.priority as priority, m.status as menuStatus, m.description as description, "
			+ "	  :unitId as unitId, :channelId as channelId, 'IAC' as txnEntStatus, '' as txnEntDispPriority "
			+ "  FROM OCS_T_MENU_MASTER m inner join  ocs_t_txn_entitlement e on m.menu_id=e.menu_id "
			+ "	  WHERE e.unit_id =:unitId and e.channel_id=:channelId and m.parent_menu_id is null AND m.status = 'ACT')", nativeQuery = true)
	List<MenuEntitlementView> getAllMenuList(@Param("unitId") String unitId, @Param("channelId") String channelId);

	@Query("SELECT m.menuId as menuId, m.parent as parent, m.priority as priority, m.status as menuStatus, m.description as description,"
			+ "  t.unitId as unitId, t.channelId as channelId, t.status as txnEntStatus, t.dispPriority as txnEntDispPriority"
			+ "  FROM Menu m " + " LEFT OUTER JOIN TxnEntitlement  t ON m.menuId = t.menuId "
			+ " WHERE t.unitId=:unitId AND t.channelId=:channelId" + " AND t.status IN ('ACT','IAC') AND m.status IN ('ACT','IAC') "
			+ " ORDER BY m.parent, m.priority ASC")
	List<MenuEntitlementView> getEntitledMenuList(@Param("unitId") String unitId, @Param("channelId") String channelId);

	@Query(value = "SELECT menu.menu_id as menuId from OCS_T_MENU_MASTER menu where menu.parent_menu_id is NULL AND"
			+ "  menu.status IN ('ACT','IAC') ORDER BY menu.menu_id", nativeQuery = true)
	List<String> getMenuIdList();

	@Query(value = "SELECT menu.menu_id as menuId from OCS_T_MENU_MASTER menu where menu.parent_menu_id is NULL"
			+ " ORDER BY menu.menu_id", nativeQuery = true)
	List<String> getAllMenuIdList();

	@Query("SELECT m.menuId as menuId, t.status as menuStatus ,t.dispPriority as priority,m.description as description, "
			+ "m.screenId as screenId"
			+ " FROM Menu m  LEFT OUTER JOIN TxnEntitlement t ON m.menuId = t.menuId"
			+ "  WHERE t.unitId=:unitId AND t.channelId=:channelId AND m.parent IS NULL AND m.status IN ('ACT', 'IAC')"
			+ "AND m.screenId IS NOT NULL ORDER BY m.parent, t.dispPriority ASC")
	List<MenuView> menuList(@Param("unitId") String unitId, @Param("channelId") String channelId);
	
	@Query("SELECT m.menuId as menuId, t.status as menuStatus,t.dispPriority as priority,m.screenId as screenId,m.description as description"
			+ " FROM Menu m  LEFT OUTER JOIN TxnEntitlement t ON m.menuId = t.menuId"
			+ "  WHERE t.unitId=:unitId AND t.channelId=:channelId AND m.parent IS NULL AND m.status IN ('ACT', 'IAC') AND t.dispPriority=:priority  "
			+ "AND m.screenId IS NOT NULL ORDER BY m.parent, t.dispPriority ASC")
	List<MenuView> menuListWithPriority(@Param("unitId") String unitId, @Param("channelId") String channelId,@Param("priority") String priority);

	@Query(value = "SELECT menu.menu_id from OCS_T_MENU_MASTER menu where menu.parent_menu_id=:menuId "
			+ "AND  menu.I18N_SCREEN_ID IS NOT NULL AND menu.status IN ('ACT','IAC') "
			+ "ORDER BY menu.menu_id", nativeQuery = true)
	List<String> getSubMenuList(@Param("menuId") String menuId);

	@Query("SELECT m.menuId as menuId, t.status as menuStatus,t.dispPriority as priority,m.screenId as screenId,m.description as description "
			+ " FROM Menu m  LEFT OUTER JOIN TxnEntitlement t ON m.menuId = t.menuId"
			+ "  WHERE t.unitId=:unitId AND t.channelId=:channelId AND m.parent=:subMenuId AND m.status IN ('ACT','IAC') "
			+ "AND m.screenId IS NOT NULL ORDER BY m.parent, m.priority ASC")
	List<MenuView> getSub2MenuList(@Param("unitId") String unitId, @Param("channelId") String channelId,@Param("subMenuId") String subMenuId);

	@Query(value = "SELECT menu.menu_id from OCS_T_MENU_MASTER menu where menu.parent_menu_id=:subMenuId "
			+ "AND  menu.I18N_SCREEN_ID IS NOT NULL AND menu.status IN ('ACT','IAC') "
			+ "ORDER BY menu.menu_id", nativeQuery = true)
	List<String> getSubMenu2List(@Param("subMenuId") String subMenuId);

	@Query("SELECT m.menuId as menuId, t.status as menuStatus,t.dispPriority as priority,m.screenId as screenId,m.description as description"
			+ " FROM Menu m  LEFT OUTER JOIN TxnEntitlement t ON m.menuId = t.menuId"
			+ "  WHERE t.unitId=:unitId AND t.channelId=:channelId AND m.parent=:subMenuId AND m.status IN ('ACT','IAC') AND t.dispPriority=:priority "
			+ "AND m.screenId IS NOT NULL ORDER BY m.parent, m.priority ASC")
	List<MenuView> getSub2MenuListByPriority(@Param("unitId") String unitId, @Param("channelId") String channelId,@Param("subMenuId") String subMenuId,@Param("priority") String priority);


	@Query(value = "SELECT m.*"
			+ " FROM OCS_T_MENU_MASTER m  LEFT OUTER JOIN OCS_T_TXN_ENTITLEMENT t ON m.menu_id = t.menu_id"
			+ "  WHERE t.unit_id=:unitId AND t.channel_id=:channelId AND m.PARENT_MENU_ID IS NULL AND m.status IN ('ACT','IAC') "
			+ "AND m.I18N_SCREEN_ID IS NOT NULL ORDER BY m.PARENT_MENU_ID, t.DISP_PRIORITY ASC", nativeQuery = true)
	List<Menu> menuDeatils(@Param("unitId") String unitId, @Param("channelId") String channelId);
	
	@Query(value = "SELECT m.*"
			+ " FROM OCS_T_MENU_MASTER m  LEFT OUTER JOIN OCS_T_TXN_ENTITLEMENT t ON m.menu_id = t.menu_id"
			+ "  WHERE t.unit_id=:unitId AND t.channel_id=:channelId AND m.PARENT_MENU_ID IS NULL AND m.status IN ('ACT','IAC') "
			+ "AND m.I18N_SCREEN_ID=:menuId ORDER BY m.PARENT_MENU_ID, t.DISP_PRIORITY ASC", nativeQuery = true)
	List<Menu> subMenuDeatils(@Param("unitId") String unitId, @Param("channelId") String channelId,@Param("menuId") String menuId);



}
