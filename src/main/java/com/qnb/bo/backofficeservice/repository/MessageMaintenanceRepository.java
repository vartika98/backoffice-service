package com.qnb.bo.backofficeservice.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.MaintenanceMessageEntity;

import jakarta.transaction.Transactional;

@Repository
@Transactional
public interface MessageMaintenanceRepository extends JpaRepository<MaintenanceMessageEntity, Long>{

	List<MaintenanceMessageEntity> findByMessageType(String messageType);
	
	@EntityGraph(value = "MaintenanceMessageEntity.messageEntityGraph")
	@Query("SELECT msg FROM MaintenanceMessageEntity msg"
			+ " INNER JOIN MaintenanceMessageAppDetailsEntity msgApp on msg.txnId = msgApp.txnId"
			+ " WHERE msgApp.unitId IN :unitIds"
			+ " AND msgApp.appId IN :appIds"
			+ " AND msg.status <> 'TER'"
			+ " AND msg.txnId <> :refNo")
	List<MaintenanceMessageEntity> getExistingMaintenanceByRefNo(@Param("unitIds") Set<String> unitIds,
			@Param("appIds") Set<Long> appIds, @Param("refNo") Long refNo);
	
	@EntityGraph(value = "MaintenanceMessageEntity.messageEntityGraph")
	@Query("SELECT msg FROM MaintenanceMessageEntity msg "
	        + "INNER JOIN MaintenanceMessageAppDetailsEntity msgApp on msg.txnId = msgApp.txnId "
	        + "WHERE msgApp.unitId IN :unitIds "
	        + "AND msgApp.appId IN :appIds "
	        + "AND msg.status <> 'TER' ")
	List<MaintenanceMessageEntity> getExistingMaintenance(@Param("unitIds") Set<String> unitIds,
	        @Param("appIds") Set<Long> appIds);

	
	@EntityGraph(value = "MaintenanceMessageEntity.messageEntityGraph")
	@Query("SELECT msg FROM MaintenanceMessageEntity msg "
			+ "WHERE msg.txnId=:refNo ")
	MaintenanceMessageEntity getAllMaintenanceMsgByRefNo(@Param("refNo") Long refNo);
}
