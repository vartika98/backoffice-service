package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.NotificationParamConfigEntity;

import jakarta.transaction.Transactional;

@Repository
@Transactional
public interface NotificationParamConfigRepo extends JpaRepository<NotificationParamConfigEntity,Long>{

	List<NotificationParamConfigEntity> findByStatus(String status);
	
	NotificationParamConfigEntity findByNotificationId(String notificationId);

	
}
