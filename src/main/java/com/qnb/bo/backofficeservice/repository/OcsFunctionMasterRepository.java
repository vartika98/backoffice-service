package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.OcsFunctionMaster;
import com.qnb.bo.backofficeservice.views.BOMenuEntitlementView;

@Repository
public interface OcsFunctionMasterRepository extends JpaRepository<OcsFunctionMaster, String> {

	@Query(value = "SELECT p.product_code as prodCode,p.product_desc as prodDesc,"
			+ "s.sub_product_code as subProdCode,"
			+ "s.sub_product_desc as subProdDesc,f.function_code as functionCode,"
			+ "f.function_desc as functionDesc "
			+ "FROM bko_ocs_t_product_master p,bko_ocs_sub_product_master s, "
			+ "bko_ocs_function_master f WHERE f.sub_product_code=s.sub_product_code "
			+ "AND f.product_code=s.product_code AND f.product_code=p.product_code",nativeQuery = true)
	List<BOMenuEntitlementView> getMenuEntitlement();

	List<OcsFunctionMaster> findByProductCodeAndSubProductCode(String string, String string2);

}
