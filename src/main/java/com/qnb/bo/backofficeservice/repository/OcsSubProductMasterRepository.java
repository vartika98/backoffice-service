package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.OcsSubProductMaster;

@Repository
public interface OcsSubProductMasterRepository extends JpaRepository<OcsSubProductMaster, String>{

	List<OcsSubProductMaster> findByProductCode(String productCode);

}
