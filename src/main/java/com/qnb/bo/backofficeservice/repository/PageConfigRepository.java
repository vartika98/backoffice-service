package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.Channel;
import com.qnb.bo.backofficeservice.entity.PageConfig;
import com.qnb.bo.backofficeservice.entity.Unit;

@Repository
public interface PageConfigRepository extends JpaRepository<PageConfig, Integer> {

	@Query(value = "SELECT * FROM OCS_T_SCREEN_CONFIG WHERE UNIT_ID =:unitId AND  STATUS IN :statuses AND CHANNEL_ID =:channelId AND SCREEN_ID =:page", nativeQuery = true)
	List<PageConfig> findByUnitIdAndChannelIdAndPage(@Param("unitId") String unitId,@Param("statuses")List<String> statuses,
			@Param("channelId") String channelId, @Param("page") String page);

	@Query(value = "SELECT * FROM OCS_T_SCREEN_CONFIG " + "WHERE UNIT_ID =:unitId " + "  AND CHANNEL_ID =:channelId "
			+ "  AND STATUS IN :statuses " + "  AND SCREEN_ID =:screenIds " + "  AND CONFIG_KEY =:configKey "
			+ "  AND CONFIG_VALUE =:configValue", nativeQuery = true)
	List<PageConfig> findPageConfig(@Param("unitId") String unitId, @Param("channelId") String channelId,@Param("statuses")List<String> statuses,
			@Param("screenIds") String screenIds, @Param("configKey") String configKey,
			@Param("configValue") String configValue);

	@Query(value = "SELECT screen_id FROM OCS_T_SCREEN_CONFIG WHERE unit_id =:unitId AND channel_id =:channelId  ORDER BY date_created desc", nativeQuery = true)
	List<String> findPageByUnitIdAndChannelId(@Param("unitId") String unitId, @Param("channelId") String channelId);
	}

