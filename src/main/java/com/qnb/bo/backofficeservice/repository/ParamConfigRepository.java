package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.ParamConfig;

@Repository
public interface ParamConfigRepository extends JpaRepository<ParamConfig, Integer> {

	List<ParamConfig> findByUnit_UnitIdAndChannel_ChannelIdAndStatusInOrderByModifiedTimeDesc(String unitId, String channelId,
			List<String> statusLst);

	List<ParamConfig> findByKeyStartsWithAndUnit_UnitIdAndStatusAndChannel_ChannelId(String key, String unitId,
			String status, String channel);

	@Query(value = "SELECT DISTINCT * FROM OCS_T_PARAM_CONFIG p WHERE p.conf_key = :configKey and p.status = 'ACT' order by date_created", nativeQuery = true)
	List<ParamConfig> findDistinctValuesForKey(@Param("configKey") String configKey);

}
