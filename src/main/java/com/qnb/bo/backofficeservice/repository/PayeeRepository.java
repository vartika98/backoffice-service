package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.PayeeEntity;
import com.qnb.bo.backofficeservice.views.BillPayPayeeView;

@Repository
public interface PayeeRepository extends JpaRepository<PayeeEntity, Long>{

	List<PayeeEntity> findAllByStatusOrderByPriorityAsc(String status);
	
	PayeeEntity findByPayeeIdAndStatusIn(String payeeId, List<String> status);
	
	@Query(value = "select t1.payee_id as payeeId, t1.payee_desc as PayeeDesc, t1.status as status,"
			+ " t1.payee_logo as payeeLogo, t1.unit_code as unitCode, t2.unit_desc as unitCodeDesc, "
			+ "t1.priority as priority from ocs_t_utility_payees t1 "
			+ "left join ocs_t_unit_master t2 on t1.unit_code = t2.unit_id "
			+ "where t1.status in ('ACT', 'IAC') order by t1.date_modified desc", nativeQuery = true)
	List<BillPayPayeeView> getPayees();
}
