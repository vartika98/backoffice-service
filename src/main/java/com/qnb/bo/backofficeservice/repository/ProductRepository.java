package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.Product;



@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

	List<Product> findByUnit_UnitIdAndLangCodeAndStatus(String unitId, String langCode, String string);

	List<Product> findByUnit_UnitIdAndProdCodeAndStatus(String unitId, String prodCode, String string);
	
	List<Product> findByUnit_UnitIdAndStatus(String unitId, String status);


}
