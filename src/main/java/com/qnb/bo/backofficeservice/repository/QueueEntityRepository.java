package com.qnb.bo.backofficeservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qnb.bo.backofficeservice.entity.QueueEntity;

public interface QueueEntityRepository extends JpaRepository<QueueEntity,String>{

}
