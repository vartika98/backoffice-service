package com.qnb.bo.backofficeservice.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.RRmessage;


@Repository

public interface RRmessageRepository extends JpaRepository<RRmessage, Integer> {

	@Query(value = "SELECT * FROM OCS_T_RR_MESSAGES WHERE CHANNEL_ID = :channelId AND CATEGORY_CODE = :categoryCode AND REQUEST_DATE BETWEEN TO_CHAR(TO_DATE(:startDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:endDate, 'DD-MM-YY')+1,'DD-MON-YY') ORDER BY DATE_CREATED DESC", nativeQuery = true)
	List<RRmessage> findByChannelIdAndCategoryCodeAndRequestDateBetweenOrderByDateCreatedDesc(
			@Param("channelId") String channelId, @Param("categoryCode") String categoryCode,
			@Param("startDate") String startDate, @Param("endDate") String endDate);

	@Query(value = "SELECT COUNT(*) FROM OCS_T_RR_MESSAGES WHERE CHANNEL_ID = :channelId AND CATEGORY_CODE = :categoryCode AND RESPONSE_CODE = :responseCode AND REQUEST_DATE BETWEEN TO_CHAR(TO_DATE(:startDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:endDate, 'DD-MM-YY')+1,'DD-MON-YY') ORDER BY DATE_CREATED DESC", nativeQuery = true)
	Integer findCountByChannelIdAndCategoryCodeAndResponseCodeAndRequestDateBetweenOrderByDateCreatedDesc(
			@Param("channelId") String channelId, @Param("categoryCode") String categoryCode,
			@Param("startDate") String startDate, @Param("endDate") String endDate,
			@Param("responseCode") String responseCode);

	@Query(value = "SELECT * FROM OCS_T_RR_MESSAGES WHERE CHANNEL_ID = :channelId AND REQUEST_DATE BETWEEN TO_CHAR(TO_DATE(:startDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:endDate, 'DD-MM-YY')+1,'DD-MON-YY') ORDER BY DATE_CREATED DESC", nativeQuery = true)
	List<RRmessage> findByChannelIdAndRequestDateBetweenOrderByDateCreatedDesc(@Param("channelId") String channelId,
			@Param("startDate") String startDate, @Param("endDate") String endDate);

	@Query(value = "SELECT CASE WHEN FO_AUDIT_FLAG = 'Y' AND RR_AUDIT_FLAG = 'Y' THEN 'Y' ELSE 'N' END AS AUDIT_FLAG_STATUS FROM OCS_CATEGORY_MASTER WHERE CATEGORY_CODE = :categoryCode", nativeQuery = true)
	String getAuditRequired(@Param("categoryCode") String categoryCode);

	@Query(value = "SELECT * FROM OCS_T_RR_MESSAGES WHERE CHANNEL_ID = :channelId AND CATEGORY_CODE = :categoryCode AND UUID=:guid AND REQUEST_DATE BETWEEN TO_CHAR(TO_DATE(:startDate, 'DD-MM-YY'),'DD-MON-YY') "
			+ "AND TO_CHAR(TO_DATE(:endDate, 'DD-MM-YY')+1,'DD-MON-YY') ORDER BY DATE_CREATED DESC", nativeQuery = true)
	List<RRmessage> findByChannelIdAndCategoryCodeAndRequestDateBetweenAndUuidOrderByDateCreatedDesc(
			String channelId, String categoryCode, String startDate, String endDate, String guid);

	@Query(value = "SELECT * FROM OCS_T_RR_MESSAGES WHERE CHANNEL_ID = :channelId AND CATEGORY_CODE = :categoryCode AND GLOBAL_ID=:gId AND REQUEST_DATE BETWEEN TO_CHAR(TO_DATE(:startDate, 'DD-MM-YY'),'DD-MON-YY') "
			+ "AND TO_CHAR(TO_DATE(:endDate, 'DD-MM-YY')+1,'DD-MON-YY') ORDER BY DATE_CREATED DESC", nativeQuery = true)
	List<RRmessage> findByChannelIdAndCategoryCodeAndRequestDateBetweenAndGlobalIdOrderByDateCreatedDesc(
			String channelId, String categoryCode, String startDate, String endDate, String gId);

	List<RRmessage> findByUuidOrderByDateCreatedDesc(String guid);
	
	@Query(value = "SELECT * FROM OCS_T_RR_MESSAGES WHERE CHANNEL_ID = :channelId AND GLOBAL_ID=:gId "
			+ "AND REQUEST_DATE BETWEEN TO_CHAR(TO_DATE(:startDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:endDate, 'DD-MM-YY')+1,'DD-MON-YY') ORDER BY DATE_CREATED DESC", nativeQuery = true)
	List<RRmessage> getRRByGId(@Param("channelId") String channelId,@Param("gId") String gId,
			@Param("startDate") String startDate, @Param("endDate") String endDate);
	
	@Query(value = "SELECT * FROM OCS_T_RR_MESSAGES WHERE CHANNEL_ID = :channelId AND GLOBAL_ID=:gId "
			+ "AND CATEGORY_CODE = :categoryCode AND REQUEST_DATE BETWEEN TO_CHAR(TO_DATE(:startDate, 'DD-MM-YY'),'DD-MON-YY') AND TO_CHAR(TO_DATE(:endDate, 'DD-MM-YY')+1,'DD-MON-YY') ORDER BY DATE_CREATED DESC", nativeQuery = true)
	List<RRmessage> getRRByGIdAndCategoryCode(
			@Param("channelId") String channelId, @Param("categoryCode") String categoryCode,@Param("gId") String gId,
			@Param("startDate") String startDate, @Param("endDate") String endDate);


}
