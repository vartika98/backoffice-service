package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.qnb.bo.backofficeservice.entity.UserRole;

public interface RoleRepository extends JpaRepository<UserRole, Integer> {
	
	List<UserRole> findAllByGroupName(String groupName);

	@Query("SELECT DISTINCT ur.description FROM UserRole ur WHERE ur.groupName = :groupName")
	List<String> findDistinctRoleIdByGroupId(String groupName);

	UserRole findByGroupName(String groupName);

	UserRole findByRoleLevel(String userRoleLevel);

	List<UserRole> findAllByOrderByRoleLevelAsc();

	List<UserRole> findAllByOrderByDateCreatedDesc();
}
