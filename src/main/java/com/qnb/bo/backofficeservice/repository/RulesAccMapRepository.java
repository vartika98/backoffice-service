package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.RulesAccMapEntity;

import jakarta.transaction.Transactional;

@Repository
@Transactional
public interface RulesAccMapRepository extends JpaRepository<RulesAccMapEntity, Long>{

	@Transactional
	@Modifying
	@Query(value = "DELETE FROM BKO_RULES_ACC_MAP_MASTER d WHERE d.RULE_ID =:ruleId", nativeQuery = true)
	void deleteByRuleId(@Param("ruleId") long ruleId);
	
	List<RulesAccMapEntity> findByRuleId(Long ruleId);
}
