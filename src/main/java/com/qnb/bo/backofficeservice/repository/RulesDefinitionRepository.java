package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.RulesDefinition;

import jakarta.transaction.Transactional;

@Repository
@Transactional
public interface RulesDefinitionRepository extends JpaRepository<RulesDefinition, Long>{

	List<RulesDefinition> findByRuleId(long ruleId);
	
	@Transactional
	@Modifying
	@Query(value = "SELECT RULE_PARSED_ID FROM BKO_OCS_RULES_DEFINITION WHERE RULE_ID =:ruleId", nativeQuery = true)
	List<Long> findParsedId(@Param("ruleId") long ruleId);
	
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM BKO_OCS_RULES_DEFINITION d WHERE d.RULE_ID =:ruleId", nativeQuery = true)
	void deleteByRuleId(@Param("ruleId") long ruleId);
}
