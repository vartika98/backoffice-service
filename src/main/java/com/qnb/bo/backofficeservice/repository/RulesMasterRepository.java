package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qnb.bo.backofficeservice.entity.RulesMaster;

public interface RulesMasterRepository extends JpaRepository<RulesMaster, Long>{

	RulesMaster findByRuleNameAndStatusIn(String ruleName, List<String> status);
	
	List<RulesMaster> findByStatusIn(List<String> status);
}
