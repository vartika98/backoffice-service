package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.RulesParser;

import jakarta.transaction.Transactional;

@Repository
@Transactional
public interface RulesParserRepository extends JpaRepository<RulesParser, Long>{

	@Transactional
	@Modifying
	@Query(value = "DELETE FROM BKO_OCS_RULES_PARSER p WHERE p.RULE_PARSED_ID IN (:parsedIdList)", nativeQuery = true)
	void deleteByParsedIdList(@Param("parsedIdList") List<Long> parsedIdList);
}
