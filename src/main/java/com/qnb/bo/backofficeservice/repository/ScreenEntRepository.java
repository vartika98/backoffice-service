package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qnb.bo.backofficeservice.entity.ScreenEntitlement;

public interface ScreenEntRepository extends JpaRepository<ScreenEntitlement, Integer> {
	
	List<ScreenEntitlement> findAllByUnit_UnitIdAndChannel_ChannelIdAndWidget(String unitId, String channelId, String device);


}
