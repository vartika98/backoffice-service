package com.qnb.bo.backofficeservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.SpringSessionAttributes;

@Repository
public interface SpringSessionAttributeRepository extends JpaRepository<SpringSessionAttributes, String>{

}
