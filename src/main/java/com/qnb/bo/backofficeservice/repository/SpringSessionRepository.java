package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.SpringSession;

@Repository
public interface SpringSessionRepository extends JpaRepository<SpringSession, String> {

	@Query("SELECT s FROM SpringSession s WHERE s.expiryTime > :currentTime")
    List<SpringSession> findActiveSessions(@Param("currentTime") Long currentTime);

}
