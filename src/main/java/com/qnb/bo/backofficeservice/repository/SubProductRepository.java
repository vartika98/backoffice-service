package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qnb.bo.backofficeservice.entity.Product;
import com.qnb.bo.backofficeservice.entity.SubProduct;

public interface SubProductRepository extends JpaRepository<SubProduct, Integer> {

	List<SubProduct> findByUnit_UnitIdAndLangCodeInAndStatusIn(String unitId, List<String> langCodeList, List<String> status);

	List<SubProduct> findByUnit_UnitIdAndProdCodeAndSubProdAndStatus(String unitId, String prodCode, String subprod,
			String status);

}
