package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.SurveyMessage;

@Repository
public interface SurveyMessageRepository extends JpaRepository<SurveyMessage, Long> {

	boolean existsBySurveyId(long id);

	SurveyMessage findBySurveyId(long id);

	void deleteBySurveyId(long id);

	List<SurveyMessage> findByUnitIdAndSegmentTypeAndStatus(String unitId, String segmentType, String status);

	@Query(value = "select * from OCS_T_SURVEY_MASTER where UNIT_ID=:unitId AND TRUNC(SYSDATE) <= TRUNC(END_DATE) AND STATUS='Y' AND SEGMENT_TYPE=:segmentType", nativeQuery = true)
	List<SurveyMessage> findByUnitIdAndSegmentType(String unitId, String segmentType);

}