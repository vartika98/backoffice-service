package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.TermsAndCondition;

@Repository
public interface TermsConditionRepository extends JpaRepository<TermsAndCondition,Integer> {
	
	List<TermsAndCondition>findByUnitIdAndModIdAndSubModIdAndChannelIdAndStatusOrderByCreatedTimeDesc(String unitId,String modId,String subModId,String channelId,String status);

	@Query("SELECT DISTINCT tc.modId ,tc.remarks FROM TermsAndCondition tc WHERE tc.unitId = :unitId AND tc.channelId = :channelId")
    List<String> findDistinctModuleId(@Param("unitId") String unitId, @Param("channelId") String channelId);

	@Query("SELECT DISTINCT tc.subModId FROM TermsAndCondition tc WHERE tc.unitId = :unitId AND tc.channelId = :channelId AND tc.modId = :moduleId")
    List<String> findDistinctSubModuleId(@Param("unitId") String unitId, @Param("channelId") String channelId, String moduleId);

	List<TermsAndCondition> findByUnitIdAndModIdAndSubModIdAndChannelIdAndTcUrlId(String unitId, String moduleId,
			String subModuleId, String channelId, String tcUrlId);

	List<TermsAndCondition> findByTcUrlId(String tcUrlId);
	
	List<TermsAndCondition> findByUnitIdAndChannelIdAndStatusIn(String unitId, String channelId, List<String> statusList);

	List<TermsAndCondition> findByUnitIdAndChannelIdAndStatusInOrderByModifiedTimeDesc(String unitId, String channelId, List<String> status);

}
