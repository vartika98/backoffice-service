package com.qnb.bo.backofficeservice.repository;

import com.qnb.bo.backofficeservice.entity.TransferEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TransferRepository extends JpaRepository<TransferEntity, Long> {

    @Query(value = "SELECT * FROM ocs_t_transfers WHERE channel_id = :channelId AND transfer_type = :categoryCode AND transfer_init BETWEEN TO_DATE(:startDate, 'DD-MM-YY') AND TO_DATE(:endDate, 'DD-MM-YY') + INTERVAL '1' DAY ORDER BY transfer_init DESC", nativeQuery = true)
    List<TransferEntity> findByChannelAndCTransferTypeAndDate(
            @Param("channelId") String channelId, @Param("categoryCode") String categoryCode,
            @Param("startDate") String startDate, @Param("endDate") String endDate);

    @Query(value = "SELECT * FROM ocs_t_transfers WHERE transfer_type IN (:categoryCodes) AND transfer_init BETWEEN TO_DATE(:startDateStr, 'DD-MM-YY') AND TO_DATE(:endDateStr, 'DD-MM-YY') + INTERVAL '1' DAY ORDER BY transfer_init DESC", nativeQuery = true)
    List<TransferEntity> getConfigBasedTransferData(List<String> categoryCodes, String startDateStr, String endDateStr);

    @Query(value = "SELECT * FROM ocs_t_transfers " +
            "WHERE channel_id = :channelId " +
            "AND transfer_type = :categoryCode " +
            "AND (:globalIdCondition = 0 OR global_id = :globalId) " +
            "AND (:guIdCondition = 0 OR guid = :guId) " +
            "AND (:txnRefIdCondition = 0 OR txn_refid = :txnRefId) " +
            "AND transfer_init BETWEEN TO_DATE(:startDate, 'DD-MM-YY') AND TO_DATE(:endDate, 'DD-MM-YY') + INTERVAL '1' DAY " +
            "ORDER BY transfer_init DESC", nativeQuery = true)
    List<TransferEntity> findByRequestConditions(String channelId, String categoryCode, String startDate, String endDate, String globalId, String guId, String txnRefId, boolean globalIdCondition, boolean guIdCondition, boolean txnRefIdCondition);
}
