package com.qnb.bo.backofficeservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.TxnCategoryEntity;

@Repository
public interface TxnCategoryRepository extends JpaRepository<TxnCategoryEntity, Integer> {

	@Query(value = "SELECT RR_AUDIT_REQ from OCS_T_FO_CATEGORY_CODE WHERE CATEGORY_CODE=?1", nativeQuery = true)
	public String getAuditRequired(String categoryCode);
	
}
