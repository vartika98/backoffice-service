package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.TxnEntitlement;

@Repository
public interface TxnEntitlementRepository extends JpaRepository<TxnEntitlement, Integer> {

	List<TxnEntitlement> findByChannelIdAndUnitId(String channelId, String unitId);
	
	@Query(value="SELECT MAX(e.DISP_PRIORITY) FROM TxnEntitlement e",nativeQuery = true)
    int findMaxNumericField(); 
	
	@Query(value = "SELECT txn.* from OCS_T_TXN_ENTITLEMENT txn ,OCS_T_MENU_MASTER menu "
			+ "where txn.MENU_ID = menu.MENU_ID AND menu.parent_menu_id is NULL "
			+ "AND txn.unit_id=:unitId and txn.channel_id =:channelId AND menu.status='ACT' "
			+ "ORDER BY menu.MENU_ID", nativeQuery = true)
	List<TxnEntitlement> menuList(@Param("unitId") String unitId, @Param("channelId") String channelId);
	
	@Query(value = "SELECT txn.* from OCS_T_TXN_ENTITLEMENT txn ,OCS_T_MENU_MASTER menu "
			+ "where txn.MENU_ID = menu.MENU_ID AND menu.parent_menu_id=:menuId "
			+ "AND txn.unit_id=:unitId and txn.channel_id =:channelId AND menu.status='ACT' "
			+ "ORDER BY menu.MENU_ID", nativeQuery = true)
	List<TxnEntitlement> subMenuList(@Param("unitId") String unitId, @Param("channelId") String channelId,@Param("menuId") String menuId);

	@Query(value = "SELECT txn.* from OCS_T_TXN_ENTITLEMENT txn ,OCS_T_MENU_MASTER menu "
			+ "where txn.MENU_ID = menu.MENU_ID AND menu.parent_menu_id=:subMenuId "
			+ "AND txn.unit_id=:unitId and txn.channel_id =:channelId AND menu.status='ACT' "
			+ "ORDER BY menu.MENU_ID", nativeQuery = true)
	List<TxnEntitlement> subMenu2List(@Param("unitId") String unitId, @Param("channelId") String channelId,@Param("subMenuId") String subMenuId);

}
