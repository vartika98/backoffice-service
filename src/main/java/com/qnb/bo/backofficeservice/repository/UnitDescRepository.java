package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.qnb.bo.backofficeservice.entity.UnitDesc;

public interface UnitDescRepository extends JpaRepository<UnitDesc, Integer> {

	@Query(value = "select DISTINCT lang_code from ocs_t_unit_language where unit_id=:unitId",nativeQuery = true)
	List<String> getDistinctLangCode(@Param("unitId") String unitId);

	List<UnitDesc> findByUnitIdAndStatusInOrderByCreatedTimeDesc(String string, List<String> status);

	List<UnitDesc> findByUnitIdAndStatusIn(String unitId, List<String> status);

	List<UnitDesc> findByUnitId(String unitId);

	
}
