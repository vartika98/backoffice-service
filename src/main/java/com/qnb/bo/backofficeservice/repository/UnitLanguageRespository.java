package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.UnitLanguage;

@Repository
public interface UnitLanguageRespository extends JpaRepository<UnitLanguage, String> {

	List<UnitLanguage> findByUnit_UnitIdAndChannel_ChannelId(String unit, String channel);

	@Query(value = "select DISTINCT lang_code from ocs_t_unit_language where unit_id=:unitId",nativeQuery = true)
	List<String> getDistinctLangCode(@Param("unitId") String unitId);

	List<UnitLanguage> findAllByStatusIn(List<String> Status);

	List<UnitLanguage> findAllByStatusInOrderByCreatedTimeDesc(List<String> status);

	List<UnitLanguage> findByStatusIn(List<String> status);

	List<UnitLanguage> findByUnit_UnitIdAndChannel_ChannelIdAndStatusIn(String unitId, String channelId, List<String> of);

}
