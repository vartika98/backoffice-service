package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.qnb.bo.backofficeservice.entity.Unit;


@Repository
public interface UnitRespository extends JpaRepository<Unit, Integer> {

	
	List<Unit> findByStatus(@Param("status") String status);

	Unit findByUnitId(String unitId);

	List<Unit> findAllByStatusInOrderByCreatedTimeDesc(List<String> status);

}
