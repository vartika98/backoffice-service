package com.qnb.bo.backofficeservice.repository;

import com.qnb.bo.backofficeservice.entity.UrlProvider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UrlProviderRepository extends JpaRepository<UrlProvider, Long> {

}
