package com.qnb.bo.backofficeservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.UserAuthEntity;

 

@Repository
public interface UserAuthRepository extends JpaRepository<UserAuthEntity, String>{
	
	UserAuthEntity findByUserId(String userId);
 
	UserAuthEntity findByUserIdAndPassword(String userId, String password);
}
