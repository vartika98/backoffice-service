package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.GroupDefEntity;
import com.qnb.bo.backofficeservice.entity.UserGroupsMembershipEntity;

@Repository
public interface UserGroupMembershipRepository extends JpaRepository<UserGroupsMembershipEntity, String> {

    List<UserGroupsMembershipEntity> findByUser_UserId(@Param("userId") String userId);

    Boolean existsByGroupId(GroupDefEntity groupDefEntity);

}
