package com.qnb.bo.backofficeservice.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.UserEntity;
import com.qnb.bo.backofficeservice.enums.UserStatus;


@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {

	Boolean existsByUserId(String userId);

	UserEntity findByUserId(String id);

	UserEntity findByUserIdAndUserStatus(String id, String userStatus);

	UserEntity findByUserIdAndUserStatusAndExpiryDateGreaterThan(@Param("userId") String userId,
			@Param("status") UserStatus status, @Param("currentTime") Date currentTime);

	UserEntity findByUserIdAndStatus(String username, String status);

}
