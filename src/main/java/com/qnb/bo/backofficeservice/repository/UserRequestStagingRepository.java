package com.qnb.bo.backofficeservice.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.UserRequestStagingEntity;


@Repository
public interface UserRequestStagingRepository extends JpaRepository<UserRequestStagingEntity, String>{

	Set<UserRequestStagingEntity> findByRequestIdInOrderByCreatedTimeAsc(List<String> requestIdLst);

}
