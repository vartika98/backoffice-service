package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.UserRoleMapping;
import com.qnb.bo.backofficeservice.views.BoMenuAccessView;
@Repository
public interface UserRoleMappingRepository extends JpaRepository<UserRoleMapping, Integer>{

	UserRoleMapping findByGroupName(String groupName);

	boolean existsByUserId(String userId);

	List<UserRoleMapping> findByUserId(String userId);

	boolean existsByUserIdAndGroupNameAndUserRoleLevelAndGroupCode(String userId, String groupName, String roleLevel,
			String groupCode);

	void deleteByUserId(String userId);

	@Query(value = "SELECT "
			+ "t4.PRODUCT_CODE AS prodcode,"
			+ "t4.PRODUCT_DESC AS proddesc,  "
			+ "t5.SUB_PRODUCT_CODE AS subprodcode,"
			+ "t5.sub_product_desc AS subprodcodedesc,  "
			+ "t6.function_code AS functioncode, "
			+ "t6.function_desc AS functioncodedesc, "
			+ "t5.sub_product_url AS subProductUrl "
			+ "FROM   bko_ocs_t_product_master   t4  "
			+ "JOIN bko_ocs_sub_product_master t5 ON t4.PRODUCT_CODE = t5.product_code  "
			+ "JOIN bko_ocs_function_master t6 ON t5.SUB_PRODUCT_CODE = t6.sub_product_code "
			+ "ORDER BY  t4.periority,t5.periority ASC", nativeQuery = true)
	List<BoMenuAccessView> getAccessibleMenusTest(@Param("userId") String userId, @Param("status") String status);

	
	@Query(value = "SELECT "
			+ "    t1.prod_code        AS prodcode,"
			+ "    t4.product_desc     AS proddesc,"
			+ "    t1.sub_prod_code    AS subprodcode,"
			+ "    t5.sub_product_desc AS subprodcodedesc,"
			+ "    t1.function_code    AS functioncode,"
			+ "    t6.function_desc    AS functioncodedesc,"
			+ "    t5.sub_product_url AS subProductUrl"
			+ "    FROM"
			+ "         bko_ocs_t_group_product_mapping t1"
			+ "    JOIN bko_user_roles_mapping     t2 ON t1.group_name = t2.user_group_code"
			+ "    JOIN bko_ocs_t_group_master     t3 ON t2.user_group_code = t3.group_name"
			+ "    JOIN bko_ocs_t_product_master   t4 ON t1.prod_code = t4.product_code "
			+ "    JOIN bko_ocs_sub_product_master t5 ON t1.sub_prod_code = t5.sub_product_code"
			+ "    JOIN bko_ocs_function_master    t6 ON t1.function_code = t6.function_code"
			+ "                                       AND t1.prod_code = t6.product_code"
			+ "                                       AND t1.sub_prod_code = t6.sub_product_code"
			+ "		WHERE"
			+ "     t2.user_id=:userId "
			+ "    	AND t3.status=:status"
			+ "    	ORDER BY  t4.periority,t5.periority ASC", nativeQuery = true)
	List<BoMenuAccessView> getAccessibleMenus(@Param("userId") String userId, @Param("status") String status);

	UserRoleMapping findByGroupNameAndUserId(String groupName, String userId);
}
