package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.qnb.bo.backofficeservice.entity.UserUnitsMembershipEntity;

public interface UserUnitMembershipRepository extends JpaRepository<UserUnitsMembershipEntity, String> {

	@Query(value = "Select UNIT_ID from BKO_T_USER_UNIT_MEMBERSHIP where USER_ID =:userId", nativeQuery = true)
	List<String> getUnit(@Param("userId") String userId);
}