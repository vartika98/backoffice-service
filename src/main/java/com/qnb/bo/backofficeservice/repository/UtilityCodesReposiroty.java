package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.UtilityCodesEntity;
import com.qnb.bo.backofficeservice.views.BillPayUtilityCodes;
import com.qnb.bo.backofficeservice.views.BillPayUtilityView;

@Repository
public interface UtilityCodesReposiroty extends JpaRepository<UtilityCodesEntity, Long> {

	List<UtilityCodesEntity> findByPayeeIdAndStatus(String payeeId, String status);

	@Query(value = "SELECT t1.UTILITY_CODE as UtilityCode, t1.UTILITY_TYPE as UtilityType,t1.UTILITY_DESC as UtilityDesc, t3.UTILITY_DESC as UtilityTypeDesc, t1.PARTIAL_PAYMENT as PartialPayment, t1.ADD_PAYEE_CHECK as AddPayeeCheck, t2.DENOMINATION as Denomination "
			+ "FROM OCS_T_UTILITY_CODES t1 "
			+ "LEFT JOIN OCS_T_UTILITY_PREPAID_DENOMINATIONS t2 ON t1.UTILITY_CODE = t2.UTILITY_CODE "
			+ "LEFT JOIN OCS_T_UTILITY_TYPES t3 ON t1.UTILITY_TYPE = t3.UTILITY_TYPE "
			+ "WHERE t1.PAYEE_ID=:payeeId AND t1.STATUS=:status ORDER BY t1.DATE_MODIFIED DESC", nativeQuery = true)
	List<BillPayUtilityView> findByPayeeAndStatus(@Param("payeeId") String payeeId, @Param("status") String status);
	
	UtilityCodesEntity findByUtilityCodeAndStatusIn(String utilityCode, List<String> status);
	
	@Query(value = "select t1.utility_code as utilityCode, t1.utility_desc as utilityCodeDesc, t1.payee_id as payeeId, "
			+ "t2.payee_desc as payeeDesc, t1.utility_type as utilityType, t3.utility_desc as utilityTypeDesc, "
			+ "t1.unit_code as unitCode, t4.unit_desc as unitCodeDesc, t1.utility_cmsn_fee as commissionFee, "
			+ "t1.status as status from ocs_t_utility_codes t1 left join ocs_t_utility_payees t2 on "
			+ "t1.payee_id = t2.payee_id left join "
			+ "ocs_t_utility_types t3 on t1.utility_type = t3.utility_type left join "
			+ "ocs_t_unit_master t4 on t1.unit_code = t4.unit_id "
			+ "where t1.utility_type in :utilityType and t1.status in ('ACT', 'IAC') order by t1.date_modified desc", nativeQuery = true)
	List<BillPayUtilityCodes> findByUtilityType(@Param("utilityType") List<String> utilityType);
}
