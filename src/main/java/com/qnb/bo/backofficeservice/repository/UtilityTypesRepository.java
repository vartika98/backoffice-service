package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.UtilityTypesEntity;

@Repository
public interface UtilityTypesRepository extends JpaRepository<UtilityTypesEntity, Long>{

	List<UtilityTypesEntity> findByStatusInOrderByDateModifiedDesc(List<String> status);
	
	UtilityTypesEntity findByUtilityTypeAndStatusIn(String utilityType, List<String> status);
	
	@Query(value = "SELECT DISTINCT UTILITY_TYPE FROM OCS_T_UTILITY_TYPES WHERE STATUS IN ('ACT', 'IAC') ", nativeQuery = true)
	List<String> fetchAllUtilityTypes();
}
