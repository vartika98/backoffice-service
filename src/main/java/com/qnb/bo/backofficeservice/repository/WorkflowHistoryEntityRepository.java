package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.WorkflowHistoryEntityOCS;

@Repository
@EnableJpaRepositories
public interface WorkflowHistoryEntityRepository extends JpaRepository<WorkflowHistoryEntityOCS, Integer>{

	WorkflowHistoryEntityOCS findByRuleIdAndStatusIn(String ruleId, List<String> status);

	WorkflowHistoryEntityOCS findByRefNo(Integer refNo);

}
