package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.workflow.WorkflowHistoryEntity;


@Repository
public interface WorkflowHistoryRepository extends JpaRepository<WorkflowHistoryEntity, String> {
	

	WorkflowHistoryEntity findByWorkflowIdAndTaskId(String workFlowId, String taskId);

	List<WorkflowHistoryEntity> findByWorkflowIdOrderBySeqNumber(String workflowId);
}
