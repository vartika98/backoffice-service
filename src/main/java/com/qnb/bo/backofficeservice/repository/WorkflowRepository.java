package com.qnb.bo.backofficeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.bo.backofficeservice.entity.workflow.WorkflowEntity;
import com.qnb.bo.backofficeservice.enums.ProcessFlow;
import com.qnb.bo.backofficeservice.enums.WorkflowStatus;

@Repository
public interface WorkflowRepository extends JpaRepository<WorkflowEntity, String> {

	List<WorkflowEntity> findByRequestIdInOrderByCreatedTimeAsc(List<String> requestIds);

	List<WorkflowEntity> findByInitiatedGroupInAndProcessIdInAndWorkflowStatusInOrderByCreatedTimeAsc(
			List<String> roleList, List<String> userProcessId, List<WorkflowStatus> workflowStatus);

	int countByInitiatedGroupInAndProcessIdInAndWorkflowStatusIn(List<String> roleList, List<String> userProcessId,
			List<WorkflowStatus> status);

	@Query(value = "SELECT COUNT(*) FROM BKO_T_WORKFLOW e "
			+ "WHERE e.LAST_MODIFIED_TIME BETWEEN :startDate AND :endDate " + "AND e.INITIATED_GROUP IN (:roleList) "
			+ "AND e.PROCESS_ID IN (:userProcessIds)" + "AND e.WORKFLOW_STATUS IN (:status)", nativeQuery = true)
	int countByInitiatedGroupAndProcessIdAndWorkflowStatusAndLastModifiedDateBetween(
			@Param("startDate") String startDate, 
			@Param("endDate") String endDate,
			@Param("roleList") List<String> roleList,
			@Param("userProcessIds") List<String> userProcessIds,
			@Param("status") List<String> statusStrings);

}
