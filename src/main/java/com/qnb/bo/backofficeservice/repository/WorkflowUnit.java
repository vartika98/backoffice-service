package com.qnb.bo.backofficeservice.repository;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import com.qnb.bo.backofficeservice.entity.workflow.WorkflowAuditEntity;

import lombok.Data;

@Data
@Entity
@Table(name = "BKO_T_WORKFLOW_UNIT")
public class WorkflowUnit extends WorkflowAuditEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "WORKFLOW_ID")
	private String workflowId;

	@Column(name = "UNIT_ID")
	private String unitId;
}
