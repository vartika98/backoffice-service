package com.qnb.bo.backofficeservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface WorkflowUnitRepository extends JpaRepository<WorkflowUnit, String>{

	WorkflowUnit findByWorkflowId(String workflowId);

}
