package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.ProductDTO;
import com.qnb.bo.backofficeservice.entity.Product;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface AddProductService {

	GenericResponse<Product> addProduct(List<ProductDTO> addproductRequest);

	GenericResponse<List<Product>> searchProduct(List<ProductDTO> addproductRequest);

	GenericResponse<List<Map<String,String>>> searchProdProduct();

}
 