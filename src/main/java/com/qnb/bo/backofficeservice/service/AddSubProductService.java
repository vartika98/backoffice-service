package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.ProductDTO;
import com.qnb.bo.backofficeservice.entity.SubProduct;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface AddSubProductService {

	GenericResponse<SubProduct> addSubProduct(List<ProductDTO> addproductRequest);

	GenericResponse<List<SubProduct>> searchSubProduct(List<ProductDTO> addproductRequest);

}
