package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.ApplicationMaintenance.ApplicationResponse;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface ApplicationMaintenanceService {

	GenericResponse<List<ApplicationResponse>> getApplicationList(List<String> units, String status);
}
