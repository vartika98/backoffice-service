package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.EmpIdAndCustSegDTO;
import com.qnb.bo.backofficeservice.entity.CfmsParamEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface ApplyForProductService {

	GenericResponse<EmpIdAndCustSegDTO> createApplyForProduct(List<EmpIdAndCustSegDTO> empAndCustSegRequest, String reqProd,
			String custServprod);

	GenericResponse<EmpIdAndCustSegDTO> deleteApplyForProduct(long id);

	GenericResponse<List<CfmsParamEntity>> searchApplyForProduct(String reqProd, String custServprod);

	GenericResponse<EmpIdAndCustSegDTO> getApplyForProduct(long id);

	GenericResponse<EmpIdAndCustSegDTO> updateApplyForProduct(EmpIdAndCustSegDTO empAndCustSegRequest);

}
