package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.EmpIdAndCustSegDTO;
import com.qnb.bo.backofficeservice.dto.SearchRequest;
import com.qnb.bo.backofficeservice.entity.CfmsParamEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface ApplyForSubProductService {

	GenericResponse<EmpIdAndCustSegDTO> createApplyForSubProduct(List<EmpIdAndCustSegDTO> empAndCustSegRequest,
			String appprod, String subprod);

	GenericResponse<List<CfmsParamEntity>> searchApplyForSubProduct(String typeOfproduct, String subprod,
			String appprod);

	GenericResponse<EmpIdAndCustSegDTO> updateApplyForSubProduct(EmpIdAndCustSegDTO empAndCustSegRequest);

	GenericResponse<List<EmpIdAndCustSegDTO>> searchByUnitApplyForSubProduct(SearchRequest unitId, String custServprod,
			String reqProd);

}
