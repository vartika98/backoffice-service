package com.qnb.bo.backofficeservice.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.GraphDateCountDto;
import com.qnb.bo.backofficeservice.dto.audit.AuditListRequest;
import com.qnb.bo.backofficeservice.dto.audit.AuditListResponse;
import com.qnb.bo.backofficeservice.model.GenericResponse;


public interface AuditService {

	GenericResponse<List<AuditListResponse>> getAuditList(AuditListRequest auditRequest);

	GenericResponse<Map<String,String>> getExcelBoAuditReport(AuditListRequest auditRequest) throws IOException;

	GenericResponse<List<Map<String, String>>> getAuditDetailsList(Map<String, String> request);

	GenericResponse<GraphDateCountDto> getAuditCountByDateRange(AuditListRequest auditRequest);

	GenericResponse<GraphDateCountDto> getLoginCountByConfig();
}
