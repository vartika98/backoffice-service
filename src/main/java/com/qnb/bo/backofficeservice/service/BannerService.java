package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.banner.BannerReqDto;
import com.qnb.bo.backofficeservice.dto.banner.BannerResponse;
import com.qnb.bo.backofficeservice.dto.banner.ScreenIdDropdownDto;
import com.qnb.bo.backofficeservice.entity.BannerDetailsEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface BannerService {

	public GenericResponse<List<BannerResponse>> selectBanner(String unit, String channel, String langCode, String screenId,
			String imgResol, String disPriority);
	
	public GenericResponse<List<ScreenIdDropdownDto>> selectScreenId(String unit, String channel);
	
	public GenericResponse<List<BannerDetailsEntity>> manageBanner(List<BannerReqDto> reqList);
	
}
