package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.BeneficiaryDetailRequest;
import com.qnb.bo.backofficeservice.entity.BeneficiaryEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface BeneficiaryService {

	GenericResponse<List<BeneficiaryEntity>> getBeneficiaryListForApproval(BeneficiaryDetailRequest request);

	GenericResponse<BeneficiaryEntity> getBeneficiaryData(BeneficiaryDetailRequest request);

	GenericResponse<BeneficiaryEntity> getBeneficiaryApproveReject(BeneficiaryDetailRequest request);

	

}
