package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.billPay.DenominationDto;
import com.qnb.bo.backofficeservice.dto.billPay.ManagePayeeDto;
import com.qnb.bo.backofficeservice.dto.billPay.ManageUtilityCodesDto;
import com.qnb.bo.backofficeservice.dto.billPay.ManageUtilityTypesDto;
import com.qnb.bo.backofficeservice.dto.billPay.PayeeDto;
import com.qnb.bo.backofficeservice.dto.billPay.UtilityCodesDto;
import com.qnb.bo.backofficeservice.dto.billPay.UtilityDto;
import com.qnb.bo.backofficeservice.dto.billPay.UtilityTypesDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface BillPayService {

	GenericResponse<List<PayeeDto>> getPayees();

	GenericResponse<List<PayeeDto>> managePayees(List<ManagePayeeDto> dtolist);

	GenericResponse<List<UtilityDto>> getUtilities(String payeeId);
	
	GenericResponse<List<UtilityCodesDto>> getUtilitiesByTypes(String utilityType);

	GenericResponse<List<UtilityDto>> manageUtilityCodes(List<ManageUtilityCodesDto> dtolist);

	GenericResponse<List<UtilityTypesDto>> getUtilityTypes();

	GenericResponse<List<UtilityTypesDto>> manageUtilityTypes(List<ManageUtilityTypesDto> dtoList);

	GenericResponse<List<DenominationDto>> getDenominations(String utilityCode);

	GenericResponse<List<DenominationDto>> manageDenominations(List<DenominationDto> dtoList);
}
