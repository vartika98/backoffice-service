package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.boMenu.AccessibleMenus;
import com.qnb.bo.backofficeservice.dto.boMenu.BoMenuDto;
import com.qnb.bo.backofficeservice.dto.boMenu.BoMenuEntitlementRes;
import com.qnb.bo.backofficeservice.dto.boMenu.ProductResDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface BoMenuEntitlementService {

	GenericResponse<List<BoMenuDto>> getMenuByUser(String userNo);

	GenericResponse<List<BoMenuEntitlementRes>> getMenuEntitlement();
	
	GenericResponse<List<ProductResDto>> getProductSubProduct();

	GenericResponse<List<Map<String, String>>> productList();

	GenericResponse<List<Map<String, String>>> subProdList(Map<String, String> req);

	GenericResponse<List<Map<String, String>>> functionList(Map<String, String> req);
	
	GenericResponse<List<AccessibleMenus>> getAccessibleMenus(Map<String, String> req);
}
