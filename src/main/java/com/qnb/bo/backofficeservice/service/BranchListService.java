package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.branch.BranchListDTO;
import com.qnb.bo.backofficeservice.dto.branch.BranchListRequest;
import com.qnb.bo.backofficeservice.dto.branch.BranchParameterRequest;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface BranchListService {

	GenericResponse<List<BranchListDTO>> getBranchList(BranchListRequest branchReq);

	GenericResponse<Map<String, String>> manageBranches(BranchListRequest reqBody);

}
