package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.CategoryDetailsDto;
import com.qnb.bo.backofficeservice.entity.CategoryDetails;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface CategoryCodeDetailsService {

	GenericResponse<List<CategoryDetails>> updateCategoryDetails(List<CategoryDetailsDto> categoryDetailsDto);

	GenericResponse<List<Map<String, String>>> categoryCodeList();

	GenericResponse<List<CategoryDetails>> summaryList(Map<String, String> request);

}
