package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.CategoryMasterDto;
import com.qnb.bo.backofficeservice.dto.categoryCode.CategoryCodeDto;
import com.qnb.bo.backofficeservice.entity.CategoryMaster;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface CategoryCodeMasterService {

	GenericResponse<List<CategoryMaster>> getCategoryCodeMasterList();

	GenericResponse<List<CategoryMaster>> updateCategoryMaster(List<CategoryMasterDto> categoryMasterDto);

	GenericResponse<List<CategoryCodeDto>> getCategoryCodes();

}
