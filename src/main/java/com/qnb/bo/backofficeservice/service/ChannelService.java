package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.channel.ChannelListRes;
import com.qnb.bo.backofficeservice.dto.channel.ManageChannelReqDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface ChannelService {


	GenericResponse<ChannelListRes> getAllChannels(String defaultStatus);

	GenericResponse<String> manageChannels(List<ManageChannelReqDto> manageChannelReqDtoList);
	
	GenericResponse<ChannelListRes> getAllChannelsSummary();
}