package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.enums.WorkflowStatus;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface CountService {

	GenericResponse<Map<String, Map<String, Integer>>> getUserCountRequest(List<String> userGroup,
			List<WorkflowStatus> status);

	GenericResponse<Map<String, Map<String, Integer>>> getCountBYMonthRequest(List<String> userGroup,
			List<WorkflowStatus> status, int lastMonths);

	GenericResponse<Map<String, Map<String, Integer>>> getAllCountRequestByMonth(List<String> userGroup,
			List<WorkflowStatus> status, int lastMonths);

}
