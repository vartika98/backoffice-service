package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.country.CountryDetails;
import com.qnb.bo.backofficeservice.dto.country.ManagenCntryReqDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface CountryService {

	GenericResponse<Map<String, String>> saveCountry(ManagenCntryReqDto requestBody);

	GenericResponse<List<CountryDetails>> cntryList();

}
