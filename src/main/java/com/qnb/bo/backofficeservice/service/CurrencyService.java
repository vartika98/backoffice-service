package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.currency.CurrencyDetails;
import com.qnb.bo.backofficeservice.dto.currency.CurrencyDto;
import com.qnb.bo.backofficeservice.dto.currency.ManageCurrReqDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface CurrencyService {

	GenericResponse<List<CurrencyDetails>> currencyLst(Map<String, String> reqBody);

	GenericResponse<Map<String, String>> manageCurrency(List<ManageCurrReqDto> reqBody);

}
