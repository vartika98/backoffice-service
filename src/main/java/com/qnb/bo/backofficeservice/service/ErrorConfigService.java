package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.ErrorConfigDto;
import com.qnb.bo.backofficeservice.dto.ErrorConfigResponse;
import com.qnb.bo.backofficeservice.dto.ErrorConfigServiceType;
import com.qnb.bo.backofficeservice.dto.errorConfig.ErrorConfigRequest;
import com.qnb.bo.backofficeservice.dto.errorConfig.ErrorConfigServiceTypeResponse;
import com.qnb.bo.backofficeservice.entity.ErrorConfig;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface ErrorConfigService {

//	GenericResponse<List<ErrorConfigDto>> getList(Map<String, String> request);

	GenericResponse<List<ErrorConfig>> updateErrorConfigs(List<ErrorConfigDto> requests);

	GenericResponse<ErrorConfigServiceTypeResponse> getServiceTypes(ErrorConfigRequest errReq);

	GenericResponse<List<ErrorConfigResponse>> getErrorConfigList(ErrorConfigRequest errReq);

}
