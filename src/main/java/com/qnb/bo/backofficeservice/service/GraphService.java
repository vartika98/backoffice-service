package com.qnb.bo.backofficeservice.service;

import com.qnb.bo.backofficeservice.dto.ConfigBasedCategoryRequest;
import com.qnb.bo.backofficeservice.dto.GraphCategoryCodeCountDto;
import com.qnb.bo.backofficeservice.dto.GraphDateCountDto;
import com.qnb.bo.backofficeservice.dto.GraphDataDto;
import com.qnb.bo.backofficeservice.dto.audit.AuditListRequest;
import com.qnb.bo.backofficeservice.dto.audit.SummaryRequest;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface GraphService {

    GenericResponse<GraphDataDto> getAuditDataByDateRange(String entityName, SummaryRequest request);

    GenericResponse<GraphDateCountDto> getEntityCountByDateRange(String graphName, AuditListRequest auditRequest);

    GenericResponse<GraphCategoryCodeCountDto> getConfigBasedTransferCount(String graphName, ConfigBasedCategoryRequest configBasedCategoryRequest);
}
