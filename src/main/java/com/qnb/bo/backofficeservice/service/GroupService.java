package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.userGroup.CreateGroupReq;
import com.qnb.bo.backofficeservice.dto.userGroup.GroupSummary;
import com.qnb.bo.backofficeservice.entity.BOGroupEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.GenericResult;

public interface GroupService {

	GenericResponse<List<BOGroupEntity>> groupList();

	GenericResponse<List<GroupSummary>> userGroupSummary(Map<String, String> reqBody);

	GenericResult createGroup(List<CreateGroupReq> reqBody);


}
