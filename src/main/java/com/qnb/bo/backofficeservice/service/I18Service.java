package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.txn.LabelListRes;
import com.qnb.bo.backofficeservice.dto.txn.ManageLabelReqDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;


public interface I18Service {

	GenericResponse<LabelListRes> labelList(Map<String, String> reqBody);

	GenericResponse<Map<String, String>> manageLabels(List<ManageLabelReqDto> reqBody);

	GenericResponse<Map<String, String>> getLabelByLang(String unit,String channel,String lang,String screenId);

	GenericResponse<byte[]> i18ToExcel(Map<String, String> reqBody);
}
