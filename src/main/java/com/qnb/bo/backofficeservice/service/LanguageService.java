package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.unit.LanguageDto;
import com.qnb.bo.backofficeservice.entity.Language;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface LanguageService {

	GenericResponse<List<LanguageDto>> languageList(Map<String, String> reqBody);
	GenericResponse<List<Language>> manageLang(List<LanguageDto>dto);
	GenericResponse<List<LanguageDto>> languageSumarry();
	

}
