package com.qnb.bo.backofficeservice.service;

import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface LoginService {

	public GenericResponse<String> userAuthentication(String userId, String password);

	public  Long getActiveUserCount();
}
