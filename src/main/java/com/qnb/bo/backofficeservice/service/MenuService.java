package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.txn.ManageMenuReqDto;
import com.qnb.bo.backofficeservice.dto.txn.MenuI18ReqDto;
import com.qnb.bo.backofficeservice.dto.txn.MenuListRes;
import com.qnb.bo.backofficeservice.dto.txn.MenuListResponse;
import com.qnb.bo.backofficeservice.dto.txn.TransactionEntitlementResponse;
import com.qnb.bo.backofficeservice.entity.ScreenEntitlement;
import com.qnb.bo.backofficeservice.model.GenericResponse;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MenuService {

	GenericResponse<MenuListRes> moduleList(Map<String, String> reqBody);

	GenericResponse<Map<String, String>> manageMenus(ManageMenuReqDto reqBody);

	GenericResponse<List<TransactionEntitlementResponse>> getTxnEntitlementList(Map<String, String> reqBody);

	GenericResponse<List<Map<String, String>>> menusWithTranslation(Map<String, String> reqBody);

	GenericResponse<Map<String, String>> manageMenuTranslation(List<MenuI18ReqDto> reqBody);

	GenericResponse<List<Map<String, String>>> subMenusWithTranslation(Map<String, String> reqBody);

	GenericResponse<Map<String, String>> manageSubMenuTranslation(List<MenuI18ReqDto> reqBody);

	GenericResponse<List<Map<String, String>>> subMenus2WithTranslation(Map<String, String> reqBody);

	GenericResponse<Map<String, List<Map<String, String>>>> getScreenEntitlementListMb(Map<String, String> reqBody);

	Flux<GenericResponse<Map<String, List<Map<String, String>>>>> getScreenEntitlementListReact(Map<String, String> reqBody);

	GenericResponse<List<MenuListResponse>> getAllMenuList(Map<String, String> reqBody);
	
	GenericResponse<Map<String, String>> manageSubMenu2Translation(List<MenuI18ReqDto> reqBody);

	GenericResponse<List<String>> priorityList(Map<String, String> reqBody);
}
