package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.MessageMaintenance.MaintenanceListResponse;
import com.qnb.bo.backofficeservice.dto.MessageMaintenance.MaintenanceMessage;
import com.qnb.bo.backofficeservice.entity.MaintenanceMessageEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface MessageMaintenanceService {

	GenericResponse<MaintenanceListResponse> getMaintenanceList(String msgType);
	
	GenericResponse<List<MaintenanceMessageEntity>> manageMaintenance(List<MaintenanceMessage> req);
}
