package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.NotificationParamConfigDTO;
import com.qnb.bo.backofficeservice.model.GenericResponse;




public interface NotificationParamConfigService {

	GenericResponse<List<NotificationParamConfigDTO>> getSummary();

	GenericResponse<List<NotificationParamConfigDTO>> manageNotifications(List<NotificationParamConfigDTO> request);
	

}
