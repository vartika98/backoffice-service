package com.qnb.bo.backofficeservice.service;

import com.qnb.bo.backofficeservice.dto.notification.NotificationDto;
import com.qnb.bo.backofficeservice.enums.NotificationStatus;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface NotificationService {

	GenericResponse<NotificationStatus> sendNotification(NotificationDto message);

}
