package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.PageConfigDto;
import com.qnb.bo.backofficeservice.entity.PageConfig;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface PageConfigService {

	GenericResponse<List<PageConfigDto>> getList(Map<String, String> request);

	GenericResponse<List<PageConfig>> updatePageConfigs(List<PageConfigDto> errorConfigDto);

	GenericResponse<List<Map<String, String>>> getScreenId(Map<String, String> request);

}
