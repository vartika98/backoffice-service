package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.categoryCode.CategoryCodeDto;
import com.qnb.bo.backofficeservice.dto.parameter.ManageParamReqDto;
import com.qnb.bo.backofficeservice.dto.parameter.ParameterRes;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.GenericResult;

public interface ParameterMaintenanceService {

	GenericResponse<List<ParameterRes>> parameterList(Map<String, String> reqBody);

	GenericResult manageParameter(List<ManageParamReqDto> reqBody);

	GenericResponse<List<CategoryCodeDto>> getDistinctParamConfig(String key);
}
