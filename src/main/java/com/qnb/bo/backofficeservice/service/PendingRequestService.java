package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.pendinRequest.PendingReq;
import com.qnb.bo.backofficeservice.dto.pendinRequest.PendingReqResponse;
import com.qnb.bo.backofficeservice.dto.pendinRequest.WorkFlowRequest;
import com.qnb.bo.backofficeservice.dto.role.RoleDetailsRes;
import com.qnb.bo.backofficeservice.dto.user.UserDetailsRes;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.GenericResult;

public interface PendingRequestService {

	GenericResponse<List<PendingReqResponse<UserDetailsRes>>> userList(PendingReq reqBody);

	GenericResponse<List<PendingReqResponse<RoleDetailsRes>>> roleList(PendingReq reqBody);
	
    GenericResponse<GenericResult> pendingRequest(WorkFlowRequest workFlowRequest); 
	
	

}
