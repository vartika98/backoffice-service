package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.mbAudit.ReqDto;
import com.qnb.bo.backofficeservice.dto.mbAudit.ResponseDto;
import com.qnb.bo.backofficeservice.dto.mbAudit.ResponseDtoMB;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface RRAuditService {
	
	public GenericResponse<List<ResponseDtoMB>> getSummary(ReqDto req);
	
	public GenericResponse<List<ResponseDto>> getSummaryByChannel(ReqDto req);

}
