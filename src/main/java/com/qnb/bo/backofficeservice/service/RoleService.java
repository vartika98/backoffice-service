package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.role.RoleDto;
import com.qnb.bo.backofficeservice.dto.role.RoleHierarchiRequest;
import com.qnb.bo.backofficeservice.dto.role.UserRoleDto;
import com.qnb.bo.backofficeservice.entity.UserRole;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface RoleService {


	GenericResponse<List<UserRole>> crateRole(RoleHierarchiRequest request);

	GenericResponse<List<UserRoleDto>> roleList(Map<String, String> request);

}
