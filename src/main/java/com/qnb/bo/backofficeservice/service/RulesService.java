package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.rules.RulesDto;
import com.qnb.bo.backofficeservice.dto.rules.RulesSummaryResDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface RulesService {

	GenericResponse<String> manageRule(List<RulesDto> dto);

	GenericResponse<List<RulesSummaryResDto>> getSummary();
}
