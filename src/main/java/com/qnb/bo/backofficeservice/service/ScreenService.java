package com.qnb.bo.backofficeservice.service;

import java.util.Map;

import com.qnb.bo.backofficeservice.dto.txn.MenuListRes;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface ScreenService {

	GenericResponse<MenuListRes> screenList(Map<String, String> reqBody);

}
