package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.sessionManagement.ActiveUsersResponse;
import com.qnb.bo.backofficeservice.dto.sessionManagement.SessionReqDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface SessionManagementService {

	public GenericResponse<List<ActiveUsersResponse>> getActiveSessions();
	
	public GenericResponse<Map<String, Integer>> getLoginCount(SessionReqDto req);
}
