package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.SurveyRequestDTO;
import com.qnb.bo.backofficeservice.dto.SurveySearchRequest;
import com.qnb.bo.backofficeservice.entity.ParamConfig;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface SurveyService {
	
	GenericResponse<SurveyRequestDTO> createSurveyMessage(SurveyRequestDTO surveyRequest);

	GenericResponse<SurveyRequestDTO> deleteSurveyMessage(SurveyRequestDTO id);

	GenericResponse<List<SurveyRequestDTO>> searchSurveyMessage(SurveySearchRequest surveySearchRequest);

	GenericResponse<SurveyRequestDTO> SurveyMessageByid(SurveyRequestDTO id);

	List<ParamConfig> getSegmentKeysByUnit(SurveyRequestDTO unitId);

	GenericResponse<SurveyRequestDTO> updateSurveyMessage(SurveyRequestDTO surveyRequest);
}
