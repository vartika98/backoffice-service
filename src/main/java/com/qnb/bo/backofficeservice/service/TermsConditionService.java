package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.TermsCondition.TermsAndConditionDto;
import com.qnb.bo.backofficeservice.dto.TermsCondition.TermsAndConditionRequest;
import com.qnb.bo.backofficeservice.dto.TermsCondition.TermsAndConditionResponse;
import com.qnb.bo.backofficeservice.dto.TermsCondition.TncResponse;
import com.qnb.bo.backofficeservice.entity.TermsAndCondition;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface TermsConditionService {

	GenericResponse<List<TncResponse>> getSummary(TermsAndConditionRequest tncReq);

	GenericResponse<List<TermsAndCondition>> manageTermsAndCondition(List<TermsAndConditionDto> tncDto);

	GenericResponse<Map<String, List<Map<String, String>>>> getModuleIdList(String unitId, String channelId);

	GenericResponse<Map<String, List<Map<String, String>>>> getSubModuleIdList(String unitId, String channelId,
			String moduleId);

}
