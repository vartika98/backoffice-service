package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.unit.UnitLanguageDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface UnitLanguageService {

	GenericResponse<Map<String, String>> saveUnitLanguage(List<UnitLanguageDto> requestBody);

	GenericResponse<List<UnitLanguageDto>> getSummaryUnitLanguage();


}
