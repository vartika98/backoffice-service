package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.unit.UnitDto;
import com.qnb.bo.backofficeservice.dto.unit.UnitListRes;
import com.qnb.bo.backofficeservice.entity.Unit;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface UnitService {

	GenericResponse<UnitListRes> getAllUnits(String unitStatus);

	GenericResponse<List<Unit>> manageUnit(List<UnitDto> unitRequest);

	GenericResponse<List<UnitDto>> unitSummary();

	
}
