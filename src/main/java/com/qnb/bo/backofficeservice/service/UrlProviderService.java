package com.qnb.bo.backofficeservice.service;

import com.qnb.bo.backofficeservice.dto.ApiRequest;
import com.qnb.bo.backofficeservice.dto.ApiResponse;
import com.qnb.bo.backofficeservice.entity.UrlProvider;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UrlProviderService {

    GenericResponse<List<UrlProvider>> getAllUrl();

    GenericResponse<ApiResponse> executeApi(ApiRequest request);
}
