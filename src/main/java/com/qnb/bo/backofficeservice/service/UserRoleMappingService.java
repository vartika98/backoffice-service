package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.Map;

import com.qnb.bo.backofficeservice.dto.GroupRoleDto;
import com.qnb.bo.backofficeservice.dto.userRole.UserRoleMappingDto;
import com.qnb.bo.backofficeservice.entity.UserEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;

public interface UserRoleMappingService {

	GenericResponse<List<UserEntity>> manageUser(List<UserRoleMappingDto> unitRequest);

	GenericResponse<List<UserRoleMappingDto>> userList();

	GenericResponse<List<GroupRoleDto>> groupList(Map<String, String> groupRequest);

}
