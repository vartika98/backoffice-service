package com.qnb.bo.backofficeservice.service;

import java.util.List;

import com.qnb.bo.backofficeservice.dto.workflow.WorkflowReqResponse;
import com.qnb.bo.backofficeservice.enums.ProcessFlow;
import com.qnb.bo.backofficeservice.enums.WorkflowStatus;

public interface WorkflowService {

	List<WorkflowReqResponse> getWorkflowDetails(List<WorkflowStatus> workflowStatus, List<String> userProcessId);

}
