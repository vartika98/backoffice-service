package com.qnb.bo.backofficeservice.service;

import java.util.List;
import java.util.stream.Collectors;

import com.qnb.bo.backofficeservice.utils.SHA3Hashing;

public interface Workflowable {
	 
	default List<String> getHashValue() {
		
		var identifier = getIdempotentRequestIdentifier();
		if (identifier != null && !identifier.isEmpty()) {
			return identifier.stream().map(SHA3Hashing::hashWithMessageDigest).collect(Collectors.toList());
		}
		return List.of();
	}
 
	List<String> getIdempotentRequestIdentifier();
	String getIdempotentRequestDetails();
 
}
