package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.ProductDTO;
import com.qnb.bo.backofficeservice.entity.Product;
import com.qnb.bo.backofficeservice.mapper.ProductMapper;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.LanguageRepository;
import com.qnb.bo.backofficeservice.repository.ProductRepository;
import com.qnb.bo.backofficeservice.repository.UnitRespository;
import com.qnb.bo.backofficeservice.service.AddProductService;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Transactional
@Service
@Slf4j
public class AddProductServiceImpl implements AddProductService {

	@Autowired
	private ProductRepository productRepo;
	@Autowired
	private ProductMapper prodMapper;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private UnitRespository unitRespository;

	@Autowired
	private LanguageRepository langRepo;

	@Override
	public GenericResponse<Product> addProduct(List<ProductDTO> addproductRequestList) {
		var response = new GenericResponse<Product>();
		var listToAdd = new ArrayList<Product>();
		var notFound = new ArrayList<>();
		var alreadyExist = new ArrayList<>();
		try {
			var unit = unitRespository.findAll();
			var existingLangList = langRepo.findAll();
			addproductRequestList.forEach(addproductRequest -> {
				var unitDet = unit.stream().filter(units -> units.getUnitId().equals(addproductRequest.getUnitId()))
						.collect(Collectors.toList());
				var langcode = existingLangList.stream()
						.filter(lang -> lang.getId().equals(addproductRequest.getLangCode()))
						.collect(Collectors.toList());
				if (addproductRequest.getAction().equalsIgnoreCase("ADD")) {
					var existingProduct = productRepo.findByUnit_UnitIdAndProdCodeAndStatus(
							addproductRequest.getUnitId(), addproductRequest.getProdCode(), "ACT");
					if (Objects.nonNull(existingProduct) && existingProduct.size() > 0) {
						alreadyExist.add(addproductRequest.getProdCode());
					} else {

						var product = new Product();
						product.setProdCode(addproductRequest.getProdCode());
						product.setProdDesc(addproductRequest.getProdDesc());
						product.setDesc(addproductRequest.getDesc());
						product.setModifiedBy("BO");
						product.setCreatedBy("BO");
						product.setStatus(addproductRequest.getStatus());
						product.setLangCode(langcode.get(0).getId());
						product.setUnit(unitDet.get(0));
						/*
						 * product = productRepo.save(product); response.setData(product); resultVo =
						 * new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
						 */
						listToAdd.add(product);
					}
				} else if (addproductRequest.getAction().equalsIgnoreCase("MOD")) {
					var exists = productRepo.existsById(Integer.valueOf(addproductRequest.getId()));
					if (exists) {
						var product = productRepo.findById(Integer.valueOf(addproductRequest.getId())).get();
						product.setProdDesc(addproductRequest.getProdDesc());
						product.setDesc(addproductRequest.getDesc());
						product.setModifiedBy("BO");
						product.setCreatedBy("BO");
						product.setLangCode(langcode.get(0).getId());
						product.setStatus(addproductRequest.getStatus());
						/*
						 * product = productRepo.save(product); response.setData(product); resultVo =
						 * new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
						 */
						listToAdd.add(product);
					} else {
						notFound.add(String.valueOf(addproductRequest.getProdCode()));
					}
				} else if (addproductRequest.getAction().equalsIgnoreCase("DEL")) {
					var exists = productRepo.existsById(Integer.valueOf(addproductRequest.getId()));
					if (exists) {
						var product = productRepo.findById(Integer.valueOf(addproductRequest.getId())).get();
						product.setStatus("IAC");
						product.setModifiedBy("BO");
						/*
						 * product = productRepo.save(product); response.setData(product); resultVo =
						 * new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
						 */
						listToAdd.add(product);
					} else {
						notFound.add(String.valueOf(addproductRequest.getProdCode()));
					}
				}
			});
			log.info("The Prod List need to be added {}", listToAdd);
			if (!listToAdd.isEmpty()) {
				productRepo.saveAll(listToAdd);
			}
			if (!alreadyExist.isEmpty()) {
				resultVo = new ResultUtilVO(AppConstant.ALREADY_EXISTS_CODE, StringUtils.join(alreadyExist, ','));
			}
			if (!notFound.isEmpty()) {
				resultVo = new ResultUtilVO(AppConstant.NOT_FOUND, StringUtils.join(notFound, ','));
			}

		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<Product>> searchProduct(List<ProductDTO> addproductRequestList) {
		var response = new GenericResponse<List<Product>>();
		try {
			addproductRequestList.forEach(addproductRequest -> {
				var productList = new ArrayList<Product>();
				if(addproductRequest.getLangCode().equalsIgnoreCase("ALL")) {
					productList = (ArrayList<Product>) productRepo.findByUnit_UnitIdAndStatus(addproductRequest.getUnitId(), "ACT");
				}else
				{
					productList = (ArrayList<Product>) productRepo.findByUnit_UnitIdAndLangCodeAndStatus(addproductRequest.getUnitId(),
							addproductRequest.getLangCode(), "ACT");
				}
				if (Objects.nonNull(productList)) {
					response.setData(productList);
					resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
				} else {
					resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, "Product Not Found");
				}
			});
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<Map<String, String>>> searchProdProduct() {
		var response = new GenericResponse<List<Map<String, String>>>();
		var list = new ArrayList<Map<String, String>>();
		try {
			var productList = productRepo.findAll();

			productList.forEach(prod -> {
				var product = new HashedMap<String, String>();
				product.put("name", prod.getProdCode().toString());
				product.put("desc", prod.getProdDesc().toString());
				list.add(product);
			});

			response.setData(list);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
}
