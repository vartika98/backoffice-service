package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.ProductDTO;
import com.qnb.bo.backofficeservice.entity.SubProduct;
import com.qnb.bo.backofficeservice.mapper.ProductMapper;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.BOUnitLanguageRepository;
import com.qnb.bo.backofficeservice.repository.LanguageRepository;
import com.qnb.bo.backofficeservice.repository.SubProductRepository;
import com.qnb.bo.backofficeservice.repository.UnitRespository;
import com.qnb.bo.backofficeservice.service.AddSubProductService;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class AddSubProductServiceImpl implements AddSubProductService {

	@Autowired
	private SubProductRepository subProductRepo;
	@Autowired
	private ProductMapper prodMapper;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private UnitRespository unitRespository;

	@Autowired
	private LanguageRepository langRepo;
	
	@Autowired
	private BOUnitLanguageRepository bOUnitLanguageRepository;

	@Override
	public GenericResponse<SubProduct> addSubProduct(List<ProductDTO> addproductRequestList) {
		GenericResponse<SubProduct> response = new GenericResponse<SubProduct>();
		var listToAdd = new ArrayList<SubProduct>();
		var notFound = new ArrayList<String>();
		var alreadyExist = new ArrayList<String>();
		try {
			var unit = unitRespository.findAll();
			var existingLangList = langRepo.findAll();
			addproductRequestList.forEach(addproductRequest -> {
				var unitDet = unit.stream().filter(units -> units.getUnitId().equals(addproductRequest.getUnitId()))
						.collect(Collectors.toList());
				var langcode = existingLangList.stream()
						.filter(lang -> lang.getId().equals(addproductRequest.getLangCode()))
						.collect(Collectors.toList());
				if (addproductRequest.getAction().equalsIgnoreCase("ADD")) {
					var existingSubProduct = subProductRepo.findByUnit_UnitIdAndProdCodeAndSubProdAndStatus(
							addproductRequest.getUnitId(), addproductRequest.getProdCode(),
							addproductRequest.getSubprod(), "ACT");
					if (Objects.nonNull(existingSubProduct) && existingSubProduct.size() > 0) {
						alreadyExist.add(addproductRequest.getProdCode());
					}
					var subProduct = new SubProduct();
					subProduct.setProdCode(addproductRequest.getProdCode());
					subProduct.setProdDesc(addproductRequest.getProdDesc());
					subProduct.setDesc(addproductRequest.getDesc());
					subProduct.setModifiedBy("BO");
					subProduct.setCreatedBy("BO");
					subProduct.setSubProd(addproductRequest.getSubprod());
					subProduct.setStatus(addproductRequest.getStatus());
					subProduct.setLangCode(langcode.get(0).getId());
					subProduct.setUnit(unitDet.get(0));
					/*
					 * subProduct = subProductRepo.save(subProduct); response.setData(subProduct);
					 */
					listToAdd.add(subProduct);
					resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
				} else if (addproductRequest.getAction().equalsIgnoreCase("MOD")) {
					var exists = subProductRepo.existsById(Integer.valueOf(addproductRequest.getId()));
					if (exists) {
						var subProduct = subProductRepo.findById(Integer.valueOf(addproductRequest.getId())).get();
						subProduct.setProdDesc(addproductRequest.getProdDesc());
						subProduct.setDesc(addproductRequest.getDesc());
						subProduct.setModifiedBy("BO");
						subProduct.setCreatedBy("BO");
						subProduct.setLangCode(langcode.get(0).getId());
						subProduct.setStatus(addproductRequest.getStatus());
						subProduct.setSubProd(addproductRequest.getSubprod());
						/*
						 * subProduct = subProductRepo.save(subProduct); response.setData(subProduct);
						 */
						listToAdd.add(subProduct);
						resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
					} else {
						notFound.add(String.valueOf(addproductRequest.getSubprod()));
					}
				} else if (addproductRequest.getAction().equalsIgnoreCase("DEL")) {
					var exists = subProductRepo.existsById(Integer.valueOf(addproductRequest.getId()));
					if (exists) {
						var subProduct = subProductRepo.findById(Integer.valueOf(addproductRequest.getId())).get();
						subProduct.setStatus("IAC");
						subProduct.setModifiedBy("BO");
						/*
						 * subProduct = subProductRepo.save(subProduct); response.setData(subProduct);
						 */
						listToAdd.add(subProduct);
						resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
					} else {
						notFound.add(String.valueOf(addproductRequest.getProdCode()));
					}
				}
			});
			log.info("The Sub product need to be added {}", listToAdd);
			if (!listToAdd.isEmpty()) {
				subProductRepo.saveAll(listToAdd);
			}
			if (!alreadyExist.isEmpty()) {
				resultVo = new ResultUtilVO(AppConstant.ALREADY_EXISTS_CODE, StringUtils.join(alreadyExist, ','));
			}
			if (!notFound.isEmpty()) {
				resultVo = new ResultUtilVO(AppConstant.NOT_FOUND, StringUtils.join(notFound, ','));
			}
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<SubProduct>> searchSubProduct(List<ProductDTO> addSubProductRequest) {
		var response = new GenericResponse<List<SubProduct>>();
		try {
			var langList = bOUnitLanguageRepository.getAllDistinctLanguages();
			addSubProductRequest.forEach(addproductRequest -> {
				var langCodeList = new ArrayList<String>();
				if(addproductRequest.getLangCode().equalsIgnoreCase("ALL")) {
					langCodeList = (ArrayList<String>) langList;
				} else {
					langCodeList.add(addproductRequest.getLangCode());
				}
				
				var productList = subProductRepo.findByUnit_UnitIdAndLangCodeInAndStatusIn(addproductRequest.getUnitId(),
						langCodeList, List.of("ACT", "IAC"));
				if (Objects.nonNull(productList)) {
					response.setData(productList);
					resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
				} else {
					resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, "Product Not Found");
				}
			});
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
