package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.ApplicationMaintenance.ApplicationResponse;
import com.qnb.bo.backofficeservice.enums.TxnStatus;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.ApplicationRepository;
import com.qnb.bo.backofficeservice.service.ApplicationMaintenanceService;
import com.qnb.bo.backofficeservice.views.ApplicationView;

import io.micrometer.common.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AppMaintenanceServiceImpl implements ApplicationMaintenanceService {
	
	@Autowired
	private ApplicationRepository applicationRepository;
	
	private ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<List<ApplicationResponse>> getApplicationList(List<String> units, String status) {
		var response = new GenericResponse<List<ApplicationResponse>>();
		var data = new ArrayList<ApplicationView>();
		var resData = new ArrayList<ApplicationResponse>();
		resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		try {
			if(!StringUtils.isEmpty(status) && TxnStatus.parseTxnStatus(status) == null)
			  {
				resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
			  }
			else {
			if ((units != null && !units.isEmpty()) && !StringUtils.isEmpty(status)) {
				data = (ArrayList<ApplicationView>) applicationRepository.getAllApplicationsListByUnitAndStatus(units, TxnStatus.parseTxnStatus(status));
			} else if (units != null && !units.isEmpty()) {
				data =  (ArrayList<ApplicationView>) applicationRepository.getAllApplicationsListByUnit(units);
			} else if (!StringUtils.isEmpty(status)) {
				data = (ArrayList<ApplicationView>) applicationRepository.getApplicationsListByStatus(TxnStatus.parseTxnStatus(status));
			} else {
				data = (ArrayList<ApplicationView>) applicationRepository.getAllApplications();
			}
			
			resData = (ArrayList<ApplicationResponse>) ApplicationResponse.toResponse(data);
			response.setData(resData);
			}
		} catch (Exception e) {
			log.info("Exception in calling getApplicationList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
			response.setStatus(resultVo);
			return response;
	}

}