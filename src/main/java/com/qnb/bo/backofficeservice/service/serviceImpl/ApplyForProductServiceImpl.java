package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.CfmsParamEntityIdDTO;
import com.qnb.bo.backofficeservice.dto.EmpIdAndCustSegDTO;
import com.qnb.bo.backofficeservice.entity.CfmsParamEntity;
import com.qnb.bo.backofficeservice.entity.Unit;
import com.qnb.bo.backofficeservice.mapper.EmpAndCustSegMapper;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.CfmsParamRepo;
import com.qnb.bo.backofficeservice.repository.UnitRespository;
import com.qnb.bo.backofficeservice.service.ApplyForProductService;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class ApplyForProductServiceImpl implements ApplyForProductService {

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private EmpAndCustSegMapper empAndCustSegMapper;

	@Autowired
	private CfmsParamRepo cfmsParamRepo;

	private CfmsParamEntityIdDTO id = null;

	@Autowired
	private UnitRespository unitMasterRespository;

	@Override
	public GenericResponse<EmpIdAndCustSegDTO> createApplyForProduct(List<EmpIdAndCustSegDTO> empAndCustSegRequestList,
			String serviceCode, String custOption) {
		var response = new GenericResponse<EmpIdAndCustSegDTO>();
		var listToAdd = new ArrayList<CfmsParamEntity>();
		var notFound = new ArrayList<String>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			empAndCustSegRequestList.forEach(empAndCustSegRequest -> {
				if (empAndCustSegRequest.getAction().equalsIgnoreCase("ADD")) {
					var transKey = cfmsParamRepo.findMaxTransKey() + 1;
					log.info("transkey{}", transKey);
					id = empAndCustSegRequest.getId();
					id.setTransKey(transKey);
					id.setServiceCode(serviceCode);
					id.setCustOption(custOption);
					empAndCustSegRequest.setId(id);
					empAndCustSegRequest.setEmployeeId("ADMIN");
					/*
					 * CfmsParamEntity cfmsParam =
					 * cfmsParamRepo.save(empAndCustSegMapper.toEntity(empAndCustSegRequest));
					 * response.setData(empAndCustSegMapper.toDto(cfmsParam)); resultVo = new
					 * ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
					 */
					listToAdd.add(empAndCustSegMapper.toEntity(empAndCustSegRequest));
				} else if (empAndCustSegRequest.getAction().equalsIgnoreCase("MOD")) {
					var exists = cfmsParamRepo.existsById_TransKey(empAndCustSegRequest.getId().getTransKey());
					if (exists) {
						var cfmsParam = cfmsParamRepo.findById_TransKey(empAndCustSegRequest.getId().getTransKey());
						log.info("{}", cfmsParam);
						cfmsParam.setTransValue(empAndCustSegRequest.getTransValue());
						cfmsParam.setCfmsValue(empAndCustSegRequest.getCfmsValue());
						cfmsParam.setValueArb(empAndCustSegRequest.getValueArb());
						cfmsParam.setStatus(empAndCustSegRequest.getStatus());
						/*
						 * CfmsParamEntity updatecfmsParam = cfmsParamRepo.save(cfmsParam);
						 * response.setData(empAndCustSegMapper.toDto(updatecfmsParam)); resultVo = new
						 * ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
						 */
						listToAdd.add(cfmsParam);
					} else {
						notFound.add(String.valueOf(empAndCustSegRequest.getId().getTransKey()));
					}
				} else if (empAndCustSegRequest.getAction().equalsIgnoreCase("DEL")) {
					var exists = cfmsParamRepo.existsById_TransKey(empAndCustSegRequest.getTransId());
					if (exists) {
						var cfmsParam = cfmsParamRepo.findById_TransKey(empAndCustSegRequest.getTransId());
						log.info("{}", cfmsParam);
						/* cfmsParamRepo.deleteById_TransKey(empAndCustSegRequest.getTransId()); */
						cfmsParam.setStatus(AppConstant.DEL);
						/*
						 * response.setData(empAndCustSegMapper.toDto(cfmsParam)); resultVo = new
						 * ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
						 */
						listToAdd.add(cfmsParam);
					} else {
						notFound.add(String.valueOf(empAndCustSegRequest.getId().getTransKey()));
					}
				}
			});
			log.info("The CFMS List need to be added {}", listToAdd);
			if (!listToAdd.isEmpty()) {
				cfmsParamRepo.saveAll(listToAdd);
			}
			if (!notFound.isEmpty()) {
				resultVo = new ResultUtilVO(AppConstant.ALREADY_EXISTS_CODE, StringUtils.join(notFound, ','));
			}
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<EmpIdAndCustSegDTO> deleteApplyForProduct(long id) {
		var response = new GenericResponse<EmpIdAndCustSegDTO>();
		try {
			var exists = cfmsParamRepo.existsById_TransKey(id);
			if (exists) {
				var cfmsParam = cfmsParamRepo.findById_TransKey(id);
				log.info("{}", cfmsParam);
				cfmsParamRepo.deleteById_TransKey(id);
				response.setData(empAndCustSegMapper.toDto(cfmsParam));
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			} else {
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, "Product Deleted Successfully");
			}
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<CfmsParamEntity>> searchApplyForProduct(String custOption, String serviceCode) {
		var configMap = new HashMap<String, String>();
		var response = new GenericResponse<List<CfmsParamEntity>>();
		var accessedUnit = new ArrayList<String>();
		try {
			var unitList = unitMasterRespository.findAll();
			unitList.forEach(configObj -> {
				configMap.put(String.valueOf(configObj.getId()), configObj.getDescription());
				accessedUnit.add(configObj.getUnitId());
			});
			var empIdAndCustSegList = cfmsParamRepo.findById_CustOptionAndId_ServiceCodeAndId_UnitIdInAndStatusIn(serviceCode,
					custOption, accessedUnit, new ArrayList<String>(List.of("ACT", "IAC")));
			response.setData(empIdAndCustSegList);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);

		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;

	}

	@Override
	public GenericResponse<EmpIdAndCustSegDTO> getApplyForProduct(long id) {
		var response = new GenericResponse<EmpIdAndCustSegDTO>();
		try {
			var exists = cfmsParamRepo.existsById_TransKey(id);
			if (exists) {
				var cfmsParam = cfmsParamRepo.findById_TransKey(id);
				response.setData(empAndCustSegMapper.toDto(cfmsParam));
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			} else {
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, "No Record Found");
			}
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<EmpIdAndCustSegDTO> updateApplyForProduct(EmpIdAndCustSegDTO empAndCustSegRequest) {
		var response = new GenericResponse<EmpIdAndCustSegDTO>();
		try {
			var exists = cfmsParamRepo.existsById_TransKey(empAndCustSegRequest.getId().getTransKey());
			if (exists) {
				var cfmsParam = cfmsParamRepo.findById_TransKey(empAndCustSegRequest.getId().getTransKey());
				log.info("{}", cfmsParam);
				cfmsParam.setTransValue(empAndCustSegRequest.getTransValue());
				cfmsParam.setCfmsValue(empAndCustSegRequest.getCfmsValue());
				cfmsParam.setValueArb(empAndCustSegRequest.getValueArb());
				var updatecfmsParam = cfmsParamRepo.save(cfmsParam);
				response.setData(empAndCustSegMapper.toDto(updatecfmsParam));
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			} else {
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, "Update Successfully");
			}
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
