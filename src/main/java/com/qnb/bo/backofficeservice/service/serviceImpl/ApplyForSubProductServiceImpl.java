package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.CfmsParamEntityIdDTO;
import com.qnb.bo.backofficeservice.dto.EmpIdAndCustSegDTO;
import com.qnb.bo.backofficeservice.dto.SearchRequest;
import com.qnb.bo.backofficeservice.entity.CfmsParamEntity;
import com.qnb.bo.backofficeservice.mapper.EmpAndCustSegMapper;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.CfmsParamRepo;
import com.qnb.bo.backofficeservice.repository.UnitRespository;
import com.qnb.bo.backofficeservice.repository.UserUnitMembershipRepository;
import com.qnb.bo.backofficeservice.service.ApplyForSubProductService;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class ApplyForSubProductServiceImpl implements ApplyForSubProductService {

	@Autowired
	private EmpAndCustSegMapper empAndCustSegMapper;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private CfmsParamRepo cfmsParamRepo;

	private CfmsParamEntityIdDTO id = null;

	@Autowired
	private UserUnitMembershipRepository userUnitRepo;

	@Autowired
	private UnitRespository unitMasterRespository;

	@Override
	public GenericResponse<EmpIdAndCustSegDTO> createApplyForSubProduct(
			List<EmpIdAndCustSegDTO> empAndCustSegRequestList, String serviceCode, String custOption) {
		// TODO Auto-generated method stub
		var response = new GenericResponse<EmpIdAndCustSegDTO>();
		var listToAdd = new ArrayList<CfmsParamEntity>();
		var notFound = new ArrayList<String>();
		try {
			empAndCustSegRequestList.forEach(empAndCustSegRequest -> {
				if (empAndCustSegRequest.getAction().equalsIgnoreCase("ADD")) {
					var transKey = cfmsParamRepo.findMaxTransKey() + 1;
					log.info("transkey{}", transKey);
					id = empAndCustSegRequest.getId();
					id.setTransKey(transKey);
					id.setServiceCode(serviceCode);
					id.setCustOption(custOption);
					empAndCustSegRequest.setId(id);
					empAndCustSegRequest.setEmployeeId("ADMIN");
					/*
					 * CfmsParamEntity cfmsParam =
					 * cfmsParamRepo.save(empAndCustSegMapper.toEntity(empAndCustSegRequest));
					 * response.setData(empAndCustSegMapper.toDto(cfmsParam)); resultVo = new
					 * ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
					 */
					listToAdd.add(empAndCustSegMapper.toEntity(empAndCustSegRequest));
				} else if (empAndCustSegRequest.getAction().equalsIgnoreCase("MOD")) {
					var exists = cfmsParamRepo.existsById_TransKey(empAndCustSegRequest.getId().getTransKey());
					if (exists) {
						var cfmsParam = cfmsParamRepo.findById_TransKey(empAndCustSegRequest.getId().getTransKey());
						log.info("{}", cfmsParam);
						cfmsParam.setTransValue(empAndCustSegRequest.getTransValue());
						cfmsParam.setCfmsValue(empAndCustSegRequest.getCfmsValue());
						cfmsParam.setValueArb(empAndCustSegRequest.getValueArb());
						cfmsParam.setTypeOfProd(empAndCustSegRequest.getTypeOfProd());
						cfmsParam.setStatus(empAndCustSegRequest.getStatus());
						listToAdd.add(cfmsParam);
						/*
						 * CfmsParamEntity updatecfmsParam = cfmsParamRepo.save(cfmsParam);
						 * response.setData(empAndCustSegMapper.toDto(updatecfmsParam)); resultVo = new
						 * ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
						 */
					} else {
						notFound.add(String.valueOf(empAndCustSegRequest.getId().getTransKey()));
					}
				} else if (empAndCustSegRequest.getAction().equalsIgnoreCase("DEL")) {
					var exists = cfmsParamRepo.existsById_TransKey(empAndCustSegRequest.getTransId());
					if (exists) {
						var cfmsParam = cfmsParamRepo.findById_TransKey(empAndCustSegRequest.getTransId());
						/* cfmsParamRepo.deleteById_TransKey(empAndCustSegRequest.getTransId()); */
						cfmsParam.setStatus("IAC");
						/*
						 * response.setData(empAndCustSegMapper.toDto(cfmsParam)); resultVo = new
						 * ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
						 */
						listToAdd.add(cfmsParam);
					} else {
						notFound.add(String.valueOf(empAndCustSegRequest.getId().getTransKey()));
					}
				}
			});
			log.info("The CFMS List need to be added {}", listToAdd);
			if (!listToAdd.isEmpty()) {
				cfmsParamRepo.saveAll(listToAdd);
			}
			if (!notFound.isEmpty()) {
				resultVo = new ResultUtilVO(AppConstant.ALREADY_EXISTS_CODE, StringUtils.join(notFound, ','));
			}
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<CfmsParamEntity>> searchApplyForSubProduct(String typeOfProd, String custOption,
			String serviceCode) {
		// TODO Auto-generated method stub
		var response = new GenericResponse<List<CfmsParamEntity>>();
		var accessedUnit = new ArrayList<String>();	
		try {
			var unitList = unitMasterRespository.findAll();
			unitList.forEach(configObj -> {
				accessedUnit.add(configObj.getUnitId());
			});
			var empIdAndCustSegList = cfmsParamRepo.findByTypeOfProdAndId_CustOptionAndId_ServiceCodeAndId_UnitIdIn(
					typeOfProd, custOption, serviceCode, accessedUnit);
			response.setData(empIdAndCustSegList);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<EmpIdAndCustSegDTO> updateApplyForSubProduct(EmpIdAndCustSegDTO empAndCustSegRequest) {
		// TODO Auto-generated method stub
		var response = new GenericResponse<EmpIdAndCustSegDTO>();
		try {
			var exists = cfmsParamRepo.existsById_TransKey(empAndCustSegRequest.getId().getTransKey());
			if (exists) {
				var cfmsParam = cfmsParamRepo.findById_TransKey(empAndCustSegRequest.getId().getTransKey());
				log.info("{}", cfmsParam);
				cfmsParam.setTransValue(empAndCustSegRequest.getTransValue());
				cfmsParam.setCfmsValue(empAndCustSegRequest.getCfmsValue());
				cfmsParam.setValueArb(empAndCustSegRequest.getValueArb());
				cfmsParam.setTypeOfProd(empAndCustSegRequest.getTypeOfProd());
				var updatecfmsParam = cfmsParamRepo.save(cfmsParam);
				response.setData(empAndCustSegMapper.toDto(updatecfmsParam));
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			} else {
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, "Update Successfully");
			}
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;

	}

	@Override
	public GenericResponse<List<EmpIdAndCustSegDTO>> searchByUnitApplyForSubProduct(SearchRequest unitId,
			String custOption, String serviceCode) {
		// TODO Auto-generated method stub
		var response = new GenericResponse<List<EmpIdAndCustSegDTO>>();
		try {
			if (!unitId.getUnitId().isEmpty() && unitId.getUnitId() != null) {
				var list = new ArrayList<EmpIdAndCustSegDTO>();
				list = (ArrayList<EmpIdAndCustSegDTO>) empAndCustSegMapper.toDto(cfmsParamRepo
						.findById_UnitIdAndId_CustOptionAndId_ServiceCode(unitId.getUnitId(), custOption, serviceCode));
				if (!list.isEmpty() && list.size() > 0) {
					list = (ArrayList<EmpIdAndCustSegDTO>) list.stream()
							.sorted(Comparator.comparing(EmpIdAndCustSegDTO::getTransValue))
							.collect(Collectors.toList());
				}
				response.setData(list);
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			} else {
				var list = empAndCustSegMapper
						.toDto(cfmsParamRepo.findById_CustOptionAndId_ServiceCode(custOption, serviceCode));
				if (!list.isEmpty() && list.size() > 0) {
					list = list.stream().sorted(Comparator.comparing(EmpIdAndCustSegDTO::getTransValue))
							.collect(Collectors.toList());
				}
				response.setData(list);
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			}
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
