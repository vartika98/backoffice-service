package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.GraphDateCountDto;
import com.qnb.bo.backofficeservice.dto.GraphDayCountDto;
import com.qnb.bo.backofficeservice.dto.audit.AuditListRequest;
import com.qnb.bo.backofficeservice.dto.audit.AuditListResponse;
import com.qnb.bo.backofficeservice.entity.ParamConfig;
import com.qnb.bo.backofficeservice.entity.audit.AuditMaster;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.AuditDetailsRepository;
import com.qnb.bo.backofficeservice.repository.AuditRepository;
import com.qnb.bo.backofficeservice.repository.ParamConfigRepository;
import com.qnb.bo.backofficeservice.service.AuditService;
import com.qnb.bo.backofficeservice.utils.DateUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AuditServiceImpl implements AuditService {

	@Autowired
	private AuditRepository auditRepository;

	public static XSSFWorkbook workbook;

	public static XSSFSheet sheet;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private AuditDetailsRepository auditDetailRepo;

	@Autowired
	private ParamConfigRepository paramConfigRepository;

	@Override
	public GenericResponse<List<AuditListResponse>> getAuditList(AuditListRequest auditRequest) {
		var response = new GenericResponse<List<AuditListResponse>>();
		var auditLst = new ArrayList<AuditListResponse>();
		var auditDetails = new ArrayList<AuditMaster>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			if (Objects.nonNull(auditRequest.getGuid()) && !auditRequest.getGuid().isEmpty()) {
				auditDetails = auditRepository.findByTxnRefNo(auditRequest.getGuid());
			}else if(Objects.nonNull(auditRequest.getUserNo()) && !auditRequest.getUserNo().isEmpty()){				if (AppConstant.ALL.equals(auditRequest.getCategoryCode())) {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditListByUserNo(
							auditRequest.getFromDate(), auditRequest.getToDate(), auditRequest.getChannelId(),
							auditRequest.getUserNo());
				} else {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditSumByUserNo(
							auditRequest.getCategoryCode(), auditRequest.getFromDate(), auditRequest.getToDate(),
							auditRequest.getChannelId(), auditRequest.getUserNo());
				}
			}else if((Objects.nonNull(auditRequest.getUserId()) && !auditRequest.getUserId().isEmpty())
					&& (Objects.nonNull(auditRequest.getGid())) && !auditRequest.getGid().isEmpty()){
				if (AppConstant.ALL.equals(auditRequest.getCategoryCode())) {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditLstByUserIdAndGId(
							auditRequest.getFromDate(), auditRequest.getToDate(), auditRequest.getChannelId(),
							auditRequest.getUserId(),auditRequest.getGid());
				} else {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditSumByUserIdAndGId(
							auditRequest.getCategoryCode(), auditRequest.getFromDate(), auditRequest.getToDate(),
							auditRequest.getChannelId(), auditRequest.getUserId(),auditRequest.getGid());
				}
			}else if((Objects.nonNull(auditRequest.getUserId()) && !auditRequest.getUserId().isEmpty())
					&& (Objects.isNull(auditRequest.getGid()))){
				if (AppConstant.ALL.equals(auditRequest.getCategoryCode())) {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditLstByUserId(
							auditRequest.getFromDate(), auditRequest.getToDate(), auditRequest.getChannelId(),
							auditRequest.getUserId());
				} else {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditSumByUserId(
							auditRequest.getCategoryCode(), auditRequest.getFromDate(), auditRequest.getToDate(),
							auditRequest.getChannelId(), auditRequest.getUserId());
				}
			}else if((Objects.isNull(auditRequest.getUserId()))
					&& ( Objects.nonNull(auditRequest.getGid()) && !auditRequest.getGid().isEmpty())) {
				if (AppConstant.ALL.equals(auditRequest.getCategoryCode())) {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditLstByGId(
							auditRequest.getFromDate(), auditRequest.getToDate(), auditRequest.getChannelId(),
							auditRequest.getGid());
				} else {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditSumByGId(
							auditRequest.getCategoryCode(), auditRequest.getFromDate(), auditRequest.getToDate(),
							auditRequest.getChannelId(), auditRequest.getGid());
				}
			} 
			else {
				if (AppConstant.ALL.equals(auditRequest.getCategoryCode())) {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditList(
							auditRequest.getFromDate(), auditRequest.getToDate(), auditRequest.getChannelId());
				} else {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditSum(
							auditRequest.getCategoryCode(), auditRequest.getFromDate(), auditRequest.getToDate(),
							auditRequest.getChannelId());
				}
			}
			if (Objects.nonNull(auditDetails) && !auditDetails.isEmpty()) {
				auditDetails.forEach(getData -> {
					var audit = new AuditListResponse();
					audit.setAuditRefno(isNullObj(String.valueOf(getData.getAuditRefno())));
					audit.setAuditDate(isNullObj(String.valueOf(getData.getAuditDate())));
					audit.setCategoryCode(isNullObj(getData.getCatCode()));
					audit.setUnitId(isNullObj(getData.getUnitId()));
					audit.setChannelId(isNullObj(getData.getChannel()));
					audit.setGuid(isNullObj(getData.getTxnRefNo()));
					audit.setMw_errCode(isNullObj(getData.getMwErrCode()));
					audit.setMw_errDesc(isNullObj(getData.getMwErrDesc()));
					audit.setOcs_errCode(isNullObj(getData.getOcsErrCode()));
					audit.setOcs_errDesc(isNullObj(getData.getOcsErrDesc()));
					audit.setStatus(isNullObj(getData.getStatus()));
					audit.setTraceId(isNullObj(getData.getTxnRefNo()));
					audit.setImei(isNullObj(getData.getImei()));
					audit.setIpAddress(isNullObj(getData.getHostIp()));
					auditLst.add(audit);
				});
				response.setData(auditLst);
			} else {
				resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "No audit list");
			}
		} catch (Exception e) {
			log.info("Exception in getAuditList :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	public String isNullObj(Object obj) {
		if (Objects.nonNull(obj) && !obj.toString().equalsIgnoreCase("null") && !obj.toString().isEmpty())
			return String.valueOf(obj);
		else
			return "";
	}

	@Override
	public GenericResponse<List<Map<String, String>>> getAuditDetailsList(Map<String, String> request) {
		var response = new GenericResponse<List<Map<String, String>>>();
		var auditDetLst = new ArrayList<Map<String, String>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var auditDetailLst = auditDetailRepo.findByAuditRef_AuditRefno(Integer.valueOf(request.get("auditRefNo")));
			if (auditDetailLst.isEmpty()) {
				resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "No auditDetails");
			} else {
				auditDetailLst.forEach(auditDet -> {
					var detail = new HashMap<String, String>();
					detail.put("paramKey", auditDet.getParamName());
					detail.put("paramValue", auditDet.getParamValue());
					auditDetLst.add(detail);
				});
				response.setData(auditDetLst);
			}
		} catch (Exception e) {
			log.info("Exception in getAuditDetailsList :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<Map<String, String>> getExcelBoAuditReport(AuditListRequest auditRequest)
			throws IOException {
		var response = new GenericResponse<Map<String, String>>();
		var resMap = new HashMap<String, String>();
		var auditDetails = new ArrayList<AuditMaster>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.SUCCESS);
			if (Objects.nonNull(auditRequest.getGuid()) && !auditRequest.getGuid().isEmpty()) {
				auditDetails = auditRepository.findByTxnRefNo(auditRequest.getGuid());
			}else if(Objects.nonNull(auditRequest.getUserNo()) && !auditRequest.getUserNo().isEmpty()){				if (AppConstant.ALL.equals(auditRequest.getCategoryCode())) {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditListByUserNo(
							auditRequest.getFromDate(), auditRequest.getToDate(), auditRequest.getChannelId(),
							auditRequest.getUserNo());
				} else {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditSumByUserNo(
							auditRequest.getCategoryCode(), auditRequest.getFromDate(), auditRequest.getToDate(),
							auditRequest.getChannelId(), auditRequest.getUserNo());
				}
			}else if((Objects.nonNull(auditRequest.getUserId()) && !auditRequest.getUserId().isEmpty())
					&& (Objects.nonNull(auditRequest.getGid())) && !auditRequest.getGid().isEmpty()){
				if (AppConstant.ALL.equals(auditRequest.getCategoryCode())) {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditLstByUserIdAndGId(
							auditRequest.getFromDate(), auditRequest.getToDate(), auditRequest.getChannelId(),
							auditRequest.getUserId(),auditRequest.getGid());
				} else {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditSumByUserIdAndGId(
							auditRequest.getCategoryCode(), auditRequest.getFromDate(), auditRequest.getToDate(),
							auditRequest.getChannelId(), auditRequest.getUserId(),auditRequest.getGid());
				}
			}else if((Objects.nonNull(auditRequest.getUserId()) && !auditRequest.getUserId().isEmpty())
					&& (Objects.isNull(auditRequest.getGid()))){
				if (AppConstant.ALL.equals(auditRequest.getCategoryCode())) {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditLstByUserId(
							auditRequest.getFromDate(), auditRequest.getToDate(), auditRequest.getChannelId(),
							auditRequest.getUserId());
				} else {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditSumByUserId(
							auditRequest.getCategoryCode(), auditRequest.getFromDate(), auditRequest.getToDate(),
							auditRequest.getChannelId(), auditRequest.getUserId());
				}
			}else if((Objects.isNull(auditRequest.getUserId()))
					&& ( Objects.nonNull(auditRequest.getGid()) && !auditRequest.getGid().isEmpty())) {
				if (AppConstant.ALL.equals(auditRequest.getCategoryCode())) {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditLstByGId(
							auditRequest.getFromDate(), auditRequest.getToDate(), auditRequest.getChannelId(),
							auditRequest.getGid());
				} else {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditSumByGId(
							auditRequest.getCategoryCode(), auditRequest.getFromDate(), auditRequest.getToDate(),
							auditRequest.getChannelId(), auditRequest.getGid());
				}
			} 
			else {
				if (AppConstant.ALL.equals(auditRequest.getCategoryCode())) {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditList(
							auditRequest.getFromDate(), auditRequest.getToDate(), auditRequest.getChannelId());
				} else {
					auditDetails = (ArrayList<AuditMaster>) auditRepository.getAuditSum(
							auditRequest.getCategoryCode(), auditRequest.getFromDate(), auditRequest.getToDate(),
							auditRequest.getChannelId());
				}
			}
			if (auditDetails.isEmpty()) {
				resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "no audit data is present.");
			} else {
				var base64xls = createExcel(auditDetails, auditRequest.getFromDate(), auditRequest.getToDate());
				resMap.put("base64", base64xls);
			}
			response.setData(resMap);
		} catch (Exception e) {
			log.info("Exception in getAuditDetailsList :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	private String createExcel(List<AuditMaster> auditDetails, String fromDate, String toDate) throws IOException {
		var base64xls = "";
		workbook = new XSSFWorkbook();
		sheet = workbook.createSheet("BO Audit Report");
		sheet.setDefaultColumnWidth(20);
		var header = "BO Audit Transaction Details";
		var sub_header = new String[] { "Audit Reference Number", "Transaction Date", "BO User Name",
				"Transaction Code", "Transaction Description", "Unit" };
		var rowMain_i = 1;
		var row = sheet.createRow(rowMain_i);
		if (!auditDetails.isEmpty()) {
			var cell = row.createCell(2);
			cell.setCellValue(header);
			cell.setCellStyle(headingstyle(3, 7, rowMain_i, rowMain_i));
			row = sheet.createRow(++rowMain_i);
			var cell2 = row.createCell(3);
			cell2.setCellValue(" Transaction Details  Date : From " + fromDate + " To " + toDate);
			cell2.setCellStyle(subHeadingStyle(2, 7, rowMain_i, rowMain_i));
			++rowMain_i;
			row = sheet.createRow(++rowMain_i);
			for (int i = 0; i < sub_header.length; i++) {
				Cell cell3 = row.createCell(i);
				cell3.setCellValue(sub_header[i]);
				cell3.setCellStyle(titleStyle());
			}
			var rowMain = rowMain_i;
			for (AuditMaster txnLst : auditDetails) {
				rowMain = rowMain + 1;
				var rowList = sheet.createRow(++rowMain_i);
				var cellp1 = rowList.createCell(0);
				cellp1.setCellValue(isNullObj(txnLst.getAuditRefno()));
				cellp1.setCellStyle(textCellStyle());
				var cellp2 = rowList.createCell(1);
				cellp2.setCellValue(isNullObj(txnLst.getAuditDate()));
				cellp2.setCellStyle(textCellStyle());
				var cellp3 = rowList.createCell(2);
				cellp3.setCellValue(isNullObj(txnLst.getUserId()));
				cellp3.setCellStyle(textCellStyle());
				var cellp4 = rowList.createCell(3);
				cellp4.setCellValue(isNullObj(txnLst.getCatCode()));
				cellp4.setCellStyle(textCellStyle());
				var cellp5 = rowList.createCell(4);
				cellp5.setCellValue(txnLst.getCatCode());
				cellp5.setCellStyle(textCellStyle());
				var cellp6 = rowList.createCell(5);
				cellp6.setCellValue(isNullObj(txnLst.getUnitId()));
				cellp6.setCellStyle(textCellStyle2());
			}

//			  if (true) { 
//				  FileOutputStream fileOut = new FileOutputStream(new
//			  File("D:/Job1.xlsx")); workbook.write(fileOut); workbook.close();
//			  fileOut.close(); }

			try (ByteArrayOutputStream stream = new ByteArrayOutputStream();) {
				workbook.write(stream);
				workbook.close();
				base64xls = Base64.getEncoder().encodeToString(stream.toByteArray());
			}
		}
		return base64xls;
	}

	public XSSFCellStyle noDataFund() {
		var font = workbook.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setFontName("Times New Roman");
		font.setColor(IndexedColors.RED.getIndex());
		font.setBold(true);

		var style = workbook.createCellStyle();
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFont(font);
		return style;
	}

	public XSSFCellStyle headingstyle(int col_i, int col_j, int row_i, int row_j) {
		sheet.addMergedRegion(new CellRangeAddress(row_i, row_j, col_i + 1, col_j - 1));
		var font = workbook.createFont();
		font.setFontHeightInPoints((short) 20);
		font.setFontName("Times New Roman");
		font.setColor(IndexedColors.RED.getIndex());
		font.setBold(true);

		var style = workbook.createCellStyle();
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		style.setFont(font);
		return style;
	}

	public XSSFCellStyle subHeadingStyle(int col_i, int col_j, int row_i, int row_j) {
		sheet.addMergedRegion(new CellRangeAddress(row_i, row_j, col_i + 1, col_j - 1));
		var font = workbook.createFont();
		font.setFontName("Courier New");
		font.setFontHeight((short) (10 * 20));
		font.setColor(IndexedColors.BLACK.getIndex());

		var style = workbook.createCellStyle();
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setWrapText(true);
		style.setFont(font);
		return style;
	}

	public XSSFCellStyle titleStyle() {
		var font = workbook.createFont();
		font.setFontName("Times New Roman");
		font.setFontHeightInPoints((short) 12);
		font.setBold(true);
		font.setColor(IndexedColors.BLUE1.getIndex());

		var style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFont(font);
		return style;
	}

	public XSSFCellStyle amountCellStyle() {
		var font = workbook.createFont();
		font.setFontName("Courier New");
		font.setFontHeight((short) (10 * 20));
		font.setColor(IndexedColors.BLACK.getIndex());

		var style = workbook.createCellStyle();
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setWrapText(true);
		style.setVerticalAlignment(VerticalAlignment.TOP);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFont(font);
		return style;
	}

	public XSSFCellStyle textCellStyle() {
		var font = workbook.createFont();
		font.setFontName("Courier New");
		font.setFontHeight((short) (10 * 20));
		font.setColor(IndexedColors.BLACK.getIndex());

		var style = workbook.createCellStyle();
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setWrapText(true);
		style.setVerticalAlignment(VerticalAlignment.TOP);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFont(font);
		return style;
	}

	public XSSFCellStyle textCellStyle2() {
		var font = workbook.createFont();
		font.setFontName("Courier New");
		font.setFontHeight((short) (10 * 20));
		font.setColor(IndexedColors.BLACK.getIndex());

		var style = workbook.createCellStyle();
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setWrapText(true);
		style.setVerticalAlignment(VerticalAlignment.TOP);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFont(font);
		return style;
	}

	public XSSFCellStyle errorStyle() {
		var font = workbook.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setFontName("Times New Roman");
		font.setColor(IndexedColors.RED.getIndex());
		font.setBold(true);

		var style = workbook.createCellStyle();
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFont(font);
		return style;
	}

	@Override
	public GenericResponse<GraphDateCountDto> getAuditCountByDateRange(AuditListRequest auditRequest) {

		var entityList = auditRepository.findByChannelAndCategoryAndDate(auditRequest.getChannelId(),
				auditRequest.getCategoryCode(), auditRequest.getFromDate(), auditRequest.getToDate());

		return getGraphData(entityList, auditRequest.getFromDate(), auditRequest.getToDate());
	}

	@Override
	public GenericResponse<GraphDateCountDto> getLoginCountByConfig() {
		// Get the current date
		var currentDate = Instant.now();

		var endDateStr = DateUtil.dateInStringFormat(Date.from(currentDate));

		var paramConfigList = (List<ParamConfig>) paramConfigRepository
				.findDistinctValuesForKey(AppConstant.BO_TRANSFER_MONITORING_DAYS);
		var numberOfDays = paramConfigList.isEmpty() ? AppConstant.ONE_MONTH_IN_DAYS
				: Integer.parseInt(paramConfigList.get(0).getValue());

		// Calculate end time by adding numberOfDays to the current date
		var startDate = currentDate.minus(Duration.ofDays(numberOfDays));
		var startDateStr = DateUtil.dateInStringFormat(Date.from(startDate));

		var entityList = auditRepository.findLoginCountByDateRange(AppConstant.LOGIN_CATEGORY_CODE, startDateStr,
				endDateStr);
		return getGraphData(entityList, startDateStr, endDateStr);
	}

	private GenericResponse<GraphDateCountDto> getGraphData(List<AuditMaster> entityList, String fromDate,
			String toDate) {
		var response = new GenericResponse<GraphDateCountDto>();

		try {

			var groupedByDate = entityList.stream().collect(
					Collectors.groupingBy(auditMaster -> DateUtil.dateInStringFormat(auditMaster.getAuditDate())));

			var graphDayCountDtos = Stream
					.iterate(DateUtil.getLocalDate(fromDate, DateUtil.DD_MM_YYYY), date -> date.plusDays(1))
					.limit(ChronoUnit.DAYS.between(DateUtil.getLocalDate(fromDate, DateUtil.DD_MM_YYYY),
							DateUtil.getLocalDate(toDate, DateUtil.DD_MM_YYYY).plusDays(1)))
					.map(date -> {
						var localDateInString = DateUtil.dateInStringFormat(date, DateUtil.DD_MM_YYYY);
						var auditMasterEntityForDate = groupedByDate.getOrDefault(localDateInString,
								Collections.emptyList());

						var successCount = auditMasterEntityForDate.stream().filter(this::isSuccess).count();

						var failureCount = auditMasterEntityForDate.size() - successCount;
						return new GraphDayCountDto(DateUtil.dateInStringFormat(date, DateUtil.DD_MMM), successCount,
								failureCount);
					}).collect(Collectors.toList());

			if (CollectionUtils.isEmpty(graphDayCountDtos)) {
				resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "No data found");
			} else {
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.SUCCESS);
				var totalSuccessCount = entityList.stream().filter(this::isSuccess).count();
				var graphDateCountDtos = new GraphDateCountDto(totalSuccessCount,
						(entityList.size() - totalSuccessCount), graphDayCountDtos);
				response.setData(graphDateCountDtos);
			}

		} catch (Exception e) {
			log.error("Exception: {}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}

		response.setStatus(resultVo);
		return response;
	}

	private boolean isSuccess(AuditMaster auditMaster) {
		return AppConstant.SUCCESS.equalsIgnoreCase(auditMaster.getStatus());
	}

}
