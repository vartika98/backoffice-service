package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.banner.BannerReqDto;
import com.qnb.bo.backofficeservice.dto.banner.BannerResponse;
import com.qnb.bo.backofficeservice.dto.banner.ScreenIdDropdownDto;
import com.qnb.bo.backofficeservice.entity.BannerDetailsEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.BannerRepository;
import com.qnb.bo.backofficeservice.service.BannerService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BannerServiceImpl implements BannerService {

	@Autowired
	private BannerRepository bannerRepository;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<List<BannerResponse>> selectBanner(String unit, String channel, String langCode,
			String screenId, String imgResol, String disPriority) {

		var response = new GenericResponse<List<BannerResponse>>();
		var bannerList = new ArrayList<BannerDetailsEntity>();
		var bannerResList = new ArrayList<BannerResponse>();
		try {
			log.info("inside banner service : ");
			var langCodeList = new ArrayList<String>();
			var imageResolList = new ArrayList<String>();
			var disPriorityList = new ArrayList<Integer>();
			var statusList = new ArrayList<>(Arrays.asList("ACT", "IAC"));

			if (langCode.equals("ALL")) {
				langCodeList = new ArrayList<>(Arrays.asList(AppConstant.EN, AppConstant.AR, AppConstant.FR));
			} else {
				langCodeList = new ArrayList<>(Arrays.asList(langCode));
			}
			if (imgResol.equals("ALL")) {
				imageResolList = new ArrayList<>(
						Arrays.asList(AppConstant.RESOL_SMALL, AppConstant.RESOL_MEDIUM, AppConstant.RESOL_LARGE));
			} else {
				imageResolList = new ArrayList<>(Arrays.asList(imgResol));
			}
			if (disPriority.equals("ALL")) {
				disPriorityList = new ArrayList<>(Arrays.asList(AppConstant.DISP_PRIORITY_1,
						AppConstant.DISP_PRIORITY_2, AppConstant.DISP_PRIORITY_3));
			} else {
				disPriorityList = new ArrayList<>(Arrays.asList(Integer.parseInt(disPriority)));
			}

			bannerList = (ArrayList<BannerDetailsEntity>) bannerRepository
					.findById_ScreenIdAndId_UnitIdAndId_ChannelIdAndId_LangCodeInAndImageResolInAndDisPriorityInAndStatusInOrderByDateCreatedDesc(
							screenId, unit, channel, langCodeList, imageResolList, disPriorityList, statusList);

			if (bannerList != null && !bannerList.isEmpty()) {
				bannerList.forEach(banner -> {
					var bannerResponse = BannerResponse.builder().segmentType(banner.getId().getSegmentType())
							.bannerId(banner.getId().getBannerId()).bannerUrl(banner.getBannerUrl())
							.description(banner.getDescription()).dispPriority(banner.getDisPriority())
							.imageResolution(banner.getImageResol()).linkType(banner.getLinkType())
							.redirectionUrl(banner.getRedirectionUrl()).status(banner.getStatus())
							.langCode(banner.getId().getLangCode()).build();
					bannerResList.add(bannerResponse);
				});

				response.setData(bannerResList);
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			} else {
				log.info("Banner list is empty");
				resultVo = new ResultUtilVO(AppConstant.EMPTY_BANNER_LIST_CODE, AppConstant.EMPTY_BANNER_LIST_DESC);
			}
		} catch (Exception e) {
			log.info("Exception in calling SelectBanner :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<ScreenIdDropdownDto>> selectScreenId(String unit, String channel) {
		var response = new GenericResponse<List<ScreenIdDropdownDto>>();
		var bannerList = new ArrayList<Object[]>();
		var bannerResList = new ArrayList<ScreenIdDropdownDto>();
		try {
			bannerResList.add(new ScreenIdDropdownDto("NEW_SCREENID", "Add New Screen Id"));
			bannerList = (ArrayList<Object[]>) bannerRepository
					.findDistinctScreenIdDescriptionByUnitAndChannelAndStatus(unit, channel);
			if (bannerList != null) {
				if (!bannerList.isEmpty()) {
					bannerList.forEach(banner -> {
						ScreenIdDropdownDto screenIdDropdown = ScreenIdDropdownDto.builder()
								.screenId((String) banner[0]).description((String) banner[1]).build();
						bannerResList.add(screenIdDropdown);
					});
					resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
				} else {
					log.info("Banner list is empty");
					resultVo = new ResultUtilVO(AppConstant.EMPTY_BANNER_LIST_CODE, AppConstant.EMPTY_BANNER_LIST_DESC);
				}
			} else {
				log.info("Banner list is empty");
				resultVo = new ResultUtilVO(AppConstant.EMPTY_BANNER_LIST_CODE, AppConstant.EMPTY_BANNER_LIST_DESC);
			}
		} catch (Exception e) {
			log.info("Exception in calling selectScreenId :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setData(bannerResList);
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<BannerDetailsEntity>> manageBanner(List<BannerReqDto> reqList) {
		var response = new GenericResponse<List<BannerDetailsEntity>>();
		var bannerList = new ArrayList<BannerDetailsEntity>();
		resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		try {
			reqList.forEach(bannerObj -> {
				var entity = bannerRepository
						.findById_UnitIdAndId_ChannelIdAndId_SegmentTypeAndId_ScreenIdAndId_LangCodeAndId_BannerId(
								bannerObj.getUnitId(), bannerObj.getChannelId(), bannerObj.getSegmentType(),
								bannerObj.getScreenId(), bannerObj.getLangCode(), bannerObj.getBannerId());
				if (bannerObj.getAction().equals("ADD")) {
					if (entity != null) {
						log.info("Banner already exists, Add operation failed!");
						resultVo = new ResultUtilVO(AppConstant.BANNER_ALREADY_EXISTS_CODE,
								AppConstant.BANNER_ALREADY_EXISTS_DESC);
					} else {
						var bannerEntity = bannerRepository.findById_BannerId(bannerObj.getBannerId());
						if (Objects.nonNull(bannerEntity)) {
							resultVo = new ResultUtilVO(AppConstant.BANNER_ALREADY_EXISTS_CODE,
									"Banner with bannerId: " + bannerObj.getBannerId() + " already exists!");
						} else {
							var bannerwithPriority = bannerRepository
									.findById_UnitIdAndId_ChannelIdAndId_ScreenIdAndId_LangCodeAndImageResolAndDisPriorityAndStatus(
											bannerObj.getUnitId(), bannerObj.getChannelId(), bannerObj.getScreenId(),
											bannerObj.getLangCode(), bannerObj.getImageResolution(),
											bannerObj.getDispPriority(),
											AppConstant.ACT);

							if (Objects.nonNull(bannerwithPriority) && !bannerwithPriority.isEmpty()) {
								resultVo = new ResultUtilVO(AppConstant.BANNER_ALREADY_EXISTS_CODE,
										"Banner with priority: " + bannerObj.getDispPriority()
												+ " already exists! Please change the priority.");
							} else {
								var banner = BannerDetailsEntity.toEntity(bannerObj);
								bannerList.add(banner);
							}
						}
					}
				} else if (bannerObj.getAction().equals("MODIFY")) {
					if (entity == null) {
						log.info("Banner does not exists, Modify operation failed!");
						resultVo = new ResultUtilVO(AppConstant.BANNER_NOT_EXISTS_CODE,
								AppConstant.BANNER_NOT_EXISTS_DESC);
					} else {
						var entityToModify = BannerDetailsEntity.toModify(bannerObj, entity);
						bannerList.add(entityToModify);
					}
				} else if (bannerObj.getAction().equals("DELETE")) {
					if (entity == null) {
						log.info("Banner does not exists, Delete operation failed!");
						resultVo = new ResultUtilVO(AppConstant.BANNER_NOT_EXISTS_CODE,
								AppConstant.BANNER_NOT_EXISTS_DESC);
					} else {
						entity.setStatus(AppConstant.DEL);
						bannerList.add(entity);
					}
				} else {
					log.info("Invalid action");
					resultVo = new ResultUtilVO(AppConstant.INVALID_ACTION_CODE, AppConstant.INVALID_ACTION_DESC);
				}
			});
			if (bannerList != null) {
				bannerRepository.saveAll(bannerList);
			}
		} catch (Exception e) {
			log.info("Exception in calling manage-banner :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
}