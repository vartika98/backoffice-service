package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.BeneficiaryDetailRequest;
import com.qnb.bo.backofficeservice.entity.BeneficiaryEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.BeneficiaryRepository;
import com.qnb.bo.backofficeservice.service.BeneficiaryService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BeneficiaryServiceImpl implements BeneficiaryService {

	@Autowired
	private BeneficiaryRepository benRepo;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<List<BeneficiaryEntity>> getBeneficiaryListForApproval(BeneficiaryDetailRequest req) {
		log.debug("request {}", req);
		var response = new GenericResponse<List<BeneficiaryEntity>>();
		var beneficiaries = new ArrayList<BeneficiaryEntity>();
		try {
			var outsideQNB = (req.getWithinQNB() != null && req.getWithinQNB().equalsIgnoreCase("Y")) ? true : false;
			var flagList = Arrays.asList("M", "A");
			log.debug("getBeneficiaryListForApproval() outsideQNB {} ", req.getWithinQNB());
			if (!req.getUnits().isEmpty()) {

				log.info("outsideQNB flag is ", outsideQNB, "req.getUnits() is ", req.getUnits(), "Y", "The status is ",
						req.getStatus(), "The flog list is ", flagList);
				beneficiaries = outsideQNB
						? (ArrayList<BeneficiaryEntity>) benRepo
								.getQNBBenficiaryForApprovalByUnitsAndTxnNameFlagOrderByCreatedDateDes(req.getUnits(),
										"Y", req.getStatus(), flagList)
						: (ArrayList<BeneficiaryEntity>) benRepo
								.getBenficiaryForApprovalByUnitsAndTxnNameFlagOrderByCreatedDateDes(req.getUnits(), "Y",
										req.getStatus(), flagList);
			} else {
				log.debug("else getBeneficiaryListForApproval() outsideQNB else {} ", outsideQNB);
				beneficiaries = outsideQNB
						? (ArrayList<BeneficiaryEntity>) benRepo
								.findByBoApprvalAndStatusAndRoutingChannelAndTxnNameFlagOrderByDateCreatedDes("Y",
										req.getStatus(), "QNB", flagList)
						: (ArrayList<BeneficiaryEntity>) benRepo
								.findByBoApprvalAndStatusAndTxnNameFlagOrderByDateCreatedDes("Y", req.getStatus(),
										flagList);

			}
			response.setData(beneficiaries);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);

		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<BeneficiaryEntity> getBeneficiaryData(BeneficiaryDetailRequest request) {

		var response = new GenericResponse<BeneficiaryEntity>();
		try {
			var beneficiary = new BeneficiaryEntity();
			var exists = benRepo.existsByRefNo(request.getRefNo());
			if (exists) {
				beneficiary = benRepo.findByRefNo(request.getRefNo());
				response.setData(beneficiary);
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			} else {
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, "No Data Found");
			}
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<BeneficiaryEntity> getBeneficiaryApproveReject(BeneficiaryDetailRequest request) {
		var response = new GenericResponse<BeneficiaryEntity>();
		var beneficiary = new BeneficiaryEntity();
		try {
			var exists = benRepo.existsByRefNo(request.getRefNo());
			if (exists) {
				beneficiary = benRepo.findByRefNo(request.getRefNo());
				if (request.getStatus().equalsIgnoreCase("APR")) {
					beneficiary.setStatus("Y");
				} else {
					beneficiary.setStatus("N");
				}
				benRepo.save(beneficiary);
				response.setData(beneficiary);
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			} else {
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, "No Data Found");
			}

		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
}
