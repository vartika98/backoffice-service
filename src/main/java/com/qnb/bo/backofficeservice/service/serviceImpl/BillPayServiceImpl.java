package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.billPay.DenominationDto;
import com.qnb.bo.backofficeservice.dto.billPay.ManagePayeeDto;
import com.qnb.bo.backofficeservice.dto.billPay.ManageUtilityCodesDto;
import com.qnb.bo.backofficeservice.dto.billPay.ManageUtilityTypesDto;
import com.qnb.bo.backofficeservice.dto.billPay.PayeeDto;
import com.qnb.bo.backofficeservice.dto.billPay.UtilityCodesDto;
import com.qnb.bo.backofficeservice.dto.billPay.UtilityDto;
import com.qnb.bo.backofficeservice.dto.billPay.UtilityTypesDto;
import com.qnb.bo.backofficeservice.entity.PayeeEntity;
import com.qnb.bo.backofficeservice.entity.PrePaidDenominations;
import com.qnb.bo.backofficeservice.entity.UtilityCodesEntity;
import com.qnb.bo.backofficeservice.entity.UtilityTypesEntity;
import com.qnb.bo.backofficeservice.mapper.BillPayMapper;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.DenominationRepository;
import com.qnb.bo.backofficeservice.repository.PayeeRepository;
import com.qnb.bo.backofficeservice.repository.UtilityCodesReposiroty;
import com.qnb.bo.backofficeservice.repository.UtilityTypesRepository;
import com.qnb.bo.backofficeservice.service.BillPayService;
import com.qnb.bo.backofficeservice.views.BillPayDenominationView;
import com.qnb.bo.backofficeservice.views.BillPayPayeeView;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BillPayServiceImpl implements BillPayService {

	@Autowired
	private PayeeRepository payeeRepository;

	@Autowired
	private UtilityCodesReposiroty utilityCodesRepository;

	@Autowired
	private UtilityTypesRepository utilityTypesRepository;

	@Autowired
	private DenominationRepository denominationRepository;

	@Autowired
	private BillPayMapper mapper;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<List<PayeeDto>> getPayees() {
		var response = new GenericResponse<List<PayeeDto>>();
		var payeeList = new ArrayList<PayeeDto>();
		var payeeListView = new ArrayList<BillPayPayeeView>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			payeeListView = (ArrayList<BillPayPayeeView>) payeeRepository.getPayees();
			payeeListView.forEach(view -> {
				var payeeId = view.getPayeeId();
				var payeeDesc = view.getPayeeDesc();
				var status = view.getStatus();
				var payeeLogo = "";
				try {
					payeeLogo = Objects.nonNull(view.getPayeeLogo()) ? view.getPayeeLogo().getSubString(1, (int) view.getPayeeLogo().length()) : null;
				} catch (SQLException e) {
					log.info("Exception:{}", e);
				}
				var unitCode = view.getUnitCode();
				var unitCodeDesc = view.getUnitCodeDesc();
				var priority = view.getPriority();
				
				payeeList.add(new PayeeDto(payeeId, payeeDesc, status, payeeLogo, unitCode, 
						unitCodeDesc, priority));
			});
			
			response.setData(payeeList);
		} catch (Exception e) {
			log.info("Exception:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<UtilityDto>> getUtilities(String payeeId) {
		var response = new GenericResponse<List<UtilityDto>>();
		var result = new ArrayList<UtilityDto>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var list = utilityCodesRepository.findByPayeeAndStatus(payeeId, AppConstant.ACT);
			var utilityMap = new LinkedHashMap<String, UtilityDto>();
			list.forEach(view -> {
				var utilityCode = view.getUtilityCode();
				var utilityType = view.getUtilityType();
				var utilityDesc = view.getUtilityDesc();
				var utilityTypeDesc = view.getUtilityTypeDesc();
				var partialPayment = view.getPartialPayment();
				var addPayeeCheck = view.getAddPayeeCheck();
				var denomination = view.getDenomination();
				utilityMap
						.computeIfAbsent(utilityCode,
								key -> new UtilityDto(utilityCode, utilityType, utilityDesc, utilityTypeDesc,
										partialPayment, addPayeeCheck, new ArrayList<>()))
						.getDenominations().add(denomination);
			});
			result = new ArrayList<>(utilityMap.values());
			response.setData(result);
		} catch (Exception e) {
			log.info("Exception:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<PayeeDto>> managePayees(List<ManagePayeeDto> dtolist) {
		var response = new GenericResponse<List<PayeeDto>>();
		var status = new ArrayList<String>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			status.add("ACT");
			status.add("IAC");
			var payeesToAdd = new ArrayList<PayeeEntity>();
			dtolist.forEach(payee -> {
				var payeeEntity = payeeRepository.findByPayeeIdAndStatusIn(payee.getPayeeId(), status);
				if (payee.getAction().equalsIgnoreCase("ADD")) {
					if (payeeEntity != null) {// entity already exists
						resultVo = new ResultUtilVO(AppConstant.PAYEE_EXISTS_CODE, AppConstant.PAYEE_EXISTS
								+ " (Cannot Add Payee having Payee Id: " + payee.getPayeeId() + ")");
						log.info("Payee already exists Add action cannot be performed");
					} else {// Add the entity
						var entity = new PayeeEntity();
						entity.setPayeeId(payee.getPayeeId());
						entity.setPayeeDesc(payee.getPayeeDesc());
						entity.setPayeeLogo(payee.getPayeeLogo());
						entity.setPriority(payee.getPriority());
						entity.setStatus(AppConstant.ACT);
						entity.setUnitCode(payee.getUnitCode());
						payeesToAdd.add(entity);
					}
				} else if (payee.getAction().equalsIgnoreCase("MODIFY")) {
					if (payeeEntity == null) { // payee does not exists cannot modify
						resultVo = new ResultUtilVO(AppConstant.PAYEE_DOES_NOT_EXISTS_CODE,
								AppConstant.PAYEE_DOES_NOT_EXISTS + " (Cannot Modify Payee having Payee Id: "
										+ payee.getPayeeId() + ")");
						log.info("Payee does not exists Modify action cannot be performed");
					} else {
						payeeEntity.setPayeeDesc(
								payee.getPayeeDesc() != null ? payee.getPayeeDesc() : payeeEntity.getPayeeDesc());
						payeeEntity.setPayeeLogo(
								payee.getPayeeLogo() != null ? payee.getPayeeLogo() : payeeEntity.getPayeeLogo());
						payeeEntity.setPriority(
								payee.getPriority() != null ? payee.getPriority() : payeeEntity.getPriority());
						payeeEntity.setUnitCode(
								payee.getUnitCode() != null ? payee.getUnitCode() : payeeEntity.getUnitCode());
						payeeEntity.setStatus(payee.getStatus() != null ? payee.getStatus() : payeeEntity.getStatus());

						payeesToAdd.add(payeeEntity);
					}
				} else if (payee.getAction().equalsIgnoreCase("DELETE")) {
					if (payeeEntity == null) { // payee does not exists cannot delete
						resultVo = new ResultUtilVO(AppConstant.PAYEE_DOES_NOT_EXISTS_CODE,
								AppConstant.PAYEE_DOES_NOT_EXISTS + " (Cannot Modify Payee having Payee Id: "
										+ payee.getPayeeId() + ")");
						log.info("Payee does not exists Delete action cannot be performed");
					} else {
						payeeEntity.setStatus(AppConstant.DEL);
						payeesToAdd.add(payeeEntity);
					}

				} else {
					resultVo = new ResultUtilVO(AppConstant.INVALID_ACTION_CODE, AppConstant.INVALID_ACTION_DESC);
					log.info("Invalid action, Action other than (ADD, MODIFY, and DELETE)");
				}
			});
			payeeRepository.saveAll(payeesToAdd);
		} catch (Exception e) {
			log.info("Exception:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<UtilityTypesDto>> getUtilityTypes() {
		var response = new GenericResponse<List<UtilityTypesDto>>();
		var result = new ArrayList<UtilityTypesDto>();
		var status = new ArrayList<String>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			status.add("ACT");
			status.add("IAC");
			result = (ArrayList<UtilityTypesDto>) mapper
					.toUtilityTypesDto(utilityTypesRepository.findByStatusInOrderByDateModifiedDesc(status));
			response.setData(result);
		} catch (Exception e) {
			log.info("Exception:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<UtilityTypesDto>> manageUtilityTypes(List<ManageUtilityTypesDto> dtoList) {
		var response = new GenericResponse<List<UtilityTypesDto>>();
		var utilityTypesToAdd = new ArrayList<UtilityTypesEntity>();
		var status = new ArrayList<String>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			status.add("ACT");
			status.add("IAC");
			dtoList.forEach(dto -> {
				var entity = utilityTypesRepository.findByUtilityTypeAndStatusIn(dto.getUtilityType(), status);
				if (dto.getAction().equalsIgnoreCase("ADD")) {
					if (entity != null) {// utlity type already exists
						resultVo = new ResultUtilVO(AppConstant.UTILITY_TYPE_EXISTS_CODE,
								AppConstant.UTILITY_TYPE_EXISTS + " (Cannot Add Utility type having Type: "
										+ dto.getUtilityType() + ")");
						log.info("Utility type already exists Add action cannot be performed");
					} else {
						var utilityType = new UtilityTypesEntity();
						utilityType.setUtilityType(dto.getUtilityType());
						utilityType.setUtilityDesc(dto.getUtilityDesc());
						utilityType.setStatus(dto.getStatus());
						utilityTypesToAdd.add(utilityType);
					}
				} else if (dto.getAction().equalsIgnoreCase("MODIFY")) {
					if (entity == null) {// utlity type does not exists cannot modify
						resultVo = new ResultUtilVO(AppConstant.UTILITY_TYPE_DOES_NOT_EXISTS_CODE,
								AppConstant.UTILITY_TYPE_DOES_NOT_EXISTS + " (Cannot Modify Utility type having Type: "
										+ dto.getUtilityType() + ")");
						log.info("Utility type does not exists Modify action cannot be performed");
					} else {
						entity.setUtilityDesc(
								dto.getUtilityDesc() != null ? dto.getUtilityDesc() : entity.getUtilityDesc());
						entity.setStatus(dto.getStatus() != null ? dto.getStatus() : entity.getStatus());
						utilityTypesToAdd.add(entity);
					}
				} else if (dto.getAction().equalsIgnoreCase("DELETE")) {
					if (entity == null) {// utlity type does not exists cannot delete
						resultVo = new ResultUtilVO(AppConstant.UTILITY_TYPE_DOES_NOT_EXISTS_CODE,
								AppConstant.UTILITY_TYPE_DOES_NOT_EXISTS + " (Cannot Delete Utility type having Type: "
										+ dto.getUtilityType() + ")");
						log.info("Utility type does not exists Delete action cannot be performed");
					} else {
						entity.setStatus(AppConstant.DEL);
						utilityTypesToAdd.add(entity);
					}
				} else {
					resultVo = new ResultUtilVO(AppConstant.INVALID_ACTION_CODE, AppConstant.INVALID_ACTION_DESC);
					log.info("Invalid action, Action other than (ADD, MODIFY, and DELETE)");
				}
			});
			utilityTypesRepository.saveAll(utilityTypesToAdd);
		} catch (Exception e) {
			log.info("Exception:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<UtilityDto>> manageUtilityCodes(List<ManageUtilityCodesDto> dtolist) {
		var response = new GenericResponse<List<UtilityDto>>();
		var utilityTypesToAdd = new ArrayList<UtilityCodesEntity>();
		var denominations = new ArrayList<PrePaidDenominations>();
		var status = new ArrayList<String>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			status.add("ACT");
			status.add("IAC");
			dtolist.forEach(dto -> {
				var entity = utilityCodesRepository.findByUtilityCodeAndStatusIn(dto.getUtilityCode(), status);
				if (dto.getAction().equalsIgnoreCase("ADD")) {
					if (entity != null) { // entity already exists
						resultVo = new ResultUtilVO(AppConstant.UTILITY_CODE_EXISTS, AppConstant.UTILITY_CODE_EXISTS
								+ " (Cannot Add Utility Code having Code: " + dto.getUtilityCode() + ")");
						log.info("Utility code already exists Add action cannot be performed");
					} else {
						var utilityCode = new UtilityCodesEntity();
						utilityCode.setUtilityCode(dto.getUtilityCode());
						utilityCode.setUtilityDesc(dto.getUtilityDesc());
						utilityCode.setPayeeId(dto.getPayeeId());
						utilityCode.setStatus(AppConstant.ACT);
						utilityCode.setUtilityType(dto.getUtilityType());
						utilityCode.setUtilitySuspAcc(dto.getUtilitySuspAcc());
						utilityCode.setUnitCode(dto.getUnitCode());
						utilityCode.setPartialPayment(dto.getPartialPayment());
						utilityCode.setAddPayeeCheck(dto.getAddPayeeCheck());
						utilityCode.setUtilityCmsnFee(dto.getUtilityCmsnFee());

						if(Objects.nonNull(dto.getDenominationList())) {
							dto.getDenominationList().forEach(d -> {
								PrePaidDenominations denoEntity = new PrePaidDenominations();
								denoEntity.setUtilityCode(dto.getUtilityCode());
								denoEntity.setDenomination(d.getDenomination());
								denoEntity.setStatus(AppConstant.ACT);
								denominations.add(denoEntity);
							});
						}
						utilityTypesToAdd.add(utilityCode);
					}
				} else if (dto.getAction().equalsIgnoreCase("MODIFY")) {
					if (entity == null) { // entity does not exists cannot modify
						resultVo = new ResultUtilVO(AppConstant.UTILITY_CODE_DOES_NOT_EXISTS_CODE,
								AppConstant.UTILITY_CODE_DOES_NOT_EXISTS + " (Cannot Modify Utility code having Code: "
										+ dto.getUtilityCode() + ")");
						log.info("Utility code does not exists Modify action cannot be performed");
					} else {
						entity.setUtilityDesc(
								dto.getUtilityDesc() != null ? dto.getUtilityDesc() : entity.getUtilityDesc());
						entity.setPayeeId(dto.getPayeeId() != null ? dto.getPayeeId() : entity.getPayeeId());
						entity.setStatus(dto.getStatus() != null ? dto.getStatus() : entity.getStatus());
						entity.setUtilityType(
								dto.getUtilityType() != null ? dto.getUtilityType() : entity.getUtilityType());
						entity.setUtilitySuspAcc(
								dto.getUtilitySuspAcc() != null ? dto.getUtilitySuspAcc() : entity.getUtilitySuspAcc());
						entity.setUnitCode(dto.getUnitCode() != null ? dto.getUnitCode() : entity.getUnitCode());
						entity.setPartialPayment(
								dto.getPartialPayment() != null ? dto.getPartialPayment() : entity.getPartialPayment());
						entity.setAddPayeeCheck(
								dto.getAddPayeeCheck() != null ? dto.getAddPayeeCheck() : entity.getAddPayeeCheck());
						entity.setUtilityCmsnFee(
								dto.getUtilityCmsnFee() != null ? dto.getUtilityCmsnFee() : entity.getUtilityCmsnFee());
						utilityTypesToAdd.add(entity);
					}
					if(Objects.nonNull(dto.getDenominationList())) {
						var updateDlist = dto.getDenominationList();
						var existingDList = denominationRepository.findByUtilityCode(dto.getUtilityCode());
						var presentDenominationList = existingDList.stream().map(i -> {
							return i.getDenomination();
						}).collect(Collectors.toList());

						var updateDenominationMap = updateDlist.stream()
								.collect(Collectors.toMap(i -> i.getDenomination(), i -> i.getStatus()));

						existingDList.forEach(existsObj -> { // adding already present
							if (updateDenominationMap.containsKey(existsObj.getDenomination())) {
								existsObj.setStatus(updateDenominationMap.get(existsObj.getDenomination()));
								denominations.add(existsObj);
							} else { // delete which is not present
								denominationRepository.deleteByUtilityCodeAndDenomination(existsObj.getUtilityCode(),
										existsObj.getDenomination());
							}
						});

						updateDlist.forEach(updObj -> {
							if (!presentDenominationList.contains(updObj.getDenomination())) {
								var newEntity = new PrePaidDenominations();
								newEntity.setUtilityCode(dto.getUtilityCode());
								newEntity.setDenomination(updObj.getDenomination());
								newEntity.setStatus(updObj.getStatus());
								denominations.add(newEntity);
							}
						});
					}
					
				} else if (dto.getAction().equalsIgnoreCase("DELETE")) {
					if (entity == null) { // entity does not exists cannot delete
						resultVo = new ResultUtilVO(AppConstant.UTILITY_CODE_DOES_NOT_EXISTS_CODE,
								AppConstant.UTILITY_CODE_DOES_NOT_EXISTS + " (Cannot Delete Utility code having Code: "
										+ dto.getUtilityCode() + ")");
						log.info("Utility code does not exists Delete action cannot be performed");
					} else {
						entity.setStatus(AppConstant.DEL);
						utilityTypesToAdd.add(entity);
					}
				} else {
					resultVo = new ResultUtilVO(AppConstant.INVALID_ACTION_CODE, AppConstant.INVALID_ACTION_DESC);
					log.info("Invalid action, Action other than (ADD, MODIFY, and DELETE)");
				}
			});
			utilityCodesRepository.saveAll(utilityTypesToAdd);
			denominationRepository.saveAll(denominations);
		} catch (Exception e) {
			log.info("Exception:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<DenominationDto>> getDenominations(String utilityCode) {
		var response = new GenericResponse<List<DenominationDto>>();
		var result = new ArrayList<DenominationDto>();
		try {
			var list = new ArrayList<BillPayDenominationView>();
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			if(utilityCode.equalsIgnoreCase("ALL")) {
				list = (ArrayList<BillPayDenominationView>) denominationRepository.findByStatus();
			} else {
				list = (ArrayList<BillPayDenominationView>) denominationRepository.findByUtilityCodeAndStatus(utilityCode);
			}
			list.forEach(obj -> {
				result.add(new DenominationDto(obj.getUtilityCode(), obj.getDenomination(), obj.getStatus(),
						obj.getUtilityDesc(), obj.getTxnId()));
			});
			response.setData(result);
		} catch (Exception e) {
			log.info("Exception:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<UtilityCodesDto>> getUtilitiesByTypes(String utilityType) {
		var response = new GenericResponse<List<UtilityCodesDto>>();
		var result = new ArrayList<UtilityCodesDto>();
		var utilityTypesList = new ArrayList<String>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			if(utilityType.equalsIgnoreCase("ALL")) {
				utilityTypesList = (ArrayList<String>) utilityTypesRepository.fetchAllUtilityTypes();
			}else {
				utilityTypesList.add(utilityType);
			}
			var list = utilityCodesRepository.findByUtilityType(utilityTypesList);
			list.forEach(view -> {
				var utilityCode = view.getUtilityCode();
				var utilityCodeDesc = view.getUtilityCodeDesc();
				var payeeId = view.getPayeeId();
				var payeeDesc = view.getPayeeDesc();
				var utilityTpe = view.getUtilityType();
				var utilityTypeDesc = view.getUtilityTypeDesc();
				var unitCode = view.getUnitCode();
				var unitCodeDesc = view.getUnitCodeDesc();
				var commissionFee = view.getCommissionFee();
				var status = view.getStatus();
				
				result.add(new UtilityCodesDto(utilityCode, utilityCodeDesc, payeeId, payeeDesc, utilityTpe, 
						utilityTypeDesc, unitCode, unitCodeDesc, commissionFee, status));
			});
			response.setData(result);
		} catch (Exception e) {
			log.info("Exception:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<DenominationDto>> manageDenominations(List<DenominationDto> dtoList) {
		var response = new GenericResponse<List<DenominationDto>>();
		var denominationsToAdd = new ArrayList<PrePaidDenominations>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			dtoList.forEach(dto -> {
				PrePaidDenominations denomination = denominationRepository.findByTxnId(dto.getTxnId());
				List<PrePaidDenominations> denoList =  denominationRepository.findByUtilityCodeAndDenomination(dto.getUtilityCode(), dto.getDenomination());
				
				if(dto.getAction().equalsIgnoreCase("ADD")) {
					if(!denoList.isEmpty()) { //denomination already exists
						resultVo = new ResultUtilVO(AppConstant.DENOMINATION_EXISTS_CODE, "Denomination with same value "+ dto.getDenomination()+" exists!");
						log.info("Denomination already exists");
					} else {// add the denomination
						PrePaidDenominations deno = new PrePaidDenominations();
						deno.setUtilityCode(dto.getUtilityCode());
						deno.setDenomination(dto.getDenomination());
						deno.setStatus(dto.getStatus());
						denominationsToAdd.add(deno);
					}
				} else if (dto.getAction().equalsIgnoreCase("MODIFY")) {
					if (Objects.nonNull(denomination)) { // denomination exists, modify
						denomination.setUtilityCode(Objects.nonNull(dto.getUtilityCode()) ? dto.getUtilityCode() : denomination.getUtilityCode());
						denomination.setDenomination( Objects.nonNull(dto.getDenomination()) ? dto.getDenomination() : denomination.getDenomination());
						denomination.setStatus(Objects.nonNull(dto.getStatus()) ? dto.getStatus() : denomination.getStatus());
						denominationsToAdd.add(denomination);
					} else {//cannot modify
						resultVo = new ResultUtilVO(AppConstant.DENOMINATION_DOES_NOT_EXISTS_CODE, "Denomination does not exists!");
						log.info("Denomination does not exists!");
					}
				} else if(dto.getAction().equalsIgnoreCase("DELETE")) {
					if(Objects.nonNull(denomination)) { //delete
						denomination.setStatus(AppConstant.DEL);
						denominationsToAdd.add(denomination);
					} else { //cannot delete
						resultVo = new ResultUtilVO(AppConstant.DENOMINATION_DOES_NOT_EXISTS_CODE, "Denomination does not exists!");
						log.info("Denomination does not exists!");
					}
				} else {
					resultVo = new ResultUtilVO(AppConstant.INVALID_ACTION_CODE, AppConstant.INVALID_ACTION_DESC);
					log.info("Invalid action, Action other than (ADD, MODIFY, and DELETE)");
				}
			});
			
			denominationRepository.saveAll(denominationsToAdd);
		} catch (Exception e) {
			log.info("Exception:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
