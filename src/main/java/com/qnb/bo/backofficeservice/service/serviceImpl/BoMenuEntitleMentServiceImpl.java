package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.boMenu.AccessibleMenus;
import com.qnb.bo.backofficeservice.dto.boMenu.BoMenuDto;
import com.qnb.bo.backofficeservice.dto.boMenu.BoMenuEntitlementRes;
import com.qnb.bo.backofficeservice.dto.boMenu.BoSubMenuRes;
import com.qnb.bo.backofficeservice.dto.boMenu.FunctionResDto;
import com.qnb.bo.backofficeservice.dto.boMenu.ProductResDto;
import com.qnb.bo.backofficeservice.dto.boMenu.SubProductWithFunctions;
import com.qnb.bo.backofficeservice.dto.boMenu.SubproductResDto;
import com.qnb.bo.backofficeservice.entity.UserRoleMapping;
import com.qnb.bo.backofficeservice.mapper.MenuMapper;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.BoMenuEntitlementRepository;
import com.qnb.bo.backofficeservice.repository.OcsFunctionMasterRepository;
import com.qnb.bo.backofficeservice.repository.OcsProductMasterRepository;
import com.qnb.bo.backofficeservice.repository.OcsSubProductMasterRepository;
import com.qnb.bo.backofficeservice.repository.UserRoleMappingRepository;
import com.qnb.bo.backofficeservice.service.BoMenuEntitlementService;
import com.qnb.bo.backofficeservice.views.BOMenuEntitlementView;
import com.qnb.bo.backofficeservice.views.BoMenuAccessView;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BoMenuEntitleMentServiceImpl implements BoMenuEntitlementService {

	@Autowired
	private BoMenuEntitlementRepository boMenuRepository;

	@Autowired
	private MenuMapper mapper;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private OcsFunctionMasterRepository functionRepo;

	@Autowired
	private OcsProductMasterRepository productMasterRepo;

	@Autowired
	private OcsSubProductMasterRepository subProdRepo;
	
	@Autowired
	private UserRoleMappingRepository userRoleMappingRepo;

	@Override
	public GenericResponse<List<BoMenuDto>> getMenuByUser(String userNo) {
		var response = new GenericResponse<List<BoMenuDto>>();
		var menuList = new ArrayList<BoMenuDto>();
		try {
			menuList = (ArrayList<BoMenuDto>) mapper
					.boMenutoDto(boMenuRepository.findByUserNoAndStatus(userNo, AppConstant.ACT));
			response.setData(menuList);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			log.info("Exception details in getMenuByUser:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<BoMenuEntitlementRes>> getMenuEntitlement() {
		var response = new GenericResponse<List<BoMenuEntitlementRes>>();
		var boMenuList = new ArrayList<BoMenuEntitlementRes>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var prodDetLst = functionRepo.getMenuEntitlement();
			if (!prodDetLst.isEmpty()) {
				var prodLst = prodDetLst.stream().collect(Collectors.groupingBy(BOMenuEntitlementView::getProdCode,
						LinkedHashMap::new, Collectors.toList()));
				prodLst.entrySet().forEach(prod -> {
					var menu = new BoMenuEntitlementRes();
					menu.setProductCode(prod.getValue().get(0).getProdCode());
					menu.setProductDesc(prod.getValue().get(0).getProdDesc());
					var subProdLst = prod.getValue().stream().collect(Collectors.groupingBy(
							BOMenuEntitlementView::getSubProdCode, LinkedHashMap::new, Collectors.toList()));
					var subMenuLst = new ArrayList<BoSubMenuRes>();
					subProdLst.entrySet().forEach(subProd -> {
						var subMenu = new BoSubMenuRes();
						subMenu.setSubProductcode(subProd.getValue().get(0).getSubProdCode());
						subMenu.setSubProductDesc(subProd.getValue().get(0).getSubProdDesc());
						var functionList = new ArrayList<Map<String, String>>();
						subProd.getValue().forEach(func -> {
							var function = new HashMap<String, String>();
							function.put("functionCode", func.getFunctionCode());
							function.put("functionDesc", func.getFunctionDesc());
							functionList.add(function);
							subMenu.setFuntion(functionList);
						});
						subMenuLst.add(subMenu);
					});
					menu.setSubProduct(subMenuLst);
					boMenuList.add(menu);
				});
				log.info(":{}", boMenuList);
				response.setData(boMenuList);
			}
		} catch (Exception e) {
			log.info("Exception details in getMenuEntitlement :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<ProductResDto>> getProductSubProduct() {
		var response = new GenericResponse<List<ProductResDto>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var productList = productMasterRepo.findAll();
			var result = new ArrayList<ProductResDto>();
			productList.stream().forEach(product -> {
				var dto = new ProductResDto();
				var subDtoList = new ArrayList<SubproductResDto>();
				dto.setProductCode(product.getProductCode());
				dto.setProductDesc(product.getProductDescription());
				var subList = product.getSubProducts();
				subList.forEach(subProduct -> {
					var subDto = new SubproductResDto();
					subDto.setSubProductCode(subProduct.getSubProductCode());
					subDto.setSubProductDesc(subProduct.getSubProductDesc());
					subDtoList.add(subDto);
				});
				dto.setSubProduct(subDtoList);
				result.add(dto);
			});
			response.setData(result);
		} catch (Exception e) {
			log.info("Exception :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<Map<String, String>>> productList() {
		var response = new GenericResponse<List<Map<String, String>>>();
		var productLst = new ArrayList<Map<String, String>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var productList = productMasterRepo.findAll();
			if (productList.isEmpty()) {
				resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "No product is available.");
			} else {
				productList.forEach(product -> {
					var productMap = new HashMap<String, String>();
					productMap.put("productCode", product.getProductCode());
					productMap.put("productDesc", product.getProductDescription());
					productLst.add(productMap);
				});
				response.setData(productLst);
			}
		} catch (Exception e) {
			log.info("Exception in calling productList:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<Map<String, String>>> subProdList(Map<String, String> req) {
		var response = new GenericResponse<List<Map<String, String>>>();
		var subProdLst = new ArrayList<Map<String, String>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var subProdList = subProdRepo.findByProductCode(req.get("prodCode"));
			if (subProdList.isEmpty()) {
				resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "No sub product is available.");
			} else {
				subProdList.forEach(subProd -> {
					var subProdMap = new HashMap<String, String>();
					subProdMap.put("subProdCode", subProd.getSubProductCode());
					subProdMap.put("subProdDesc", subProd.getSubProductDesc());
					subProdLst.add(subProdMap);
				});
				response.setData(subProdLst);
			}
		} catch (Exception e) {
			log.info("Exception in calling subProdList:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<Map<String, String>>> functionList(Map<String, String> req) {
		var response = new GenericResponse<List<Map<String, String>>>();
		var functionLst = new ArrayList<Map<String, String>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var functionList = functionRepo.findByProductCodeAndSubProductCode(req.get("prodCode"),
					req.get("subProdCode"));
			if (functionList.isEmpty()) {
				resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "No sub product is available.");
			} else {
				functionList.forEach(function -> {
					var funcMap = new HashMap<String, String>();
					funcMap.put("functionCode", function.getFunctionCode());
					funcMap.put("functionDesc", function.getFunctionDesc());
					functionLst.add(funcMap);
				});
				response.setData(functionLst);
			}
		} catch (Exception e) {
			log.info("Exception in calling functionList:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<AccessibleMenus>> getAccessibleMenus(Map<String, String> req) {
		var response = new GenericResponse<List<AccessibleMenus>>();
		try {
			String userId = req.get("userId");
			
			List<BoMenuAccessView> result = new ArrayList<>();
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			
			if(Objects.nonNull(userId) && "ram123".equalsIgnoreCase(userId)) {
				result = userRoleMappingRepo.getAccessibleMenusTest(userId, AppConstant.ACT);
			} else {
				result = userRoleMappingRepo.getAccessibleMenus(userId, AppConstant.ACT);
			}
			var resultMap = new LinkedHashMap<String, AccessibleMenus>();
			result.forEach(obj -> {
				var productCode = obj.getProdCode();
				var productDesc = obj.getProdDesc();
				var subProductCode = obj.getSubProdCode();
				var subProductDesc = obj.getSubProdCodeDesc();
				var subProductUrl = obj.getSubProductUrl();
				var functionCode = obj.getFunctionCode();
				var functionDesc = obj.getFunctionCodeDesc();

				if (resultMap.containsKey(productCode)) {
					var menu = resultMap.get(productCode);
					var subProducts = menu.getSubProducts();
					var subProductFound = false;
					for (SubProductWithFunctions subProduct : subProducts) {
						if (subProduct.getSubProductCode().equals(subProductCode)) {
							subProduct.getFunctions().add(new FunctionResDto(functionCode, functionDesc));
							subProductFound = true;
							break;
						}
					}
					if (!subProductFound) {
						var subProduct = new SubProductWithFunctions();
						subProduct.setSubProductCode(subProductCode);
						subProduct.setSubProductDesc(subProductDesc);
						subProduct.setSubProductUrl(subProductUrl);
						var functions = new ArrayList<FunctionResDto>();
						functions.add(new FunctionResDto(functionCode, functionDesc));
						subProduct.setFunctions(functions);
						subProducts.add(subProduct);
					}
				} else {
					var menu = new AccessibleMenus();
					menu.setProductCode(productCode);
					menu.setProductDesc(productDesc);
					var subProducts = new ArrayList<SubProductWithFunctions>();
					var subProduct = new SubProductWithFunctions();
					subProduct.setSubProductCode(subProductCode);
					subProduct.setSubProductDesc(subProductDesc);
					subProduct.setSubProductUrl(subProductUrl);
					var functions = new ArrayList<FunctionResDto>();
					functions.add(new FunctionResDto(functionCode, functionDesc));
					subProduct.setFunctions(functions);
					subProducts.add(subProduct);
					menu.setSubProducts(subProducts);
					resultMap.put(productCode, menu);
				}
			});

			response.setData(new ArrayList<>(resultMap.values()));
		} catch (Exception e) {
			log.info("Exception :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
}
