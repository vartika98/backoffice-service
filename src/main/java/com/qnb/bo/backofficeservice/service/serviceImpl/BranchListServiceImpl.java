package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.branch.BranchListDTO;
import com.qnb.bo.backofficeservice.dto.branch.BranchListRequest;
import com.qnb.bo.backofficeservice.entity.BranchEntity;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.BranchesRepository;
import com.qnb.bo.backofficeservice.repository.UnitRespository;
import com.qnb.bo.backofficeservice.service.BranchListService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BranchListServiceImpl implements BranchListService {

	@Autowired
	private BranchesRepository brachRepo;

	@Autowired
	private UnitRespository unitRespository;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<List<BranchListDTO>> getBranchList(BranchListRequest branchReq) {

		var response = new GenericResponse<List<BranchListDTO>>();
		try {
			var dtoList = new ArrayList<BranchListDTO>();
			var errorreq = brachRepo.findByUnitIdAndStatus(branchReq.getUnitId(), "ACT");
			if (Objects.nonNull(errorreq)) {
				dtoList = (ArrayList<BranchListDTO>) errorreq.stream().map(req -> {
					var dto = new BranchListDTO();
					dto.setTxnId(req.getTxnId());
					dto.setUnitId(req.getUnitId());
					dto.setBranchCode(req.getBranchCode());
					dto.setCountryCode(req.getCountryCode());
					dto.setBranchDesc(req.getBranchDesc());
					dto.setBranchDescAr(req.getBranchDescAr());
					dto.setBranchDescFr(req.getBranchDescFr());
					dto.setCity(req.getCity());
					dto.setAddress(req.getAddress());
					dto.setCollectionBranchFlag(req.getCollectionBranchFlag());
					dto.setStatus(req.getStatus());
					dto.setCreatedBy(req.getCreatedBy());
					dto.setDateCreated(req.getDateCreated());
					dto.setModifiedBy(req.getModifiedBy());
					dto.setDateModified(req.getDateModified());
					dto.setBranchId(req.getTxnId());
					return dto;
				}).collect(Collectors.toList());
			}
			response.setData(dtoList);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);

		} catch (Exception e) {
			log.info("Exception in calling getAllBranches :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<Map<String, String>> manageBranches(BranchListRequest reqBody) {
		var response = new GenericResponse<Map<String, String>>();
		var list = new ArrayList<BranchEntity>();
		try {
			var existingBranchList = brachRepo.findByunitAndBranchCode(reqBody.getUnitId(),
					reqBody.getParamDetails().getBranchCode());
			if (Objects.nonNull(existingBranchList) && !existingBranchList.isEmpty()) {
				existingBranchList.forEach(req -> {
					req.setStatus(reqBody.getParamDetails().getAction().equalsIgnoreCase("mod") ? "ACT" : "DEL");
					if (reqBody.getParamDetails().getAction().equalsIgnoreCase("mod")) {
						req.setUnitId(reqBody.getUnitId());
						req.setCountryCode(reqBody.getParamDetails().getCountryCode());
						req.setBranchCode(reqBody.getParamDetails().getBranchCode());
						req.setBranchDesc(reqBody.getParamDetails().getBranchDesc());
						req.setBranchDescAr(reqBody.getParamDetails().getBranchDescAr());
						req.setBranchDescFr(reqBody.getParamDetails().getBranchDescFr());
						req.setCity(reqBody.getParamDetails().getCity());
						req.setAddress(reqBody.getParamDetails().getAddress());
						req.setCollectionBranchFlag(reqBody.getParamDetails().getCollectionBranchFlag());
						req.setStatus("ACT");
						req.setCreatedBy(reqBody.getParamDetails().getCreatedBy());
						req.setDateCreated(reqBody.getParamDetails().getDateCreated());
						req.setModifiedBy(reqBody.getParamDetails().getModifiedBy());
						req.setDateModified(reqBody.getParamDetails().getDateModified());
						list.add(req);
					}
				});

			} else {
				var req = new BranchEntity();
				req.setUnitId(reqBody.getUnitId());
				req.setCountryCode(reqBody.getParamDetails().getCountryCode());
				req.setBranchCode(reqBody.getParamDetails().getBranchCode());
				req.setBranchDesc(reqBody.getParamDetails().getBranchDesc());
				req.setBranchDescAr(reqBody.getParamDetails().getBranchDescAr());
				req.setBranchDescFr(reqBody.getParamDetails().getBranchDescFr());
				req.setCity(reqBody.getParamDetails().getCity());
				req.setAddress(reqBody.getParamDetails().getAddress());
				req.setCollectionBranchFlag(reqBody.getParamDetails().getCollectionBranchFlag());
				req.setStatus("ACT");
				req.setCreatedBy(reqBody.getParamDetails().getCreatedBy());
				req.setDateCreated(reqBody.getParamDetails().getDateCreated());
				req.setModifiedBy(reqBody.getParamDetails().getModifiedBy());
				req.setDateModified(reqBody.getParamDetails().getDateModified());
				list.add(req);

			}
			if (Objects.nonNull(list) && list.size() > 0) {
				brachRepo.saveAll(list);
			}
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);

		} catch (Exception e) {
			log.info("Exception in Branch :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
