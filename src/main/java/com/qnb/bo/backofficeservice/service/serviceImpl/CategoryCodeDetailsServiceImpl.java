package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.CategoryDetailsDto;
import com.qnb.bo.backofficeservice.entity.CategoryDetails;
import com.qnb.bo.backofficeservice.entity.CategoryMaster;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.CategoryCodeDetailsRepository;
import com.qnb.bo.backofficeservice.repository.CategoryCodeMasterRepository;
import com.qnb.bo.backofficeservice.service.CategoryCodeDetailsService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CategoryCodeDetailsServiceImpl implements CategoryCodeDetailsService {

	@Autowired
	private CategoryCodeDetailsRepository CategoryCodeDetailsRepository;
	@Autowired
	private CategoryCodeMasterRepository categoryCodeMasterRepo;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<List<CategoryDetails>> updateCategoryDetails(List<CategoryDetailsDto> categoryDetailsDto) {
		var response = new GenericResponse<List<CategoryDetails>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var existingCategoryCode = CategoryCodeDetailsRepository.findAll();
			var categoriesToSave = new ArrayList<CategoryDetails>();

			// for (CategoryDetailsDto request : categoryDetailsDto) {
			categoryDetailsDto.forEach(request -> {
				var categoryCode = categoryCodeMasterRepo.findByCategoryCode(request.getCategoryCode());
				if (categoryCode == null) {
					resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, "CategoryCode not valid: ");
					response.setStatus(resultVo);
					return;
				}
				var categoryExists = existingCategoryCode.stream()
						.anyMatch(category -> category.getCategoryCode().equals(request.getCategoryCode())
								&& category.getFieldName().equals(request.getFieldName())
								&& (category.getStatus().equals(AppConstant.ACT)
										|| category.getStatus().equals(AppConstant.IAC)));
				if ("MODIFY".equalsIgnoreCase(request.getAction()) || "DELETE".equalsIgnoreCase(request.getAction())) {
					if (categoryExists) {
						var existingCategory = existingCategoryCode.stream()
								.filter(category -> category.getCategoryCode().equals(request.getCategoryCode())
										&& category.getFieldName().equals(request.getFieldName()))
								.findFirst().orElse(null);
						if (Objects.nonNull(existingCategory)) {

							if ("MODIFY".equalsIgnoreCase(request.getAction())) {
								existingCategory.setFieldName(request.getFieldName());
								existingCategory.setStatus(request.getStatus());
								existingCategory.setDescription(request.getDescription());
								existingCategory.setCreatedAt(LocalDateTime.now());
								existingCategory.setStatus(request.getStatus());
								existingCategory.setCreatedBy("BO");
								categoriesToSave.add(existingCategory);
							} else if ("DELETE".equalsIgnoreCase(request.getAction())) {
								existingCategory.setStatus(AppConstant.DEL);
								categoriesToSave.add(existingCategory);
							}
						}
					} else {
						resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "Category with ID "
								+ request.getCategoryCode() + " and Code " + request.getCategoryCode() + " not found.");
					}
				} else if ("ADD".equalsIgnoreCase(request.getAction())) {
					if (categoryExists) {
						resultVo = new ResultUtilVO(AppConstant.ALREADY_EXISTS_CODE,
								"category  with ID " + " already exists.");
					} else {
						var newCategory = new CategoryDetails();
						newCategory.setCategoryCode(request.getCategoryCode());
//	                    newCategory.setCategoryCode(request.);
						newCategory.setFieldName(request.getFieldName());
						newCategory.setDescription(request.getDescription());
						newCategory.setCreatedAt(LocalDateTime.now());
						newCategory.setStatus(request.getStatus());
						newCategory.setCreatedBy("BO");
						categoriesToSave.add(newCategory);
					}
				}
				if (!categoriesToSave.isEmpty()) {
					var updatedConfigs = CategoryCodeDetailsRepository.saveAll(categoriesToSave);
				}
			});

		} catch (Exception e) {
			log.error("Error in Category management: {}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<Map<String, String>>> categoryCodeList() {
		var response = new GenericResponse<List<Map<String, String>>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var categoryList = CategoryCodeDetailsRepository.findDistinctCategogyCode();

			var categoryCodeList = new ArrayList<Map<String, String>>();
			categoryList.forEach(category -> {
				var categoryMap = new HashMap<String, String>();
				categoryMap.put("categoryCode", category);
				categoryCodeList.add(categoryMap);
			});

			response.setData(categoryCodeList);
		} catch (Exception e) {
			log.error("Error in Category management: {}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}

		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<CategoryDetails>> summaryList(Map<String, String> request) {
		var response = new GenericResponse<List<CategoryDetails>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var summary = CategoryCodeDetailsRepository.findByCategoryCodeAndStatusInOrderByCreatedAtDesc(
					request.get("categoryCode"), List.of("ACT", "IAC"));
			response.setData(summary);

		} catch (Exception e) {
			log.error("Error in Category management: {}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
