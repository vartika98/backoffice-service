package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.CategoryMasterDto;
import com.qnb.bo.backofficeservice.dto.categoryCode.CategoryCodeDto;
import com.qnb.bo.backofficeservice.entity.CategoryMaster;
import com.qnb.bo.backofficeservice.mapper.CategoryCodeMapper;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.CategoryCodeMasterRepository;
import com.qnb.bo.backofficeservice.service.CategoryCodeMasterService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CategoryCodeMasterServiceImpl implements CategoryCodeMasterService {

	@Autowired
	private CategoryCodeMasterRepository categoryCodeMasterRepo;

	@Autowired
	private CategoryCodeMapper catCodeMapper;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<List<CategoryMaster>> getCategoryCodeMasterList() {
		var response = new GenericResponse<List<CategoryMaster>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var existingCategoryCode = categoryCodeMasterRepo.findAllByStatusIn(List.of("ACT", "IAC"));
			response.setData(existingCategoryCode);

		} catch (Exception e) {
			log.error("Error in Category summary: {}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<CategoryMaster>> updateCategoryMaster(List<CategoryMasterDto> categoryMasterDto) {
		var response = new GenericResponse<List<CategoryMaster>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var existingCategoryCode = categoryCodeMasterRepo.findAll();
			var categoriesToSave = new ArrayList<CategoryMaster>();

			// for (CategoryMasterDto request : categoryMasterDto) {
			categoryMasterDto.forEach(request -> {
				var categoryExists = existingCategoryCode.stream()
						.anyMatch(category -> category.getCategoryCode().equals(request.getCategoryCode()) &&
//	        		        category.get().equals(request.getCategoryCode()) &&
								(category.getStatus().equals(AppConstant.ACT)
										|| category.getStatus().equals(AppConstant.IAC)));
				if ("MODIFY".equalsIgnoreCase(request.getAction()) || "DELETE".equalsIgnoreCase(request.getAction())) {
					if (categoryExists) {
						var existingCategory = existingCategoryCode.stream()
								.filter(lang -> lang.getCategoryCode().equals(request.getCategoryCode())).findFirst()
								.orElse(null);

						if (Objects.nonNull(existingCategory)) {
							if ("MODIFY".equalsIgnoreCase(request.getAction())) {
								existingCategory.setBoAuditFlag(request.getBoAuditFlag());
								existingCategory.setFoAuditFlag(request.getFoAuditFlag());
								existingCategory.setRrAuditFlag(request.getRrAuditFlag());
								existingCategory.setCreatedTime(LocalDateTime.now());
								existingCategory.setStatus(request.getStatus());
								existingCategory.setDescription(request.getDescription());
								existingCategory.setCreatedBy("BO");
								categoriesToSave.add(existingCategory);
							} else if ("DELETE".equalsIgnoreCase(request.getAction())) {
								existingCategory.setStatus(AppConstant.DEL);
								categoriesToSave.add(existingCategory);
							}
						}
					} else {
						resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "Category with ID "
								+ request.getCategoryCode() + " and Code " + request.getCategoryCode() + " not found.");
					}
				} else if ("ADD".equalsIgnoreCase(request.getAction())) {
					if (categoryExists) {
						resultVo = new ResultUtilVO(AppConstant.ALREADY_EXISTS_CODE,
								"category  with ID " + " already exists.");
					} else {
						var newCategory = new CategoryMaster();
						newCategory.setCategoryCode(request.getCategoryCode());
						newCategory.setBoAuditFlag(request.getBoAuditFlag());
						newCategory.setFoAuditFlag(request.getFoAuditFlag());
						newCategory.setRrAuditFlag(request.getRrAuditFlag());
						newCategory.setCreatedTime(LocalDateTime.now());
						newCategory.setDescription(request.getDescription());
						newCategory.setStatus(request.getStatus());
						newCategory.setCreatedBy("BO");
						categoriesToSave.add(newCategory);
					}
				}
				if (!categoriesToSave.isEmpty()) {
					var updatedConfigs = categoryCodeMasterRepo.saveAll(categoriesToSave);
				}
			});
		} catch (Exception e) {
			log.error("Error in Category management: {}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	// category list
	@Override
	public GenericResponse<List<CategoryCodeDto>> getCategoryCodes() {
		var response = new GenericResponse<List<CategoryCodeDto>>();
		var responseDataList = new ArrayList<CategoryCodeDto>();
		try {
			responseDataList = (ArrayList<CategoryCodeDto>) categoryCodeMasterRepo.findAll().stream().filter(x->x.getStatus().equals("ACT"))
					.map(category -> catCodeMapper.toDto(category)).collect(Collectors.toList());
			var allCategory = new CategoryCodeDto();
			allCategory.setCategoryCode("ALL");
			allCategory.setDescription("ALL");
			responseDataList.add(allCategory);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			response.setData(responseDataList);
		} catch (Exception e) {
			log.info("Exception in Category service:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
}
