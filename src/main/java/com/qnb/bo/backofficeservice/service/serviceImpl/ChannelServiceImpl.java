package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.channel.ChannelListRes;
import com.qnb.bo.backofficeservice.dto.channel.ManageChannelReqDto;
import com.qnb.bo.backofficeservice.entity.Channel;
import com.qnb.bo.backofficeservice.mapper.ChannelMapper;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.ChannelRespository;
import com.qnb.bo.backofficeservice.service.ChannelService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ChannelServiceImpl implements ChannelService {

	@Autowired
	private ChannelRespository channelRepo;

	@Autowired
	private ChannelMapper channelMapper;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<ChannelListRes> getAllChannels(String defaultStatus) {
		var response = new GenericResponse<ChannelListRes>();
		var resData = new ChannelListRes();
		try {
			var channelLst = channelRepo.findByStatus(defaultStatus).stream()
					.map(channel -> channelMapper.channelModeltoDto(channel)).collect(Collectors.toList());
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			resData.setChannels(channelLst);
			response.setData(resData);
		} catch (Exception e) {
			log.info("Exception in calling getAllChannels :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<String> manageChannels(List<ManageChannelReqDto> manageChannelReqDtoList) {
		var response = new GenericResponse<String>();
		response.setData("");
		resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		var channelsToAdd = new ArrayList<Channel>();
		try {
			var channelsList = channelRepo.findAll();
			manageChannelReqDtoList.stream().forEach(reqDto -> {
				if (reqDto.getAction().equals("ADD")) { // To add channel
					var channelExists = channelsList.stream()
							.anyMatch(channel -> channel.getChannelId().equals(reqDto.getChannelId()));
					if (channelExists) {
						// channel already exists
						log.info(String.format("Channel with channel id %s already exists. Channel creation failed",
								reqDto.getChannelId()));
						resultVo = new ResultUtilVO(AppConstant.GEN_CHANNEL_EXISTS_CODE,
								AppConstant.GEN_CHANNEL_EXISTS_DESC);
					} else {
						var channel = new Channel();
						channel.setChannelId(reqDto.getChannelId());
						channel.setChannelDesc(reqDto.getChannelDesc());
						channel.setDescription(reqDto.getDescription());
						channel.setCreatedBy("BO");
						channel.setModifiedBy("BO");
						channel.setStatus(AppConstant.ACT);
						channelsToAdd.add(channel);
					}
				} else if (reqDto.getAction().equals("DELETE")) { // To delete channel
					var existingChannel = channelsList.stream()
							.filter(channel -> channel.getChannelId().equals(reqDto.getChannelId())).findFirst()
							.orElse(null);

					if (existingChannel == null) {
						log.info(String.format("Channel with channel id %s does not exists. Channel deletion failed",
								reqDto.getChannelId()));
						resultVo = new ResultUtilVO(AppConstant.GEN_CHANNEL_NOT_EXISTS_CODE,
								AppConstant.GEN_CHANNEL_NOT_EXISTS_DESC);
					} else {
						var channel = new Channel();
						channel.setChannelId(existingChannel.getChannelId());
						channel.setChannelDesc(existingChannel.getChannelDesc());
						channel.setDescription(existingChannel.getDescription());
						channel.setStatus(AppConstant.DEL);
						channel.setModifiedBy("BO");
						channel.setModifiedTime(LocalDateTime.now());
						channel.setCreatedBy(existingChannel.getCreatedBy());
						channel.setTxnId(existingChannel.getTxnId());
						channel.setCreatedTime(existingChannel.getCreatedTime());
						channelsToAdd.add(channel);
					}
				} else { // To Modify channel
					var existingChannel = channelsList.stream()
							.filter(channel -> channel.getChannelId().equals(reqDto.getChannelId())).findFirst()
							.orElse(null);
					if (existingChannel == null) {
						log.info(
								String.format("Channel with channel id %s does not exists. Channel modification failed",
										reqDto.getChannelId()));
						resultVo = new ResultUtilVO(AppConstant.GEN_CHANNEL_NOT_EXISTS_CODE,
								AppConstant.GEN_CHANNEL_NOT_EXISTS_DESC);
					} else {
						// Modify the channel
						var channel = new Channel();
						channel.setChannelId(reqDto.getChannelId());
						channel.setChannelDesc(reqDto.getChannelDesc());
						channel.setDescription(reqDto.getDescription());
						channel.setStatus(reqDto.getStatus());
						channel.setModifiedBy("BO");
						channel.setModifiedTime(LocalDateTime.now());
						channel.setCreatedBy(existingChannel.getCreatedBy());
						channel.setTxnId(existingChannel.getTxnId());
						channel.setCreatedTime(existingChannel.getCreatedTime());
						channelsToAdd.add(channel);
					}
				}
			});
			// save to CHANNEL_MASTER table
			if (!channelsToAdd.isEmpty()) {
				channelRepo.saveAll(channelsToAdd);
			}

		} catch (Exception e) {
			log.info("Exception in calling manageChannels :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<ChannelListRes> getAllChannelsSummary() {
		var response = new GenericResponse<ChannelListRes>();
		var resData = new ChannelListRes();
		try {
			var channelLst = channelRepo.findByStatusInOrderByCreatedTimeDesc(List.of("ACT", "IAC")).stream()
					.map(channel -> channelMapper.channelModeltoDto(channel)).collect(Collectors.toList());
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			resData.setChannels(channelLst);
			response.setData(resData);
		} catch (Exception e) {
			log.info("Exception in calling getAllChannelsSummary :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
