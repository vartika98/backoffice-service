package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.enums.ProcessFlow;
import com.qnb.bo.backofficeservice.enums.WorkflowStatus;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.UserRepository;
import com.qnb.bo.backofficeservice.repository.WorkflowRepository;
import com.qnb.bo.backofficeservice.service.CountService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CountServiceImpl implements CountService {
	
	@Autowired
	private WorkflowRepository workflowRepository;

	@Autowired
	private UserRepository userRepo;
	
	private ResultUtilVO resultVo = new ResultUtilVO();
	
	@Autowired
	private WorkflowServiceImpl workflowServiceImpl;

	@Override
	public GenericResponse<Map<String, Map<String, Integer>>> getUserCountRequest(List<String> userGroup,
			List<WorkflowStatus> status) {
		var response = new GenericResponse<Map<String, Map<String, Integer>>>();
		var requestCount = new HashMap<String, Map<String, Integer>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);

			var roleLst = workflowServiceImpl.roleList("TQA1913");
			var roleList = !roleLst.getData().isEmpty() ? roleLst.getData(): Collections.emptyList();
			var userProcessIds = ProcessFlow.getUserProcessId();

			var processIdMap = new HashMap<String, List<String>>();
			processIdMap.put("user", userProcessIds);
			processIdMap.put("role", ProcessFlow.getRoleProcessId());
			processIdMap.put("channel", ProcessFlow.getChannelProcessId());
			processIdMap.put("label", ProcessFlow.getLabelProcessId());
			processIdMap.put("IPO", ProcessFlow.getIPOProcessId());
			processIdMap.put("smartInst", ProcessFlow.getSmartInstProcessId());
			processIdMap.put("ERR", ProcessFlow.getERRProcessId());
			processIdMap.put("branch", ProcessFlow.getBranchProcessId());
			processIdMap.put("loyaltyHarrods", ProcessFlow.getLoyaltyHarrods());
			processIdMap.put("B2B", ProcessFlow.getB2BProcessId());
			processIdMap.put("planned", ProcessFlow.getPlannedProcessId());
			processIdMap.put("customer", ProcessFlow.getCustomerProcessId());
			processIdMap.put("application", ProcessFlow.applicationProcessId());
			processIdMap.put("transaction", ProcessFlow.getTxnManagement());
			processIdMap.put("hbtf", ProcessFlow.getHBTFProcessId());
			processIdMap.put("Ooredoo", ProcessFlow.getOodredooProcessId());
			processIdMap.put("Credit-card", ProcessFlow.getCreditCardProcessId());
			processIdMap.put("delete", ProcessFlow.getDeleteProcessId());
			processIdMap.put("parameter", ProcessFlow.getParamProcessId());

			var totalCountMap = new HashMap<String, Integer>();
			//for (Map.Entry<String, List<String>> entry : processIdMap.entrySet()) {
			 processIdMap.entrySet().forEach(entry->{
				var totalCount = workflowRepository.countByInitiatedGroupInAndProcessIdInAndWorkflowStatusIn((List<String>) roleList,
						entry.getValue(), status);
				totalCountMap.put(entry.getKey(), totalCount);
			});
			if (status.contains(WorkflowStatus.PENDING_FOR_APPROVAL) && !status.contains(WorkflowStatus.RETURNED)) {
				requestCount.put("request_pending", totalCountMap);
			} else if (status.contains(WorkflowStatus.PENDING_FOR_APPROVAL)
					&& status.contains(WorkflowStatus.RETURNED)) {
				requestCount.put("pending_approvals", totalCountMap);
			}
		} catch (Exception e) {
			log.error("Exception in fetching user approval counts: {}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, "Error in fetching user approval counts");
		}
		response.setStatus(resultVo);
		response.setData(requestCount);
		return response;
	}

	@Override
	public GenericResponse<Map<String, Map<String, Integer>>> getCountBYMonthRequest(List<String> userGroup,
			List<WorkflowStatus> status, int lastMonths) {
		var response = new GenericResponse<Map<String, Map<String, Integer>>>();
		ResultUtilVO resultVo;
		var requestCount = new HashMap<String, Map<String, Integer>>();
		try {
			var hasPendingApprovalStatus = status.contains(WorkflowStatus.PENDING_FOR_APPROVAL);
			var hasReturnedStatus = status.contains(WorkflowStatus.RETURNED);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);

			var customDateFormat = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH);
			var currentTimestamp = new Timestamp(System.currentTimeMillis());
			var calendar = Calendar.getInstance();
			calendar.setTimeInMillis(currentTimestamp.getTime());
			calendar.add(Calendar.MONTH, -lastMonths);
			var startDate = new Timestamp(calendar.getTimeInMillis());

			var formattedNow = customDateFormat.format(currentTimestamp);
			var formattedStart = customDateFormat.format(startDate);
			log.info("formattedStart::" + formattedStart);
			log.info("formattedNow::" + formattedNow);

			var roleLst = workflowServiceImpl.roleList("TQA1913");
			var roleList = !roleLst.getData().isEmpty() ? roleLst.getData(): Collections.emptyList();
			var userProcessIds = ProcessFlow.getUserProcessId();
			var statusStrings = status.stream().map(Enum::name).collect(Collectors.toList());

			var totalUserCount = workflowRepository
					.countByInitiatedGroupAndProcessIdAndWorkflowStatusAndLastModifiedDateBetween(formattedStart,
							formattedNow, (List<String>) roleList, userProcessIds, statusStrings);

			var totalCountMap = new HashMap<String, Integer>();
			totalCountMap.put("user", totalUserCount);
			if (hasPendingApprovalStatus && !hasReturnedStatus) {
				requestCount.put("request_pending", totalCountMap);
			} else if (hasPendingApprovalStatus && hasReturnedStatus) {
				requestCount.put("pending_approvals", totalCountMap);
			}
		} catch (Exception e) {
			log.error("Exception in fetching user approval counts: {}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, "Error in fetching user approval counts");
		}
		response.setStatus(resultVo);
		response.setData(requestCount);
		return response;
	}

	@Override
	public GenericResponse<Map<String, Map<String, Integer>>> getAllCountRequestByMonth(List<String> userGroup,
			List<WorkflowStatus> status, int lastMonths) {
		var response = new GenericResponse<Map<String, Map<String, Integer>>>();
		var resultVo=new ResultUtilVO();
		var requestCount = new HashMap<String, Map<String, Integer>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var roleLst = workflowServiceImpl.roleList("TQA1913");
			var roleList = !roleLst.getData().isEmpty() ? roleLst.getData(): Collections.emptyList();
			var userProcessIds = ProcessFlow.getUserProcessId();
			var processIdMap = new HashMap<String, List<String>>();
			processIdMap.put("user", userProcessIds);
			processIdMap.put("role", ProcessFlow.getRoleProcessId());
			processIdMap.put("channel", ProcessFlow.getChannelProcessId());
			processIdMap.put("label", ProcessFlow.getLabelProcessId());
			processIdMap.put("IPO", ProcessFlow.getIPOProcessId());
			processIdMap.put("smartInst", ProcessFlow.getSmartInstProcessId());
			processIdMap.put("ERR", ProcessFlow.getERRProcessId());
			processIdMap.put("branch", ProcessFlow.getBranchProcessId());
			processIdMap.put("loyaltyHarrods", ProcessFlow.getLoyaltyHarrods());
			processIdMap.put("B2B", ProcessFlow.getB2BProcessId());
			processIdMap.put("planned", ProcessFlow.getPlannedProcessId());
			processIdMap.put("customer", ProcessFlow.getCustomerProcessId());
			processIdMap.put("application", ProcessFlow.applicationProcessId());
			processIdMap.put("transaction", ProcessFlow.getTxnManagement());
			processIdMap.put("hbtf", ProcessFlow.getHBTFProcessId());
			processIdMap.put("Ooredoo", ProcessFlow.getOodredooProcessId());
			processIdMap.put("Credit-card", ProcessFlow.getCreditCardProcessId());
			processIdMap.put("delete", ProcessFlow.getDeleteProcessId());
			processIdMap.put("parameter", ProcessFlow.getParamProcessId());

			var totalCountMap = new HashMap<String, Integer>();
			var customDateFormat = new SimpleDateFormat("dd-MMM-yy hh.mm.ss", Locale.ENGLISH);
			var currentTimestamp = new Timestamp(System.currentTimeMillis());
			var calendar = Calendar.getInstance();
			calendar.setTimeInMillis(currentTimestamp.getTime());
			calendar.add(Calendar.MONTH, -lastMonths); // Adjust the number of months as needed
			var startDate = new Timestamp(calendar.getTimeInMillis());

			var formattedNow = customDateFormat.format(currentTimestamp);
			var formattedStart = customDateFormat.format(startDate);
			log.info("the time is ::" + formattedStart);
			var statusStrings = status.stream().map(Enum::name).collect(Collectors.toList());
			//for (Map.Entry<String, List<String>> entry : processIdMap.entrySet()) {
			processIdMap.entrySet().forEach(entry -> {
				int totalUserCount = workflowRepository
						.countByInitiatedGroupAndProcessIdAndWorkflowStatusAndLastModifiedDateBetween(formattedStart,
								formattedNow, (List<String>) roleList, entry.getValue(), statusStrings);
				totalCountMap.put(entry.getKey(), totalUserCount);
			});
			if (status.contains(WorkflowStatus.PENDING_FOR_APPROVAL) && !status.contains(WorkflowStatus.RETURNED)) {
				requestCount.put("request_pending", totalCountMap);
			} else if (status.contains(WorkflowStatus.PENDING_FOR_APPROVAL)
					&& status.contains(WorkflowStatus.RETURNED)) {
				requestCount.put("pending_approvals", totalCountMap);
			}
		} catch (Exception e) {
			log.error("Exception in fetching user approval counts: {}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, "Error in fetching user approval counts");
		}
		response.setStatus(resultVo);
		response.setData(requestCount);
		return response;
	}

}
