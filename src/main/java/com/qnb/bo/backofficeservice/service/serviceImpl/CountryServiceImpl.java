package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.country.CountryDetails;
import com.qnb.bo.backofficeservice.dto.country.ManagenCntryReqDto;
import com.qnb.bo.backofficeservice.entity.Country;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.CountryRepository;
import com.qnb.bo.backofficeservice.repository.LanguageMasterRepository;
import com.qnb.bo.backofficeservice.service.CountryService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CountryServiceImpl implements CountryService {
	
	
	private ResultUtilVO resultVo = new ResultUtilVO();
	
	@Autowired
	private LanguageMasterRepository languageRepo;
	
	@Autowired
	private CountryRepository cntryRepo;

	@Override
	public GenericResponse<Map<String, String>> saveCountry(ManagenCntryReqDto requestBody) {
		
		var response = new GenericResponse<Map<String, String>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			log.info("Inside saveCountry Call:::{}");
			var newCntryList = new ArrayList<Country>();
		var countryList = cntryRepo.findAllByStatusIn(List.of(AppConstant.ACT,AppConstant.IAC));
			var delStatusCountryList = cntryRepo.findAllByStatusIn(List.of(AppConstant.DEL));
			requestBody.getCountryDetails().stream().forEach(cntryData->{
				var existedCntyLst = countryList.stream()
						.filter(existedCntry->existedCntry.getCountryCode().equals(cntryData.getCountryCode()))
						.collect(Collectors.toList());
				var existedDel  = delStatusCountryList.stream()
						.filter(existedCntry->existedCntry.getCountryCode().equals(cntryData.getCountryCode()))
						.collect(Collectors.toList());
				if(existedCntyLst.isEmpty()) {
					if("MODIFY".equalsIgnoreCase(cntryData.getAction()) || "DELETE".equalsIgnoreCase(cntryData.getAction())) {
						resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE,"Contry_Code doesn't exists.");
					}else {
						if(!existedDel.isEmpty()) {
							existedDel.forEach(delDataToAdd ->{
								delDataToAdd.setIsoCountryCode(cntryData.getIsoCountryCode());    
								delDataToAdd.setCountryCodeIso3a(cntryData.getCountryCodeIso3a());
								delDataToAdd.setEurozoneFlag(cntryData.getEurozoneFlag());        
								delDataToAdd.setBaseCurrency(cntryData.getBaseCurrency());        
								delDataToAdd.setCountryCode(cntryData.getCountryCode());          
								delDataToAdd.setCountryDesc(cntryData.getCountryDesc());          
								delDataToAdd.setMobNoPrefix(cntryData.getMobNoPrefix());          
								delDataToAdd.setNationality(cntryData.getNationality());          
								delDataToAdd.setRemarks(cntryData.getRemarks());                  
								delDataToAdd.setStatus(cntryData.getStatus());                    
								delDataToAdd.setCreatedBy(AppConstant.DEFAULT_CREATED_BY);        
								delDataToAdd.setLangEn(cntryData.getDescEn());                    
								delDataToAdd.setLangAr(cntryData.getDescAr());                    
								delDataToAdd.setLangFr(cntryData.getDescFr());                    
								delDataToAdd.setStatus(cntryData.getStatus().toUpperCase());      
								delDataToAdd.setColor(cntryData.getColor());                      
								delDataToAdd.setUnitDesc(cntryData.getUnitDesc());                
								newCntryList.add(delDataToAdd);
							});
							
						}else {
							var cntry = new Country();
							cntry.setIsoCountryCode(cntryData.getIsoCountryCode());
							cntry.setCountryCodeIso3a(cntryData.getCountryCodeIso3a());
							cntry.setEurozoneFlag(cntryData.getEurozoneFlag());
							cntry.setBaseCurrency(cntryData.getBaseCurrency());
							cntry.setCountryCode(cntryData.getCountryCode());
							cntry.setCountryDesc(cntryData.getCountryDesc());
							cntry.setMobNoPrefix(cntryData.getMobNoPrefix());
							cntry.setNationality(cntryData.getNationality());
							cntry.setRemarks(cntryData.getRemarks());
							cntry.setStatus(cntryData.getStatus());
							cntry.setCreatedBy(AppConstant.DEFAULT_CREATED_BY);
							cntry.setLangEn(cntryData.getDescEn());
							cntry.setLangAr(cntryData.getDescAr());
							cntry.setLangFr(cntryData.getDescFr());
							cntry.setStatus(cntryData.getStatus().toUpperCase());
							cntry.setColor(cntryData.getColor());
							cntry.setUnitDesc(cntryData.getUnitDesc());
							newCntryList.add(cntry);
						}
					}
				}else {
					if("ADD".equalsIgnoreCase(cntryData.getAction())) {
						resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE,"Country_Code already exists.");
					}else {
						
						existedCntyLst.forEach(extCntry -> {
							extCntry.setModifiedBy(AppConstant.DEFAULT_CREATED_BY);
							extCntry.setModifiedTime(LocalDateTime.now());
							if ("MODIFY".equalsIgnoreCase(cntryData.getAction())) {
								extCntry.setCountryDesc(cntryData.getCountryDesc());
								extCntry.setStatus(cntryData.getStatus());
								extCntry.setRemarks(cntryData.getRemarks());
								extCntry.setLangEn(cntryData.getDescEn());
								extCntry.setLangAr(cntryData.getDescAr());
								extCntry.setLangFr(cntryData.getDescFr());
								extCntry.setMobNoPrefix(cntryData.getMobNoPrefix());
								extCntry.setBaseCurrency(cntryData.getBaseCurrency());
								extCntry.setCountryCodeIso3a(cntryData.getCountryCodeIso3a());
								extCntry.setIsoCountryCode(cntryData.getIsoCountryCode());
								extCntry.setUnitDesc(cntryData.getUnitDesc());
							}
							else {
								extCntry.setStatus(AppConstant.DEL);
							}
							newCntryList.add(extCntry);
						});
					}
				}
			});
			log.info("Data in newCntryList:::{}", newCntryList);
			if(!newCntryList.isEmpty()) {
				cntryRepo.saveAll(newCntryList);
			}
		} catch (Exception e) {
			log.info("Exception Occured in SaveCountry:::{}",e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<CountryDetails>> cntryList() {
		var response = new GenericResponse<List<CountryDetails>>();
		var newCntryLst = new ArrayList<CountryDetails>();
		try {
			var cntryLst = cntryRepo.findAllByStatusInOrderByModifiedTimeDesc(List.of(AppConstant.ACT,AppConstant.IAC));
			if(!cntryLst.isEmpty()) {
				cntryLst.forEach(cntry ->{
					var cntryDetails = new CountryDetails();
					cntryDetails.setCountryCode(cntry.getCountryCode());
					cntryDetails.setBaseCurrency(cntry.getBaseCurrency());
					cntryDetails.setCountryCodeIso3a(cntry.getCountryCodeIso3a());
					cntryDetails.setCountryDesc(cntry.getCountryDesc());
					cntryDetails.setEurozoneFlag(cntry.getEurozoneFlag());
					cntryDetails.setIsoCountryCode(cntry.getIsoCountryCode());
					cntryDetails.setNationality(cntry.getNationality());
					cntryDetails.setMobNoPrefix(cntry.getMobNoPrefix());
					cntryDetails.setRemarks(cntry.getRemarks());
					cntryDetails.setStatus(cntry.getStatus());
					cntryDetails.setDescEn(cntry.getLangEn());
					cntryDetails.setDescAr(cntry.getLangAr());
					cntryDetails.setDescFr(cntry.getLangFr());
					newCntryLst.add(cntryDetails);
				});
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
				log.info("Sorted List :::{}",newCntryLst);
			}
		} catch (Exception e) {
			log.info("Exeception Occured in cntryList:::{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setData(newCntryLst);
		response.setStatus(resultVo);
		return response;
	}

}
