package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.currency.CurrencyDetails;
import com.qnb.bo.backofficeservice.dto.currency.ManageCurrReqDto;
import com.qnb.bo.backofficeservice.entity.CurrencyEntity;
import com.qnb.bo.backofficeservice.enums.ActionType;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.CurrencyRepository;
import com.qnb.bo.backofficeservice.repository.UnitLanguageRespository;
import com.qnb.bo.backofficeservice.service.CurrencyService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CurrencyServiceImpl implements CurrencyService {

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private CurrencyRepository currencyRepo;

	@Autowired
	private UnitLanguageRespository unitLanguageRepo;

	@Override
	public GenericResponse<List<CurrencyDetails>> currencyLst(Map<String, String> reqBody) {
		var response = new GenericResponse<List<CurrencyDetails>>();
		var currencyLst = new ArrayList<CurrencyDetails>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var currLst = currencyRepo.findByUnitIdAndStatusInOrderByModifiedTimeDesc(reqBody.get(AppConstant.UNIT_ID),
					List.of(AppConstant.ACT, AppConstant.IAC));
			if (!currLst.isEmpty()) {
				var currGrp = currLst.stream().collect(Collectors.groupingBy(CurrencyEntity::getCurrencyCode,
						LinkedHashMap::new, Collectors.toList()));
				currGrp.entrySet().forEach(curr -> {
					var currency = new CurrencyDetails();
					currency.setCurrencyCode(curr.getValue().get(0).getCurrencyCode());
					currency.setCurrencyIsoCode(curr.getValue().get(0).getCurrIsoCode());
					currency.setNoOfDecimal(String.valueOf(curr.getValue().get(0).getNoOfDecimal()));
					currency.setQcb(String.valueOf(curr.getValue().get(0).getQcb()));
					currency.setSpotRateReciprocal(curr.getValue().get(0).getSpotRateReciprocal());
					currency.setStatus(curr.getValue().get(0).getStatus());
					curr.getValue().forEach(currDet -> {
						/*
						 * switch (currDet.getLangCode()){ case "en":{
						 * currency.setDesc_en(currDet.getLangDesc()); break; } case "ar": {
						 * currency.setDesc_ar(currDet.getLangDesc()); break; } case "fr": {
						 * currency.setDesc_fr(currDet.getLangDesc()); break; } }
						 * 
						 */
						switch (currDet.getLangCode()) {
						case "en" -> currency.setDesc_en(currDet.getLangDesc());
						case "ar" -> currency.setDesc_ar(currDet.getLangDesc());
						case "fr" -> currency.setDesc_fr(currDet.getLangDesc());
						default ->
							throw new IllegalArgumentException("Unsupported language code: " + currDet.getLangCode());
						};
					});
					currencyLst.add(currency);
				});
			}
			response.setData(currencyLst);
		} catch (Exception e) {
			log.info("Exception in currencyLst :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<Map<String, String>> manageCurrency(List<ManageCurrReqDto> reqBody) {
		var response = new GenericResponse<Map<String, String>>();
		var currencyToadd = new ArrayList<CurrencyEntity>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var currCodeLst = currencyRepo.findByUnitId(reqBody.get(0).getUnitId());
			var langList = unitLanguageRepo.getDistinctLangCode(reqBody.get(0).getUnitId());
			reqBody.forEach(req -> {
				var currData = req.getCurrencyDetails().get(0);
				var existedCurrLst = currCodeLst.stream()
						.filter(existedCurr -> existedCurr.getCurrencyCode().equals(currData.getCurrencyCode())
								&& (AppConstant.ACT.equals(existedCurr.getStatus())
										|| AppConstant.IAC.equals(existedCurr.getStatus())))
						.collect(Collectors.toList());
				if (existedCurrLst.isEmpty()) {
					if (ActionType.MODIFY.toString().equals(currData.getAction())
							|| ActionType.DELETE.toString().equals(currData.getAction())) {
						resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "Currency Code doesn't exists.");
					} else {
						langList.forEach(lang -> {
							var curr = new CurrencyEntity();
							curr.setUnitId(reqBody.get(0).getUnitId());
							curr.setCurrencyCode(currData.getCurrencyCode());
							curr.setCurrIsoCode(currData.getCurrencyIsoCode());
							curr.setQcb(Integer.parseInt(currData.getQcb()));
							curr.setSpotRateReciprocal(currData.getSpotRateReciprocal());
							curr.setNoOfDecimal(Integer.parseInt(currData.getNoOfDecimal()));
							curr.setLangCode(lang);
							curr.setLangDesc(curr.getLangCode().equals("en") ? currData.getDesc_en()
									: (curr.getLangCode().equals("ar") ? currData.getDesc_ar()
											: currData.getDesc_fr()));
							curr.setStatus(currData.getStatus());
							curr.setCreatedBy("BO");
							currencyToadd.add(curr);
						});
					}
				} else {
					if (ActionType.ADD.toString().equals(currData.getAction())) {
						resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "Currency Code already exists.");
					} else {
						existedCurrLst.forEach(extCurr -> {
							extCurr.setStatus(
									currData.getAction().equals(ActionType.MODIFY.toString()) ? currData.getStatus()
											: (currData.getAction().equals(ActionType.DELETE.toString())
													? AppConstant.DEL
													: AppConstant.ACT));
							if (currData.getAction().equals(ActionType.MODIFY.toString())) {
								if (extCurr.getLangCode().equals("en")) {
									extCurr.setLangDesc(currData.getDesc_en());
								} else if (extCurr.getLangCode().equals("ar")) {
									extCurr.setLangDesc(currData.getDesc_ar());
								} else {
									extCurr.setLangDesc(currData.getDesc_fr());
								}
								extCurr.setModifiedBy("BO");
								extCurr.setModifiedTime(LocalDateTime.now());
								extCurr.setQcb(Integer.parseInt(currData.getQcb()));
								extCurr.setNoOfDecimal(Integer.parseInt(currData.getNoOfDecimal()));
								extCurr.setSpotRateReciprocal(currData.getSpotRateReciprocal());
							}
							currencyToadd.add(extCurr);
						});
					}
				}
			});
			if (!currencyToadd.isEmpty()) {
				log.info("data to be added:{} ,:{}", currencyToadd);
				currencyRepo.saveAll(currencyToadd);
			}
		} catch (Exception e) {
			log.info("Exception in manageCurrency :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
