package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.ErrorConfigDto;
import com.qnb.bo.backofficeservice.dto.ErrorConfigResponse;
import com.qnb.bo.backofficeservice.dto.ErrorConfigServiceType;
import com.qnb.bo.backofficeservice.dto.errorConfig.ErrorConfigRequest;
import com.qnb.bo.backofficeservice.dto.errorConfig.ErrorConfigServiceTypeResponse;
import com.qnb.bo.backofficeservice.dto.errorConfig.ErrorDescriptions;
import com.qnb.bo.backofficeservice.entity.ErrorConfig;
import com.qnb.bo.backofficeservice.enums.TxnStatus;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.ErrorConfigRepository;
import com.qnb.bo.backofficeservice.repository.UnitLanguageRespository;
import com.qnb.bo.backofficeservice.service.ErrorConfigService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ErrorConfigServiceImpl implements ErrorConfigService {

	@Autowired
	private ErrorConfigRepository errConfigRepo;
	@Autowired
	private UnitLanguageRespository unitLanguageRespository;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<List<ErrorConfigResponse>> getErrorConfigList(ErrorConfigRequest errReq) {
		var response = new GenericResponse<List<ErrorConfigResponse>>();
		try {
			var errorConfig = errConfigRepo.getErrorConfig(errReq.getUnitId(), errReq.getChannelId(),
					errReq.getServiceType());

			var errorGrp = errorConfig.stream()
					.collect(Collectors.groupingBy(error -> error.getMwErrCode() + "|" + error.getOcsErrCode(),
							LinkedHashMap::new, Collectors.toList()));
			var errorDetailsDtos = new ArrayList<ErrorConfigResponse>();
			errorGrp.entrySet().forEach(entry -> {
				var keyParts = entry.getKey().split("\\|");
				var mwErrCode = keyParts[0];
				var ocsErrCode = keyParts[1];
				var groupedErrors = entry.getValue();
				var errValuesList = groupedErrors.stream().map(ErrorDescriptions::toErrorDescriptions)
						.collect(Collectors.toList());
				var errDescEng = "";
				var errDescArb = "";
				var errDescFr = "";
				for (ErrorDescriptions error : errValuesList) {
					/*
					 * switch (error.getLangCode()) { case "en": errDescEng = error.getLangDesc();
					 * break; case "ar": errDescArb = error.getLangDesc(); break; case "fr":
					 * errDescFr = error.getLangDesc(); break; default: break; }
					 */

					switch (error.getLangCode()) {
					case "en" -> errDescEng = error.getLangDesc();
					case "ar" -> errDescArb = error.getLangDesc();
					case "fr" -> errDescFr = error.getLangDesc();
					default -> throw new IllegalArgumentException("Unsupported language code:");
					};

				}
				var errorDetail = ErrorConfigResponse.builder().unitId(errReq.getUnitId())
						.channelId(errReq.getChannelId()).serviceType(errReq.getServiceType()).mwErrorCode(mwErrCode)
						.ocsErrorCode(ocsErrCode).status(TxnStatus.valueOf(errValuesList.get(0).getStatus()))
						.errDescEn(errDescEng).errDescArb(errDescArb).errDescFr(errDescFr)
						.remarks(errValuesList.get(0).getRemarks()).dateCreated(groupedErrors.get(0).getDateCreated())
						.txnId(String.valueOf(groupedErrors.get(0).getTxnId())).build();
				errorDetailsDtos.add(errorDetail);
			});
			Collections.sort(errorDetailsDtos,
					Comparator.comparing(ErrorConfigResponse::getDateCreated, Comparator.reverseOrder())
							.thenComparing(ErrorConfigResponse::getMwErrorCode, Comparator.reverseOrder()));
			response.setData(errorDetailsDtos);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<ErrorConfig>> updateErrorConfigs(List<ErrorConfigDto> requests) {
		var response = new GenericResponse<List<ErrorConfig>>();
		var date = new Date(System.currentTimeMillis());

		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var updatedConfigs = new ArrayList<ErrorConfig>();
			var configsToSave = new ArrayList<ErrorConfig>();
			var existingConfigsList = new ArrayList<ErrorConfig>();
			for (ErrorConfigDto request : requests) {
				var existingConfigs = errConfigRepo.findByUnitIdAndChannelIdAndServiceTypeAndMwErrCodeAndOcsErrCode(
						request.getUnitId(), request.getChannelId(), request.getServiceType(), request.getMWErrorCode(),
						request.getOcsErrorCode());
				existingConfigsList.addAll(existingConfigs);
				var langList = unitLanguageRespository.findByUnit_UnitIdAndChannel_ChannelId(request.getUnitId(),
						request.getChannelId());
				if ("ADD".equalsIgnoreCase(request.getAction())) {
					if (!existingConfigs.isEmpty()) {
						resultVo = new ResultUtilVO(AppConstant.ALREADY_EXISTS_CODE, AppConstant.ALREADY_EXISTS_DESC);
						continue;
					}
					var configExistsInSaveList = configsToSave.stream()
							.anyMatch(config -> config.getUnitId().equals(request.getUnitId())
									&& config.getChannelId().equals(request.getChannelId())
									&& config.getServiceType().equals(request.getServiceType())
									&& config.getMwErrCode().equals(request.getMWErrorCode())
									&& config.getOcsErrCode().equals(request.getOcsErrorCode()));
					if (!configExistsInSaveList) {
						langList.forEach(lang -> {
							var newConfig = new ErrorConfig();
							newConfig.setUnitId(request.getUnitId());
							newConfig.setChannelId(request.getChannelId());
							newConfig.setLang(lang.getLanguage().getId());
							newConfig.setServiceType(request.getServiceType());
							newConfig.setMwErrCode(request.getMWErrorCode());
							newConfig.setOcsErrCode(request.getOcsErrorCode());
							setErrDescForLang(newConfig, request, lang.getLanguage().getId());
							newConfig.setStatus(AppConstant.ACT);
							newConfig.setRemarks(request.getRemarks());
							newConfig.setModifiedBy("BO");
							newConfig.setCreatedBy("BO");
							newConfig.setDateCreated(date);
							configsToSave.add(newConfig);
						});
					}
				}
				// for (ErrorConfig existingConfig : existingConfigs) {
				existingConfigs.forEach(existingConfig -> {
					if ("DELETE".equalsIgnoreCase(request.getAction())) {
						existingConfig.setStatus(AppConstant.DEL);
					} else if ("MODIFY".equalsIgnoreCase(request.getAction())) {
						langList.forEach(lang -> {
							existingConfig.setRemarks(request.getRemarks());
							if (lang.getLanguage().getId().equals(existingConfig.getLang())) {
								setErrDescForLang(existingConfig, request, lang.getLanguage().getId());
							}
						});
					}
					existingConfig.setModifiedBy("BO");
					existingConfig.setDateModified(date);
					configsToSave.add(existingConfig);
				});
			}
			if (!configsToSave.isEmpty()) {
				updatedConfigs.addAll(errConfigRepo.saveAll(configsToSave));
			} else if (existingConfigsList.isEmpty()) {
				resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "No data found ");
			}
		} catch (Exception e) {
			response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC));
		}
		response.setStatus(resultVo);
		return response;
	}

	private void setErrDescForLang(ErrorConfig errorConfig, ErrorConfigDto request, String langCode) {
		/*
		 * switch (langCode) { case "en":
		 * errorConfig.setErrDesc(request.getEnErrorDesc()); break; case "fr":
		 * errorConfig.setErrDesc(request.getFrErrorDesc()); break; case "ar":
		 * errorConfig.setErrDesc(request.getArErrorDesc()); break;
		 * 
		 * default: break; }
		 */
		switch (langCode) {
		case "en" -> errorConfig.setErrDesc(request.getEnErrorDesc());
		case "fr" -> errorConfig.setErrDesc(request.getFrErrorDesc());
		case "ar" -> errorConfig.setErrDesc(request.getArErrorDesc());
		default -> throw new IllegalArgumentException("Unsupported language code:");
		}
		;
	}

	@Override
	public GenericResponse<ErrorConfigServiceTypeResponse> getServiceTypes(ErrorConfigRequest errReq) {
		var response = new GenericResponse<ErrorConfigServiceTypeResponse>();
		var serviceTypeResponse = new ErrorConfigServiceTypeResponse();
		var serviceTypes = new ArrayList<ErrorConfigServiceType>();
		resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		try {
			var errorConfig = errConfigRepo.findDistinctServiceTypes(errReq.getUnitId(), errReq.getChannelId());
			if (!errorConfig.isEmpty()) {
				serviceTypes = (ArrayList<ErrorConfigServiceType>) errorConfig.stream().map(entry -> {
					var values = entry.split(",");
					var config = new ErrorConfigServiceType();
					config.setServiceType(values[0].trim());
					config.setServiceDesc(values[1].trim());
					return config;
				}).collect(Collectors.toList());
				var defaultServiceType = new ErrorConfigServiceType();
				defaultServiceType.setServiceType("ADD_NEW_SERVICE");
				defaultServiceType.setServiceDesc("Add New Service");
				serviceTypes.add(defaultServiceType);
				serviceTypeResponse.setServiceTypes(serviceTypes);
				response.setData(serviceTypeResponse);
				resultVo = (new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC));
			} else {
				var defaultServiceType = new ErrorConfigServiceType();
				defaultServiceType.setServiceType("ADD_NEW_SERVICE");
				defaultServiceType.setServiceDesc("Add New Service");
				serviceTypes.add(defaultServiceType);
				serviceTypeResponse.setServiceTypes(serviceTypes);
				response.setData(serviceTypeResponse);
				resultVo = (new ResultUtilVO(AppConstant.RESULT_CODE, "No data found"));
			}
		} catch (Exception e) {
			serviceTypeResponse.setServiceTypes(Collections.emptyList());
			response.setData(serviceTypeResponse);
			resultVo = (new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC));
		}
		response.setStatus(resultVo);
		return response;
	}

}
