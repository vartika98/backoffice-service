package com.qnb.bo.backofficeservice.service.serviceImpl;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.ConfigBasedCategoryRequest;
import com.qnb.bo.backofficeservice.dto.GraphCategoryCodeCountDto;
import com.qnb.bo.backofficeservice.dto.GraphDataDto;
import com.qnb.bo.backofficeservice.dto.GraphDateCountDto;
import com.qnb.bo.backofficeservice.dto.audit.AuditListRequest;
import com.qnb.bo.backofficeservice.dto.audit.SummaryRequest;
import com.qnb.bo.backofficeservice.enums.GraphType;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.service.AuditService;
import com.qnb.bo.backofficeservice.service.GraphService;
import com.qnb.bo.backofficeservice.service.TransferService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class GraphServiceImpl implements GraphService {

    @Autowired
    private AuditService auditService;

    @Autowired
    private TransferService transferService;

    @Override
    public GenericResponse<GraphDataDto> getAuditDataByDateRange(String entityName, SummaryRequest request) {

        var response = new GenericResponse<GraphDataDto>();
        try{
            response = switch (GraphType.valueOf(entityName.toUpperCase())) {
                case TRANSFER -> transferService.getTransferDataSummary(request);
                default -> {
                    var message = String.format("Action Id:%s is not supported.", entityName);
                    log.info(message);
                    yield new GenericResponse<>(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, message), null);
                }
            };
        } catch (Exception e) {
            log.info("Exception in getAuditDataByDateRange :{}", e);
             var resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
            response.setStatus(resultVo);
        }
        return response;
    }

    @Override
    public GenericResponse<GraphDateCountDto> getEntityCountByDateRange(String graphName, AuditListRequest auditRequest) {
        var response = new GenericResponse<GraphDateCountDto>();
        try{
            response = switch (GraphType.valueOf(graphName.toUpperCase())) {
                case LOGIN -> auditService.getLoginCountByConfig();
                case AUDIT -> auditService.getAuditCountByDateRange(auditRequest);
                case TRANSFER -> transferService.getTransferDetailsByDateRange(auditRequest);
                default -> {
                    response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC));
                    yield response;
                }
            };

        } catch (Exception e) {
            log.info("Exception in getEntityCountByDateRange :{}", e);
            response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC));
        }
        return response;
    }

    @Override
    public GenericResponse<GraphCategoryCodeCountDto> getConfigBasedTransferCount(String graphName, ConfigBasedCategoryRequest configBasedCategoryRequest) {
        var response = new GenericResponse<GraphCategoryCodeCountDto>();
        try{
            response = switch (GraphType.valueOf(graphName.toUpperCase())) {
                case TRANSFER -> transferService.getConfigBasedTransferCount(configBasedCategoryRequest);
                default -> {
                    var message = String.format("Action Id:%s is not supported.", graphName);
                    log.info(message);
                    yield new GenericResponse<>(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, message), null);
                }
            };
        } catch (Exception e) {
            log.info("Exception in getConfigBasedTransferCount :{}", e);
            var resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
            response.setStatus(resultVo);
        }

        return response;
    }


}
