package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.userGroup.CreateGroupReq;
import com.qnb.bo.backofficeservice.dto.userGroup.GroupSummary;
import com.qnb.bo.backofficeservice.entity.BOGroupEntity;
import com.qnb.bo.backofficeservice.entity.BOGroupMaster;
import com.qnb.bo.backofficeservice.entity.BoGroupProductMap;
import com.qnb.bo.backofficeservice.entity.OcsFunctionMaster;
import com.qnb.bo.backofficeservice.entity.OcsProductMaster;
import com.qnb.bo.backofficeservice.entity.OcsSubProductMaster;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.GenericResult;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.BOGroupMasterRepository;
import com.qnb.bo.backofficeservice.repository.BOGroupRepository;
import com.qnb.bo.backofficeservice.repository.BOMenuRepository;
import com.qnb.bo.backofficeservice.repository.BoGroupProdMapRespository;
import com.qnb.bo.backofficeservice.repository.OcsFunctionMasterRepository;
import com.qnb.bo.backofficeservice.repository.OcsProductMasterRepository;
import com.qnb.bo.backofficeservice.repository.OcsSubProductMasterRepository;
import com.qnb.bo.backofficeservice.service.GroupService;
import com.qnb.bo.backofficeservice.utils.RandomUuidGenerator;
import com.qnb.bo.backofficeservice.views.GroupSummaryView;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GroupServiceImpl implements GroupService {

	@Autowired
	private BOMenuRepository boMenuRepository;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private BoGroupProdMapRespository boGroupProdRepo;

	@Autowired
	private BOGroupRepository boGroupRepository;

	@Autowired
	private OcsProductMasterRepository productMasterRepo;

	@Autowired
	private OcsFunctionMasterRepository functionRepo;

	@Autowired
	private OcsSubProductMasterRepository subProdRepo;

	@Autowired
	private BOGroupMasterRepository boGroupmasterRepo;

	@Override
	public GenericResponse<List<BOGroupEntity>> groupList() {
		var response = new GenericResponse<List<BOGroupEntity>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var groupCodeList = (List<BOGroupEntity>) boGroupRepository.findAll();
			response.setData(groupCodeList);
		} catch (Exception e) {
			log.info("Exception in groupList:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResult createGroup(List<CreateGroupReq> reqBody) {
		var response = new GenericResult();
		var grpToBeSave = new ArrayList<BoGroupProductMap>();
		var grpMasterToBeSave = new ArrayList<BOGroupMaster>();
		var grpToBeDeleted = new ArrayList<String>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			if("DELETE".equals(reqBody.get(0).getAction())) {
				var groupMenu = (ArrayList<BOGroupMaster>)boGroupmasterRepo.findByGroupId(reqBody.get(0).getGroupId());
				var groupProductDet = (ArrayList<BoGroupProductMap>)boGroupProdRepo.findByGroupId(reqBody.get(0).getGroupId());
				reqBody.forEach(grp->{
					var existingGrp = (ArrayList<BOGroupMaster>)groupMenu.stream().filter(grpDet->grpDet.getGroupName().equals(grp.getGroupName())).collect(Collectors.toList());
					if(!existingGrp.isEmpty()) {
						existingGrp.get(0).setStatus("DEL");
						existingGrp.get(0).setModifiedBy("BO");
						existingGrp.get(0).setModifiedTime(LocalDateTime.now());
						grpMasterToBeSave.add(existingGrp.get(0));
						
						var existingGrpProd = groupProductDet.stream().filter(prod->prod.getGroupdProdMapId()
								.equals(existingGrp.get(0).getTxnId())).collect(Collectors.toList());
						if(!existingGrpProd.isEmpty()) {
							grpToBeDeleted.add(String.valueOf(existingGrpProd.get(0).getGroupdProdMapId()));
						}
					}
				});
			}
			else {
				var groupMenuDet = boGroupmasterRepo.findByGroupIdAndGroupNameAndStatusNot(reqBody.get(0).getGroupId(),
						reqBody.get(0).getGroupName(),AppConstant.DEL);
				var groupProductDet = boGroupProdRepo.getList(reqBody.get(0).getGroupId(), reqBody.get(0).getGroupName());
				if (Objects.isNull(groupMenuDet)) {
					if ("ADD".equals(reqBody.get(0).getAction())) {
						var grpMaster = new BOGroupMaster();
						grpMaster.setTxnId(RandomUuidGenerator.uUIDAsString());
						grpMaster.setGroupId(reqBody.get(0).getGroupId());
						grpMaster.setGroupName(reqBody.get(0).getGroupName());
						grpMaster.setGroupDesc(reqBody.get(0).getGroupDesc());
						grpMaster.setUnitId(reqBody.get(0).getUnitId());
						grpMaster.setStatus(AppConstant.ACT);
						grpMaster.setCreatedBy("BO");
						grpMaster.setCreatedTime(LocalDateTime.now());
						grpMaster.setRemarks(reqBody.get(0).getComment());
						boGroupmasterRepo.save(grpMaster);
	
						reqBody.get(0).getAccessTo().forEach(grpAccess -> {
							var group = new BoGroupProductMap();
							group.setGroupdProdMapId(grpMaster.getTxnId());
							group.setGroupId(reqBody.get(0).getGroupId());
							group.setGroupName(reqBody.get(0).getGroupName());
							group.setGroupDesc(reqBody.get(0).getGroupDesc());
							group.setProdCode(String.valueOf(grpAccess.get("productCode")));
							group.setSubProdCode(String.valueOf(grpAccess.get("subProdCode")));
							group.setFunctionCode(String.valueOf(grpAccess.get("functionCode")));
							grpToBeSave.add(group);
						});
					} else {
						resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "Group doesn't exists.");
					}
				} else {
					if ("ADD".equals(reqBody.get(0).getAction())) {
						resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "Group Name already exists.");
					} else {
						groupMenuDet.setStatus(reqBody.get(0).getStatus());
						groupMenuDet.setGroupDesc(reqBody.get(0).getGroupDesc());
						groupMenuDet.setRemarks(reqBody.get(0).getComment());
						grpMasterToBeSave.add(groupMenuDet);
//						boGroupmasterRepo.save(groupMenuDet);
						boGroupProdRepo.deleteProductById(groupProductDet);
						reqBody.get(0).getAccessTo().forEach(grpAccess -> {
							var group = new BoGroupProductMap();
							group.setGroupdProdMapId(groupMenuDet.getTxnId());
							group.setGroupId(reqBody.get(0).getGroupId());
							group.setGroupName(reqBody.get(0).getGroupName());
							group.setGroupDesc(reqBody.get(0).getGroupDesc());
							group.setProdCode(String.valueOf(grpAccess.get("productCode")));
							group.setSubProdCode(String.valueOf(grpAccess.get("subProdCode")));
							group.setFunctionCode(String.valueOf(grpAccess.get("functionCode")));
							grpToBeSave.add(group);
						});
					}
				}
			}
			if (!grpToBeSave.isEmpty() || !grpMasterToBeSave.isEmpty() || !grpToBeDeleted.isEmpty()) {
				boGroupmasterRepo.saveAll(grpMasterToBeSave);
				boGroupProdRepo.saveAll(grpToBeSave);
				boGroupProdRepo.deleteProductByProdId(grpToBeDeleted);
			}
		} catch (Exception e) {
			log.info("Exception in createGroup:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setResult(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<GroupSummary>> userGroupSummary(Map<String, String> reqBody) {
		var response = new GenericResponse<List<GroupSummary>>();
		var grpLst = new ArrayList<GroupSummary>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var groupCode = (BOGroupEntity) boGroupRepository.findByGroupCode(reqBody.get("groupId"));
			var summaryList = (ArrayList<GroupSummaryView>) boGroupmasterRepo.getSummary(reqBody.get("groupId"));
			if (summaryList.isEmpty()) {
				resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "No data present.");
			} else {
				var productList = (ArrayList<OcsProductMaster>) productMasterRepo.findAll();
				var subProdLst = (ArrayList<OcsSubProductMaster>) subProdRepo.findAll();
				var functionLst = (ArrayList<OcsFunctionMaster>) functionRepo.findAll();
				var grpSumMap = summaryList.stream().collect(
						Collectors.groupingBy(GroupSummaryView::getGroupName, LinkedHashMap::new, Collectors.toList()));
				grpSumMap.entrySet().forEach(group -> {
					var grpSum = new GroupSummary();
					var prodNameLst = new HashSet<String>();
					var subProdNameLst = new HashSet<String>();
					var funcLst = new HashSet<String>();
					grpSum.setGroupId(group.getValue().get(0).getGroupId());
					grpSum.setGroupIdDesc(groupCode.getGroupName());
					grpSum.setGroupName(group.getKey());
					grpSum.setGroupDesc(group.getValue().get(0).getGroupDescription());
					grpSum.setUnitId(group.getValue().get(0).getUnit());
					grpSum.setRemarks(group.getValue().get(0).getRemarks());
					grpSum.setStatus(group.getValue().get(0).getStatus());
					group.getValue().forEach(grpDet -> {
						var productLst = (ArrayList<OcsProductMaster>) productList.stream()
								.filter(prod -> prod.getProductCode().equals(grpDet.getProdCode()))
								.collect(Collectors.toList());
						if (!productLst.isEmpty()) {
							prodNameLst.add(productLst.get(0).getProductDescription());
						}

						var subProductLst = (ArrayList<OcsSubProductMaster>) subProdLst.stream()
								.filter(subProd -> subProd.getProductCode().equals(grpDet.getProdCode())
										&& subProd.getSubProductCode().equals(grpDet.getSubProdCode()))
								.collect(Collectors.toList());
						if (!subProductLst.isEmpty()) {
							subProdNameLst.add(subProductLst.get(0).getSubProductDesc());
						}

						var functLst = (ArrayList<OcsFunctionMaster>) functionLst.stream()
								.filter(fun -> fun.getProductCode().equals(grpDet.getProdCode())
										&& fun.getSubProductCode().equals(grpDet.getSubProdCode())
										&& fun.getFunctionCode().equals(grpDet.getFunctionCode()))
								.collect(Collectors.toList());
						if (!functLst.isEmpty()) {
							funcLst.add(functLst.get(0).getFunctionDesc());
						}
					});
					grpSum.setProductName(prodNameLst.stream().collect(Collectors.toList()));
					grpSum.setSubProdName(subProdNameLst.stream().collect(Collectors.toList()));
					grpSum.setFunctionName(funcLst.stream().collect(Collectors.toList()));
					grpLst.add(grpSum);
				});
				response.setData(grpLst);
			}
		} catch (Exception e) {
			log.info("Exception in userGroupSummary:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
