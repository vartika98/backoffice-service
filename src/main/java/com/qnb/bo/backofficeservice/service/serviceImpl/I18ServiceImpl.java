package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.txn.LabelDescription;
import com.qnb.bo.backofficeservice.dto.txn.LabelListRes;
import com.qnb.bo.backofficeservice.dto.txn.LabelResponseDto;
import com.qnb.bo.backofficeservice.dto.txn.ManageLabelReqDto;
import com.qnb.bo.backofficeservice.entity.I18N;
import com.qnb.bo.backofficeservice.entity.Language;
import com.qnb.bo.backofficeservice.entity.UnitLanguage;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.ChannelRespository;
import com.qnb.bo.backofficeservice.repository.I18NRepository;
import com.qnb.bo.backofficeservice.repository.MenuRepository;
import com.qnb.bo.backofficeservice.repository.UnitLanguageRespository;
import com.qnb.bo.backofficeservice.repository.UnitRespository;
import com.qnb.bo.backofficeservice.service.I18Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class I18ServiceImpl implements I18Service {

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private I18NRepository i18NRepo;

	@Autowired
	private UnitLanguageRespository unitLanguageRepo;

	@Autowired
	private ChannelRespository channelRepo;

	@Autowired
	private UnitRespository unitRespository;
	
	@Autowired
	private MenuRepository menuRepository;
	
	public static XSSFWorkbook workbook;
	
	public static XSSFSheet sheet;

	
	@Override
	public GenericResponse<byte[]> i18ToExcel(Map<String, String> reqBody) {
	    GenericResponse<byte[]> response = new GenericResponse<>();
	    GenericResponse<LabelListRes> list = labelList(reqBody);
	    try {
	    	var listResponse = list.getData().getI18();
	        if (listResponse == null) {
	            resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "No i18ToExcel data is present.");
	            response.setStatus(resultVo);
	    		response.setData(null);
	        } else {
	            ByteArrayInputStream is = downloadExcel(listResponse);
	            byte[] byteData = IOUtils.toByteArray(is); // Use Apache Commons IO for conversion
				String base64 =Base64.getEncoder().encodeToString(byteData);
				System.out.println(base64);
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
				response.setStatus(resultVo);
	            response.setData(byteData);
	        }
	    } catch (Exception e) {
			log.info("Exception in moduleList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
	        response.setStatus(resultVo);
			response.setData(null);
	    }
	    return response;
	}

	
	 public ByteArrayInputStream downloadExcel(List<LabelResponseDto> listResponse) throws IOException {
	        workbook = new XSSFWorkbook();
	        ByteArrayOutputStream 	out =new ByteArrayOutputStream();
	       try {
	    	  
		        sheet = workbook.createSheet("LABEL MAINTENANCE i18 Data");

		        // Create header row for header
		        Row row1 = sheet.createRow(0);
		        String[] headers = {"Key", "Arabic","English", "French","Status", "Description"};
		       
		        
		        for(int i=0;i<headers.length;i++) {
		        	Cell cell=row1.createCell(i);	
		        	cell.setCellValue(headers[i]);
		        }
		        
		        
		        int rowNum = 1;
		        for (LabelResponseDto item : listResponse) {
		            Row row = sheet.createRow(rowNum++);
		            row.createCell(0).setCellValue(item.getLabelKey());
		            List<LabelDescription> labelValues = item.getLabelValue();
		            for (int i = 0; i < labelValues.size(); i++) {
		            
		            	      Collections.sort(labelValues,Comparator.comparing(LabelDescription::getLangCode));
		            
				              row.createCell(i + 1).setCellValue(labelValues.get(i).getLangValue());
		
		            }
		             
		            row.createCell(4).setCellValue(item.getStatus());
		            row.createCell(5).setCellValue(item.getDescription());
		        }
		        
		        workbook.write(out);
				return new ByteArrayInputStream(out.toByteArray());
				
	       }catch(Exception e) {
	    	   return null;
	       }finally {
	    	   workbook.close();
	    	   out.close();
		}

	       
	    }

	@Override
	public GenericResponse<LabelListRes> labelList(Map<String, String> reqBody) {
		var response = new GenericResponse<LabelListRes>();
		var resData = new LabelListRes();
		var responseLst = new ArrayList<LabelResponseDto>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var labelList = i18NRepo.getLabelList(reqBody.get(AppConstant.UNIT_ID), reqBody.get(AppConstant.CHANNEL_ID),
					reqBody.get("screenId"));
			if (Objects.nonNull(labelList) && !labelList.isEmpty()) {
				var langList = unitLanguageRepo
						.findByUnit_UnitIdAndChannel_ChannelId(reqBody.get(AppConstant.UNIT_ID), reqBody.get(AppConstant.CHANNEL_ID));
				if (Objects.nonNull(langList) && !langList.isEmpty()) {
					var lngLst = langList.stream().map(UnitLanguage::getLanguage).collect(Collectors.toList());
					var labelGrp = labelList.stream().collect(Collectors.groupingBy(I18N::getKey,LinkedHashMap::new,Collectors.toList()));
					labelGrp.entrySet().forEach(lb -> {
						var labelValueList = lb.getValue().stream()
								.filter(langCode -> lngLst.stream().map(Language::getId).anyMatch(langId ->langCode.getLangCode().equals(langId)))
								.map(LabelDescription::toLabelDescription).collect(Collectors.toList());
						
						lngLst.forEach(lang -> {
							var langExists = labelValueList.stream().anyMatch(
									lbValue -> lbValue != null && lbValue.getLangCode().equals(lang.getId()));
							if (!langExists) {
								var lbLangDesc = LabelDescription.builder().langCode(lang.getId())
										.build();
								labelValueList.add(lbLangDesc);
							}
						});
						var labelDetail = LabelResponseDto.builder().unitId(reqBody.get(AppConstant.UNIT_ID))
								.channelId(reqBody.get(AppConstant.CHANNEL_ID)).screenId(reqBody.get("screenId"))
								.labelKey(lb.getKey()).status(lb.getValue().get(0).getStatus())
								.labelValue(labelValueList).description(lb.getValue().get(0).getDescription()).build();
						responseLst.add(labelDetail);
					});
				}
				resData.setI18(responseLst);
				response.setData(resData);
			}
		} catch (Exception e) {
			log.info("Exception in moduleList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<Map<String, String>> manageLabels(List<ManageLabelReqDto> reqBody) {
		var response = new GenericResponse<Map<String, String>>();
		var labelsToadd = new ArrayList<I18N>();
		var duplicateLabels = new ArrayList<String>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var ch = channelRepo.findAll();
			var unit = unitRespository.findAll();
			var existingLabelLst = i18NRepo.findAll();
			var menuLst = menuRepository.getMenuIdList();
			reqBody.stream().forEach(labelData ->{
				var dublicateKey = new ArrayList<String>();
				if(menuLst.contains(labelData.getMenuId())) {
				labelData.getLabelDetails().forEach(label -> label.getLabelValue().stream().forEach(lbl -> {
					var existingLabel = existingLabelLst.stream()
							.filter(lblFil -> lblFil.getKey().equals(label.getLabelKey())
									&& lblFil.getLangCode().equals(lbl.getLangCode()) && lblFil.getChannel().getChannelId().equals(labelData.getChannelId())
									&& lblFil.getUnit().getUnitId().equals(labelData.getUnitId())
									&& lblFil.getPage().equals(labelData.getScreenId())
									)
							.collect(Collectors.toList());
					var unitDet = unit.stream().filter(units->units.getUnitId().equals(labelData.getUnitId())).collect(Collectors.toList());
					var channelDet = ch.stream().filter(channel->channel.getChannelId().equals(labelData.getChannelId())).collect(Collectors.toList());
					if (!unitDet.isEmpty() && !channelDet.isEmpty() && existingLabel.isEmpty()) {
						var existedInLst = labelsToadd.stream().filter(addedLbl -> addedLbl.getKey().equals(label.getLabelKey()) && addedLbl.getLangCode().equals(lbl.getLangCode()) 
								&& addedLbl.getChannel().getChannelId().equals(labelData.getChannelId()) && addedLbl.getUnit().getUnitId().equals(labelData.getUnitId()))
								.collect(Collectors.toList());
						if(existedInLst.isEmpty()) {
							var txn = new I18N();
							txn.setChannel(channelDet.get(0));
							txn.setUnit(unitDet.get(0));
							txn.setKey(label.getLabelKey());
							txn.setValue(lbl.getLangValue());
							txn.setDescription(label.getRemark());
							txn.setPage(labelData.getScreenId());
							txn.setLangCode(lbl.getLangCode());
							txn.setGroup("Default");
							txn.setStatus(label.getStatus());
							txn.setCreatedBy("SYSTEM");
							txn.setCreatedTime(LocalDateTime.now());
							labelsToadd.add(txn);
						}else {
							dublicateKey.add(label.getLabelKey());
						}
					} else {
						existingLabel.forEach(extLabel -> {
							if("ADD".equals(label.getAction())) {
								dublicateKey.add(label.getLabelKey());
							}else {
								extLabel.setStatus(label.getAction().equals("MODIFY") ? label.getStatus():(label.getAction().equals("DELETE")?"DEL":"ACT"));
								if (label.getAction().equals("MODIFY")) {
									extLabel.setGroup("Default");
									extLabel.setKey(label.getLabelKey());
									extLabel.setValue(lbl.getLangValue());
									extLabel.setLangCode(lbl.getLangCode());
									extLabel.setDescription(label.getRemark());
									extLabel.setModifiedBy("SYSTEM");
									extLabel.setModifiedTime(LocalDateTime.now());
								}
								labelsToadd.add(extLabel);
							}
						});
					}
				}));
				}
			   if(!dublicateKey.isEmpty()){
				   duplicateLabels.add(dublicateKey.get(0));
			   }
			});
			// to save in i18N table:
			log.info("labels to be added:{}", labelsToadd);
			if (!labelsToadd.isEmpty()) {
				i18NRepo.saveAll(labelsToadd);
			}
			if(!duplicateLabels.isEmpty()) {
				log.info("duplicate labels :{}", duplicateLabels);
				resultVo = new ResultUtilVO(AppConstant.DUPLICATE_LABEL_CODE, StringUtils.join(duplicateLabels, ','));
			}
		} catch (Exception e) {
			log.info("Exception in moduleList :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<Map<String, String>> getLabelByLang(String unit,String channel,String lang,String screenId){
		var response = new GenericResponse<Map<String, String>>();
		var resMap = new HashMap<String, String>();
		try {
			var labelLst = i18NRepo.findByUnit_UnitIdAndChannel_ChannelIdAndLangCodeAndPage(unit,channel,lang,screenId);
			resMap = (HashMap<String, String>) labelLst.stream().collect(Collectors.toMap(I18N::getKey,I18N::getValue));
			response.setData(resMap);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		}catch(Exception e) {
			log.info("Exception in getLabelByLang :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
	


}
