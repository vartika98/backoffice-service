package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.unit.LanguageDto;
import com.qnb.bo.backofficeservice.entity.Language;
import com.qnb.bo.backofficeservice.entity.UnitLanguage;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.LanguageRepository;
import com.qnb.bo.backofficeservice.repository.UnitLanguageRespository;
import com.qnb.bo.backofficeservice.service.LanguageService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LanguageServiceImpl implements LanguageService{

	private  ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private UnitLanguageRespository unitLanguageRepo;
	@Autowired
	private LanguageRepository langRepo;
	
	@Override
	public GenericResponse<List<LanguageDto>> languageList(Map<String, String> reqBody) {
		var response = new GenericResponse<List<LanguageDto>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var langList = unitLanguageRepo
					.findByUnit_UnitIdAndChannel_ChannelId(reqBody.get(AppConstant.UNIT_ID), reqBody.get(AppConstant.CHANNEL_ID));
			if (Objects.nonNull(langList) && !langList.isEmpty()) {
				var lngLst = langList.stream().map(UnitLanguage::getLanguage).collect(Collectors.toList());
				var langDtoLst = lngLst.stream().map(c -> LanguageDto.builder().langCode(c.getId()).langDesc(c.getTitle())
						.remarks(c.getDescription())
						.build())
				.collect(Collectors.toList());
				response.setData(langDtoLst);
			}
		} catch (Exception e) {
			log.info("Exception in languageList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
	@Override
	public GenericResponse<List<Language>> manageLang(List<LanguageDto> requests) {
	    var response = new GenericResponse<List<Language>>();
	    try {
	        resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
	        var existingLangList = langRepo.findAll();
	        var langsToSave = new ArrayList<Language>();
	        requests.forEach(request->{
	            var langExists = existingLangList.stream().anyMatch(lang -> lang.getId().equals(request.getLangCode())&& (lang.getStatus().equals(AppConstant.ACT) || lang.getStatus().equals(AppConstant.IAC)));
	            if ("MODIFY".equalsIgnoreCase(request.getAction()) || "DELETE".equalsIgnoreCase(request.getAction())) {
	                if (langExists) {
	                    var existingLang = existingLangList.stream()
	                            .filter(lang -> lang.getId().equals(request.getLangCode()))
	                            .findFirst()
	                            .orElse(null);
	                    
	                    if (Objects.nonNull(existingLang)) {
	                        if ("MODIFY".equalsIgnoreCase(request.getAction())) {
	                            existingLang.setStatus(request.getStatus());
	                            existingLang.setTitle(request.getLangDesc());
	                            existingLang.setDescription(request.getRemarks());
	                            existingLang.setModifiedBy("System");
	                            existingLang.setModifiedTime(LocalDateTime.now());
	                            langsToSave.add(existingLang);
	                        } else if ("DELETE".equalsIgnoreCase(request.getAction())) {
	                            existingLang.setStatus(AppConstant.DEL);
	                            langsToSave.add(existingLang);
	                        }
	                    }
	                } else {
	                    resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "Language with ID " + request.getLangCode() + " not found.");
	                }
	            } else if ("ADD".equalsIgnoreCase(request.getAction())) {
	                if (langExists) {
	                	var existingLang = existingLangList.stream()
	                            .filter(lang -> lang.getId().equals(request.getLangCode()) && (lang.getStatus().equals(AppConstant.ACT) || lang.getStatus().equals(AppConstant.IAC)))
	                            .findFirst()
	                            .orElse(null);
	                	 if (Objects.nonNull(existingLang)) {
	                         resultVo = new ResultUtilVO(AppConstant.ALREADY_EXISTS_CODE, "Language with ID " + request.getLangCode() + " already exists.");
	                     }
	                } else {
	                    var newLang = new Language();
	                    newLang.setId(request.getLangCode());
	                    newLang.setTitle(request.getLangDesc());
	                    newLang.setDescription(request.getRemarks());
	                    newLang.setStatus(request.getStatus());
	                    newLang.setCreatedTime(LocalDateTime.now());
	                    newLang.setCreatedBy("BO");
	                    langsToSave.add(newLang);
	                }
	            }
	        });
	        
	        if (!langsToSave.isEmpty()) {
	            var updatedConfigs = langRepo.saveAll(langsToSave);
	        }
	    } catch (Exception e) {
	        log.info("Error in Language management: {}", e);
	        resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
	    }
	    response.setStatus(resultVo);
	    return response;
	}
	@Override
	public GenericResponse<List<LanguageDto>> languageSumarry() {
	    var response = new GenericResponse<List<LanguageDto>>();
	    try {
	        resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
	        var langList = langRepo.findByStatusOrderByModifiedTimeDesc(AppConstant.ACT);
	        
	        var langDtoLst = langList.stream().map(c -> LanguageDto.builder().langCode(c.getId()).langDesc(c.getTitle()).status(c.getStatus())
					.remarks(c.getDescription())
					.build())
			.collect(Collectors.toList());
	        response.setData(langDtoLst);
	    } catch (Exception e) {
	        resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
	    }
	    response.setStatus(resultVo);
	    return response;
	}
}
