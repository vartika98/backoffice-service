package com.qnb.bo.backofficeservice.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.UserAuthRepository;
import com.qnb.bo.backofficeservice.service.LoginService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService{
	
	@Autowired
	private UserAuthRepository userAuthRepository;
	
	private ResultUtilVO resultVo = new ResultUtilVO();
	
	@Override
	public GenericResponse<String> userAuthentication(String userId, String password) {
		var response = new GenericResponse<String>();
		try {
			var user = userAuthRepository.findByUserIdAndPassword(userId, password);
			if(user != null) {
				log.info("Login Successful!");
				response.setData("Login Successful!");
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.SUCCESS);
			}else {
				log.info("Bad Credentials!");
				response.setData("Bad Credentials!");
				resultVo = new ResultUtilVO(AppConstant.UNAUTHORIZED_CODE, AppConstant.UNAUTHORIZED_DESC);
			}
		} catch (Exception e) {
			log.info("Exception in Login service {}" + e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public Long getActiveUserCount(){

		//return springSessionRepository.count(); //TODO:- un comment this once db is ready
		var minValue = 1000;
		var maxValue = 2000;
		var randomDouble = Math.random();
		var randomLong = (long)(minValue + (maxValue - minValue)*randomDouble);
		return randomLong;
	}
}
