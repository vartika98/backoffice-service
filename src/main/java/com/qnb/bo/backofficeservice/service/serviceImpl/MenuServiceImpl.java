package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.txn.ManageMenuReqDto;
import com.qnb.bo.backofficeservice.dto.txn.MenuI18ReqDto;
import com.qnb.bo.backofficeservice.dto.txn.MenuListRes;
import com.qnb.bo.backofficeservice.dto.txn.MenuListResponse;
import com.qnb.bo.backofficeservice.dto.txn.MenuMaster;
import com.qnb.bo.backofficeservice.dto.txn.MenuResponseDto;
import com.qnb.bo.backofficeservice.dto.txn.TransactionEntitlementResponse;
import com.qnb.bo.backofficeservice.dto.txn.TxnEntitlementDto;
import com.qnb.bo.backofficeservice.entity.Channel;
import com.qnb.bo.backofficeservice.entity.I18N;
import com.qnb.bo.backofficeservice.entity.Menu;
import com.qnb.bo.backofficeservice.entity.ParamConfig;
import com.qnb.bo.backofficeservice.entity.ScreenEntitlement;
import com.qnb.bo.backofficeservice.entity.TxnEntitlement;
import com.qnb.bo.backofficeservice.entity.Unit;
import com.qnb.bo.backofficeservice.entity.UnitLanguage;
import com.qnb.bo.backofficeservice.entity.audit.AuditMaster;
import com.qnb.bo.backofficeservice.enums.ActionType;
import com.qnb.bo.backofficeservice.enums.TxnStatus;
import com.qnb.bo.backofficeservice.mapper.MenuMapper;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.ChannelRespository;
import com.qnb.bo.backofficeservice.repository.I18NRepository;
import com.qnb.bo.backofficeservice.repository.MenuRepository;
import com.qnb.bo.backofficeservice.repository.ParamConfigRepository;
import com.qnb.bo.backofficeservice.repository.ScreenEntRepository;
import com.qnb.bo.backofficeservice.repository.TxnEntitlementRepository;
import com.qnb.bo.backofficeservice.repository.UnitLanguageRespository;
import com.qnb.bo.backofficeservice.repository.UnitRespository;
import com.qnb.bo.backofficeservice.service.MenuService;
import com.qnb.bo.backofficeservice.views.MenuEntitlementView;
import com.qnb.bo.backofficeservice.views.MenuView;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

@Service
@Slf4j
public class MenuServiceImpl implements MenuService {

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private DataSource dataSource;

	@Autowired
	private TxnEntitlementRepository txnEntitlementRepository;

	@Autowired
	private MenuRepository menuRepository;

	@Autowired
	private I18NRepository i18NRepo;

	@Autowired
	private UnitLanguageRespository unitLanguageRepo;

	@Autowired
	private ChannelRespository channelRepo;

	@Autowired
	private UnitRespository unitRespository;

	@Autowired
	private ScreenEntRepository screenEntRepo;

	@Autowired
	private MenuMapper menuMapper;

	@Autowired
	private ParamConfigRepository paramConfigRepository;

	@SuppressWarnings("unchecked")
	@Override
	public GenericResponse<MenuListRes> moduleList(Map<String, String> reqBody) {
		var response = new GenericResponse<MenuListRes>();
		var resData = new MenuListRes();
		var errorCode = "";
		var menuList = new ArrayList<Map<String, String>>();
		try {
			var jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("ocs_t_menu_list");
			try {
				var in = new MapSqlParameterSource().addValue("P_UNIT_ID", reqBody.get(AppConstant.UNIT_ID))
						.addValue("P_CHANNEL_ID", reqBody.get(AppConstant.CHANNEL_ID)).addValue("p_menu_list", "out")
						.addValue("out_errorcode", "out").addValue("out_errormsg", "out");
				log.info("ocs_t_menu_list procedure inputs are :::{}", String.valueOf(in));
				var out = jdbcCall.execute(in);
				log.info("ocs_t_menu_list procedure call response: {}", String.valueOf(out));
				if (Objects.nonNull(out) && Objects.nonNull(out.get("P_MENU_LIST"))) {
					menuList = (ArrayList<Map<String, String>>) out.get("P_MENU_LIST");
				}
			} catch (Exception e) {
				log.info("Exception in ocs_t_menu_list procedure :{}", e);
			}
			if (Objects.nonNull(menuList) && !menuList.isEmpty()) {
				var menuRes = new ArrayList<MenuResponseDto>();
				menuList.forEach(menu -> {
					var menuDto = new MenuResponseDto();
					menuDto.setDescription(menu.get("DESCRIPTION"));
					menuDto.setMenuId(menu.get("MENUID"));
					menuDto.setStatus(menu.get("STATUS"));
					menuRes.add(menuDto);
				});
				resData.setMenus(menuRes);
				response.setData(resData);
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			}
		} catch (Exception e) {
			log.info("Exception in moduleList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<Map<String, String>> manageMenus(ManageMenuReqDto reqBody) {
		var response = new GenericResponse<Map<String, String>>();
		final var txnListForUpdate = new ArrayList<TxnEntitlement>();
		try {
			var existedTxnLst = txnEntitlementRepository.findByChannelIdAndUnitId(reqBody.getChannelId(),
					reqBody.getUnitId());
			reqBody.getTxnDetails().forEach(txn -> {
				var existingTxn = existedTxnLst.stream().filter(TxnFil -> TxnFil.getMenuId().equals(txn.getMenuId()))
						.collect(Collectors.toList());
				if (!existingTxn.isEmpty()) {
					existingTxn.forEach(extTxn -> {
						extTxn.setStatus(txn.getStatus());
						txnListForUpdate.add(extTxn);
					});
				}
			});
			log.info("menus to be updated:{}", txnListForUpdate);
			if (!txnListForUpdate.isEmpty()) {
				txnEntitlementRepository.saveAll(txnListForUpdate);
			}
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			log.info("Exception in manageMenus :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<TransactionEntitlementResponse>> getTxnEntitlementList(Map<String, String> reqBody) {
		var response = new GenericResponse<List<TransactionEntitlementResponse>>();
		var menuAllList = new ArrayList<MenuEntitlementView>();
		var menuMasterList = new ArrayList<MenuMaster>();
		var menuLst = new ArrayList<TransactionEntitlementResponse>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var menuList = menuRepository.getEntitledMenuList(reqBody.get(AppConstant.UNIT_ID),
					reqBody.get(AppConstant.CHANNEL_ID));
			menuAllList = (ArrayList<MenuEntitlementView>) menuRepository
					.getAllMenuList(reqBody.get(AppConstant.UNIT_ID), reqBody.get(AppConstant.CHANNEL_ID));
			if (Objects.nonNull(menuList) && menuList.size() > 0 && Objects.nonNull(menuAllList)
					&& menuAllList.size() > 0) {
				menuList.addAll(menuAllList);
				menuMasterList = (ArrayList<MenuMaster>) sortMenuEntitlementList(menuList);
			}
			menuLst = (ArrayList<TransactionEntitlementResponse>) menuMasterList.stream()
					.map(TransactionEntitlementResponse::menuMasterResponse).collect(Collectors.toList());
			response.setData(menuLst);
		} catch (Exception e) {
			log.info("Exception in txnEntitlementLst :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;

	}

	public List<MenuMaster> sortMenuEntitlementList(List<MenuEntitlementView> menuAllList) {
		try {
			var parentList = new ArrayList<MenuEntitlementView>();
			var viewMap = new HashMap<String, MenuEntitlementView>();
			parentList = (ArrayList<MenuEntitlementView>) menuAllList.stream()
					.filter(menu -> menu.getParent() == null && menu.getPriority() != null)
					.sorted(Comparator.comparing(MenuEntitlementView::getPriority)).collect(Collectors.toList());
			var parentSet = new HashSet<MenuEntitlementView>();
			parentList.forEach(view -> {
				if (view.getPriority() != null && !viewMap.containsKey(view.getMenuId()))
					viewMap.put(view.getMenuId(), view);
			});
			parentSet.addAll(viewMap.values());
			parentList = (ArrayList<MenuEntitlementView>) parentSet.stream()
					.filter(menu -> menu.getParent() == null && menu.getPriority() != null)
					.sorted(Comparator.comparing(MenuEntitlementView::getPriority)).collect(Collectors.toList());
			log.info("parentList  {}====>", parentList.size());
			var menuGroupMap = menuAllList.stream().filter(menu -> menu.getParent() != null)
					.collect(Collectors.groupingBy(MenuEntitlementView::getParent));
			var menuMasterList = new ArrayList<MenuMaster>();
			parentList.forEach(parent -> {
				var subMenuList = new ArrayList<MenuMaster>();
				var txnEntitlement = new ArrayList<TxnEntitlementDto>();
				if (parent.getTxnEntStatus() != null && (parent.getTxnEntStatus().equals(TxnStatus.ACT)
						|| parent.getTxnEntStatus().equals(TxnStatus.IAC))) {
					txnEntitlement
							.add(getTxnEntitlement(parent.getUnitId(), parent.getMenuId(), parent.getChannelId()));
				}
				if (parent.getTxnEntStatus() != null && parent.getTxnEntStatus().equals(TxnStatus.IAC)
						&& "HOME".equalsIgnoreCase(parent.getMenuId())) {
					log.info("two Homes", parent.getMenuId());
				} else {
					var menuObj = MenuMaster.builder().menuId(parent.getMenuId()).description(parent.getDescription())
							.subMenus(forSubMenu(menuGroupMap, parent, subMenuList)).txnEntitlement(txnEntitlement)
							.build();
					menuMasterList.add(menuObj);
				}
			});

			return menuMasterList;

		} catch (Exception e) {
			log.error("Error Occured in sortMenuEntitlementList method", e.getCause());
		}
		return null;
	}

	private static TxnEntitlementDto getTxnEntitlement(String unitId, String menuId, String channelId) {
		return TxnEntitlementDto.builder().unitId(unitId).channelId(channelId).menuId(menuId).build();
	}

	private static List<MenuMaster> forSubMenu(Map<String, List<MenuEntitlementView>> menuGroupMap,
			MenuEntitlementView parent, List<MenuMaster> subMenuList) {
		var subMenu = menuGroupMap.get(parent.getMenuId());

		if (subMenu != null) {
			subMenu = subMenu.stream().sorted(Comparator.comparing(MenuEntitlementView::getPriority))
					.collect(Collectors.toList());
			subMenu.forEach(menu -> {
				var sub = fromSubMenu(menu);

				if (menuGroupMap.get(menu.getMenuId()) != null && !menuGroupMap.get(menu.getMenuId()).isEmpty()) {
					var subMenus = new ArrayList<MenuMaster>();
					sub.setSubMenus(forSubMenu(menuGroupMap, menu, subMenus));
				}
				subMenuList.add(sub);
			});
		}
		return subMenuList;
	}

	public static MenuMaster fromSubMenu(MenuEntitlementView menu) {

		var txnEntitlement = new ArrayList<TxnEntitlementDto>();
		if (menu.getTxnEntStatus() != null && menu.getTxnEntStatus().equals(TxnStatus.ACT)) {
			txnEntitlement.add(getTxnEntitlementStatus(menu.getUnitId(), menu.getMenuId(), menu.getChannelId(), "ACT"));
			log.info("UnitId in ACT", menu.getUnitId());
		}
		if (menu.getTxnEntStatus() != null && menu.getTxnEntStatus().equals(TxnStatus.IAC)) {
			txnEntitlement.add(getTxnEntitlementStatus(menu.getUnitId(), menu.getMenuId(), menu.getChannelId(), "IAC"));
		}
		return MenuMaster.builder().menuId(menu.getMenuId()).description(menu.getDescription()).parent(menu.getParent())
				.txnEntitlement(txnEntitlement).build();
	}

	private static TxnEntitlementDto getTxnEntitlementStatus(String unitId, String menuId, String channelId,
			String status) {
		return TxnEntitlementDto.builder().unitId(unitId).channelId(channelId).menuId(menuId).status(status).build();
	}

	@Override
	public GenericResponse<List<Map<String, String>>> menusWithTranslation(Map<String, String> reqBody) {
		var response = new GenericResponse<List<Map<String, String>>>();
		var responseLst = new ArrayList<Map<String, String>>();
		try {
			List<MenuView> menuStatusList;
			if ("ALL".equals(reqBody.get("priority"))) {
				menuStatusList = (ArrayList<MenuView>) menuRepository.menuList(reqBody.get(AppConstant.UNIT_ID),
						reqBody.get(AppConstant.CHANNEL_ID));
			} else {
				menuStatusList = (ArrayList<MenuView>) menuRepository.menuListWithPriority(
						reqBody.get(AppConstant.UNIT_ID), reqBody.get(AppConstant.CHANNEL_ID), reqBody.get("priority"));
			}
			var menuList = menuStatusList.stream().map(MenuView::getMenuId).collect(Collectors.toList());
			var menuTranslationLst = i18NRepo
					.findByUnit_UnitIdAndChannel_ChannelIdAndKeyInAndStatusNotOrderByCreatedTime(
							reqBody.get(AppConstant.UNIT_ID), reqBody.get(AppConstant.CHANNEL_ID), menuList, "DEL");
			var labelGrp = menuTranslationLst.stream()
					.collect(Collectors.groupingBy(I18N::getKey, LinkedHashMap::new, Collectors.toList()));
			labelGrp.entrySet().forEach(lb -> {
				var menuData = menuStatusList.stream().filter(menu -> menu.getMenuId().equals(lb.getKey()))
						.collect(Collectors.toList()).get(0);
				var resMap = new LinkedHashMap<String, String>();
				resMap.put("menuId", menuData.getMenuId());
//				resMap.put("status", lb.getValue().get(0).getStatus());
				resMap.put("status", menuData.getMenuStatus());
				resMap.put("priority", menuData.getPriority());
				lb.getValue().forEach(label -> {
					resMap.put("value_" + label.getLangCode(), label.getValue());
				});
				resMap.put("screenId", menuData.getScreenId());
				resMap.put("description", menuData.getDescription());
				responseLst.add(resMap);
			});
			Collections.sort(responseLst, Comparator.comparing(m -> m.get("priority")));
			response.setData(responseLst);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			log.info("Exception in menusWithTranslation :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<Map<String, String>> manageMenuTranslation(List<MenuI18ReqDto> reqBody) {
		var response = new GenericResponse<Map<String, String>>();
		var txnToBeAdded = new ArrayList<TxnEntitlement>();
		var i18ToBeAdded = new ArrayList<I18N>();
		var menuToBeAdded = new ArrayList<Menu>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var ch = channelRepo.findByChannelId(reqBody.get(0).getChannelId());
			var unit = unitRespository.findByUnitId(reqBody.get(0).getUnitId());
			var langList = unitLanguageRepo.findByUnit_UnitIdAndChannel_ChannelId(reqBody.get(0).getUnitId(),
					reqBody.get(0).getChannelId());
			var parentMenuLst = menuRepository.getAllMenuIdList();
			var menuList = txnEntitlementRepository.menuList(reqBody.get(0).getUnitId(), reqBody.get(0).getChannelId());
			var menuTranslationLst = i18NRepo.findByUnit_UnitIdAndChannel_ChannelIdAndKeyIn(reqBody.get(0).getUnitId(),
					reqBody.get(0).getChannelId(), parentMenuLst);
			var menuStatusList = (ArrayList<Menu>) menuRepository.menuDeatils(reqBody.get(0).getUnitId(),
					reqBody.get(0).getChannelId());

			reqBody.get(0).getMenuDetails().forEach(menu -> {
				var addMenu = "N";
				var addTxnEntitlement = "N";
				var addI18N = "N";

				var existedMenuLst = menuList.stream()
						.filter(existedMenu -> existedMenu.getMenuId().equals(menu.getMenuId()))
						.collect(Collectors.toList());
				var existedMenuMasterLst = menuStatusList.stream()
						.filter(existedMenu -> existedMenu.getMenuId().equals(menu.getMenuId()))
						.collect(Collectors.toList());
				var existedTranslationLst = menuTranslationLst.stream()
						.filter(existedI18Menu -> existedI18Menu.getKey().equals(menu.getMenuId()))
						.collect(Collectors.toList());
				if (ActionType.ADD.toString().equals(menu.getAction())) {
					if (parentMenuLst.contains(menu.getMenuId())) {
						if (!existedMenuLst.isEmpty()) {
							if (!existedTranslationLst.isEmpty()) {
								resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "MenuId already exists.");
							} else {
								addI18N = "Y";
							}
						} else {
							addTxnEntitlement = "Y";
						}
					} else {
						addMenu = "Y";
						addTxnEntitlement = "Y";
						addI18N = "Y";
					}
				} else if (ActionType.MODIFY.toString().equals(menu.getAction())) {
					if (!existedTranslationLst.isEmpty()) {
						existedTranslationLst.forEach(existedTxn -> {
							if (existedTxn.getLangCode().equals("en")) {
								existedTxn.setValue(menu.getValue_en());
							} else if (existedTxn.getLangCode().equals("ar")) {
								existedTxn.setValue(menu.getValue_ar());
							} else {
								existedTxn.setValue(menu.getValue_fr());
							}
							existedTxn.setModifiedBy("BO");
							existedTxn.setStatus(menu.getStatus());
							i18ToBeAdded.add(existedTxn);
						});
					} else {
						resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE,
								"MenuId translation doesn't exists.");
					}
					if(!existedMenuMasterLst.isEmpty()) {
						existedMenuMasterLst.get(0).setDescription(menu.getDescription());
//						existedMenuMasterLst.get(0).setStatus(menu.getStatus());
						menuToBeAdded.add(existedMenuMasterLst.get(0));					
					}
					if(!existedMenuLst.isEmpty()) {
						existedMenuLst.get(0).setDispPriority(Integer.parseInt(menu.getPriority()));
						existedMenuLst.get(0).setStatus(menu.getStatus());
						txnToBeAdded.add(existedMenuLst.get(0));		
					}
				}
				if ("Y".equals(addMenu)) {
					var parentMenu = new Menu();
					parentMenu.setMenuId(menu.getMenuId());
					parentMenu.setDescription(menu.getDescription());
					parentMenu.setMenuType("HEADER");
					parentMenu.setPageId(AppConstant.METADATA_GROUP);
					parentMenu.setLink("#");
					parentMenu.setPriority(Integer.valueOf(menu.getPriority()));
					parentMenu.setStatus(AppConstant.ACT);
					parentMenu.setCreatedBy("BO");
					parentMenu.setScreenId(menu.getScreenId());
					menuToBeAdded.add(parentMenu);
				}
				if ("Y".equals(addTxnEntitlement)) {
					var txn = new TxnEntitlement();
					txn.setChannelId(reqBody.get(0).getChannelId());
					txn.setUnitId(reqBody.get(0).getUnitId());
					txn.setCustomerSegment("DEFAULT");
					txn.setMenuId(menu.getMenuId());
					txn.setDispPriority(Integer.valueOf(menu.getPriority()));
					txn.setCreatedBy("BO");
					txn.setStatus("ACT");
					txn.setCreatedTime(LocalDateTime.now());
					txnToBeAdded.add(txn);
				}
				if ("Y".equals(addI18N)) {
					langList.forEach(lang -> {
						var menuTranslation = new I18N();
						menuTranslation.setChannel(ch);
						menuTranslation.setUnit(unit);
						menuTranslation.setPage(menu.getScreenId());
						menuTranslation.setGroup("MENU");
						menuTranslation.setKey(menu.getMenuId());
						menuTranslation.setDescription(menu.getDescription());
						menuTranslation.setLangCode(lang.getLanguage().getId());
						menuTranslation.setValue(menuTranslation.getLangCode().equals("en") ? menu.getValue_en()
								: (menuTranslation.getLangCode().equals("ar") ? menu.getValue_ar()
										: menu.getValue_fr()));
						menuTranslation.setCreatedBy("BO");
						menuTranslation.setStatus(AppConstant.ACT);
						i18ToBeAdded.add(menuTranslation);
					});
				}
			});
			if (!txnToBeAdded.isEmpty() || !i18ToBeAdded.isEmpty()) {
				log.info("data to be added:{} ,:{}", txnToBeAdded, i18ToBeAdded);
				menuRepository.saveAll(menuToBeAdded);
				txnEntitlementRepository.saveAll(txnToBeAdded);
				i18NRepo.saveAll(i18ToBeAdded);
			}
		} catch (Exception e) {
			log.info("Exception in manageMenuTranslation :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<Map<String, String>>> subMenusWithTranslation(Map<String, String> reqBody) {
		var response = new GenericResponse<List<Map<String, String>>>();
		var responseLst = new ArrayList<Map<String, String>>();
		try {
			List<MenuView> subMenuView;
			if ("ALL".equals(reqBody.get("priority"))) {
				subMenuView = menuRepository.getScreenList(reqBody.get(AppConstant.UNIT_ID),
						reqBody.get(AppConstant.CHANNEL_ID), reqBody.get("menuId"));
			}else {
				subMenuView = menuRepository.getSubMenuByPriority(reqBody.get(AppConstant.UNIT_ID),
						reqBody.get(AppConstant.CHANNEL_ID), reqBody.get("menuId"), reqBody.get("priority"));
			}
			var subMenuList = subMenuView.stream().map(MenuView::getMenuId).collect(Collectors.toList());
			var subMenuTranslationLst = i18NRepo
					.findByUnit_UnitIdAndChannel_ChannelIdAndGroupAndKeyInAndStatusNotOrderByCreatedTime(
							reqBody.get(AppConstant.UNIT_ID), reqBody.get(AppConstant.CHANNEL_ID), "MENU", subMenuList,
							"DEL");
			var labelGrp = subMenuTranslationLst.stream()
					.collect(Collectors.groupingBy(I18N::getKey, LinkedHashMap::new, Collectors.toList()));
			labelGrp.entrySet().forEach(lb -> {
				var submenuData = subMenuView.stream().filter(submenu -> submenu.getMenuId().equals(lb.getKey()))
						.collect(Collectors.toList()).get(0);
				var resMap = new HashMap<String, String>();
				resMap.put("menuId", reqBody.get("menuId"));
				resMap.put("subMenuId", submenuData.getMenuId());
				resMap.put("status", submenuData.getMenuStatus());
				resMap.put("priority", submenuData.getPriority());
				resMap.put("screenId", submenuData.getScreenId());
				resMap.put("description", submenuData.getDescription());
				lb.getValue().forEach(label -> {
					resMap.put("value_" + label.getLangCode(), label.getValue());
				});
				Collections.sort(responseLst, Comparator.comparing(m -> m.get("priority")));
				responseLst.add(resMap);
			});
			response.setData(responseLst);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			log.info("Exception in subMenusWithTranslation :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<Map<String, String>> manageSubMenuTranslation(List<MenuI18ReqDto> reqBody) {
		var response = new GenericResponse<Map<String, String>>();
		var resMap = new HashMap<String, String>();
		var submenuNotExisted = new ArrayList<String>();
		var txnToBeAdded = new ArrayList<TxnEntitlement>();
		var i18ToBeAdded = new ArrayList<I18N>();
		var subMenuToBeAdded = new ArrayList<Menu>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var ch = channelRepo.findByChannelId(reqBody.get(0).getChannelId());
			var unit = unitRespository.findByUnitId(reqBody.get(0).getUnitId());
			var langList = unitLanguageRepo.findByUnit_UnitIdAndChannel_ChannelId(reqBody.get(0).getUnitId(),
					reqBody.get(0).getChannelId());

			var parentMenuLst = menuRepository.getAllMenuIdList();
			var allSubMenuLst = menuRepository.getSubMenuList(reqBody.get(0).getMenuId());
			var subMenuList = txnEntitlementRepository.subMenuList(reqBody.get(0).getUnitId(),
					reqBody.get(0).getChannelId(), reqBody.get(0).getMenuId());
			var subMenuTranslationLst = i18NRepo.findByUnit_UnitIdAndChannel_ChannelIdAndKeyIn(
					reqBody.get(0).getUnitId(), reqBody.get(0).getChannelId(), allSubMenuLst);
			var submenuStatusList = (ArrayList<Menu>) menuRepository.subMenuDeatils(reqBody.get(0).getUnitId(),
					reqBody.get(0).getChannelId(),reqBody.get(0).getMenuId());

			if (!parentMenuLst.contains(reqBody.get(0).getMenuId())) {
				resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "ParentMenuId doesn't exists.");
			} else {
				reqBody.get(0).getMenuDetails().stream().forEach(subMenu -> {
					var addSubMenu = "N";
					var addTxnEntitlement = "N";
					var addI18N = "N";

					var existedMenuLst = subMenuList.stream()
							.filter(existedMenu -> existedMenu.getMenuId().equals(subMenu.getSubMenuId()))
							.collect(Collectors.toList());
					var existedSubMenuLst = submenuStatusList.stream()
							.filter(existedMenu -> existedMenu.getMenuId().equals(subMenu.getSubMenuId()))
							.collect(Collectors.toList());
					var existedTranslationLst = subMenuTranslationLst.stream()
							.filter(existedI18SubMenu -> existedI18SubMenu.getKey().equals(subMenu.getSubMenuId()))
							.collect(Collectors.toList());
					if (ActionType.ADD.toString().equals(subMenu.getAction())) {
						if (allSubMenuLst.contains(subMenu.getSubMenuId())) {
							if (!existedMenuLst.isEmpty()) {
								if (!existedTranslationLst.isEmpty()) {
									resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE,
											"SubMenuId already exists.");
								} else {
									addI18N = "Y";
								}
							} else {
								addTxnEntitlement = "Y";
								addI18N = "Y";
							}

						} else {
							addSubMenu = "Y";
							addTxnEntitlement = "Y";
							addI18N = "Y";
						}

					} else if (ActionType.MODIFY.toString().equals(subMenu.getAction())) {
						if (!existedTranslationLst.isEmpty()) {
							existedTranslationLst.forEach(existedTxn -> {
								if (existedTxn.getLangCode().equals("en")) {
									existedTxn.setValue(subMenu.getValue_en());
								} else if (existedTxn.getLangCode().equals("ar")) {
									existedTxn.setValue(subMenu.getValue_ar());
								} else {
									existedTxn.setValue(subMenu.getValue_fr());
								}
								existedTxn.setModifiedBy("BO");
								existedTxn.setStatus(subMenu.getStatus());
								i18ToBeAdded.add(existedTxn);
							});
						}
						if(!subMenuList.isEmpty()) {
							subMenuList.get(0).setDispPriority(Integer.parseInt(subMenu.getPriority()));
//							subMenuList.get(0).setStatus(subMenu.getStatus());
							txnToBeAdded.add(subMenuList.get(0));
						}
						if(!existedSubMenuLst.isEmpty()) {
							existedSubMenuLst.get(0).setDescription(subMenu.getDescription());
							existedSubMenuLst.get(0).setStatus(subMenu.getStatus());
							subMenuToBeAdded.add(existedSubMenuLst.get(0));
						}
					}
					if ("Y".equals(addSubMenu)) {
						var subMenuEntity = new Menu();
						subMenuEntity.setMenuId(subMenu.getSubMenuId());
						subMenuEntity.setDescription(subMenu.getDescription());
						subMenuEntity.setMenuType("HEADER");
						subMenuEntity.setPageId(AppConstant.METADATA_GROUP);
						subMenuEntity.setLink("#");
						subMenuEntity.setPriority(Integer.valueOf(subMenu.getPriority()));
						subMenuEntity.setStatus(AppConstant.ACT);
						subMenuEntity.setCreatedBy("BO");
						subMenuEntity.setScreenId(subMenu.getScreenId());
						subMenuEntity.setParent(reqBody.get(0).getMenuId());
						subMenuToBeAdded.add(subMenuEntity);
					}
					if ("Y".equals(addTxnEntitlement)) {
						var txn = new TxnEntitlement();
						txn.setChannelId(reqBody.get(0).getChannelId());
						txn.setUnitId(reqBody.get(0).getUnitId());
						txn.setCustomerSegment("DEFAULT");
						txn.setMenuId(subMenu.getSubMenuId());
						txn.setDispPriority(Integer.valueOf(subMenu.getPriority()));
						txn.setCreatedBy("BO");
						txn.setStatus(AppConstant.ACT);
						txn.setCreatedTime(LocalDateTime.now());
						txnToBeAdded.add(txn);
					}
					if ("Y".equals(addI18N)) {
						langList.forEach(lang -> {
							var menuTranslation = new I18N();
							menuTranslation.setChannel(ch);
							menuTranslation.setUnit(unit);
							menuTranslation.setPage(subMenu.getScreenId());
							menuTranslation.setGroup("MENU");
							menuTranslation.setKey(subMenu.getSubMenuId());
							menuTranslation.setDescription(subMenu.getDescription());
							menuTranslation.setLangCode(lang.getLanguage().getId());
							menuTranslation.setValue(menuTranslation.getLangCode().equals("en") ? subMenu.getValue_en()
									: (menuTranslation.getLangCode().equals("ar") ? subMenu.getValue_ar()
											: subMenu.getValue_fr()));
							menuTranslation.setCreatedBy("BO");
							menuTranslation.setStatus(AppConstant.ACT);
							i18ToBeAdded.add(menuTranslation);
						});
					}
				});
			}
			if (!txnToBeAdded.isEmpty() || !i18ToBeAdded.isEmpty() || !subMenuToBeAdded.isEmpty()) {
				log.info("data to be added:{} ,:{}", txnToBeAdded, i18ToBeAdded, subMenuToBeAdded);
				menuRepository.saveAll(subMenuToBeAdded);
				txnEntitlementRepository.saveAll(txnToBeAdded);
				i18NRepo.saveAll(i18ToBeAdded);
			}
		} catch (Exception e) {
			log.info("Exception in manageSubMenuTranslation :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<Map<String, String>>> subMenus2WithTranslation(Map<String, String> reqBody) {
		var response = new GenericResponse<List<Map<String, String>>>();
		var subMenu2List = new ArrayList<Map<String, String>>();
		try {
			List<MenuView> subMenu2View;
			if ("ALL".equals(reqBody.get("priority"))) {
				subMenu2View = menuRepository.getSub2MenuList(reqBody.get(AppConstant.UNIT_ID),
					reqBody.get(AppConstant.CHANNEL_ID), reqBody.get("subMenuId"));
			}else {
				subMenu2View = menuRepository.getSub2MenuListByPriority(reqBody.get(AppConstant.UNIT_ID),
						reqBody.get(AppConstant.CHANNEL_ID), reqBody.get("subMenuId"),reqBody.get("priority"));
			}
			var subMenu2Lst = subMenu2View.stream().map(MenuView::getMenuId).collect(Collectors.toList());
			var subMenuTranslationLst = i18NRepo
					.findByUnit_UnitIdAndChannel_ChannelIdAndGroupAndKeyInAndStatusNotOrderByCreatedTime(
							reqBody.get(AppConstant.UNIT_ID), reqBody.get(AppConstant.CHANNEL_ID), "MENU", subMenu2Lst,
							"DEL");
			var labelGrp = subMenuTranslationLst.stream()
					.collect(Collectors.groupingBy(I18N::getKey, LinkedHashMap::new, Collectors.toList()));
			labelGrp.entrySet().forEach(lb -> {
				var submenuData = subMenu2View.stream().filter(submenu -> submenu.getMenuId().equals(lb.getKey()))
						.collect(Collectors.toList()).get(0);
				var resMap = new HashMap<String, String>();
				resMap.put("menuId", reqBody.get("menuId"));
				resMap.put("subMenuId", reqBody.get("subMenuId"));
				resMap.put("subMenu2Id", lb.getKey());
				resMap.put("status", submenuData.getMenuStatus());
				resMap.put("priority", submenuData.getPriority());
				resMap.put("description", submenuData.getDescription());
				resMap.put("screenId", submenuData.getScreenId());
				lb.getValue().forEach(label -> {
					resMap.put("value_" + label.getLangCode(), label.getValue());
				});
				subMenu2List.add(resMap);
			});
			response.setData(subMenu2List);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			log.info("Exception in subMenus2WithTranslation :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<Map<String, String>> manageSubMenu2Translation(List<MenuI18ReqDto> reqBody) {
		var response = new GenericResponse<Map<String, String>>();
		var txnToBeAdded = new ArrayList<TxnEntitlement>();
		var i18ToBeAdded = new ArrayList<I18N>();
		var subMenu2ToBeAdded = new ArrayList<Menu>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var ch = channelRepo.findByChannelId(reqBody.get(0).getChannelId());
			var unit = unitRespository.findByUnitId(reqBody.get(0).getUnitId());
			var langList = unitLanguageRepo.findByUnit_UnitIdAndChannel_ChannelId(reqBody.get(0).getUnitId(),
					reqBody.get(0).getChannelId());
			var parentMenuLst = menuRepository.getAllMenuIdList();
			var subMenuLst = menuRepository.getSubMenuList(reqBody.get(0).getMenuId());
			var allSubMenu2Lst = menuRepository.getSubMenu2List(reqBody.get(0).getSubMenuId());
			var subMenu2List = txnEntitlementRepository.subMenu2List(reqBody.get(0).getUnitId(),
					reqBody.get(0).getChannelId(), reqBody.get(0).getSubMenuId());
			var subMenu2TranslationLst = i18NRepo.findByUnit_UnitIdAndChannel_ChannelIdAndKeyIn(
					reqBody.get(0).getUnitId(), reqBody.get(0).getChannelId(), allSubMenu2Lst);

			if (!parentMenuLst.contains(reqBody.get(0).getMenuId())
					|| !subMenuLst.contains(reqBody.get(0).getSubMenuId())) {
				resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "MenuId doesn't exists.");
			} else {
				reqBody.get(0).getMenuDetails().forEach(subMenu2 -> {
					var addSubMenu2 = "N";
					var addTxnEntitlement = "N";
					var addI18N = "N";

					var existedMenuLst = subMenu2List.stream()
							.filter(existedMenu -> existedMenu.getMenuId().equals(subMenu2.getSubMenuId2()))
							.collect(Collectors.toList());
					var existedTranslationLst = subMenu2TranslationLst.stream()
							.filter(existedI18SubMenu -> existedI18SubMenu.getKey().equals(subMenu2.getSubMenuId2()))
							.collect(Collectors.toList());

					if (ActionType.ADD.toString().equals(subMenu2.getAction())) {
						if (!existedMenuLst.isEmpty()) {
							if (existedTranslationLst.isEmpty()) {
								addI18N = "Y";
							} else {
								resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE,
										"SubMenuId2 already exists.");
							}
						} else {
							addSubMenu2 = "Y";
							addTxnEntitlement = "Y";
							addI18N = "Y";
						}
					} else if (ActionType.MODIFY.toString().equals(subMenu2.getAction())) {
						if (!existedTranslationLst.isEmpty()) {
							existedTranslationLst.forEach(existedTxn -> {
								if (existedTxn.getLangCode().equals("en")) {
									existedTxn.setValue(subMenu2.getValue_en());
								} else if (existedTxn.getLangCode().equals("ar")) {
									existedTxn.setValue(subMenu2.getValue_ar());
								} else {
									existedTxn.setValue(subMenu2.getValue_fr());
								}
								existedTxn.setModifiedBy("BO");
								existedTxn.setStatus(AppConstant.ACT);
								i18ToBeAdded.add(existedTxn);
							});
						}
					}
					if ("Y".equals(addSubMenu2)) {
						var subMenuEntity = new Menu();
						subMenuEntity.setMenuId(subMenu2.getSubMenuId2());
						subMenuEntity.setDescription(subMenu2.getDescription());
						subMenuEntity.setMenuType("HEADER");
						subMenuEntity.setPageId(AppConstant.METADATA_GROUP);
						subMenuEntity.setLink("#");
						subMenuEntity.setPriority(Integer.valueOf(subMenu2.getPriority()));
						subMenuEntity.setStatus(AppConstant.ACT);
						subMenuEntity.setCreatedBy("BO");
						subMenuEntity.setScreenId(subMenu2.getScreenId());
						subMenuEntity.setParent(reqBody.get(0).getSubMenuId());
						subMenu2ToBeAdded.add(subMenuEntity);
					}
					if ("Y".equals(addTxnEntitlement)) {
						var txn = new TxnEntitlement();
						txn.setChannelId(reqBody.get(0).getChannelId());
						txn.setUnitId(reqBody.get(0).getUnitId());
						txn.setCustomerSegment("DEFAULT");
						txn.setMenuId(subMenu2.getSubMenuId2());
						txn.setDispPriority(Integer.valueOf(subMenu2.getPriority()));
						txn.setCreatedBy("BO");
						txn.setStatus(AppConstant.ACT);
						txn.setCreatedTime(LocalDateTime.now());
						txnToBeAdded.add(txn);
					}
					if ("Y".equals(addI18N)) {
						langList.forEach(lang -> {
							var menuTranslation = new I18N();
							menuTranslation.setChannel(ch);
							menuTranslation.setUnit(unit);
							menuTranslation.setPage(subMenu2.getScreenId());
							menuTranslation.setGroup("MENU");
							menuTranslation.setKey(subMenu2.getSubMenuId2());
							menuTranslation.setDescription(subMenu2.getDescription());
							menuTranslation.setLangCode(lang.getLanguage().getId());
							menuTranslation.setValue(menuTranslation.getLangCode().equals("en") ? subMenu2.getValue_en()
									: (menuTranslation.getLangCode().equals("ar") ? subMenu2.getValue_ar()
											: subMenu2.getValue_fr()));
							menuTranslation.setCreatedBy("BO");
							menuTranslation.setStatus(AppConstant.ACT);
							i18ToBeAdded.add(menuTranslation);
						});
					}
				});
			}
			if (!txnToBeAdded.isEmpty() || !i18ToBeAdded.isEmpty() || !subMenu2ToBeAdded.isEmpty()) {
				log.info("data to be added:{} ,:{}", txnToBeAdded, i18ToBeAdded, subMenu2ToBeAdded);
				menuRepository.saveAll(subMenu2ToBeAdded);
				txnEntitlementRepository.saveAll(txnToBeAdded);
				i18NRepo.saveAll(i18ToBeAdded);
			}
		} catch (Exception e) {
			log.info("Exception in manageSubMenu2Translation :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<Map<String, List<Map<String, String>>>> getScreenEntitlementListMb(
			Map<String, String> reqBody) {
		var response = new GenericResponse<Map<String, List<Map<String, String>>>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var screenEntLst = screenEntRepo.findAllByUnit_UnitIdAndChannel_ChannelIdAndWidget(reqBody.get("unitId"),
					reqBody.get("channelId"), reqBody.get("device"));
			var screenEntitlementMap = new HashMap<String, List<Map<String, String>>>();
			screenEntLst.forEach(screenEntitlement -> {
				var moduleName = screenEntitlement.getMenu().getMenuId().substring(4);
				var featureDetailsList = screenEntitlementMap.getOrDefault(moduleName, new ArrayList<>());
				var featureDetails = new HashMap<String, String>();
				featureDetails.put("feature", screenEntitlement.getScreen());
				featureDetails.put("module", screenEntitlement.getMenu().getMenuId().substring(4));
				featureDetails.put("priority",
						Objects.nonNull(screenEntitlement.getPriority())
								? String.valueOf(screenEntitlement.getPriority())
								: "");
				featureDetails.put("platform", screenEntitlement.getWidget());
				featureDetails.put("status", screenEntitlement.getEnabled());
				featureDetailsList.add(featureDetails);
				screenEntitlementMap.put(moduleName, featureDetailsList);
			});
			response.setData(screenEntitlementMap);
		} catch (Exception e) {
			log.info("Exception in getAcreenEntitlementListMb :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public Flux<GenericResponse<Map<String, List<Map<String, String>>>>> getScreenEntitlementListReact(
			Map<String, String> reqBody) {
		return Flux.defer(() -> {
			var response = new GenericResponse<Map<String, List<Map<String, String>>>>();
			try {
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
				var screenEntLst = screenEntRepo.findAllByUnit_UnitIdAndChannel_ChannelIdAndWidget(
						reqBody.get("unitId"), reqBody.get("channelId"), reqBody.get("device"));
				var screenEntitlementMap = new HashMap<String, List<Map<String, String>>>();
				screenEntLst.forEach(screenEntitlement -> {
					var moduleName = screenEntitlement.getMenu().getMenuId().substring(4);
					var featureDetailsList = screenEntitlementMap.getOrDefault(moduleName, new ArrayList<>());
					var featureDetails = new HashMap<String, String>();
					featureDetails.put("feature", screenEntitlement.getScreen());
					featureDetails.put("module", screenEntitlement.getMenu().getMenuId().substring(4));
					featureDetails.put("priority",
							Objects.nonNull(screenEntitlement.getPriority())
									? String.valueOf(screenEntitlement.getPriority())
									: "");
					featureDetails.put("platform", screenEntitlement.getWidget());
					featureDetails.put("status", screenEntitlement.getEnabled());
					featureDetailsList.add(featureDetails);
					screenEntitlementMap.put(moduleName, featureDetailsList);
				});
				response.setData(screenEntitlementMap);
			} catch (Exception e) {
				log.info("Exception in getAcreenEntitlementListMb :{}", e);
				resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
			}
			response.setStatus(resultVo);
			return Flux.just(response);
		});
	}

	@Override
	public GenericResponse<List<MenuListResponse>> getAllMenuList(Map<String, String> reqBody) {
		var response = new GenericResponse<List<MenuListResponse>>();
		var menuSubMenuListToAdd = new LinkedList<MenuListResponse>();

		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("ocs_t_menu_submenu_list");
			try {
				var in = new MapSqlParameterSource().addValue("P_UNIT_ID", reqBody.get(AppConstant.UNIT_ID))
						.addValue("P_CHANNEL_ID", reqBody.get(AppConstant.CHANNEL_ID))
						.addValue("P_CUSTOMER_SEGMENT", reqBody.get(AppConstant.CUSTOMER_SEGMENT))
						.addValue("P_LANG", reqBody.get(AppConstant.REQUEST_ACCEPT_LANG))
						.addValue("P_STATUS", AppConstant.DEFAULT_STATUS_ACT).addValue("p_txn_menu_lst", "out")
						.addValue("p_trsltion_menu_lst", "out").addValue("out_errorcode", "out")
						.addValue("out_errormsg", "out");
				log.info("ocs_t_menu_submenu_list procedure inputs are :::{}", String.valueOf(in));
				var out = jdbcCall.execute(in);
				log.info("ocs_t_menu_submenu_list procedure call response: {}", out);
				if (Objects.nonNull(out)) {
					@SuppressWarnings("unchecked")
					var txnEntitleMents = (List<Map<String, String>>) out.get("P_TXN_MENU_LST");
					@SuppressWarnings("unchecked")
					var i18Translation = (List<Map<String, String>>) out.get("P_TRSLTION_MENU_LST");
					final var i18translationMap = new HashMap<String, String>();

					// for (Map<String, String> kvPair : i18Translation) {
					i18Translation.forEach(kvPair -> {
						i18translationMap.put(kvPair.get("TRANS_KEY"), String.valueOf(kvPair.get("TRANS_VALUE")));
					});

					final var entitlementKeys = new ArrayList<String>();
					final var menus = new LinkedHashSet<Menu>();
					// for (Map<String, String> entitlement : txnEntitleMents) {
					txnEntitleMents.forEach(entitlement -> {
						var menuData = new Menu();
						menuData.setMenuId(entitlement.get("MENU_ID"));
						menuData.setParent(entitlement.get("PARENT_MENU_ID"));
						menuData.setLink(entitlement.get("LINK"));
						menuData.setPageId(entitlement.get("PAGE_ID"));
						menuData.setMenuType(entitlement.get("MENU_TYPE"));
						menuData.setDescription(entitlement.get("DESCRIPTION"));
						entitlementKeys.add(menuData.getMenuId());
						if (menuData.getParent() == null) {
							menus.add(menuData);
						}
					});
					menuSubMenuListToAdd = extractEntitledMenus(menus, entitlementKeys, i18translationMap).stream()
							.map(menu -> menuMapper.menuToDto(menu))
							.sorted(Comparator.comparingInt(MenuListResponse::getPriority))
							.collect(Collectors.toCollection(LinkedList::new));
				}
			} catch (Exception e) {
				log.info("Exception in ocs_t_menu_list procedure :{}", e);
			}
		} catch (Exception e) {
			log.info("Exception in getAllMenuList :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		;
		response.setData(menuSubMenuListToAdd);
		return response;
	}

	private Set<Menu> extractEntitledMenus(final Set<Menu> menus, final List<String> entitlementKeys,
			final Map<String, String> translationMap) {

		for (final Iterator<Menu> menuIterator = menus.iterator(); menuIterator.hasNext();) {
			final var menu = menuIterator.next();
			if (!entitlementKeys.contains(menu.getMenuId())) {
				menuIterator.remove();
				continue;
			}
			menu.setTitle(translationMap.get(menu.getMenuId()));
			if (!menu.getSubMenus().isEmpty()) {
				extractEntitledMenus(menu.getSubMenus(), entitlementKeys, translationMap);
			}
		}
		return menus;
	}

	@Override
	public GenericResponse<List<String>> priorityList(Map<String, String> reqBody) {
		GenericResponse<List<String>> response = new GenericResponse<>();
		String key = "PRIORITY_IB";
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			if ("MB".equals(reqBody.get("channel"))) {
				key = "PRIORITY_MB";
			}
			List<ParamConfig> paramList = paramConfigRepository
					.findByKeyStartsWithAndUnit_UnitIdAndStatusAndChannel_ChannelId(key, reqBody.get("unitId"),
							AppConstant.ACT, reqBody.get("channel"));
			if (!paramList.isEmpty()) {
				paramList.get(0).getValue();
				List<String> reslst = new ArrayList<>(Arrays.asList(paramList.get(0).getValue().split(",")));
				reslst.add("ALL");
				response.setData(reslst);
			}
		} catch (Exception e) {
			log.info("Exception in getAllMenuList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}