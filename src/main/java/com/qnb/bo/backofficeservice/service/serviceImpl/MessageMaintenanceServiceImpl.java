package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.MessageMaintenance.MaintenanceDto;
import com.qnb.bo.backofficeservice.dto.MessageMaintenance.MaintenanceListResponse;
import com.qnb.bo.backofficeservice.dto.MessageMaintenance.MaintenanceMessage;
import com.qnb.bo.backofficeservice.entity.MaintenanceMessageAppDetailsEntity;
import com.qnb.bo.backofficeservice.entity.MaintenanceMessageEntity;
import com.qnb.bo.backofficeservice.entity.MaintenanceMessageLangDetailsEntity;
import com.qnb.bo.backofficeservice.enums.ExceptionMessages;
import com.qnb.bo.backofficeservice.enums.TxnStatus;
import com.qnb.bo.backofficeservice.exceptions.ResourceNotFoundException;
import com.qnb.bo.backofficeservice.exceptions.ValidationException;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.ApplicationRepository;
import com.qnb.bo.backofficeservice.repository.MaintenanceMessageAppDetailsRepository;
import com.qnb.bo.backofficeservice.repository.MessageMaintenanceRepository;
import com.qnb.bo.backofficeservice.service.MessageMaintenanceService;
import com.qnb.bo.backofficeservice.utils.DateUtil;
import com.qnb.bo.backofficeservice.views.ApplicationView;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MessageMaintenanceServiceImpl implements MessageMaintenanceService {

	@Autowired
	private MessageMaintenanceRepository messageMaintenanceRepository;

	@Autowired
	private ApplicationRepository applicationRepository;

	@Autowired
	private MaintenanceMessageAppDetailsRepository maintenanceMessageAppDetailsRepo;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<MaintenanceListResponse> getMaintenanceList(String msgType) {
		var response = new GenericResponse<MaintenanceListResponse>();
		var resData = new MaintenanceListResponse();
		try {
			var messageType = "";
			if (msgType.equalsIgnoreCase("planned")) {
				messageType = "P";
			} else if (msgType.equalsIgnoreCase("unplanned")) {
				messageType = "U";
			} else {
				resultVo = new ResultUtilVO(AppConstant.INVALID_MAINTENANCE_CODE, AppConstant.INVALID_MAINTENANCE_DESC);
				response.setStatus(resultVo);
				return response;
			}

			var maintenanceMsgList = messageMaintenanceRepository
					.findByMessageType(messageType);
			log.info("The Message list is {}", maintenanceMsgList);
			var maintenanceList = new ArrayList<MaintenanceDto>();

			maintenanceMsgList.forEach(msg -> {
				var startdate = new Date();
				var enddate = new Date();
				var channels = new HashSet<String>();

				if (msgType.equalsIgnoreCase("planned")) {

					channels = (HashSet<String>) msg.getMsgAppDetails().stream().map(MaintenanceMessageAppDetailsEntity::getChannelId)
							.collect(Collectors.toSet());
					startdate = msg.getMsgAppDetails().stream()
							.map(MaintenanceMessageAppDetailsEntity::getOutageStartDateTime).min(Date::compareTo).get();
					enddate = msg.getMsgAppDetails().stream()
							.map(MaintenanceMessageAppDetailsEntity::getOutageEndDateTime).max(Date::compareTo).get();
				}
				var startDate = startdate != null ? DateUtil.dateTimeInStringFormat(startdate) : null;
				var endDate = enddate != null ? DateUtil.dateTimeInStringFormat(enddate) : null;
				var maintenanceDto = new MaintenanceDto(msg.getTxnId(), msg.getPurposeOfMsg(), startDate,
						endDate, msg.getStatus(), channels);
				maintenanceList.add(maintenanceDto);
			});
			
			if (msgType.equalsIgnoreCase("planned")) {
				Collections.sort(maintenanceList, (o1, o2) -> DateUtil.getDateTime(o1.getOutageStartDate())
						.compareTo(DateUtil.getDateTime(o2.getOutageStartDate())));
			}
			log.info("maintenanceList {}", maintenanceList);
			resData.setMaintenanceMessages(maintenanceList);
			response.setData(resData);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			log.info("Exception in calling getMaintenanceList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<MaintenanceMessageEntity>> manageMaintenance(List<MaintenanceMessage> req) { // list of maintenanceMessage
		var response = new GenericResponse<List<MaintenanceMessageEntity>>();
		var resData = new ArrayList<MaintenanceMessageEntity>();
		var messages = new ArrayList<MaintenanceMessageEntity>();
		resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		try {
			req.forEach(reqDto -> {
				if (reqDto.getAction().equals("ADD")) {

					var maintenanceDetailsToSave = new MaintenanceMessageEntity();
					if (reqDto.getMessageType().equals("P")) {
						var units = reqDto.getUnitSet();
						var appIds = reqDto.getAppIdSet();

						getApplicationListValidatedWithUnits(new ArrayList<>(units), new ArrayList<>(appIds));
						checkDuplicateMaintenance(reqDto, "ADD");

						maintenanceDetailsToSave = MaintenanceMessageEntity.toPlannedEntity(reqDto);
					} else if (reqDto.getMessageType().equals("U")) {
						maintenanceDetailsToSave = MaintenanceMessageEntity.toUnplannedEntity(reqDto);
					}

					if (maintenanceDetailsToSave != null) {
						messages.add(maintenanceDetailsToSave);
					}

				} else if (reqDto.getAction().equals("MODIFY")) {
					var msgEntity = messageMaintenanceRepository
							.getAllMaintenanceMsgByRefNo(reqDto.getRefNo());
					if (msgEntity == null) {
						log.info("Resource not found for modification");
						resultVo = new ResultUtilVO(AppConstant.INVALID_MAINTENANCE_CODE, AppConstant.INVALID_MAINTENANCE_DESC);
					} else {
						msgEntity.setStatus(reqDto.getStatus());
						msgEntity.setPurposeOfMsg(reqDto.getPurposeOfMsg());
						msgEntity = setMessageLangDetails(reqDto, msgEntity);
						if (reqDto.getMessageType().equals("P")) {
							msgEntity = setMessageAppDetails(reqDto, msgEntity);
						}
						messages.add(msgEntity);
					}

				} else if (reqDto.getAction().equals("DELETE")) {
					MaintenanceMessageEntity msgEntity = messageMaintenanceRepository
							.getAllMaintenanceMsgByRefNo(reqDto.getRefNo());
					if (msgEntity == null) {
						log.info("Resource not found for Deletion");
						resultVo = new ResultUtilVO(AppConstant.INVALID_MAINTENANCE_CODE, AppConstant.INVALID_MAINTENANCE_DESC);
					} else {
						try {
							msgEntity.setStatus(TxnStatus.TER);
							messages.add(msgEntity);
						} catch (DataIntegrityViolationException ex) {
							log.info("Exception in Deletion :{} "+ ex.getMessage());
							resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
						}
					}
				} else {
					resultVo = new ResultUtilVO(AppConstant.INVALID_MAINTENANCE_CODE, AppConstant.INVALID_MAINTENANCE_DESC);
				}
			});
			
			messageMaintenanceRepository.saveAll(messages);
			response.setData(resData);
		} catch (Exception e) {
			log.info("Exception in calling manage-maintenance :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	public List<ApplicationView> getApplicationListValidatedWithUnits(List<String> units, List<Long> appIds) {

		var unitList = new ArrayList<String>(units);
		var applicationsForUnits = applicationRepository
				.getAllApplicationsListByUnitAppIdStatus(unitList, appIds, TxnStatus.ACT);
		var distinctAppList = applicationsForUnits.stream()
				.filter(distinctByKeys(ApplicationView::getUnitId, ApplicationView::getAppName))
				.collect(Collectors.toList());

		var appAvailableforAllUnits = true;
		var currentApp = 0L;
		var currentUnit = "";
		for (Long app : appIds) {
			for (String unit : units) {
				currentApp = app;
				currentUnit = unit;

				long itemCount = distinctAppList.stream()
						.filter(rec -> rec.getTxnId().equals(app) && rec.getUnitId().equalsIgnoreCase(unit)).count();
				if (itemCount > 0)
					appAvailableforAllUnits = true;
				else {
					appAvailableforAllUnits = false;
					break;
				}
			}
			if (!appAvailableforAllUnits)
				break;
		}
		if (!appAvailableforAllUnits) {
			throw new ResourceNotFoundException(ExceptionMessages.APPLICATION_NOT_FOUND_EXCEPTION.getErrorCode(),
					String.format("Application : %s  is not allocated for the Unit : %s", currentApp, currentUnit));
		}
		return applicationsForUnits;
	}

	@SafeVarargs
	private static <T> Predicate<T> distinctByKeys(Function<? super T, ?>... KeyExtratory) {
		final var found = new ConcurrentHashMap<>();
		return t -> {
			final var keys = Arrays.stream(KeyExtratory).map(ke -> ke.apply(t)).collect(Collectors.toList());
			return found.putIfAbsent(keys, Boolean.TRUE) == null;
		};
	}

	public void checkDuplicateMaintenance(MaintenanceMessage maintenanceMessage, String action) {
		final var existingMaintenance = new ArrayList<MaintenanceMessageEntity>();

		if (action.equals("ADD")) {
			existingMaintenance.addAll(messageMaintenanceRepository
					.getExistingMaintenance(maintenanceMessage.getUnitSet(), maintenanceMessage.getAppIdSet()));
		} else {
			existingMaintenance.addAll(messageMaintenanceRepository.getExistingMaintenanceByRefNo(
					maintenanceMessage.getUnitSet(), maintenanceMessage.getAppIdSet(), maintenanceMessage.getRefNo()));
		}
		if (!existingMaintenance.isEmpty()) {
			var hasDuplicate = new boolean []{ false };
			var message = new StringBuilder(
					"Planned maintenance already exist for the following units and applications in the given date range, ");
			maintenanceMessage.getApplicationDetails().forEach(req -> {
				existingMaintenance.forEach(maintenance -> {
					var duplicate = maintenance.getMsgAppDetails().stream().anyMatch(app -> app
							.getOutageStartDateTime().toInstant().equals(req.getOutageStartDateTime().toInstant())
							&& app.getOutageEndDateTime().toInstant().equals(req.getOutageEndDateTime().toInstant()));
					if (duplicate) {
						message.append(req.getUnitId()).append(" & ").append(req.getApplication()).append(" | ");
						hasDuplicate[0] = true;
					}
				});
			});
			if (hasDuplicate[0]) {
				throw new ValidationException(ExceptionMessages.REQUEST_ALREADY_EXIST.getErrorCode(),
						message.toString() , HttpStatus.CONFLICT);
			}
		}
	}

	private MaintenanceMessageEntity setMessageLangDetails(MaintenanceMessage maintenanceReq, MaintenanceMessageEntity msgEntity) {
		maintenanceReq.getMessageDetails().forEach(msgReq -> {
			var msgLangDetails = msgEntity.getMsgLangDetails().stream()
					.filter(msgdet -> msgReq.getLang().equals(msgdet.getLang())).collect(Collectors.toList());
			if (!msgLangDetails.isEmpty()) {
				msgLangDetails.forEach(msgDet -> {
					msgDet.setReason(msgReq.getReason());
				});
			} else {
				msgEntity.getMsgLangDetails().add(MaintenanceMessageLangDetailsEntity.toEntity(msgEntity, msgReq));
			}
		});
		
		return msgEntity;
	}

	private MaintenanceMessageEntity setMessageAppDetails(MaintenanceMessage maintenanceReq, MaintenanceMessageEntity msgEntity) {
		getApplicationListValidatedWithUnits(new ArrayList<>(maintenanceReq.getUnitSet()),
				new ArrayList<>(maintenanceReq.getAppIdSet()));

		checkDuplicateMaintenance(maintenanceReq, "MODIFY");
		log.info("Deleting app details");
		maintenanceMessageAppDetailsRepo.deleteByRefNo(msgEntity.getTxnId());
		log.info("Deleted app details");

		maintenanceReq.getApplicationDetails().forEach(req -> {
			msgEntity.getMsgAppDetails().add(MaintenanceMessageAppDetailsEntity.toEntity(msgEntity, req));

		});
		return msgEntity;
	}
}
