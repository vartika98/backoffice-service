package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.NotificationParamConfigDTO;
import com.qnb.bo.backofficeservice.entity.NotificationParamConfigEntity;
import com.qnb.bo.backofficeservice.mapper.NotificationParamConfigMapper;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.NotificationParamConfigRepo;
import com.qnb.bo.backofficeservice.service.NotificationParamConfigService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class NotificationParamConfigServiceImpl implements NotificationParamConfigService {
	@Autowired
	private NotificationParamConfigRepo notificationParamConfigRepo;
	@Autowired
	private NotificationParamConfigMapper notificationParamConfigMapper;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<List<NotificationParamConfigDTO>> getSummary() {
		var response = new GenericResponse<List<NotificationParamConfigDTO>>();
		try {
			var data = notificationParamConfigMapper.toDto(notificationParamConfigRepo.findByStatus(AppConstant.ACT));
			var resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			response.setData(data);
			response.setStatus(resultVo);
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			var resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
			response.setStatus(resultVo);
		}
		return response;
	}

	public GenericResponse<List<NotificationParamConfigDTO>> manageNotifications(List<NotificationParamConfigDTO> dtoRequest) {
		var response = new GenericResponse<List<NotificationParamConfigDTO>>();
		var listNotifications = new ArrayList<NotificationParamConfigEntity>();
		resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		response.setStatus(resultVo);
		response.setData(null);
		var notFound = new ArrayList<String>();
		var newlyAdded = new ArrayList<NotificationParamConfigDTO>();
		var duplicateData = new ArrayList<String>();
		var message=new String();
		try {
			dtoRequest.forEach(req -> {
				var list = new NotificationParamConfigDTO();
				var exists = notificationParamConfigRepo.findByNotificationId(req.getNotificationId());
				if (req.getAction().equalsIgnoreCase(AppConstant.ADD)) {
					if (Objects.nonNull(exists)) {
						log.info("Notification already exixts:{}");
						resultVo = new ResultUtilVO(AppConstant.ALREADY_EXISTS_CODE, "already Existes");
						duplicateData.add(req.getNotificationId());
					} else {
						list.setCreatedBy("BO");
						list.setCreatedTime(LocalDateTime.now());
						list.setModifiedBy("BO");
						list.setModifiedTime(LocalDateTime.now());
						list.setStatus(AppConstant.ACT);
						list.setNotificationId(req.getNotificationId());
						list.setNotificationDesc(req.getNotificationDesc());
						listNotifications.add(notificationParamConfigMapper.toEntity(list));
						newlyAdded.add(list);
					}
				} else if (req.getAction().equalsIgnoreCase(AppConstant.DELETE)) {
					if (Objects.nonNull(exists)) {
						exists.setModifiedBy("BO");
						exists.setModifiedTime(LocalDateTime.now());
						exists.setStatus(AppConstant.DELETE);
						listNotifications.add(exists);
					} else {
						notFound.add(req.getNotificationId());
					//	resultVo = new ResultUtilVO(AppConstant.NOT_FOUND, "Not found");

					}
				} else if (req.getAction().equalsIgnoreCase(AppConstant.MODIFY)) {
					if (Objects.nonNull(exists)) {
						exists.setModifiedBy("BO");
						exists.setModifiedTime(LocalDateTime.now());
						exists.setStatus(req.getStatus());
						exists.setNotificationDesc(req.getNotificationDesc());
						listNotifications.add(exists);
					} else {
						notFound.add(req.getNotificationId());
						//resultVo = new ResultUtilVO(AppConstant.NOT_FOUND, "Not found");

					}
				}

			});
			notificationParamConfigRepo.saveAll(listNotifications);
			response.setData(notificationParamConfigMapper.toDto(listNotifications));
			
			if (!newlyAdded.isEmpty()) {
				message=message+"Data Added Successfully, ";
			}
			
			if (!duplicateData.isEmpty()) {
				message=message+"Data with Notification ID : "+duplicateData+ " already exists, ";
			}
			if (!notFound.isEmpty()) {
				resultVo = new ResultUtilVO(AppConstant.NOT_FOUND, "Not found");
				message=message+"Data with Notification ID : "+notFound+ " not found, ";

			}
			
			resultVo = new ResultUtilVO(AppConstant.SUCCESS, message);

		} catch (Exception e) {
			log.info("Exception Occured in manageNotifications");
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
