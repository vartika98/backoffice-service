package com.qnb.bo.backofficeservice.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.notification.NotificationDto;
import com.qnb.bo.backofficeservice.enums.NotificationStatus;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.service.NotificationService;
import com.qnb.bo.backofficeservice.utils.pushNotification.PushNotificationSender;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class NotificationServiceImpl implements NotificationService{

	ResultUtilVO resultVo = new ResultUtilVO();
	
	@Autowired
	PushNotificationSender pushSender;
	
	@Override
	public GenericResponse<NotificationStatus> sendNotification(NotificationDto message) {
		var response = new GenericResponse<NotificationStatus>();
		try {
			NotificationStatus sendStatus = pushSender.sendCommonMessage(message);
			response.setData(sendStatus);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			log.info("Exception in calling sendnotification:{}",e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
