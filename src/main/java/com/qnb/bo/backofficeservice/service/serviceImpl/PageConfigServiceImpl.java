package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.PageConfigDto;
import com.qnb.bo.backofficeservice.entity.PageConfig;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.ChannelRespository;
import com.qnb.bo.backofficeservice.repository.PageConfigRepository;
import com.qnb.bo.backofficeservice.repository.UnitRespository;
import com.qnb.bo.backofficeservice.service.PageConfigService;

@Service
public class PageConfigServiceImpl implements PageConfigService {
	@Autowired
	private PageConfigRepository pageConfigRepository;

	@Autowired
	private UnitRespository unitRepository;
	
	@Autowired
	private ChannelRespository channelRepo;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<List<PageConfigDto>> getList(Map<String, String> request) {
		var response = new GenericResponse<List<PageConfigDto>>();

		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var statusLst = new ArrayList<String>();
			statusLst.add(AppConstant.ACT);
			statusLst.add(AppConstant.IAC);
			var unitId = request.get(AppConstant.UNIT_ID);
			var channelId = request.get(AppConstant.CHANNEL_ID);
			var page = request.get("page");

			var pageConfigList = pageConfigRepository.findByUnitIdAndChannelIdAndPage(unitId,statusLst, channelId,
					page);
			var pageConfigDtoList = pageConfigList.stream().map(this::mapEntityToDto)
					.collect(Collectors.toList());
			response.setData(pageConfigDtoList);
			response.setStatus(resultVo);

		} catch (Exception e) {
			response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC));
		}
		return response;
	}

	private PageConfigDto mapEntityToDto(PageConfig pageConfig) {
		var pageConfigDto = new PageConfigDto();

		pageConfigDto.setConfigKey(pageConfig.getKey());
		pageConfigDto.setUnitId(pageConfig.getUnit().getUnitId());
		pageConfigDto.setChannelId(pageConfig.getChannelId().getChannelId());
		pageConfigDto.setComments(pageConfig.getDescription());
		pageConfigDto.setStatus(pageConfig.getStatus());
		pageConfigDto.setScreenId(pageConfig.getPage());
		pageConfigDto.setConfigValue(pageConfig.getValue());

		return pageConfigDto;
	}

	@Override
	public GenericResponse<List<PageConfig>> updatePageConfigs(List<PageConfigDto> pageConfigDto) {
		var response = new GenericResponse<List<PageConfig>>();
		var date = new Date(System.currentTimeMillis());

		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var updatedConfigs = new ArrayList<PageConfig>();
			var existingPageList = new ArrayList<PageConfig>();
			var statusLst = new ArrayList<String>();
			statusLst.add(AppConstant.ACT);
			statusLst.add(AppConstant.IAC);

			for (PageConfigDto request : pageConfigDto) {
				var unit = unitRepository.findByUnitId(request.getUnitId());
				var channel = channelRepo.findByChannelId(request.getChannelId());
				if (unit == null) {
					response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, "Unit not found for unitId: "));
					return response;
				}

				var existingConfigs = pageConfigRepository.findPageConfig(request.getUnitId(),
						request.getChannelId(),statusLst, request.getScreenId(), request.getConfigKey(),
						request.getConfigValue());
				var action = request.getAction();

				if ("DELETE".equalsIgnoreCase(action)) {
					//for (PageConfig existingConfig : existingConfigs) {
					existingConfigs.forEach(existingConfig->{
						if (existingConfig != null) {
							existingConfig.setStatus(AppConstant.DEL);
							existingConfig.setModifiedBy("BO");
							existingConfig.setModifiedTime(LocalDateTime.now());
							updatedConfigs.add(pageConfigRepository.save(existingConfig));
						}
					});
				} else if ("MODIFY".equalsIgnoreCase(action)) {
				//	for (PageConfig existingConfig : existingConfigs) {
					existingConfigs.forEach(existingConfig->{
						if (existingConfig != null) {
							existingConfig.setDescription(request.getComments());
							existingConfig.setModifiedBy("BO");
							existingConfig.setStatus(request.getStatus());
							existingConfig.setModifiedTime(LocalDateTime.now());
							updatedConfigs.add(pageConfigRepository.save(existingConfig));
						}
					});
				} else if ("ADD".equalsIgnoreCase(action)) {
					if (!existingConfigs.isEmpty()) {

						 resultVo = new ResultUtilVO(AppConstant.ALREADY_EXISTS_CODE, AppConstant.ALREADY_EXISTS_DESC);
		                    continue; 
					} else {

						var newPageConfig = new PageConfig();
						newPageConfig.setUnit(unit);
						newPageConfig.setChannelId(channel);
						newPageConfig.setKey(request.getConfigKey());
						newPageConfig.setValue(request.getConfigValue());
						newPageConfig.setCreatedBy("SYSTEM");
						newPageConfig.setCreatedTime(LocalDateTime.now());
						newPageConfig.setDescription(request.getComments());
						newPageConfig.setModifiedBy(action);
						newPageConfig.setModifiedTime(null);
						newPageConfig.setPage(request.getScreenId());
						newPageConfig.setStatus(AppConstant.ACT);

						updatedConfigs.add(pageConfigRepository.save(newPageConfig));
					}
				}
			}

			if (!updatedConfigs.isEmpty()) {
				response.setStatus(resultVo);
			} 
		} catch (Exception e) {
			 resultVo =new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<Map<String, String>>> getScreenId(Map<String, String> request) {
		var response = new GenericResponse<List<Map<String, String>>>();

		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var unit = unitRepository.findByUnitId(request.get("unitId"));
			if (unit == null) {
				response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE,
						"Unit not found for unitId: " + request.get("unitId")));
				return response;
			}
			var channel = channelRepo.findByChannelId(request.get("channelId"));
			if (channel == null) {
				response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE,
						"Channel not found for channelId: " + request.get("channelId")));
				return response;
			}
			var screenIds = pageConfigRepository.findPageByUnitIdAndChannelId(request.get("unitId"),
					request.get("channelId"));
			 var screenIdList = new ArrayList<Map<String, String>>();
		     var defaultScreenIdMap = new HashMap<String, String>();
		        defaultScreenIdMap.put("screenId", "Add_New_screenId");
		        screenIdList.add(defaultScreenIdMap);
			if(Objects.nonNull(screenIds)) {
			//for (String screenId : screenIds) {
			screenIds.forEach(screenId->{
				var screenIdMap = new HashMap<String, String>();
				screenIdMap.put("screenId", screenId);
				screenIdList.add(screenIdMap);
			});
			
			response.setData(screenIdList);
			}
			response.setStatus(new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC));
		} catch (Exception e) {
			response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC));
		}
		return response;
	}
}
