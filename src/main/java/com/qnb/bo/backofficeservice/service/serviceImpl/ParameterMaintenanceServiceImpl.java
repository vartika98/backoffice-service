package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.categoryCode.CategoryCodeDto;
import com.qnb.bo.backofficeservice.dto.parameter.ManageParamReqDto;
import com.qnb.bo.backofficeservice.dto.parameter.ParameterRes;
import com.qnb.bo.backofficeservice.entity.ParamConfig;
import com.qnb.bo.backofficeservice.mapper.ParamConfigMapper;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.GenericResult;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.ChannelRespository;
import com.qnb.bo.backofficeservice.repository.ParamConfigRepository;
import com.qnb.bo.backofficeservice.repository.UnitRespository;
import com.qnb.bo.backofficeservice.service.ParameterMaintenanceService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ParameterMaintenanceServiceImpl implements ParameterMaintenanceService{
	
	private ResultUtilVO resultVo = new ResultUtilVO();
	
	@Autowired
	private ParamConfigRepository paramConfigRepo;
	
	@Autowired
	private ParamConfigMapper paramConfigMapper;
	
	@Autowired
	private ChannelRespository channelRepo;

	@Autowired
	private UnitRespository unitRespository;
	
	@Override
	public GenericResponse<List<ParameterRes>> parameterList(Map<String, String> reqBody) {
		var response = new GenericResponse<List<ParameterRes>>();
		var resLst = new ArrayList<ParameterRes>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var statusLst = new ArrayList<String>();
			statusLst.add(AppConstant.ACT);
			statusLst.add(AppConstant.IAC);
			
			var paramConfigLst = paramConfigRepo.findByUnit_UnitIdAndChannel_ChannelIdAndStatusInOrderByModifiedTimeDesc(reqBody.get(AppConstant.UNIT_ID),reqBody.get(AppConstant.CHANNEL_ID),statusLst);
			if(Objects.nonNull(paramConfigLst) && !paramConfigLst.isEmpty()) {
				resLst = (ArrayList<ParameterRes>) paramConfigMapper.paramConfigToDto(paramConfigLst);
				response.setData(resLst);
			}
		} catch (Exception e) {
			log.info("Exception in parameterList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResult manageParameter(List<ManageParamReqDto> reqBody) {
		var response = new GenericResult();
		var paramsToadd = new ArrayList<ParamConfig>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var statusLst = new ArrayList<String>();
			statusLst.add(AppConstant.ACT);
			statusLst.add(AppConstant.IAC);
			var ch = channelRepo.findByChannelId(reqBody.get(0).getChannelId());
			var unit = unitRespository.findByUnitId(reqBody.get(0).getUnitId());
			var existingParamLst = paramConfigRepo.findByUnit_UnitIdAndChannel_ChannelIdAndStatusInOrderByModifiedTimeDesc(reqBody.get(0).getUnitId(), reqBody.get(0).getChannelId(),statusLst);
			reqBody.forEach(paramData ->{
				paramData.getParamDetails().stream().forEach(param -> {
					var existingParam = existingParamLst.stream()
							.filter(existParam -> existParam.getKey().equals(param.getKey()) && existParam.getValue().equals(param.getValue()))
							.collect(Collectors.toList());
					if (existingParam.isEmpty()) {
						var params = new ParamConfig();
						params.setChannel(ch);
						params.setUnit(unit);
						params.setKey(param.getKey());
						params.setValue(param.getValue());
						params.setDescription(param.getRemark());
						params.setStatus(param.getStatus());
						params.setCreatedBy("SYSTEM");
						params.setCreatedTime(LocalDateTime.now());
						paramsToadd.add(params);
					} else {
						existingParam.forEach(extparam -> {
							if(param.getAction().equals("ADD")) {
								resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "Already exists.");
							}
							else {
								extparam.setStatus(param.getAction().equals("MODIFY") ? param.getStatus():(param.getAction().equals("DELETE")?"DEL":"ACT"));
								if (param.getAction().equals("MODIFY")) {
									extparam.setValue(param.getValue());
									extparam.setDescription(param.getRemark());
									extparam.setModifiedBy("SYSTEM");
									extparam.setModifiedTime(LocalDateTime.now());
								}
								paramsToadd.add(extparam);
							}
						});
					}
				});
			});
			// to save in ParamConfig table:
			log.info("menus to be added:{}", paramsToadd);
			if (!paramsToadd.isEmpty()) {
				paramConfigRepo.saveAll(paramsToadd);
			}
		} catch (Exception e) {
			log.info("Exception in manageParameter :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setResult(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<CategoryCodeDto>> getDistinctParamConfig(String key) {
		var response = new GenericResponse<List<CategoryCodeDto>>();

		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, AppConstant.NO_DATA_FOUND);
			var paramConfigLst = paramConfigRepo.findDistinctValuesForKey(key.toUpperCase());

			if (!CollectionUtils.isEmpty(paramConfigLst)) {
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
				var categoryList = paramConfigLst.stream()
						.map(paramConfig -> new CategoryCodeDto(paramConfig.getValue(), paramConfig.getDescription()))
						.collect(Collectors.toList());
				response.setData(categoryList);
			}
		} catch (Exception e) {
			log.info("Exception in getDistinctParamConfig: {}", e.getMessage());
			var resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}

		response.setStatus(resultVo);
		return response;
	}

}
