package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.pendinRequest.PendingReq;
import com.qnb.bo.backofficeservice.dto.pendinRequest.PendingReqResponse;
import com.qnb.bo.backofficeservice.dto.pendinRequest.WorkFlowRequest;
import com.qnb.bo.backofficeservice.dto.role.GroupDef;
import com.qnb.bo.backofficeservice.dto.role.RoleDetailsRes;
import com.qnb.bo.backofficeservice.dto.txn.MenuListRes;
import com.qnb.bo.backofficeservice.dto.user.UserDetailsRes;
import com.qnb.bo.backofficeservice.dto.workflow.WorkflowReqResponse;
import com.qnb.bo.backofficeservice.entity.GroupDefEntity;
import com.qnb.bo.backofficeservice.entity.GroupRequestStagingEntity;
import com.qnb.bo.backofficeservice.entity.UserRequestStagingEntity;
import com.qnb.bo.backofficeservice.entity.WorkflowHistoryEntityOCS;
import com.qnb.bo.backofficeservice.entity.workflow.WorkflowHistoryEntity;
import com.qnb.bo.backofficeservice.enums.ProcessFlow;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.GenericResult;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.GroupDefRepository;
import com.qnb.bo.backofficeservice.repository.GroupRequestStagingRepository;
import com.qnb.bo.backofficeservice.repository.UserRepository;
import com.qnb.bo.backofficeservice.repository.UserRequestStagingRepository;
import com.qnb.bo.backofficeservice.repository.WorkflowHistoryEntityRepository;
import com.qnb.bo.backofficeservice.service.PendingRequestService;
import com.qnb.bo.backofficeservice.service.WorkflowService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PendingRequestServiceImpl implements PendingRequestService{
	
	private ResultUtilVO resultVo = new ResultUtilVO();
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private WorkflowService workflowService;
	
	@Autowired
	private UserRequestStagingRepository userRequestStagingRepo;
	
	@Autowired
	private GroupDefRepository groupDefRepository;
	
	@Autowired
	private GroupRequestStagingRepository groupRequestStagingRepo;
	
	@Autowired
	private WorkflowHistoryEntityRepository workflowHistoryEntityRepository;
	
	@Autowired
	private DataSource dataSource;
	
	@Override
	public GenericResponse<List<PendingReqResponse<UserDetailsRes>>> userList(PendingReq reqBody) {
		var response = new GenericResponse<List<PendingReqResponse<UserDetailsRes>>>();
	    var resLst = new ArrayList<PendingReqResponse<UserDetailsRes>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			
			var workflowRes = workflowService.getWorkflowDetails(reqBody.toWorkflowStatus(),ProcessFlow.getUserProcessId());
			if(!workflowRes.isEmpty()) {
				var requestIds = workflowRes.stream().map(WorkflowReqResponse::getRequestId).collect(Collectors.toList());
				
				var reqUserLst = userRequestStagingRepo.findByRequestIdInOrderByCreatedTimeAsc(requestIds);
				
				if(!reqUserLst.isEmpty()) {
					var userReqDetails = reqUserLst.stream().map(req->buildRes(req)).collect(Collectors.toList());
					
					
					workflowRes.stream().filter(workflow -> userReqDetails.stream().map(UserDetailsRes::getRequestId).anyMatch(requestId ->workflow.getRequestId().equals(requestId)))
					.forEach(workflow -> {
						var resMap = new PendingReqResponse<UserDetailsRes>();
						resMap.setWorkflowId(workflow.getWorkflowId());
						resMap.setRequestId(workflow.getRequestId());
						resMap.setProcessId(workflow.getProcessId());
						resMap.setTaskId(workflow.getTaskId());
						resMap.setAssignTo(workflow.getAssignTo());
						resMap.setInitiatedGroup(workflow.getInitiatedGroup());
						resMap.setAvailableActions(workflow.getAvailableActions());
						resMap.setStatus(workflow.getStatus());
						resMap.setProcessStatus(workflow.getProcessStatus());
						resMap.setHistoryCount(workflow.getHistoryCount());
						resMap.setCommentHistory(workflow.getCommentHistory());					
						var userReqLst = userReqDetails.stream().filter(userReq -> userReq.getRequestId().equals(workflow.getRequestId())).collect(Collectors.toList()).get(0);
						resMap.setPendingDetails(userReqLst);	
					
						resLst.add(resMap);
					});
				}
			}
			response.setData(resLst);
		} catch (Exception e) {
			log.info("Exception in userList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
	
	
	public UserDetailsRes buildRes(UserRequestStagingEntity userReq) {
		var response = new UserDetailsRes();
		response.setUnitId(Stream.of(userReq.getUserUnits().split(",", -1)).collect(Collectors.toList()));	
		response.setAction(userReq.getAction());	
		response.setUserId(userReq.getUserId());	
		response.setFirstName(userReq.getFirstName());
		response.setRequestId(userReq.getRequestId());		
		final var groups = userReq.getUserGroups().split(",");
		var groupDefEntities = groupDefRepository.findAllById(List.of(groups));
		response.setGroups(groupDefEntities.stream().map(this::formGroupDet).collect(Collectors.toList()));	
		return response;
	}
	
	
	private GroupDef formGroupDet(GroupDefEntity groupDefEntity) {
		return GroupDef.builder().groupId(groupDefEntity.getGroupId()).groupCode(groupDefEntity.getGroupCode())
				.groupDescription(groupDefEntity.getGroupDescription()).build();
	}


	@Override
	public GenericResponse<List<PendingReqResponse<RoleDetailsRes>>> roleList(PendingReq reqBody) {
		var response = new GenericResponse<List<PendingReqResponse<RoleDetailsRes>>>();
		var resLst = new ArrayList<PendingReqResponse<RoleDetailsRes>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			
			var workflowRes = workflowService.getWorkflowDetails(reqBody.toWorkflowStatus(),ProcessFlow.getRoleProcessId());
			if(!workflowRes.isEmpty()) {
				var requestIds = workflowRes.stream().map(WorkflowReqResponse::getRequestId).collect(Collectors.toList());
				var reqRoleLst = groupRequestStagingRepo.findByRequestIdInOrderByCreatedTimeAsc(requestIds);
				if(!reqRoleLst.isEmpty()) {
					workflowRes.stream().filter(workflow -> reqRoleLst.stream().map(GroupRequestStagingEntity::getRequestId).anyMatch(requestId ->workflow.getRequestId().equals(requestId)))
					.forEach(workflow -> {
						var resMap = new PendingReqResponse<RoleDetailsRes>();
						resMap.setWorkflowId(workflow.getWorkflowId());
						resMap.setRequestId(workflow.getRequestId());
						resMap.setProcessId(workflow.getProcessId());
						resMap.setTaskId(workflow.getTaskId());
						resMap.setAssignTo(workflow.getAssignTo());
						resMap.setInitiatedGroup(workflow.getInitiatedGroup());
						resMap.setAvailableActions(workflow.getAvailableActions());
						resMap.setStatus(workflow.getStatus());
						resMap.setProcessStatus(workflow.getProcessStatus());
						resMap.setHistoryCount(workflow.getHistoryCount());
						resMap.setCommentHistory(workflow.getCommentHistory());					
						var roleReqLst = reqRoleLst.stream().filter(roleReq -> roleReq.getRequestId().equals(workflow.getRequestId()))
								.map(GroupRequestStagingEntity::toRoleDetailsRes)
								.collect(Collectors.toList()).get(0);
						resMap.setPendingDetails(roleReqLst);
						resLst.add(resMap);
					});
				}
				response.setData(resLst);			}
		} catch (Exception e) {
			log.info("Exception in userList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
		
	public GenericResponse<GenericResult> pendingRequest(WorkFlowRequest workFlowRequest){
		var response = new GenericResponse<GenericResult>();
	
		//workflowHistory on the basis of status
		
		//create procedure for all the list of workflowHistory
		try {
		    WorkflowHistoryEntityOCS wkflwHstList=workflowHistoryEntityRepository.findByRefNo(new Integer(0).valueOf(workFlowRequest.getRefNo()));

			var jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("proc_workflow_processing");				

				var in = new MapSqlParameterSource()
						.addValue("iref_no", workFlowRequest.getRefNo())
						.addValue("imaker_id", wkflwHstList.getMakerId())
						.addValue("irule_id", wkflwHstList.getRuleId())
						.addValue("iaction", workFlowRequest.getAction())
						.addValue("IAPPROVER_ID", workFlowRequest.getApproverId())
						.addValue("out_errorcode", "out")
						.addValue("out_errormsg", "out");
				
            	log.info("proc_workflow_processing procedure inputs are :::{}", String.valueOf(in));
				var out = jdbcCall.execute(in);
				log.info("proc_workflow_processing procedure call response: {}", String.valueOf(out));
		
				resultVo = new ResultUtilVO(out.get("OUT_ERRORCODE").toString(), out.get("OUT_ERRORMSG").toString());	
				response.setData(new GenericResult(new ResultUtilVO("RefNo",workFlowRequest.getRefNo())));
				response.setStatus(resultVo);
		} catch (Exception e) {
			log.info("Exception in moduleList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
			response.setStatus(resultVo);
		}

		return response;
		}

}
