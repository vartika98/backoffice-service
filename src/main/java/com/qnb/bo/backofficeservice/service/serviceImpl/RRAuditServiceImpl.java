package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.mbAudit.ReqDto;
import com.qnb.bo.backofficeservice.dto.mbAudit.ResponseDto;
import com.qnb.bo.backofficeservice.dto.mbAudit.ResponseDtoMB;
import com.qnb.bo.backofficeservice.entity.MBRRMessages;
import com.qnb.bo.backofficeservice.entity.RRmessage;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.MbRRrepository;
import com.qnb.bo.backofficeservice.repository.RRmessageRepository;
import com.qnb.bo.backofficeservice.service.RRAuditService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RRAuditServiceImpl implements RRAuditService {

	@Autowired
	private MbRRrepository mbRRrepository;

	@Autowired
	private RRmessageRepository rrMessageRepo;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<List<ResponseDtoMB>> getSummary(ReqDto req) {
		var response = new GenericResponse<List<ResponseDtoMB>>();
		var resultList = new ArrayList<ResponseDtoMB>();
		var list = new ArrayList<MBRRMessages>();
		try {
			if(Objects.nonNull(req.getGId()) && !req.getGId().isEmpty()) {
				if (req.getCategoryCode().equals("ALL")) {
					list = (ArrayList<MBRRMessages>) mbRRrepository.findByRequestDateBetweenAndGlobalIdOrderByDateCreatedDesc(req.getStartDate(),
							req.getEndDate(),req.getGId());
				} else {
					list = (ArrayList<MBRRMessages>) mbRRrepository.findByRequestDateBetweenAndGlobalIdANdCategoryCodeOrderByDateCreatedDesc(
							 req.getStartDate(),req.getEndDate(),req.getGId(),req.getCategoryCode());
				}
			}
			else {
				if (req.getCategoryCode().equals("ALL")) {
					list = (ArrayList<MBRRMessages>) mbRRrepository.findByRequestDateBetweenOrderByDateCreatedDesc(req.getStartDate(),
							req.getEndDate());
				} else {
					list = (ArrayList<MBRRMessages>) mbRRrepository.findByCategoryCodeAndRequestDateBetweenOrderByDateCreatedDesc(
							req.getCategoryCode(), req.getStartDate(), req.getEndDate());
				}
			}
			list.forEach(l -> {
				var dto = ResponseDtoMB.builder().txnId(l.getTxnId()).unitId(l.getUnitId())
						.userNo(l.getUserNo()).categoryCode(l.getCategoryCode()).globalId(l.getGlobalId())
						.serviceId(l.getServiceId()).header(l.getHeader()).request(l.getRequest())
						.requestDate(l.getRequestDate()).response(l.getResponse()).responseCode(l.getResponseCode())
						.responseDate(l.getResponseDate()).traceKey(l.getTraceKey()).rrMessage(l.getRrMessage())
						.rrDate(l.getRrDate()).build();
				resultList.add(dto);
			});
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			response.setData(resultList);
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<ResponseDto>> getSummaryByChannel(ReqDto req) {
		var response = new GenericResponse<List<ResponseDto>>();
		var resultList = new ArrayList<ResponseDto>();
		var list = new ArrayList<RRmessage>();
		try {
			if(Objects.nonNull(req.getGuid()) && !req.getGuid().isEmpty()) {
				list = (ArrayList<RRmessage>)rrMessageRepo
						.findByUuidOrderByDateCreatedDesc(req.getGuid());
			}else if(Objects.nonNull(req.getGId()) && !req.getGId().isEmpty()) {
				if (req.getCategoryCode().equals("ALL")) {
					list = (ArrayList<RRmessage>) rrMessageRepo.getRRByGId(req.getChannelId(),
							req.getGId(), req.getStartDate(), req.getEndDate());
				} else {
					list = (ArrayList<RRmessage>) rrMessageRepo.getRRByGIdAndCategoryCode(req.getChannelId(), 
							req.getCategoryCode(),req.getGId(), req.getStartDate(), req.getEndDate());
				}
			}else {
				if (req.getCategoryCode().equals("ALL")) {
					list = (ArrayList<RRmessage>) rrMessageRepo.findByChannelIdAndRequestDateBetweenOrderByDateCreatedDesc(
							req.getChannelId(), req.getStartDate(), req.getEndDate());
				} else {
					list = (ArrayList<RRmessage>) rrMessageRepo.findByChannelIdAndCategoryCodeAndRequestDateBetweenOrderByDateCreatedDesc(
							req.getChannelId(), req.getCategoryCode(), req.getStartDate(), req.getEndDate());
				}
			}

			list.forEach(l -> {
				var dto = ResponseDto.builder().txnId(l.getTxnId()).unitId(l.getUnitId()).userNo(l.getUserNo())
						.categoryCode(l.getCategoryCode()).globalId(l.getGlobalId()).channelId(l.getChannelId())
						.uuid(l.getUuid()).categoryType(l.getCategoryType()).clientInfo(l.getClientInfo())
						.method(l.getMethod()).header(l.getHeader()).request(l.getRequest())
						.requestDate(l.getRequestDate()).response(l.getResponse()).responseCode(l.getResponseCode())
						.responseDate(l.getResponseDate()).traceKey(l.getTraceKey()).url(l.getUrl())
						.rrMessage(l.getRrMessage()).rrDate(l.getRrDate()).userNo2(l.getUserNo2())
						.txnCategoryId(l.getTxnCategoryId()).build();

				resultList.add(dto);
			});
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			response.setData(resultList);
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
