package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.role.RoleDto;
import com.qnb.bo.backofficeservice.dto.role.RoleHierarchiRequest;
import com.qnb.bo.backofficeservice.dto.role.UserRoleDto;
import com.qnb.bo.backofficeservice.entity.BOGroupEntity;
import com.qnb.bo.backofficeservice.entity.UserRole;
import com.qnb.bo.backofficeservice.enums.RoleLevelEnum;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.BOGroupRepository;
import com.qnb.bo.backofficeservice.repository.RoleRepository;
import com.qnb.bo.backofficeservice.service.RoleService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RoleServiceImpl implements RoleService {

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private BOGroupRepository boGroupRepository;

	@Override
	public GenericResponse<List<UserRole>> crateRole(RoleHierarchiRequest request) {
		var response = new GenericResponse<List<UserRole>>();
		ResultUtilVO resultVo;
		Date date = new Date(System.currentTimeMillis());

		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var createdRoles = new ArrayList<UserRole>();
			var userRoleList = roleRepository.findAllByOrderByRoleLevelAsc();
			log.info("userList" + userRoleList);
			char incrementedRoleLevel = 'A';
			if (!userRoleList.isEmpty()) {
				var lastRow = userRoleList.get(userRoleList.size() - 1);
				incrementedRoleLevel = (char) (lastRow.getRoleLevel().charAt(0) + 1);
			}
			for (int i = 0; i < request.getHierarchy().size(); i++) {
				RoleDto roleDto = request.getHierarchy().get(i);

				if ("ADD".equalsIgnoreCase(roleDto.getAction())) {
					boolean roleExists = userRoleList.stream()
							.anyMatch(userRole -> userRole.getGroupName().equals(request.getGroupId())
									&& userRole.getDescription().equals(roleDto.getDescription())
									&& !userRole.getStatus().equals("DEL"));
					if (!roleExists) {
						UserRole newRole = new UserRole();
						newRole.setGroupName(request.getGroupId());
						newRole.setBoAuthDate(date);
						newRole.setBoAuthId("123");
						newRole.setBoAuthName("AuthUser");
						newRole.setBoMakerDate(date);
						newRole.setBoMakerId("12344");
						newRole.setBoMakerName("user");
						newRole.setStatus(roleDto.getStatus());
						newRole.setUserRoleHierichy(request.getHierarchyLevel());
						newRole.setDescription(roleDto.getDescription());
						newRole.setRoleLevel(String.valueOf((char) (incrementedRoleLevel + i)));
						UserRole savedRole = roleRepository.save(newRole);
						createdRoles.add(savedRole);
					} else {
						log.info("A role with this name already exists: " + roleDto.getDescription());
						resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE,
								"A role with the name " + roleDto.getDescription() + " already exists.");
					}
				} else if ("MODIFY".equalsIgnoreCase(roleDto.getAction())) {
					userRoleList.forEach(existingRole -> {
						if (existingRole.getGroupName().equals(request.getGroupId())
								&& existingRole.getRoleLevel().equals(roleDto.getRoleLevel())) {
							existingRole.setDescription(roleDto.getDescription());
							existingRole.setUserRoleHierichy(request.getHierarchyLevel());
							existingRole.setStatus(roleDto.getStatus());
							UserRole updatedRole = roleRepository.save(existingRole);
							createdRoles.add(updatedRole);
						}
					});

				} else if ("DELETE".equalsIgnoreCase(roleDto.getAction())) {
					userRoleList.forEach(existingRole -> {
						if (existingRole.getGroupName().equals(request.getGroupId())
								&& existingRole.getDescription().equals(roleDto.getDescription())) {
							existingRole.setStatus(AppConstant.DEL);
							UserRole updatedRole = roleRepository.save(existingRole);
							createdRoles.add(updatedRole);
						}
					});
				}
			}

			response.setData(createdRoles);
			response.setStatus(resultVo);
		} catch (Exception e) {
			log.error("Error in create or modify Role: {}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
			response.setStatus(resultVo);
		}

		return response;
	}

	@Override
	public GenericResponse<List<UserRoleDto>> roleList(Map<String, String> requestGroup) {
		var response = new GenericResponse<List<UserRoleDto>>();
		var userRoleLst = new ArrayList<UserRoleDto>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			List<BOGroupEntity> groupLst = boGroupRepository.findAll();
			List<UserRole> roleList = roleRepository.findAllByOrderByDateCreatedDesc().stream()
					.filter(role -> Arrays.asList(AppConstant.ACT, AppConstant.IAC).contains(role.getStatus()))
					.collect(Collectors.toList());
			userRoleLst = (ArrayList<UserRoleDto>) roleList.stream()
					.map(role -> new UserRoleDto(role.getGroupName(),
							groupLst.stream().filter(grp -> grp.getGroupCode().equals(role.getGroupName()))
									.collect(Collectors.toList()).get(0).getGroupName(),
							role.getDescription(), role.getUserRoleHierichy(), role.getRoleLevel(), role.getStatus()))
					.collect(Collectors.toList());
			response.setData(userRoleLst);
		} catch (Exception e) {
			log.error("Error occurred while fetching role list: {}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
			response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC));
		}
		response.setStatus(resultVo);
		return response;
	}

}
