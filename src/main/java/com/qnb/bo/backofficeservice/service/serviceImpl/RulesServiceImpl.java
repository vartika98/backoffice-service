package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.rules.RulesDto;
import com.qnb.bo.backofficeservice.dto.rules.RulesSummaryResDto;
import com.qnb.bo.backofficeservice.entity.BOGroupEntity;
import com.qnb.bo.backofficeservice.entity.OcsFunctionMaster;
import com.qnb.bo.backofficeservice.entity.OcsProductMaster;
import com.qnb.bo.backofficeservice.entity.OcsSubProductMaster;
import com.qnb.bo.backofficeservice.entity.RulesAccMapEntity;
import com.qnb.bo.backofficeservice.entity.RulesDefinition;
import com.qnb.bo.backofficeservice.entity.RulesMaster;
import com.qnb.bo.backofficeservice.entity.RulesParser;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.BOGroupRepository;
import com.qnb.bo.backofficeservice.repository.OcsFunctionMasterRepository;
import com.qnb.bo.backofficeservice.repository.OcsProductMasterRepository;
import com.qnb.bo.backofficeservice.repository.OcsSubProductMasterRepository;
import com.qnb.bo.backofficeservice.repository.RulesAccMapRepository;
import com.qnb.bo.backofficeservice.repository.RulesDefinitionRepository;
import com.qnb.bo.backofficeservice.repository.RulesMasterRepository;
import com.qnb.bo.backofficeservice.repository.RulesParserRepository;
import com.qnb.bo.backofficeservice.service.RulesService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RulesServiceImpl implements RulesService {

	@Autowired
	private RulesMasterRepository rulesmasterRepo;

	@Autowired
	private RulesDefinitionRepository rulesDefinition;

	@Autowired
	private RulesParserRepository rulesParser;

	@Autowired
	private RulesAccMapRepository rulesAccMapRepository;
	
	@Autowired
	private OcsProductMasterRepository productMasterRepo;
	
	@Autowired
	private OcsSubProductMasterRepository subProdRepo;
	
	@Autowired
	private OcsFunctionMasterRepository functionRepo;

	private ResultUtilVO resultVo = new ResultUtilVO();
	
	@Autowired
	private BOGroupRepository boGroupRepository;

	@Override
	public GenericResponse<String> manageRule(List<RulesDto> dto) {
		var response = new GenericResponse<String>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var ruleMastertoAdd = new ArrayList<RulesMaster>();
			var ruleDeftoAdd = new ArrayList<RulesDefinition>();
			var ruleAccMaptoAdd = new ArrayList<RulesAccMapEntity>();
			var ruleParsertoAdd = new ArrayList<RulesParser>();
			var status = new ArrayList<String>();
			status.add("ACT");
			status.add("IAC");
			dto.forEach(obj -> {
				var rule = rulesmasterRepo.findByRuleNameAndStatusIn(obj.getRuleName(), status);
				if (obj.getAction().equals("ADD")) {
					if (rule != null) {// rule already exists
						resultVo = new ResultUtilVO(AppConstant.RULE_EXISTS_CODE, AppConstant.RULE_EXISTS + " (Cannot Add Rule having name: "+ obj.getRuleName()+")");
						log.info("Rule already exists Add action cannot be performed");
					} else {// rule does not exists add the rule
						var rulesMaster = new RulesMaster();
						rulesMaster.setGroupId(obj.getGroupId());
						rulesMaster.setRuleName(obj.getRuleName());
						rulesMaster.setBoMakerId("1234");// todo
						rulesMaster.setBoMakerName("BoMaker");// todo
						rulesMaster.setBoMakerDate(new Date(System.currentTimeMillis()));
						rulesMaster.setBoAuthDate(new Date(System.currentTimeMillis()));
						rulesMaster.setBoAuthId("123");// todo
						rulesMaster.setBoAuthName("AuthUser");// todo
						rulesMaster.setRuleDescription(obj.getRuleDesc());
						rulesMaster.setStatus(AppConstant.ACT);
						var savedRulesMaster = rulesmasterRepo.save(rulesMaster); // saving to rule_master

						if (savedRulesMaster != null) {
							var parts = Arrays.stream(obj.getCriteriaValue().split("\\bOR\\b"))
									.map(String::trim).collect(Collectors.toList());
							var listToSave = new ArrayList<RulesDefinition>();
							parts.forEach(ruledef -> {
								RulesDefinition rulesDefinition = new RulesDefinition();
								rulesDefinition.setRuleId(savedRulesMaster.getRuleId());
								rulesDefinition.setRuleDef(ruledef);
								listToSave.add(rulesDefinition);
							});
							var savedRulesDef = rulesDefinition.saveAll(listToSave); // saving to rule_definition

							var rulesAccMaplist = new ArrayList<RulesAccMapEntity>();
							obj.getFunctionIdList().stream().forEach(functionId -> {
								var rulesAccMap = new RulesAccMapEntity();
								rulesAccMap.setRuleId(savedRulesMaster.getRuleId());
								rulesAccMap.setFunctionId(functionId);
								rulesAccMap.setProductCode(obj.getProduct());
								rulesAccMap.setSubProductCode(obj.getSubProduct());
								rulesAccMap.setUniqueIdentification(UUID.randomUUID().toString());
								rulesAccMaplist.add(rulesAccMap);
							});
							rulesAccMapRepository.saveAll(rulesAccMaplist); // saving to rule_acc_mapping

							if (savedRulesDef != null) {
								var parserList = new ArrayList<RulesParser>();
								savedRulesDef.stream().forEach(def -> {
									var parsedRule = ruleParser(def.getRuleDef());
									parsedRule.entrySet().stream().forEach(entry -> {
										var rulesParser = new RulesParser();
										rulesParser.setRuleParsedId(def.getRuleParsedId());
										rulesParser.setRuleLevel(entry.getKey());
										rulesParser.setRuleCount(entry.getValue());
										parserList.add(rulesParser);
									});
								});
								rulesParser.saveAll(parserList); // saving to rules_parser
							}
						}
					}
				} else if (obj.getAction().equals("MODIFY")) {

					if (rule != null) { // rule exists so modify
						rule.setGroupId(obj.getGroupId() != null ? obj.getGroupId() : rule.getGroupId());
						rule.setRuleName(obj.getRuleName() != null ? obj.getRuleName() : rule.getRuleName());
						rule.setBoMakerId("1234");// todo
						rule.setBoMakerName("BoMaker");// todo
						rule.setBoAuthId("123");// todo
						rule.setBoAuthName("AuthUser");// todo
						rule.setRuleDescription(
								obj.getRuleDesc() != null ? obj.getRuleDesc() : rule.getRuleDescription());
						rule.setStatus(obj.getStatus() != null ? obj.getStatus() : rule.getStatus());
						ruleMastertoAdd.add(rule);

						if (obj.getCriteriaValue() != null) {
							// need to delete the existing mapping from the definition and parser table
							
							var parsedIdList = rulesDefinition.findParsedId(rule.getRuleId());
							// deleting mapping in parser table
							rulesParser.deleteByParsedIdList(parsedIdList);
							// deleting mapping in definition table
							rulesDefinition.deleteByRuleId(rule.getRuleId());

							var parts = Arrays.stream(obj.getCriteriaValue().split("\\bOR\\b"))
									.map(String::trim).collect(Collectors.toList());
							parts.forEach(ruledef -> {
								var rulesDefinition = new RulesDefinition();
								rulesDefinition.setRuleId(rule.getRuleId());
								rulesDefinition.setRuleDef(ruledef);
								ruleDeftoAdd.add(rulesDefinition);
							});
							rulesDefinition.saveAll(ruleDeftoAdd);

							ruleDeftoAdd.stream().forEach(def -> {
								var parsedRule = ruleParser(def.getRuleDef());
								parsedRule.entrySet().stream().forEach(entry -> {
									var rulesParser = new RulesParser();
									rulesParser.setRuleParsedId(def.getRuleParsedId());
									rulesParser.setRuleLevel(entry.getKey());
									rulesParser.setRuleCount(entry.getValue());
									ruleParsertoAdd.add(rulesParser);
								});
							});
							rulesParser.saveAll(ruleParsertoAdd);
						}

						if (obj.getFunctionIdList() != null && obj.getProduct() != null && !obj.getProduct().isEmpty()
								&& obj.getSubProduct() != null && !obj.getSubProduct().isEmpty()) {
							var presentMapList = rulesAccMapRepository
									.findByRuleId(rule.getRuleId());
							// deleting mapping in RulesAccMapping table
							rulesAccMapRepository.deleteByRuleId(rule.getRuleId());
							var functionalIds = presentMapList.stream().map(fId -> {
								return fId.getFunctionId();
							}).collect(Collectors.toList());

							presentMapList.forEach(mapping -> {
								if (obj.getFunctionIdList().contains(mapping.getFunctionId())) {
									var detachedMapping = new RulesAccMapEntity();
									BeanUtils.copyProperties(mapping, detachedMapping);
									// Clear the primary key to allow it to be autogenerated upon save
									detachedMapping.setTxnId(null);
									// Add the detached mapping entity to the list to be saved
									ruleAccMaptoAdd.add(detachedMapping);
								}
							});
							obj.getFunctionIdList().forEach(id -> {
								if (!functionalIds.contains(id)) {
									var rulesAccMap = new RulesAccMapEntity();
									rulesAccMap.setRuleId(rule.getRuleId());
									rulesAccMap.setFunctionId(id);
									rulesAccMap.setProductCode(obj.getProduct());
									rulesAccMap.setSubProductCode(obj.getSubProduct());
									rulesAccMap.setUniqueIdentification(UUID.randomUUID().toString());
									ruleAccMaptoAdd.add(rulesAccMap);
								}
							});
							rulesAccMapRepository.saveAll(ruleAccMaptoAdd);
						}
					} else { // rule does not exists cannot modify
						resultVo = new ResultUtilVO(AppConstant.RULE_DOES_NOT_EXISTS_CODE,
								AppConstant.RULE_DOES_NOT_EXISTS  + " (Cannot Modify Rule having name: "+ obj.getRuleName()+")");
						log.error("Rule does not exists Modify action cannot be performed");
					}
				} else { // delete
					if (rule != null) { // rule exists so delete
						rule.setStatus(AppConstant.DEL);
						ruleMastertoAdd.add(rule);
					} else { // rule does not exists cannot delete
						resultVo = new ResultUtilVO(AppConstant.RULE_DOES_NOT_EXISTS_CODE,
								AppConstant.RULE_DOES_NOT_EXISTS   + " (Cannot Delete Rule having name: "+ obj.getRuleName()+")");
						log.error("Rule does not exists Delete action cannot be performed");
					}
				}
			});
			rulesmasterRepo.saveAll(ruleMastertoAdd);
		} catch (Exception e) {
			log.error("Exception {}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
			response.setStatus(resultVo);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<RulesSummaryResDto>> getSummary() {
		var response = new GenericResponse<List<RulesSummaryResDto>>();
		try {
			var groupCodeList = (List<BOGroupEntity>) boGroupRepository.findAll();
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var productList = productMasterRepo.findAll();
			var subProdList = subProdRepo.findAll();
			var functionList = functionRepo.findAll();
			var status = new ArrayList<String>();
			status.add("ACT");
			status.add("IAC");
			var rulesmasterList = rulesmasterRepo.findByStatusIn(status);
			var resultList = new ArrayList<RulesSummaryResDto>();
			rulesmasterList.forEach(ruleMaster -> {
				var grpDet = groupCodeList.stream().filter(grp->grp.getGroupCode().equals(ruleMaster.getGroupId())).collect(Collectors.toList()).get(0);
				var summaryDto = new RulesSummaryResDto();
				summaryDto.setRuleId(ruleMaster.getRuleId());
				summaryDto.setGroupId(ruleMaster.getGroupId());
				summaryDto.setGroupIdDesc(grpDet.getGroupName());
				summaryDto.setCriteriaValue(createCriteria(ruleMaster.getRulesDefinitionList()));
				summaryDto.setRuleName(ruleMaster.getRuleName());
				summaryDto.setRuleDesc(ruleMaster.getRuleDescription());
				var productDesc = "";
				var subProductDesc = "";
				var functionDesc = "";
				for(OcsProductMaster pdt : productList) {
					if (!ruleMaster.getRulesAccMapMasterList().isEmpty()) {
						if (pdt.getProductCode()
								.equals(ruleMaster.getRulesAccMapMasterList().get(0).getProductCode())) {
							productDesc = pdt.getProductDescription();
						}
					}
				}
				
				for(OcsSubProductMaster spdt : subProdList) {
					if (!ruleMaster.getRulesAccMapMasterList().isEmpty()) {
						if (spdt.getSubProductCode()
								.equals(ruleMaster.getRulesAccMapMasterList().get(0).getSubProductCode())) {
							subProductDesc = spdt.getSubProductDesc();
						}
					}
				}
				summaryDto.setProduct(productDesc);
				summaryDto.setSubProduct(subProductDesc);
				var functionIdList = new ArrayList<String>();
				ruleMaster.getRulesAccMapMasterList().stream().forEach(mapping -> {
					functionList.forEach(fm->{
						if(fm.getFunctionCode().equals(mapping.getFunctionId())
								&& fm.getProductCode().equals(mapping.getProductCode())
								&& fm.getSubProductCode().equals(mapping.getSubProductCode())) {
							functionIdList.add(fm.getFunctionDesc());
						}
					});
				});
				summaryDto.setFunctionIdList(functionIdList);
				summaryDto.setBoMakerId(ruleMaster.getBoMakerId());
				summaryDto.setBoMakerName(ruleMaster.getBoMakerName());
				summaryDto.setBoAuthId(ruleMaster.getBoAuthId());
				summaryDto.setBoAuthName(ruleMaster.getBoAuthName());
				resultList.add(summaryDto);
			});
			response.setData(resultList);
		} catch (Exception e) {
			log.error("Exception {}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
			response.setStatus(resultVo);
		}
		response.setStatus(resultVo);
		return response;
	}

	private Map<String, Integer> ruleParser(String rule) {
		var map = new HashMap<String, Integer>();
		var parts = rule.replaceAll("[()]", "").split("\\+");
		for (String part : parts) {
			String[] pairs = part.split("(?<=\\d)(?=\\D)");
			if(pairs.length >= 2) {
				var value = Integer.parseInt(pairs[0]);
				var character = pairs[1];
				map.put(character, value);
			} else {
				var value = 1;
				var character = pairs[0];
				map.put(character, value);
			}
		}
		return map;
	}

	private String createCriteria(List<RulesDefinition> rulesDefinitionList) {
		var rule = new StringBuilder();
		rulesDefinitionList.forEach(def->{
			rule.append(def.getRuleDef());
			rule.append(" OR ");
		});
		var resString = rule.toString();
		return rule.toString().substring(0, resString.length() - 4);
	}

}
