package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.txn.MenuListRes;
import com.qnb.bo.backofficeservice.dto.txn.MenuResponseDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.service.ScreenService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ScreenServiceImpl implements ScreenService {

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private DataSource dataSource;

	@SuppressWarnings("unchecked")
	@Override
	public GenericResponse<MenuListRes> screenList(Map<String, String> reqBody) {
		var response = new GenericResponse<MenuListRes>();
		var screenList = new ArrayList<Map<String, String>>();
		var resData = new MenuListRes();
		try {
			var jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("ocs_t_screen_list");
			try {
				var in = new MapSqlParameterSource().addValue("P_UNIT_ID", reqBody.get(AppConstant.UNIT_ID))
						.addValue("P_CHANNEL_ID", reqBody.get(AppConstant.CHANNEL_ID)).addValue("P_MENU_ID", reqBody.get("menuId"))
						.addValue("p_screen_list", "out").addValue("out_errorcode", "out")
						.addValue("out_errormsg", "out");
				log.info("ocs_t_menu_list procedure inputs are :::{}", String.valueOf(in));
				var out = jdbcCall.execute(in);
				log.info("ocs_t_screen_list procedure call response: {}", String.valueOf(out));
				if (Objects.nonNull(out) && Objects.nonNull(out.get("P_SCREEN_LIST"))) {
					screenList = (ArrayList<Map<String, String>>) out.get("P_SCREEN_LIST");
				}
			} catch (Exception e) {
				log.info("Exception in ocs_t_screen_list procedure :{}", e);
			}
			if (Objects.nonNull(screenList) && !screenList.isEmpty()) {
				var screenRes = new ArrayList<MenuResponseDto>();
				//for (Map<String, String> screen : screenList) {
				screenList.forEach(screen->{
					var screenDto = new MenuResponseDto();
					screenDto.setDescription(screen.get("DESCRIPTION"));
					screenDto.setMenuId(screen.get("MENUID"));
					screenDto.setScreenId(screen.get("SCREENID"));
					screenDto.setStatus(screen.get("STATUS"));
					screenRes.add(screenDto);
				});
				resData.setMenus(screenRes);
				response.setData(resData);
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			}
		} catch (Exception e) {
			log.info("Exception in moduleList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
