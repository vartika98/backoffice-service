package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.sessionManagement.ActiveUsersResponse;
import com.qnb.bo.backofficeservice.dto.sessionManagement.SessionReqDto;
import com.qnb.bo.backofficeservice.dto.sessionManagement.SpringSessionAttributesDto;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.RRmessageRepository;
import com.qnb.bo.backofficeservice.repository.SpringSessionRepository;
import com.qnb.bo.backofficeservice.service.SessionManagementService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SessionManagementServiceImpl implements SessionManagementService {

	@Autowired
	private SpringSessionRepository springSessionRepository;

	@Autowired
	private RRmessageRepository rrMessageRepository;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<List<ActiveUsersResponse>> getActiveSessions() {
		var response = new GenericResponse<List<ActiveUsersResponse>>();
		try {
			var activeList = springSessionRepository.findActiveSessions(System.currentTimeMillis());
			var result = new ArrayList<ActiveUsersResponse>();
			activeList.forEach(m -> {

				var attributesList = new ArrayList<SpringSessionAttributesDto>();
				m.getAttributes().forEach(attribute -> {

					var dto = SpringSessionAttributesDto.builder()
							.sessionPrimaryId(attribute.getId().getSessionPrimaryId())
							.attributeName(attribute.getId().getAttributeName())
							.attributeBytes(BlobToString(attribute.getAttributeBytes())).build();

					attributesList.add(dto);
				});

				var obj = ActiveUsersResponse.builder().sessionId(m.getSessionId())
						.creationTime(m.getCreationTime()).lastAccessTime(m.getLastAccessTime())
						.maxInactiveInterval(m.getMaxInactiveInterval()).maxInactiveInterval(m.getMaxInactiveInterval())
						.expiryTime(m.getExpiryTime()).principalName(m.getPrincipalName()).attributes(attributesList)
						.build();

				result.add(obj);
			});

			response.setData(result);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<Map<String, Integer>> getLoginCount(SessionReqDto req) {
		var response = new GenericResponse<Map<String, Integer>>();
		var result = new HashMap<String, Integer>();
		try {
			var count = rrMessageRepository
					.findCountByChannelIdAndCategoryCodeAndResponseCodeAndRequestDateBetweenOrderByDateCreatedDesc(
							req.getChannelId(), AppConstant.LOGIN_CATEGORY_CODE, req.getStartDate(), req.getEndDate(),
							AppConstant.LOGIN_SUCCESS_CODE);
			result.put("LoginCount", count);
			response.setData(result);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	public String BlobToString(Blob attributeBytes) {
		var bytes = new byte[]{};
		var attributeString = "";
		try {
			bytes = attributeBytes.getBytes(1, (int) attributeBytes.length());
			attributeString = new String(bytes, StandardCharsets.UTF_8);
		} catch (SQLException e) {
			log.info("Exception details:{}", e);
		}

		return attributeString;
	}

}
