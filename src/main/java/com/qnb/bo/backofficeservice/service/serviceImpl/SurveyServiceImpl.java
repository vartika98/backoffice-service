package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.SurveyRequestDTO;
import com.qnb.bo.backofficeservice.dto.SurveySearchRequest;
import com.qnb.bo.backofficeservice.entity.ParamConfig;
import com.qnb.bo.backofficeservice.entity.SurveyMessage;
import com.qnb.bo.backofficeservice.mapper.SurveyMessageMapper;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.ParamConfigRepository;
import com.qnb.bo.backofficeservice.repository.SurveyMessageRepository;
import com.qnb.bo.backofficeservice.service.SurveyService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SurveyServiceImpl implements SurveyService {

	@Autowired
	private SurveyMessageRepository surveyMessageRepository;

	@Autowired
	private SurveyMessageMapper surveyMessageMapper;

	@Autowired
	private ParamConfigRepository paramConfigRepository;

	private static final Map<String, String> pStatusMap = new HashMap<>();
	static {
		pStatusMap.put("A", "QNB First");
		pStatusMap.put("B", "Retail Plus");
		pStatusMap.put("C", "Retail");
		pStatusMap.put("D", "Instant");
		pStatusMap.put("P", "QNB First Plus");
	}

	@Override
	public GenericResponse<SurveyRequestDTO> createSurveyMessage(SurveyRequestDTO surveyRequest) {
		var response = new GenericResponse<SurveyRequestDTO>();
		try {
			if (Objects.nonNull(surveyRequest)) {
				var surveyMessages = surveyMessageRepository
						.findByUnitIdAndSegmentType(surveyRequest.getUnitId(), surveyRequest.getSegmentType());

				if (Objects.nonNull(surveyMessages) && surveyMessages.size() > 0) {
					response.setStatus(new ResultUtilVO(AppConstant.RESULT_CODE,
							"Survey Message already for the selected unit and segment"));
				} else {
					var updatesurveyMessage = surveyMessageRepository
							.save(surveyMessageMapper.toEntity(surveyRequest));
					response.setData(surveyMessageMapper.toDto(updatesurveyMessage));
					response.setStatus(new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC));
				}
			}
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC));
		}
		return response;
	}

	@Override
	public GenericResponse<SurveyRequestDTO> deleteSurveyMessage(SurveyRequestDTO id) {
		var response = new GenericResponse<SurveyRequestDTO>();
		try {
			var exists = surveyMessageRepository.existsBySurveyId(id.getSurveyId());
			if (exists) {
				var surveyMessage = surveyMessageRepository.findBySurveyId(id.getSurveyId());
				surveyMessage.setStatus("N");
				var updatesurveyMessage = surveyMessageRepository.save(surveyMessage);
				response.setData(surveyMessageMapper.toDto(updatesurveyMessage));
				response.setStatus(new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC));
			} else {
				response.setStatus(new ResultUtilVO(AppConstant.RESULT_CODE, "Survey Not Available"));
			}
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC));
		}
		return response;
	}

	@Override
	public GenericResponse<List<SurveyRequestDTO>> searchSurveyMessage(SurveySearchRequest surveySearchRequest) {
    	var response = new GenericResponse<List<SurveyRequestDTO>>();
		try {
			var surveyList = new ArrayList<SurveyRequestDTO>();
			surveyList = (ArrayList<SurveyRequestDTO>) surveyMessageMapper.toDto(surveyMessageRepository.findByUnitIdAndSegmentTypeAndStatus(
					surveySearchRequest.getUnitId(), surveySearchRequest.getSegmentType(), "Y"));
			//for (SurveyRequestDTO survey : surveyList) {
			surveyList.forEach(survey->{
				var segmentType = String.valueOf(survey.getSegmentType());
				if (segmentType != null && !segmentType.isEmpty()) {
					log.info("poting status mapped value {}", pStatusMap.get(segmentType.toString()));
					survey.setSegmentType(pStatusMap.get(segmentType.toString()));
				}
			});
			response.setData(surveyList);
			response.setStatus(new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC));
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC));
		}
		return response;
	}

	@Override
	public GenericResponse<SurveyRequestDTO> SurveyMessageByid(SurveyRequestDTO id) {
		// TODO Auto-generated method stub
		var response = new GenericResponse<SurveyRequestDTO>();
		try {
			var exists = surveyMessageRepository.existsBySurveyId(id.getSurveyId());
			if (exists) {
				var surveyMessage = surveyMessageRepository.findBySurveyId(id.getSurveyId());
				response.setData(surveyMessageMapper.toDto(surveyMessage));
				response.setStatus(new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC));
			} else {
				response.setStatus(new ResultUtilVO(AppConstant.RESULT_CODE, "No Record Found"));
			}
		} catch (Exception e) {
			log.info("Exception details:{}", e);
			response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC));
		}
		return response;
	}

	@Override
	public List<ParamConfig> getSegmentKeysByUnit(SurveyRequestDTO surveyRequestDTO) {
		log.info("inside getConfigStartsWithKeyList method. key : {} ::status :{} ::unit :{} :: channel :{}", surveyRequestDTO);   
		var configList = paramConfigRepository.findByKeyStartsWithAndUnit_UnitIdAndStatusAndChannel_ChannelId("BO_SEGMENT_TYPE",surveyRequestDTO.getUnitId(),"ACT","IB");
		log.info("configList : {}", configList);
		return configList;
	}

	@Override
	public GenericResponse<SurveyRequestDTO> updateSurveyMessage(SurveyRequestDTO surveyRequest) {
		// TODO Auto-generated method stub
		var response = new GenericResponse<SurveyRequestDTO>();
		try {
			var exists = surveyMessageRepository.existsBySurveyId(surveyRequest.getSurveyId());
			if (exists) {
				var surveyMessage = surveyMessageRepository.findBySurveyId(surveyRequest.getSurveyId());
				surveyMessage.setSurveyCaptionEng(surveyRequest.getSurveyCaptionEng());
				surveyMessage.setSurveyCaptionArb(surveyRequest.getSurveyCaptionArb());
				surveyMessage.setSurveyQuestionEng(surveyRequest.getSurveyQuestionEng());

				surveyMessage.setSurveyQuestionArb(surveyRequest.getSurveyQuestionArb());

				surveyMessage.setSurveyAnswerOneCaptionEng(surveyRequest.getSurveyAnswerOneCaptionEng());

				surveyMessage.setSurveyAnswerTwoCaptionEng(surveyRequest.getSurveyAnswerTwoCaptionEng());

				surveyMessage.setSurveyAnswerThreeCaptionEng(surveyRequest.getSurveyAnswerThreeCaptionEng());

				surveyMessage.setSurveyAnswerFourCaptionEng(surveyRequest.getSurveyAnswerFourCaptionEng());

				surveyMessage.setSurveyAnswerOneCaptionArb(surveyRequest.getSurveyAnswerOneCaptionArb());

				surveyMessage.setSurveyAnswerTwoCaptionArb(surveyRequest.getSurveyAnswerTwoCaptionArb());

				surveyMessage.setSurveyAnswerThreeCaptionArb(surveyRequest.getSurveyAnswerThreeCaptionArb());

				surveyMessage.setSurveyAnswerFourCaptionArb(surveyRequest.getSurveyAnswerFourCaptionArb());

				surveyMessage.setCreationDate(surveyRequest.getCreationDate());

				surveyMessage.setEnableFlag(surveyRequest.getEnableFlag());

				surveyMessage.setSkipCount(surveyRequest.getSkipCount());

				surveyMessage.setStartDate(surveyRequest.getStartDate());

				surveyMessage.setEndDate(surveyRequest.getEndDate());

				surveyMessage.setCreatedBy(surveyRequest.getCreatedBy());

				surveyMessage.setModifiedBy(surveyRequest.getModifiedBy());

				surveyMessage.setModifiedDate(surveyRequest.getModifiedDate());

				var updatesurveyMessage = surveyMessageRepository.save(surveyMessage);
				response.setData(surveyMessageMapper.toDto(updatesurveyMessage));
				response.setStatus(new ResultUtilVO(AppConstant.RESULT_CODE, "Survey Page Modified Successfully"));

			} else {
				response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, "No Record Found"));
			}
		} catch (Exception e) {
			log.info("Exception details:{}", e);
		}
		return response;
	}

}
