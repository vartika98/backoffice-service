package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.TermsCondition.TermsAndConditionDto;
import com.qnb.bo.backofficeservice.dto.TermsCondition.TermsAndConditionRequest;
import com.qnb.bo.backofficeservice.dto.TermsCondition.TermsAndConditionResponse;
import com.qnb.bo.backofficeservice.dto.TermsCondition.TncResponse;
import com.qnb.bo.backofficeservice.entity.TermsAndCondition;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.TermsConditionRepository;
import com.qnb.bo.backofficeservice.repository.UnitLanguageRespository;
import com.qnb.bo.backofficeservice.repository.UnitRespository;
import com.qnb.bo.backofficeservice.service.TermsConditionService;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class TermsConditionImpl implements TermsConditionService {
	ResultUtilVO resultVo = new ResultUtilVO();
	
	@Autowired
	public TermsConditionRepository termsConditionRepo;
	@Autowired
	private UnitLanguageRespository unitLanguageRespository;
	
	@Autowired
	private UnitRespository unitMasterRespository;
	
	
	@Override
	public GenericResponse<List<TncResponse>> getSummary(TermsAndConditionRequest tncReq) {
	    var response = new GenericResponse<List<TncResponse>>();
	    resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
	    try {
	    	var summary = termsConditionRepo.findByUnitIdAndChannelIdAndStatusInOrderByModifiedTimeDesc(tncReq.getUnitId(), tncReq.getChannelId(), 
	    			new ArrayList<>(Arrays.asList("ACT", "IAC")));
	    	var map = new LinkedHashMap<String, TncResponse>();
	    	summary.forEach(obj -> {
	    		String key = obj.getModId()+"_"+obj.getSubModId()+"_"+obj.getScreenId();
	    		if(map.containsKey(key)) {
	    			TncResponse tnc = map.get(key);
	    			if(obj.getLang().equals("en")) {
	    				tnc.setTcUrlEn(obj.getTcUrl());
	    			} else if(obj.getLang().equals("fr")){
	    				tnc.setTcUrlFr(obj.getTcUrl());
	    			} else if(obj.getLang().equals("ar")) {
	    				tnc.setTcUrlAr(obj.getTcUrl());
	    			}
	    		} else {
	    			TncResponse tnc = new TncResponse();
	    			tnc.setChannelId(obj.getChannelId());
	    			tnc.setModuleId(obj.getModId());
	    			tnc.setRemarks(obj.getRemarks());
	    			tnc.setScreenId(obj.getScreenId());
	    			tnc.setSubModuleId(obj.getSubModId());
	    			tnc.setUnitId(obj.getUnitId());
	    			tnc.setStatus(obj.getStatus());
	    			tnc.setTcUrlId(obj.getTcUrlId());
	    			if(obj.getLang().equals("en")) {
	    				tnc.setTcUrlEn(obj.getTcUrl());
	    			} else if(obj.getLang().equals("fr")){
	    				tnc.setTcUrlFr(obj.getTcUrl());
	    			} else if(obj.getLang().equals("ar")) {
	    				tnc.setTcUrlAr(obj.getTcUrl());
	    			}
	    			map.put(key, tnc);
	    		}
	    	});
	    	List<TncResponse> result = new ArrayList<>(map.values().stream().collect(Collectors.toList()));
	    	response.setData(result);
	    } catch (Exception e) {
	    	log.info("Exception :" + e);
	    	resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE,AppConstant.GEN_ERROR_DESC);
	    }
	    response.setStatus(resultVo); 
	    return response;
	}

	private TermsAndConditionResponse mapToResponse(TermsAndCondition termsAndCondition) {
	    return new TermsAndConditionResponse(
	            termsAndCondition.getUnitId(),
	            termsAndCondition.getChannelId(),
	            termsAndCondition.getModId(),
	            termsAndCondition.getSubModId(),
	            termsAndCondition.getScreenId(),
	            termsAndCondition.getTcUrlId(),
	            termsAndCondition.getTcUrl(),
	            termsAndCondition.getLang(),
	            termsAndCondition.getRemarks()
	    );
	}
	@Override
	public GenericResponse<List<TermsAndCondition>> manageTermsAndCondition(List<TermsAndConditionDto> tncDtos) {
		var response = new GenericResponse<List<TermsAndCondition>>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var updated = new ArrayList<TermsAndCondition>();
			var termsToSave = new ArrayList<TermsAndCondition>();
//			var termsAndConditionList = new ArrayList<TermsAndCondition>();
			for (TermsAndConditionDto dto : tncDtos) {
				var unit = unitMasterRespository.findByUnitId(dto.getUnitId());
				if (Objects.isNull(unit)) {
					response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE,
							"Unit not found for unitId: " + dto.getUnitId()));
					return response;
				}
				var termsList = termsConditionRepo
						.findByUnitIdAndModIdAndSubModIdAndChannelIdAndTcUrlId(dto.getUnitId(), dto.getModuleId(),
								dto.getSubModuleId(), dto.getChannelId(), dto.getTcUrlId());
//				termsAndConditionList.addAll(termsList);
				var langList = unitLanguageRespository
						.findByUnit_UnitIdAndChannel_ChannelId(dto.getUnitId(), dto.getChannelId());
				var existingTermWithTcUrlId = termsConditionRepo.findByTcUrlId(dto.getTcUrlId());
//		        if (existingTermWithTcUrlId != null) {
//		            resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "TcUrlId already exists it should be unique.");
//		        }
				if ("ADD".equalsIgnoreCase(dto.getAction())) {
					if (!termsList.isEmpty()) {
						resultVo = new ResultUtilVO(AppConstant.ALREADY_EXISTS_CODE, AppConstant.ALREADY_EXISTS_DESC);
						continue;
					}
					 if (!existingTermWithTcUrlId.isEmpty()) {
				            resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "TcUrlId already exists it should be unique.");
				            continue;
					 }
					var existsInList = termsToSave.stream()
							.anyMatch(terms -> terms.getUnitId().equals(dto.getUnitId())
									&& terms.getChannelId().equals(dto.getChannelId())
									&& terms.getModId().equals(dto.getModuleId())
									&& terms.getSubModId().equals(dto.getSubModuleId())
									&& terms.getScreenId().equals(dto.getScreenId()));
					if (!existsInList) {
						langList.forEach(lang -> {
							var newTerms = new TermsAndCondition();
							newTerms.setUnitId(dto.getUnitId());
							newTerms.setChannelId(dto.getChannelId());
							newTerms.setModId(dto.getModuleId());
							newTerms.setSubModId(dto.getSubModuleId());
							newTerms.setScreenId(dto.getScreenId());
							newTerms.setLang(lang.getLanguage().getId());
							newTerms.setCreatedBy("SYSTEM");
							newTerms.setCreatedTime(LocalDateTime.now());
							newTerms.setStatus(AppConstant.ACT);
							newTerms.setRemarks(dto.getRemarks());
							setTCUrlForLang(newTerms, dto, lang.getLanguage().getId());
							newTerms.setTcUrlId(dto.getTcUrlId());
							termsToSave.add(newTerms);
						});
					}
				}
			//  for (TermsAndCondition existingTermsAndCondition : termsList) {
				termsList.forEach(existingTermsAndCondition->{
					if ("DELETE".equalsIgnoreCase(dto.getAction())) {
						existingTermsAndCondition.setStatus(AppConstant.DEL);
						existingTermsAndCondition.setModifiedBy("tqa1082");
					} else if ("MODIFY".equalsIgnoreCase(dto.getAction())) {
						existingTermsAndCondition.setScreenId(dto.getScreenId());
						existingTermsAndCondition.setModifiedBy("tqa1082");
						existingTermsAndCondition.setModifiedTime(LocalDateTime.now());
						existingTermsAndCondition.setStatus(dto.getStatus());
						existingTermsAndCondition.setRemarks(dto.getRemarks());
						langList.forEach(lang -> {
							existingTermsAndCondition.setRemarks(dto.getRemarks());
	                        if (lang.getLanguage().getId().equals(existingTermsAndCondition.getLang())) {
	                        	setTCUrlForLang(existingTermsAndCondition, dto, lang.getLanguage().getId());
	                        }
	                    });
					}
					termsToSave.add(existingTermsAndCondition);
				});
			}
			if (!termsToSave.isEmpty()) {
				updated.addAll(termsConditionRepo.saveAll(termsToSave));
			} 
//			else if (termsAndConditionList.isEmpty()) {
//				resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "No data found ");
//			}
		} catch (Exception e) {
			log.info("Error in ManageT&C :{}" + e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	private void setTCUrlForLang(TermsAndCondition newTerms, TermsAndConditionDto dto, String langCode) {
		/* switch (langCode) {
	        case "en":
	        	newTerms.setTcUrl(dto.getTcUrlEn());
	            break;
	        case "fr":
	        	newTerms.setTcUrl(dto.getTcUrlFr());
	            break;
	        case "ar":
	        	newTerms.setTcUrl(dto.getTcUrlAr());
	            break;
	        default:
	              break;
	    }
		 */
		var tcUrl = switch (langCode) {
		    case "en" -> dto.getTcUrlEn();
		    case "fr" -> dto.getTcUrlFr();
		    case "ar" -> dto.getTcUrlAr();
		    default -> "";
		  };
	   	newTerms.setTcUrl(tcUrl);

	}
	@Override
	public GenericResponse<Map<String, List<Map<String, String>>>> getModuleIdList(String unitId, String channelId) {
	   var response = new GenericResponse<Map<String, List<Map<String, String>>>>();
	    try {
	        var moduleIdList = new ArrayList<Map<String, String>>();
	        var moduleList = termsConditionRepo.findDistinctModuleId(unitId, channelId);
	        log.info("the list we are getting is :{} " + moduleList);
	        if (!moduleList.isEmpty()) {
	            //for (String entry : moduleList) {
	         moduleList.forEach(entry->{
	                var values = entry.split(",");
	                var moduleMap = new HashMap<String, String>();
	                moduleMap.put("moduleId", values[0].trim());
	                moduleMap.put("description", values[1].trim());
	                moduleIdList.add(moduleMap);
	            });
	            var defaultModuleMap = new HashMap<String, String>();
	            defaultModuleMap.put("moduleId", "ADD_NEW_MODULE_ID");
	            defaultModuleMap.put("description", "Add new module");
	            moduleIdList.add(defaultModuleMap);
	            var data = new HashMap<String, List<Map<String, String>>>();
	            data.put("moduleIds", moduleIdList);
	            response.setData(data);
	            resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
	        }else {
	            var defaultModuleMap = new HashMap<String, String>();
	            defaultModuleMap.put("moduleId", "ADD_NEW_MODULE_ID");
	            defaultModuleMap.put("description", "Add new module");
	            moduleIdList.add(defaultModuleMap);
	        }
	    } catch (Exception e) {
	        log.info("Error in ModuleList : {}" + e);
	        resultVo =  new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
	    }
	    response.setStatus(resultVo);
	    return response;
	}

	public GenericResponse<Map<String, List<Map<String, String>>>> getSubModuleIdList(String unitId ,String channelId,String moduleId) {
	    var response = new GenericResponse<Map<String, List<Map<String, String>>>>();
	    var data = new HashMap<String, List<Map<String, String>>>();
	    try {
	        var subModuleIdList = new ArrayList<Map<String, String>>();
	        var submoduleList = termsConditionRepo.findDistinctSubModuleId(unitId, channelId, moduleId);
	        if (!submoduleList.isEmpty()) {
	          // for (String entry : submoduleList) {
	           submoduleList.forEach(entry->{
	                var values = entry.split(",");
	                var subModuleMap = new HashMap<String, String>();
	                subModuleMap.put("submoduleId", values[0].trim());
	                subModuleIdList.add(subModuleMap);
	            });
	            var defaultSubModuleMap = new HashMap<String, String>();
	            defaultSubModuleMap.put("submoduleId", "ADD_NEW_SUBMODULE_ID");
	            subModuleIdList.add(defaultSubModuleMap);
	        } else {
	            var defaultSubModuleMap = new HashMap<String, String>();
	            defaultSubModuleMap.put("submoduleId", "ADD_NEW_SUBMODULE_ID");
	            subModuleIdList.add(defaultSubModuleMap);
	        }
	        data.put("subModules", subModuleIdList);
	        response.setData(data);
	        resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
	    } catch (Exception e) {
	        log.info("Error in SubmoduleList : {}", e);
	        resultVo =  new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
	    }
	    response.setStatus(resultVo);
	    return response;
	}
}

