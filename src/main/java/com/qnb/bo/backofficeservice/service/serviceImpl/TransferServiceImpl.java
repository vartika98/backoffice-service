package com.qnb.bo.backofficeservice.service.serviceImpl;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.*;
import com.qnb.bo.backofficeservice.dto.audit.AuditListRequest;
import com.qnb.bo.backofficeservice.dto.audit.SummaryRequest;
import com.qnb.bo.backofficeservice.entity.ParamConfig;
import com.qnb.bo.backofficeservice.entity.TransferEntity;
import com.qnb.bo.backofficeservice.entity.audit.AuditMaster;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.ParamConfigRepository;
import com.qnb.bo.backofficeservice.repository.TransferRepository;
import com.qnb.bo.backofficeservice.service.TransferService;
import com.qnb.bo.backofficeservice.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class TransferServiceImpl implements TransferService {

    @Autowired
    private TransferRepository transferRepository;

    @Autowired
    private ParamConfigRepository paramConfigRepository;

    private ResultUtilVO resultVo = new ResultUtilVO();

    @Override
    public GenericResponse<GraphDataDto> getTransferDataSummary(SummaryRequest request){
        var response = new GenericResponse<GraphDataDto>();

        try {

            var globalIdCondition = !request.getGlobalId().isEmpty();
            var guidCondition = !request.getGuid().isEmpty();
            var txnRefIdCondition = !request.getTxnRefId().isEmpty();

            var globalId = globalIdCondition ? request.getGlobalId() : null;
            var guid = guidCondition ? request.getGuid() : null;
            var txnRefId = txnRefIdCondition ? request.getTxnRefId() : null;

            var entityList = transferRepository.findByRequestConditions(request.getChannelId(), request.getCategoryCode(), request.getFromDate(), request.getToDate(), globalId, guid, txnRefId, globalIdCondition, guidCondition, txnRefIdCondition);
            if (!CollectionUtils.isEmpty(entityList)) {
                var totalSuccessCount = entityList.stream().filter(this::isSuccess).count();
                var graphDataDto = new GraphDataDto<>(totalSuccessCount, (entityList.size() - totalSuccessCount), entityList);
                resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.SUCCESS);
                response.setData(graphDataDto);
            } else {
                resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, AppConstant.NO_DATA_FOUND);
            }
        } catch (Exception e) {
            log.info("Exception in getTransferDataHistory :{}", e);
            resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
        }
        response.setStatus(resultVo);
        return response;
    }

    @Override
    public GenericResponse<GraphDateCountDto> getTransferDetailsByDateRange(AuditListRequest auditRequest) {
        var response = new GenericResponse<GraphDateCountDto>();

        try {

            var entityList = transferRepository.findByChannelAndCTransferTypeAndDate(auditRequest.getChannelId(), auditRequest.getCategoryCode(), auditRequest.getFromDate(), auditRequest.getToDate());
            if (!CollectionUtils.isEmpty(entityList)) {

                resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.SUCCESS);
                // Group the Entity by formatted auditDate
                var groupedByDate = entityList.stream()
                        .collect(Collectors.groupingBy(transferEntity -> DateUtil.dateInStringFormat(transferEntity.getTransferInit())));

                var totalSuccessCount = entityList.stream().filter(this::isSuccess).count();
                var graphDateCountDtos = getGraphData(groupedByDate, auditRequest.getFromDate(), auditRequest.getToDate());
                graphDateCountDtos.setTotalSuccessCount(totalSuccessCount);
                graphDateCountDtos.setTotalFailureCount(entityList.size() - totalSuccessCount);
                response.setData(graphDateCountDtos);

            } else {
                resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "No data found");
            }
        } catch (Exception e) {
            log.info("Exception in getTransferDetailsByDateRange :{}", e);
            resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
        }

        response.setStatus(resultVo);
        return response;
    }

    private boolean isSuccess(TransferEntity transferEntity) {
        return AppConstant.SUC.equalsIgnoreCase(transferEntity.getStatus());
    }

    public GenericResponse<GraphCategoryCodeCountDto> getConfigBasedTransferCount(ConfigBasedCategoryRequest configBasedCategoryRequest){

        var response = new GenericResponse<GraphCategoryCodeCountDto>();
        // Get the current date
        var currentDate = Instant.now();

        var endDateStr = DateUtil.dateInStringFormat(Date.from(currentDate));

        var paramConfigList = (List<ParamConfig>) paramConfigRepository.findDistinctValuesForKey(AppConstant.BO_TRANSFER_MONITORING_DAYS);
        var numberOfDays = paramConfigList.isEmpty() ? AppConstant.ONE_MONTH_IN_DAYS : Integer.parseInt(paramConfigList.get(0).getValue());

        // Calculate end time by adding numberOfDays to the current date
        var startDate = currentDate.minus(Duration.ofDays(numberOfDays));
        var startDateStr = DateUtil.dateInStringFormat(Date.from(startDate));

        var graphCategoryCodeCountDto = new GraphCategoryCodeCountDto();
        var catCodeConfigCountList = new ArrayList<GraphCategoryCodeCountByConfig>();
        var entityList = transferRepository.getConfigBasedTransferData(configBasedCategoryRequest.getCategoryCode(), startDateStr, endDateStr);

        configBasedCategoryRequest.getCategoryCode().forEach(categoryCode -> {
            var dtoConfig = new GraphCategoryCodeCountByConfig();
            dtoConfig.setCategoryCode(categoryCode);
            var filteredEntityList = entityList.stream().filter(entity -> categoryCode.equalsIgnoreCase(entity.getTransferType())).collect(Collectors.toList());
            if(!CollectionUtils.isEmpty(filteredEntityList)){

                var groupedByDate = filteredEntityList.stream()
                        .collect(Collectors.groupingBy(transfer -> DateUtil.dateInStringFormat(transfer.getTransferInit())));
                var categoryCodeData = getGraphData(groupedByDate, startDateStr, endDateStr);
                var totalSuccessCount = filteredEntityList.stream().filter(this::isSuccess).count();
                categoryCodeData.setTotalSuccessCount(totalSuccessCount);
                categoryCodeData.setTotalFailureCount(filteredEntityList.size() - totalSuccessCount);
                dtoConfig.setCategoryCodeData(categoryCodeData);
            }
            catCodeConfigCountList.add(dtoConfig);
        });
        graphCategoryCodeCountDto.setCategoryCodes(catCodeConfigCountList);
        response.setStatus(CollectionUtils.isEmpty(catCodeConfigCountList) ? new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC) : new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.SUCCESS));
        response.setData(graphCategoryCodeCountDto);

        return response;
    }

    public GraphDateCountDto getGraphData(Map<String, List<TransferEntity>> groupedByDate, String fromDate, String toDate) {

        try {

            var graphDayCountDtos = Stream.iterate(DateUtil.getLocalDate(fromDate, DateUtil.DD_MM_YYYY), date -> date.plusDays(1))
                    .limit(ChronoUnit.DAYS.between(DateUtil.getLocalDate(fromDate, DateUtil.DD_MM_YYYY), DateUtil.getLocalDate(toDate, DateUtil.DD_MM_YYYY).plusDays(1)))
                    .map(date -> {
                            var localDateInString = DateUtil.dateInStringFormat(date, DateUtil.DD_MM_YYYY);
                            var entityForDate = groupedByDate.getOrDefault(localDateInString, Collections.emptyList());

                            var successCount = entityForDate.stream().filter(this::isSuccess).count();

                            var failureCount = entityForDate.size() - successCount;
                            return new GraphDayCountDto(DateUtil.dateInStringFormat(date, DateUtil.DD_MMM), successCount, failureCount);
                    }).collect(Collectors.toList());

            return new GraphDateCountDto(0, 0, graphDayCountDtos);

        } catch (Exception e) {
            log.error("Exception in getGraphData: {}", e);
        }
        return null;
    }
}
