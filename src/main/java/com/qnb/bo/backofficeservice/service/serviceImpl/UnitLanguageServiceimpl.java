package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.unit.UnitLanguageDto;
import com.qnb.bo.backofficeservice.entity.BOUnitLanguage;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.BOUnitLanguageRepository;
import com.qnb.bo.backofficeservice.service.UnitLanguageService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UnitLanguageServiceimpl implements UnitLanguageService {

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private BOUnitLanguageRepository unitlanguageRepo;

	@Override
	public GenericResponse<Map<String, String>> saveUnitLanguage(List<UnitLanguageDto> requestBody) {
		var response = new GenericResponse<Map<String, String>>();
		var unitLangList = new ArrayList<BOUnitLanguage>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var lstOfActIac = unitlanguageRepo.findByStatusIn(List.of(AppConstant.ACT, AppConstant.IAC));
			requestBody.forEach(req -> {
				var existedUnitLangLst = lstOfActIac.stream()
						.filter(existedUnit -> req.getUnitId().equals(existedUnit.getUnitId())
								&& req.getChannelId().equals(existedUnit.getChannelId()))
						.collect(Collectors.toList());
				if (("MODIFY".equalsIgnoreCase(req.getAction()) || "DELETE".equalsIgnoreCase(req.getAction()))
						&& !existedUnitLangLst.isEmpty()) {
					var filterUnitLangLst = lstOfActIac.stream()
							.filter(existedUnit -> req.getUnitId().equals(existedUnit.getUnitId())
									&& req.getChannelId().equals(existedUnit.getChannelId())&& req.getLangCode().get(0).equals(existedUnit.getLangCode()))
							.collect(Collectors.toList());
					filterUnitLangLst.forEach(existData -> {
						existData.setModifiedBy(AppConstant.DEFAULT_CREATED_BY);
						existData.setModifiedTime(LocalDateTime.now());
						if ("MODIFY".equalsIgnoreCase(req.getAction())) {
							existData.setRemarks(req.getRemarks());
							existData.setStatus(req.getStatus());
							existData.setIsDefault(req.getIsDefault());
						} else {
							existData.setStatus(AppConstant.DEL);
						}
						unitLangList.add(existData);
					});
				} else {
					var existsInDb  = requestBody.stream()
							.allMatch(obj1 -> lstOfActIac.stream()
									.anyMatch(obj2 -> obj1.getChannelId().equals(obj2.getChannelId())
											&& obj1.getUnitId().equals(obj2.getUnitId()) && obj1.getLangCode().stream()
													.anyMatch(langCode -> langCode.equals(obj2.getLangCode()))));
					if ("ADD".equalsIgnoreCase(req.getAction())) {
						if(existsInDb ) {
							resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE, "Already exists");
						}else {
						req.getLangCode().forEach(lang -> {
							var unitLang = new BOUnitLanguage();
							unitLang.setIsDefault(lang.equalsIgnoreCase(req.getIsDefault()) ? "Y" : "N");
							unitLang.setUnitId(req.getUnitId());
							unitLang.setChannelId(req.getChannelId());
							unitLang.setStatus(req.getStatus());
							unitLang.setLangCode(lang);
							unitLang.setRemarks(req.getRemarks());
							unitLang.setCreatedBy(AppConstant.DEFAULT_CREATED_BY);
							unitLangList.add(unitLang);
						});
					}
				}
				}
			});
			log.info("Data in unitLangList:::{}", unitLangList);
			if (!unitLangList.isEmpty()) {
				unitlanguageRepo.saveAll(unitLangList);
			}
		} catch (Exception e) {
			log.info("Exception Occured in saveUnitLanguage:::{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<UnitLanguageDto>> getSummaryUnitLanguage() {
		var response = new GenericResponse<List<UnitLanguageDto>>();
		var unitLanguageList = new ArrayList<UnitLanguageDto>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var unitLangLst = unitlanguageRepo
					.findByStatusInOrderByCreatedTimeDesc(List.of(AppConstant.ACT, AppConstant.IAC));
			unitLangLst.forEach(unitData -> {
				var unitLangDto = new UnitLanguageDto();
				unitLangDto.setUnitId(unitData.getUnitId());
				unitLangDto.setChannelId(unitData.getChannelId());
				unitLangDto.setIsDefault(unitData.getIsDefault());
				unitLangDto.setLangCode(Collections.singletonList(unitData.getLangCode()));
				unitLangDto.setStatus(unitData.getStatus());
				unitLanguageList.add(unitLangDto);
			});
		} catch (Exception e) {
			log.info("Exception Occured in getSummaryUnitLanguage:::{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setData(unitLanguageList);
		response.setStatus(resultVo);
		return response;
	}

}
