package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.unit.UnitDto;
import com.qnb.bo.backofficeservice.dto.unit.UnitListRes;
import com.qnb.bo.backofficeservice.dto.unit.UnitMasterDto;
import com.qnb.bo.backofficeservice.entity.Unit;
import com.qnb.bo.backofficeservice.entity.UnitDesc;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.CountryRepository;
import com.qnb.bo.backofficeservice.repository.UnitDescRepository;
import com.qnb.bo.backofficeservice.repository.UnitRespository;
import com.qnb.bo.backofficeservice.service.UnitService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UnitServiceImpl implements UnitService {

	@Autowired
	private UnitRespository unitMasterRespository;

	@Autowired
	private UnitRespository unitRepo;

	@Autowired
	private UnitDescRepository unitDescRepo;
	@Autowired
	private CountryRepository countryRepo;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<UnitListRes> getAllUnits(final String unitStatus) {
		var response = new GenericResponse<UnitListRes>();
		var resData = new UnitListRes();
		try {
			var unitLst = unitMasterRespository.findByStatus(unitStatus).stream()
					.map(c -> UnitMasterDto.builder().unitCode(c.getUnitId()).unitDesc(c.getUnitDesc())
							.countryCode(c.getCountryCode())
							.countryDesc(c.getCountryDesc()).status(c.getStatus()).build())
					.collect(Collectors.toList());
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			resData.setUnits(unitLst);
			response.setData(resData);
		} catch (Exception e) {
			log.info("Exception in calling getAllChannels :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<Unit>> manageUnit(List<UnitDto> unitRequest) {
		var response = new GenericResponse<List<Unit>>();
		try {

			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var existingUnitList = unitRepo.findAll();
			var unitToSave = new ArrayList<Unit>();
			var unitDescToSave = new ArrayList<UnitDesc>();

			unitRequest.forEach(request->{
				var unitExists = existingUnitList.stream()
						.anyMatch(unit -> unit.getUnitId().equals(request.getUnitId()));
				if ("MODIFY".equalsIgnoreCase(request.getAction()) || "DELETE".equalsIgnoreCase(request.getAction())) {
					if (unitExists) {
						var existingUnit = existingUnitList.stream()
								.filter(unit -> unit.getUnitId().equals(request.getUnitId())).findFirst().orElse(null);

						if (Objects.nonNull(existingUnit)) {
							if ("MODIFY".equalsIgnoreCase(request.getAction())) {
								existingUnit.setStatus(request.getStatus());
								existingUnit.setUnitDesc(request.getUnitDesc());
								existingUnit.setColor(request.getColor());
								existingUnit.setBranchCode(request.getBranchCode());
								existingUnit.setDescription(request.getRemarks());
								existingUnit.setModifiedBy("BO");
								existingUnit.setIban(request.getIban());
								existingUnit.setBaseCur(request.getBaseCur());
								existingUnit.setCur2(request.getCur2());
								existingUnit.setCur3(request.getCur3());
								existingUnit.setOrderBy(request.getOrderBy());
								existingUnit.setModifiedTime(LocalDateTime.now());
								existingUnit.setSmsUnitId(request.getSmsUnitId());
								unitToSave.add(existingUnit);
//	                              List<UnitDesc> unitDescList = unitDescRepo.findByUnitId(existingUnit.getUnitId());
//	                              for (UnitDesc unitDesc : unitDescList) {
//	                                  unitDesc.setStatus(request.getStatus());
//	                                  unitDesc.setRemarks(request.getRemarks());         
//	                              	  unitDesc.setModifiedBy("BO");
//	                                  unitDesc.setModifiedTime(LocalDateTime.now());
//	                                  unitDescToSave.add(unitDesc);
//	                              }
							} else if ("DELETE".equalsIgnoreCase(request.getAction())) {
								existingUnit.setStatus(AppConstant.DEL);
								existingUnit.setModifiedBy("BO");
								existingUnit.setModifiedTime(LocalDateTime.now());
								unitToSave.add(existingUnit);
//	                             List<UnitDesc> unitDescList = unitDescRepo.findByUnitId(existingUnit.getUnitId());
//	                             for (UnitDesc unitDesc : unitDescList) {
//	                                 unitDesc.setStatus(AppConstant.DEL);
//	                                 unitDesc.setModifiedBy("BO");
//	                                 unitDesc.setModifiedTime(LocalDateTime.now());
//	                                 unitDescToSave.add(unitDesc);
//	                             }
							}
						}
					} else {
						resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE,
								"Unit with ID " + request.getUnitId() + " not found.");
					}
				} else if ("ADD".equalsIgnoreCase(request.getAction())) {
					if (unitExists) {
						var existingUnit = existingUnitList.stream()
								.filter(unit -> unit.getUnitId().equals(request.getUnitId())
										&& unit.getStatus().equals(AppConstant.DEL))
								.findFirst().orElse(null);

						if (Objects.nonNull(existingUnit)) {
							existingUnit.setStatus(request.getStatus());
							existingUnit.setModifiedBy("BO");
							existingUnit.setModifiedTime(LocalDateTime.now());
							existingUnit.setUnitDesc(request.getUnitDesc());
							existingUnit.setColor(request.getColor());
							existingUnit.setBranchCode(request.getBranchCode());
							existingUnit.setDescription(request.getRemarks());
							existingUnit.setIban(request.getIban());
							existingUnit.setBaseCur(request.getBaseCur());
							existingUnit.setCur2(request.getCur2());
							existingUnit.setCur3(request.getCur3());
							existingUnit.setOrderBy(request.getOrderBy());
							existingUnit.setSmsUnitId(request.getSmsUnitId());
							existingUnit.setTimeZone(request.getTimeZone());
							existingUnit.setCountryCode(request.getCountry());
							unitToSave.add(existingUnit);
						} else {

							resultVo = new ResultUtilVO(AppConstant.ALREADY_EXISTS_CODE,
									"Unit with ID " + request.getUnitId() + " already exists.");
						}
					} else {
						var newUnit = new Unit();
						newUnit.setUnitId(request.getUnitId());
						newUnit.setUnitDesc(request.getUnitDesc());
						newUnit.setStatus(request.getStatus());
						newUnit.setCreatedTime(LocalDateTime.now());
						newUnit.setCreatedBy("BO");
						newUnit.setBaseCur(request.getBaseCur());
						newUnit.setBranchCode(request.getBranchCode());
						newUnit.setColor(request.getColor());
						newUnit.setCountryCode(request.getCountry());
						newUnit.setSmsUnitId(request.getSmsUnitId());
						newUnit.setCur2(request.getCur2());
						newUnit.setCur3(request.getCur3());
						newUnit.setIban(request.getIban());
						newUnit.setOrderBy(request.getOrderBy());
						newUnit.setTimeZone(request.getTimeZone());
						newUnit.setDescription(request.getRemarks());
						unitToSave.add(newUnit);
//	                    UnitDesc unitDesc = convertToUnitDesc(request);
//	                    unitDescToSave.add(unitDesc);
					}
				}
			});
			if (!unitToSave.isEmpty()) {
				var updatedConfigs = unitRepo.saveAll(unitToSave);
//	            List<UnitDesc> desc = unitDescRepo.saveAll(unitDescToSave);
			}
		} catch (Exception e) {
			log.info("Error in unit management: {}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

//	private UnitDesc convertToUnitDesc(UnitDto dto) {
//	    UnitDesc unitDesc = new UnitDesc();
//	    unitDesc.setUnitId(dto.getUnitId());
//	    unitDesc.setUnitDesc(dto.getUnitDesc());
//	    unitDesc.setLangcode(dto.getLangCode());
//	    unitDesc.setStatus(dto.getStatus());
//	    unitDesc.setCreatedBy("BO");
//	    unitDesc.setCreatedTime(LocalDateTime.now());
//	    unitDesc.setRemarks(dto.getRemarks());
//	    return unitDesc;
//	}

	@Override
	public GenericResponse<List<UnitDto>> unitSummary() {
		var response = new GenericResponse<List<UnitDto>>();
		try {
			var resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var unitList = unitRepo
					.findAllByStatusInOrderByCreatedTimeDesc(List.of(AppConstant.ACT, AppConstant.IAC));
			var unitResSummary = unitList.stream()
					.map((Unit c) -> UnitDto.builder().unitId(c.getUnitId()).unitDesc(c.getUnitDesc())
							.color(c.getColor()).title(c.getUnitDesc()).baseCur(c.getBaseCur()).cur2(c.getCur2())
							.cur3(c.getCur3())
//	                        .country(c.getCountry())
							.timeZone(c.getTimeZone()).iban(c.getIban()).branchCode(c.getBranchCode())
							.smsUnitId(c.getSmsUnitId())
//	                        .action(c.getAction())
							.status(c.getStatus()).orderBy(c.getOrderBy()).build())
					.collect(Collectors.toList());

			response.setData(unitResSummary);
			response.setStatus(resultVo);
		} catch (Exception e) {
			var errorResultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
			response.setStatus(errorResultVo);
			e.printStackTrace();
		}
		return response;
	}

}
