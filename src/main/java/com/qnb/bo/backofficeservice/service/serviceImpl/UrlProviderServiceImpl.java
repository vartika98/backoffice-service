package com.qnb.bo.backofficeservice.service.serviceImpl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.ApiRequest;
import com.qnb.bo.backofficeservice.dto.ApiResponse;
import com.qnb.bo.backofficeservice.entity.UrlProvider;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.UrlProviderRepository;
import com.qnb.bo.backofficeservice.service.UrlProviderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class UrlProviderServiceImpl implements UrlProviderService {

    @Autowired
    private UrlProviderRepository urlProviderRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public GenericResponse<List<UrlProvider>> getAllUrl() {
        var response = new GenericResponse<List<UrlProvider>>();
        var resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);

        List<UrlProvider> urls = urlProviderRepository.findAll();
        if(!CollectionUtils.isEmpty(urls)){
            resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
            response.setData(urls);
            response.setStatus(resultVo);
        }
        return response;
    }

    @Override
    public GenericResponse<ApiResponse> executeApi(ApiRequest request) {
        var response = new GenericResponse<ApiResponse>();
        var resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
        long urlId = request.getUrlId();
        Optional<UrlProvider> url = urlProviderRepository.findById(urlId);
        if (url.isPresent()) {
            var urlProvider = url.get();
            var mwUrl = urlProvider.getMwUrl();
            HttpMethod httpMethod = HttpMethod.valueOf(request.getHttpMethod());

            // Set up HttpHeaders and request body if it's a POST request
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            Object requestBody = null;

            // Add extra headers based on the request
            if (!request.getHeader().isEmpty()) {

                //headers.add(); todo based on request
            }
            if(!request.getParameters().isEmpty()){

                mwUrl = mwUrl + "?" + request.getParameters();
            }

            if (httpMethod == HttpMethod.POST) {
                requestBody = request.getRequestBody();
            }

            try {
                var entity = new HttpEntity<>(requestBody, headers);
                var responseEntity = restTemplate.exchange(mwUrl, httpMethod, entity, String.class);
                var responseBody = responseEntity.getBody();

                // Process the response
                ObjectMapper objectMapper = new ObjectMapper();
                Map<String, Object> outerJsonMap = objectMapper.readValue(responseBody, new TypeReference<Map<String, Object>>() {});
                resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
                var apiResponse = new ApiResponse();
                //apiResponse.setStatusCode();
                apiResponse.setResponse(outerJsonMap);
                response.setStatus(resultVo);
                response.setData(apiResponse);
            } catch (JsonProcessingException e) {
                log.info("Exception in executeApi :{}", e);
                 resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
                response.setStatus(resultVo);
            }
        } else {
            resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.NO_DATA_FOUND);
            response.setStatus(resultVo);
        }
        return response;
    }

}
