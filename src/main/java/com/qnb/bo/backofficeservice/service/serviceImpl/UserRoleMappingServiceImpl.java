package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.GroupRoleDto;
import com.qnb.bo.backofficeservice.dto.userRole.UserRoleMappingDto;
import com.qnb.bo.backofficeservice.entity.BOGroupMaster;
import com.qnb.bo.backofficeservice.entity.UserEntity;
import com.qnb.bo.backofficeservice.entity.UserRole;
import com.qnb.bo.backofficeservice.entity.UserRoleMapping;
import com.qnb.bo.backofficeservice.enums.UserStatus;
import com.qnb.bo.backofficeservice.enums.UserType;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.BOGroupMasterRepository;
import com.qnb.bo.backofficeservice.repository.RoleRepository;
import com.qnb.bo.backofficeservice.repository.UserRepository;
import com.qnb.bo.backofficeservice.repository.UserRoleMappingRepository;
import com.qnb.bo.backofficeservice.service.UserRoleMappingService;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class UserRoleMappingServiceImpl implements UserRoleMappingService {
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private UserRoleMappingRepository userRoleMappingRpo;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private BOGroupMasterRepository boGroupmasterRepo;

	@Override

	public GenericResponse<List<UserEntity>> manageUser(List<UserRoleMappingDto> unitRequest) {
		var response = new GenericResponse<List<UserEntity>>();
		var users = new ArrayList<UserEntity>();
		List<UserRoleMapping> userRoleMappings = new ArrayList<>();
		Date date = new Date(System.currentTimeMillis());
		Date dob = new Date(date.getTime() - (23L * 365 * 24 * 60 * 60 * 1000)); // Corrected calculation for date of
																					// birth
		resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC); // Initialize resultVo outside
																						// the try block
		List<UserEntity> userList = new ArrayList<>();
		UserEntity existingUserDet = new UserEntity();
		try {
			var passEncoder = new BCryptPasswordEncoder();
			String hashPassword = passEncoder.encode("doha#@123");

			for (UserRoleMappingDto userRole : unitRequest) {
				var userExists = userRepo.existsByUserId(userRole.getUserId());
				if ("ADD".equals(userRole.getAction())) {
					if (userRole.getUserType().equals(UserType.STATIC.name())) {
						if (userExists) {
							resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE,
									"User already exists for Static type");
						} else {
							var newUser = new UserEntity();
							newUser.setUserId(userRole.getUserId());
							newUser.setFirstName(userRole.getName());
							newUser.setLastName(userRole.getName());
							newUser.setExpiryDate(userRole.getExpiryDate());
							newUser.setDob(dob);
							newUser.setStatus(userRole.getStatus());
							newUser.setMobileNumber("60365602");
							newUser.setEmailId("123@mannai.com");
							newUser.setUserStatus(UserStatus.ENABLED); // Enum value should be used directly
//	                    newUser.setUserType(UserType.STATIC);
							newUser.setUserType(UserType.valueOf(userRole.getUserType()));
							newUser.setCreatedSourceIp("1:1:1:1");
							newUser.setComments(userRole.getComments());
							newUser.setPassword(hashPassword);
							newUser.setCreatedBy("BO");
							newUser.setCreatedTime(date);
							newUser.setLastModifiedBy("SYSADMIN");
							newUser.setLastModifiedTime(date);

							users.add(newUser);
						}
					} else if ((userRole.getUserType().equals(UserType.LDAP.name())
							|| (userRole.getUserType().equals(UserType.FUNCTIONAL.name())))) {
						if (userExists) {
							var existingUser = userRepo.findByUserId(userRole.getUserId());
							existingUser.setFirstName(userRole.getName());
							existingUser.setLastName(userRole.getName());
							existingUser.setExpiryDate(userRole.getExpiryDate());
							existingUser.setUserStatus(UserStatus.ENABLED);
							existingUser.setDob(dob);
							existingUser.setUserType(UserType.valueOf(userRole.getUserType()));
							existingUser.setComments(userRole.getComments());
							existingUser.setLastModifiedBy("SYSADMIN");
							existingUser.setLastModifiedTime(date);
							users.add(existingUser);
						} else {
							resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE,
									"User does not exist for LDAP type");
						}
					}
				}
			}
			if (!users.isEmpty()) {
				userRepo.saveAll(users);
			}

			for (UserRoleMappingDto userRole : unitRequest) {
				if (userRole.getAction().equals("ADD")) {
					var roles = roleRepository.findAllByGroupName(userRole.getGroupName());
//	                for (String groupCode : userRole.getGroupCode()) {
					var filteredRoles = roles.stream()
							.filter(role -> userRole.getRole().equalsIgnoreCase(role.getDescription()))
							.collect(Collectors.toList());

					for (UserRole role : filteredRoles) {
						var mappingExists = userRoleMappingRpo.existsByUserIdAndGroupNameAndUserRoleLevelAndGroupCode(
								userRole.getUserId(), userRole.getGroupName(), role.getRoleLevel(),
								userRole.getGroupCode());
						if (!mappingExists) {
							var userRoleMapping = new UserRoleMapping();
							userRoleMapping.setGroupName(userRole.getGroupName());
							userRoleMapping.setUserRoleLevel(role.getRoleLevel());
							userRoleMapping.setStatus(userRole.getStatus());
							userRoleMapping.setUserId(userRepo.findByUserId(userRole.getUserId()).getUserId());
							userRoleMapping.setGroupCode(userRole.getGroupCode());
							userRoleMappings.add(userRoleMapping);
						} else {
							resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE,
									"UserRoleMapping already exists");
						}
					}
//	                }
				} else if (userRole.getAction().equals("MODIFY")) {
					var existingMappings = userRoleMappingRpo.findByUserId(userRole.getUserId());
					userRoleMappingRpo.deleteByUserId(userRole.getUserId()); // Delete existing mappings for the user
					existingUserDet = userRepo.findByUserId(userRole.getUserId());
//	                for (String groupCode : userRole.getGroupCode()) {
					var roles = roleRepository.findAllByGroupName(userRole.getGroupName());
					var filteredRoles = roles.stream()
							.filter(role -> userRole.getRole().equalsIgnoreCase(role.getDescription()))
							.collect(Collectors.toList());
					for (UserRole role : filteredRoles) {
						var userRoleMapping = new UserRoleMapping();
						userRoleMapping.setGroupName(userRole.getGroupName());
						userRoleMapping.setUserRoleLevel(role.getRoleLevel());
						userRoleMapping.setStatus(userRole.getStatus());
						userRoleMapping.setUserId(userRepo.findByUserId(userRole.getUserId()).getUserId());
						userRoleMapping.setGroupCode(userRole.getGroupCode());
						userRoleMappings.add(userRoleMapping);
					}
					if(Objects.nonNull(existingUserDet)) {
						existingUserDet.setStatus(userRole.getStatus());
						userList.add(existingUserDet);
					}
				} else if ("DELETE".equals(userRole.getAction())) {
					// Find existing UserRoleMapping by groupName and userId
					UserRoleMapping existingMapping = userRoleMappingRpo
							.findByGroupNameAndUserId(userRole.getGroupName(), userRole.getUserId());
					existingUserDet = userRepo.findByUserId(userRole.getUserId());
					if (existingMapping != null) {
						var filteredRoles = roleRepository.findAllByGroupName(userRole.getGroupName()).stream()
								.filter(role -> userRole.getRole().equalsIgnoreCase(role.getDescription()))
								.collect(Collectors.toList());
						if (!filteredRoles.isEmpty()) {
							UserRole role = filteredRoles.get(0); // Assuming there's only one role matched

							existingMapping.setUserRoleLevel(role.getRoleLevel());
							existingMapping.setGroupCode(userRole.getGroupCode()); // Change made here
							existingMapping.setStatus(AppConstant.DEL);

							userRoleMappings.add(existingMapping);
						} else {
							resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE,
									"UserRoleMapping role or group mismatch");
						}
					} else {
						// UserRoleMapping doesn't exist, handle the case accordingly
						resultVo = new ResultUtilVO(AppConstant.RESULT_ERROR_CODE,
								"UserRoleMapping doesn't exist for the provided groupName and userId");
					}
					if (Objects.nonNull(existingUserDet)) {
						existingUserDet.setStatus(AppConstant.DEL);
						userList.add(existingUserDet);
					}
				}

				if (!userRoleMappings.isEmpty() || !userList.isEmpty()) {
					userRoleMappingRpo.saveAll(userRoleMappings);
					userRepo.saveAll(userList);
				}

			}
		} catch (Exception e) {
			log.error("Exception in managing users: {}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}

		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<UserRoleMappingDto>> userList() {
		var response = new GenericResponse<List<UserRoleMappingDto>>();
		var userRoleMappingDtos = new ArrayList<UserRoleMappingDto>();

		try {
			var userList = userRepo.findAll();

			userList.forEach(user -> {
				var roleList = userRoleMappingRpo.findByUserId(user.getUserId());
				var filteredRoles = roleList.stream()
						.filter(role -> Arrays.asList(AppConstant.ACT, AppConstant.IAC).contains(role.getStatus()))
						.collect(Collectors.toList());

				if (!filteredRoles.isEmpty()) {
					UserRoleMappingDto userRoleMappingDto = new UserRoleMappingDto();
					userRoleMappingDto.setUserId(user.getUserId());
					userRoleMappingDto.setName(user.getFirstName() + " " + user.getLastName());
					userRoleMappingDto.setExpiryDate(user.getExpiryDate());
					userRoleMappingDto.setUserType(user.getUserType().toString());
					userRoleMappingDto.setComments(user.getComments());
					userRoleMappingDto.setStatus(user.getStatus());

					var groupCodes = new ArrayList<String>();
					filteredRoles.forEach(role -> {
						groupCodes.add(role.getGroupCode());
						userRoleMappingDto.setGroupName(role.getGroupName());
						userRoleMappingDto.setGroupCode(role.getGroupCode());
						userRoleMappingDto.setUserRoleLevel(role.getUserRoleLevel());
						var userRoleMapping = new UserRoleMapping();
						userRoleMapping.setGroupCode(role.getGroupCode());
						userRoleMapping.setGroupName(role.getGroupName());
						userRoleMapping.setUserRoleLevel(role.getUserRoleLevel());
//	                    userRoleMapping.setUserRoleLevel(role.getUserRoleLevel())
						UserRole rolesByLevel = roleRepository.findByRoleLevel(role.getUserRoleLevel());
						userRoleMapping.setUserRoleLevel(rolesByLevel.getDescription());
						log.info("rolesByLevel {}", rolesByLevel);
						userRoleMapping.setStatus(user.getStatus());
						userRoleMappingDto.addUserRoleMapping(userRoleMapping);
					});
//	                userRoleMappingDto.setGroupCode(groupCodes);
					userRoleMappingDtos.add(userRoleMappingDto);
				}
			});

			response.setData(userRoleMappingDtos);
			response.setStatus(new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC));
		} catch (Exception e) {
			log.error("Error occurred while fetching user list: {}", e.getMessage());
			response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC));
		}

		return response;
	}

	@Override
	public GenericResponse<List<GroupRoleDto>> groupList(Map<String, String> groupRequest) {
		var response = new GenericResponse<List<GroupRoleDto>>();
		String groupId = groupRequest.get("groupId");

		try {
			var resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			List<BOGroupMaster> groupList = boGroupmasterRepo.findAllByGroupIdOrderByCreatedTimeDesc(groupId);
			List<GroupRoleDto> filteredGroupList = new ArrayList<>();

			groupList.forEach(group -> {
				if (AppConstant.ACT.equals(group.getStatus())) {
					GroupRoleDto groupDto = new GroupRoleDto();
					groupDto.setTxnId(group.getTxnId());
					groupDto.setGroupName(group.getGroupName());
					groupDto.setGroupDescription(group.getGroupDesc());
					groupDto.setGroupId(group.getGroupId());
					groupDto.setStatus(group.getStatus());

					filteredGroupList.add(groupDto);
					response.setData(filteredGroupList);
					response.setStatus(resultVo);
				}
			});
		} catch (Exception e) {
			log.error("Error occurred while fetching groupList list: {}", e);
			response.setStatus(new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC));
		}

		return response;
	}
}