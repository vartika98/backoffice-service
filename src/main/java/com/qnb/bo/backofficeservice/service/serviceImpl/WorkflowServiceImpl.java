package com.qnb.bo.backofficeservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.pendinRequest.CommentHisDto;
import com.qnb.bo.backofficeservice.dto.workflow.WorkflowReqResponse;
import com.qnb.bo.backofficeservice.entity.GroupRolesEntity;
import com.qnb.bo.backofficeservice.entity.UserEntity;
import com.qnb.bo.backofficeservice.entity.workflow.WorkflowEntity;
import com.qnb.bo.backofficeservice.entity.workflow.WorkflowHistoryEntity;
import com.qnb.bo.backofficeservice.enums.RemarkType;
import com.qnb.bo.backofficeservice.enums.WorkflowStatus;
import com.qnb.bo.backofficeservice.model.GenericResponse;
import com.qnb.bo.backofficeservice.model.ResultUtilVO;
import com.qnb.bo.backofficeservice.repository.UserRepository;
import com.qnb.bo.backofficeservice.repository.WorkflowHistoryRepository;
import com.qnb.bo.backofficeservice.repository.WorkflowRepository;
import com.qnb.bo.backofficeservice.repository.WorkflowUnitRepository;
import com.qnb.bo.backofficeservice.service.WorkflowService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class WorkflowServiceImpl implements WorkflowService{

	
	@Autowired
	private WorkflowRepository workflowRepository;

	@Autowired
	private WorkflowUnitRepository workflowUnitRepo;

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private WorkflowHistoryRepository workflowHistoryRepo;
	
	@Autowired
	private UserRepository userRepo;

	@Override
	public  List<WorkflowReqResponse> getWorkflowDetails(List<WorkflowStatus> workflowStatus, List<String> userProcessId) {
		var response = new ArrayList<WorkflowReqResponse>();
		var roleList = new ArrayList<String>();
		try {
			var roleLst = roleList("TQA1913");
			roleList = !roleLst.getData().isEmpty() ? (ArrayList<String>) roleLst.getData(): (ArrayList<String>) Collections.EMPTY_LIST;
			var workflows = workflowRepository.findByInitiatedGroupInAndProcessIdInAndWorkflowStatusInOrderByCreatedTimeAsc(roleList,userProcessId,workflowStatus);
			response = (ArrayList<WorkflowReqResponse>) workflows.stream().map(workflow->buildMessage(workflow,commentHistory(workflow.getWorkflowId())))
				.collect(Collectors.toList());
		} catch (Exception e) {
			log.info("Exception in calling workflowHistory :{}",e);
		}
		return response;
	}
	
	private WorkflowReqResponse buildMessage(WorkflowEntity workflow, CommentHisDto commentHistory) {
		return WorkflowReqResponse.builder()
				.workflowId(workflow.getWorkflowId())
				.requestId(workflow.getRequestId())
				.processId(workflow.getProcessId())
				.taskId(workflow.getTaskId())
				.assignTo(workflow.getAssignedGroup())
				.initiatedGroup(workflow.getInitiatedGroup()).workflowGroup(workflow.getWorkflowGroup())
//				.availableActions(workflow.getAvailableActions())
				.status(workflow.getWorkflowStatus())
				.processStatus(workflow.getLastPerformedAction()).historyCount(workflow.getHistoryCount().toString())
				.commentHistory(commentHistory)
				.build();
	}
	
	
	public CommentHisDto commentHistory(String workflowId) {
		var commentHis = new CommentHisDto();
		try {
			var workflowHistories = workflowHistoryRepo.findByWorkflowIdOrderBySeqNumber(workflowId);
			if (workflowHistories != null) {
				var makerDet = new WorkflowHistoryEntity();
				var checkerDet = new WorkflowHistoryEntity();
				
				for(WorkflowHistoryEntity history : workflowHistories) {
					if ((history.getRemarkType() == RemarkType.MAKER)
							&& makerDet == null) {
						makerDet = history;
					}
					if ((history.getRemarkType() == RemarkType.CHECKER)
							&& (checkerDet == null /*|| (checkerDet.getSeqNumber() < history.getSeqNumber())*/)) {
						checkerDet = history;
					}
				}
				if(Objects.nonNull(makerDet)) {
					commentHis.setMakerComments(Objects.nonNull(makerDet.getRemark())?makerDet.getRemark():"");
					commentHis.setCreatedBy(Objects.nonNull(makerDet.getCreatedBy())?makerDet.getCreatedBy():"");
					commentHis.setCreatedTime(Objects.nonNull(makerDet.getCreatedTime())?makerDet.getCreatedTime().toString():"");
				}
				if(Objects.nonNull(checkerDet)) {
					commentHis.setCheckerComments(Objects.nonNull(checkerDet)?checkerDet.getRemark():"");
					commentHis.setLastModifiedBy(Objects.nonNull(checkerDet)?checkerDet.getCreatedBy():"");
					commentHis.setLastModifiedTime(Objects.nonNull(checkerDet)?checkerDet.getCreatedTime().toString():"");
				}
			}
		} catch (Exception e) {
			log.info("Exception in calling workflowHistory :{}",e);
		}
		return commentHis;
	}
	
	public GenericResponse<List<String>> roleList(String userName) {
		var response = new GenericResponse<List<String>>();
		var roleList = new ArrayList<String>();
		try {
			var userDetail = userRepo.findByUserId(userName);
			log.info("userDeatils :{}",userDetail);
//			 userDetail.getGroups().forEach(group ->{
//				 var roles = new ArrayList<String>();
//					if(!group.getGroupId().getGroupRoles().isEmpty()) {
//						roles = (ArrayList<String>) group.getGroupId().getGroupRoles().stream()
//			              .map(GroupRolesEntity::getRoleId)
//			              .collect(Collectors.toList());
//						roleList.addAll(roles);
//					}
//			 });
			
			log.info("roleList :{}",roleList);
			response.setData(roleList);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			log.info("Exception in userList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
