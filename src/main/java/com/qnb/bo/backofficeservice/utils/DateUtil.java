package com.qnb.bo.backofficeservice.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import org.springframework.http.HttpStatus;

import com.qnb.bo.backofficeservice.exceptions.ServiceException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DateUtil {
    public static String DD_MM_YYYY = "dd-MM-yyyy";
    public static String DD_MMM = "dd-MMM";

    public static Date getDate(String date) {
        return getDate(date, DD_MM_YYYY);
    }
 
    public static Date getDate(String date, String format) {
        var formatter = new SimpleDateFormat(format, Locale.ENGLISH);
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            //TODO: check what needs to be done
        }
        return null;
    }
 
    public static String dateInStringFormat(Date date) {
        return dateInStringFormat(date, DD_MM_YYYY);
    }
 
    public static String dateInStringFormat(Date date, String format) {
        var sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }
    
    public static Date getDateWithFormat(String date, String format) {
        var formatter = new SimpleDateFormat(DD_MM_YYYY, Locale.ENGLISH);
        try {
        	var dte = formatter.parse(date);
        	var dteFormatter = new SimpleDateFormat(DD_MM_YYYY, Locale.ENGLISH);
        	var dateString = dteFormatter.format(dte);
            return dteFormatter.parse(dateString);
        } catch (ParseException e) {
        	log.error("parse ecxeption "+ e);
        }
        return null;
    }
    
    public static Date getDateTime(String dateTime) {
		try {
			final var DD_MM_YYYY_HH_MM = "dd-MM-yyyy HH:mm";
			var dteTime = new SimpleDateFormat(DD_MM_YYYY_HH_MM);
 
			return dteTime.parse(dateTime);
		} catch (Exception e) {
			throw new ServiceException("DT001", "Error in converting date to format dd-MM-yyyy HH:mm",
					HttpStatus.BAD_REQUEST, e);
		}
	}
    
    public static String dateTimeInStringFormat(Date date) {
		final var DD_MM_YYYY_HH_MM = "dd-MM-yyyy HH:mm";
		var sdf = new SimpleDateFormat(DD_MM_YYYY_HH_MM);
		return sdf.format(date);
	}

    public static LocalDate getLocalDate(String dateString, String pattern) {
    	var formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDate.parse(dateString, formatter);
    }

    public static String dateInStringFormat(LocalDate localDate, String pattern) {
    	var formatter = DateTimeFormatter.ofPattern(pattern);
        return localDate.format(formatter);
    }

}
