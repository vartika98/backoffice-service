package com.qnb.bo.backofficeservice.utils;

public class HashingCommonUtil {

	private HashingCommonUtil() {
	}

	public static String bytesToString(byte[] hash) {
		var hexString = new StringBuilder(2 * hash.length);
		for (byte h : hash) {
			var hex = Integer.toHexString(0xff & h);
			if (hex.length() == 1) {
				hexString.append('0');
			}
			hexString.append(hex);
		}
		return hexString.toString();
	}

}
