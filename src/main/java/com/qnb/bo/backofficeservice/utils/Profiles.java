package com.qnb.bo.backofficeservice.utils;

public class Profiles {
	private Profiles() {}
    public static final String NO_AUTH = "noauth";
    public static final String BASIC_AUTH = "basicauth";
}
