package com.qnb.bo.backofficeservice.utils;

import java.util.UUID;

public class RandomUuidGenerator {

	private RandomUuidGenerator() {
	}

	public static UUID asUUID() {
		return UUID.randomUUID();
	}

	public static String uUIDAsString() {
		return UUID.randomUUID().toString();
	}

}