package com.qnb.bo.backofficeservice.utils.pushNotification;

import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.auth.oauth2.AccessToken;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.gson.JsonObject;
import com.qnb.bo.backofficeservice.constant.AppConstant;
import com.qnb.bo.backofficeservice.dto.notification.KeyConfigDTO;
import com.qnb.bo.backofficeservice.dto.notification.NotificationDto;
import com.qnb.bo.backofficeservice.entity.KeyConfig;
import com.qnb.bo.backofficeservice.enums.NotificationStatus;
import com.qnb.bo.backofficeservice.repository.KeyConfigRepository;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class PushNotificationSender {
	
	private static final String[] SCOPES = { "https://www.googleapis.com/auth/firebase.messaging" };

	private KeyConfig fcmKeyConfig;
	
	@Autowired
	KeyConfigRepository keyConfigRepo;
	
	private static NotificationStatus status = NotificationStatus.INITIATED;
	
	public void loadKeys() {
		fcmKeyConfig = keyConfigRepo.findByKeyName(AppConstant.FCM_KEY);
		log.info("FCM::{}:", fcmKeyConfig);
	}
	
	private  HttpURLConnection getConnection(String accessToken) throws IOException {
		URL url = new URL(fcmKeyConfig.getValue1().toString());
		log.info("URL   "+url);
		HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
		log.info("httpURLConnection value..", httpURLConnection);
		httpURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);
		httpURLConnection.setRequestProperty("Content-Type", "application/json; UTF-8");
		return httpURLConnection;
	}
	
	public NotificationStatus sendCommonMessage(NotificationDto notification) {
		try {
			loadKeys();
			GoogleCredentials googleCredentials1 =  GoogleCredentials.fromStream(new ByteArrayInputStream(fcmKeyConfig.getFileData())).createScoped(fcmKeyConfig.getValue2());
			log.info("Google credentials.....", googleCredentials1);
		    AccessToken token = googleCredentials1.refreshAccessToken();
		    
		    JsonObject jNotification = new JsonObject();
			jNotification.addProperty("title", notification.getTitle());
			jNotification.addProperty("body", notification.getBody());
			JsonObject jAndroidnotification = new JsonObject();
			jAndroidnotification.add("notification", jNotification);
			JsonObject jMessage = new JsonObject();
			jMessage.add("android", jAndroidnotification);
			jMessage.addProperty("token", notification.getDeviceId());
			JsonObject fcmMessage = new JsonObject();
			fcmMessage.add("message", jMessage);
			
			log.info("Sending message method called building url.getConnection() is called");
			HttpURLConnection connection = getConnection(token.getTokenValue());
			log.info("returned value from getConnection() ", connection);
			try {
				connection.setDoOutput(true);
				DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
				outputStream.writeBytes(fcmMessage.toString());
				outputStream.flush();
				outputStream.close();
		        int responseCode = connection.getResponseCode();
				if (responseCode == 200) {
					status = NotificationStatus.DELIVERED;
					String response = inputstreamToString(connection.getInputStream());
					log.info("Message sent to Firebase for delivery, response:");
					log.info(response);
				} else {
					status = NotificationStatus.FAILED;
					log.info("Unable to send message to Firebase:" + connection.getResponseMessage());
					String response = inputstreamToString(connection.getErrorStream());
					log.info(response);
				}
				}catch (Exception e) {
					log.info("Sending message method called and exception:{}"+e);
					System.out.println("Sending message method called and exception"+e);
				}
//				finally {
//					log.info("connection is closed");
//					connection.disconnect();
//				}
		} catch (Exception e) {
			log.info("exception in  sendCommonMessage :{}",e);
		}
		return status;
	}
	
	private static String inputstreamToString(InputStream inputStream) throws IOException {
		StringBuilder stringBuilder = new StringBuilder();
		Scanner scanner = new Scanner(inputStream);
		while (scanner.hasNext()) {
			stringBuilder.append(scanner.nextLine());
		}
		scanner.close();
		return stringBuilder.toString();
	}
}
