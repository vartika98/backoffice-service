package com.qnb.bo.backofficeservice.views;

import com.qnb.bo.backofficeservice.enums.TxnStatus;

public interface ApplicationView {
	Long getTxnId();
	String getAppName();
	String getAppType();
	TxnStatus getStatus();
	Long getAppsDetailsTxnId();
	String getUnitId();
	String getChannelId();
	TxnStatus getChannelStatus();
}