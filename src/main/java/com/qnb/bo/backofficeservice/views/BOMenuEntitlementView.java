package com.qnb.bo.backofficeservice.views;

public interface BOMenuEntitlementView {

	String getProdCode();
	String getProdDesc();
	String getSubProdCode();
	String getSubProdDesc();
	String getFunctionCode();
	String getFunctionDesc();
}
