package com.qnb.bo.backofficeservice.views;

public interface BillPayDenominationView {

	String getUtilityCode();
	Integer getDenomination();
	String getStatus();
	String getUtilityDesc();
	Long getTxnId();
}
