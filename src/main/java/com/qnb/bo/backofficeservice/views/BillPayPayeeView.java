package com.qnb.bo.backofficeservice.views;

import java.sql.Clob;

public interface BillPayPayeeView {

	String getPayeeId();

	String getPayeeDesc();

	String getStatus();

	Clob getPayeeLogo();

	String getUnitCode();

	String getUnitCodeDesc();

	Integer getPriority();
}
