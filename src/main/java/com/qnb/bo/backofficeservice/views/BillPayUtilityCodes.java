package com.qnb.bo.backofficeservice.views;

public interface BillPayUtilityCodes {

	String getUtilityCode();
	String getUtilityCodeDesc();
	String getPayeeId();
	String getPayeeDesc();
	String getUtilityType();
	String getUtilityTypeDesc();
	String getUnitCode();
	String getUnitCodeDesc();
	String getCommissionFee();
	String getStatus();
}
