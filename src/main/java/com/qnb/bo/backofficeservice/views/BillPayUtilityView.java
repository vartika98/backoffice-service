package com.qnb.bo.backofficeservice.views;


public interface BillPayUtilityView {
	String getUtilityCode();
	String getUtilityType();
	String getUtilityDesc();
	String getPartialPayment();
	String getUtilityTypeDesc();
	String getAddPayeeCheck();
	Integer getDenomination();
}
