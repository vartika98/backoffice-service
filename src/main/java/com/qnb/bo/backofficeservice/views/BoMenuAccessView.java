package com.qnb.bo.backofficeservice.views;

public interface BoMenuAccessView {

	String getProdCode();
	String getProdDesc();
	String getSubProdCode();
	String getSubProdCodeDesc();
	String getSubProductUrl();
	String getFunctionCode();
	String getFunctionCodeDesc();
}
