package com.qnb.bo.backofficeservice.views;

public interface GroupSummaryView {

	String getGroupId();
	String getGroupName();
	String getGroupDescription();
	String getUnit();
	String getProdCode();
	String getSubProdCode();
	String getFunctionCode();
	String getRemarks();
	String getStatus();
}
