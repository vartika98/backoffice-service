package com.qnb.bo.backofficeservice.views;

import com.qnb.bo.backofficeservice.enums.TxnStatus;

public interface MenuEntitlementView {

	String getMenuId();
    String getParent();
    Integer getPriority();
    String getMenuStatus();
    String getDescription();
    String getUnitId();
    String getChannelId();
    TxnStatus getTxnEntStatus();
    Long getTxnEntDispPriority();    
}
