package com.qnb.bo.backofficeservice.views;

public interface MenuView {

	String getMenuId();
    String getMenuStatus();
    String getPriority();
    String getDescription();
    String getScreenId();
}
